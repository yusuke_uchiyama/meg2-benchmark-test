#!/bin/bash

# ./macros/bartender_output.sh 20180100-root6.08.04-geant4.10.04.p00-gcc540/Full_noMix 30

DIR=$1
RUN_TYPENUM=$2
RUN_TYPENUM_03D=`printf %03d $RUN_TYPENUM`
INPUT_RUNTYPE=`basename $DIR`
ORIGDIR=`pwd`

if test $RUN_TYPENUM -eq 300
then
    OUTPUT_RUNTYPE="Full_mubeamOnly"
elif test $RUN_TYPENUM -eq 10
then
    OUTPUT_RUNTYPE="Full_noMix"
else
    OUTPUT_RUNTYPE=$INPUT_RUNTYPE
fi

OUTPUT_FILE=NOTE-${OUTPUT_RUNTYPE}.md


cd $DIR/..

echo "" >> $OUTPUT_FILE
# echo "\`\`\`" >> $OUTPUT_FILE
# echo $0 $*  >> $OUTPUT_FILE
# echo "\`\`\`" >> $OUTPUT_FILE
# echo "- - - -" >> $OUTPUT_FILE
echo "## ${INPUT_RUNTYPE} ${RUN_TYPENUM_03D}xx ##" >> $OUTPUT_FILE
echo "" >> $OUTPUT_FILE

# Get number of events
GREP_COMMAND="grep 'processed' $INPUT_RUNTYPE/log/b${INPUT_RUNTYPE}${RUN_TYPENUM}* | cut -d':' -f2 | cut -d' ' -f1 | awk '{sum+=\$1}END{print sum}'"
#echo $GREP_COMMAND >> $OUTPUT_FILE
NEVENTS="$(eval $GREP_COMMAND)"
echo $NEVENTS " events" | tee -a $OUTPUT_FILE
echo "" | tee -a $OUTPUT_FILE

echo "### data size ###" >> $OUTPUT_FILE
echo "\`\`\`" >> $OUTPUT_FILE
# Show root file size
#LL_COMMAND="ls -lh $INPUT_RUNTYPE | grep -E \"$RUN_TYPENUM[0-9]{2}.root\""
LL_COMMAND="ls -lh $INPUT_RUNTYPE | grep -E \"${RUN_TYPENUM}00.root\""
echo $LL_COMMAND >> $OUTPUT_FILE
echo "$(eval $LL_COMMAND)" | tee -a $OUTPUT_FILE
echo "" | tee -a $OUTPUT_FILE

DU_COMMAND="du -ch $INPUT_RUNTYPE/raw${RUN_TYPENUM_03D}* | grep -E \"total\""
echo "raw${RUN_TYPENUM_03D}*.root" >> $OUTPUT_FILE
echo "$(eval $DU_COMMAND)" | tee -a $OUTPUT_FILE
DU_COMMAND="du -ch $INPUT_RUNTYPE/sim${RUN_TYPENUM_03D}* | grep -E \"total\""
echo "sim${RUN_TYPENUM_03D}*.root" >> $OUTPUT_FILE
echo "$(eval $DU_COMMAND)" | tee -a $OUTPUT_FILE
echo "" | tee -a $OUTPUT_FILE


TREE_SIZE_COMMAND="\$MEG2SYS/bartender/megbartender -I \"$ORIGDIR/macros/CalculateSizeInTree_macro.C(\\\"$INPUT_RUNTYPE/sim${RUN_TYPENUM_03D}00.root\\\", \\\"$OUTPUT_FILE\\\")\" -b -q"
#echo $TREE_SIZE_COMMAND >> $OUTPUT_FILE
echo "$(eval $TREE_SIZE_COMMAND)"
echo "\`\`\`" >> $OUTPUT_FILE
echo "" | tee -a $OUTPUT_FILE

echo "### execution time ###" >> $OUTPUT_FILE
GREP_COMMAND="grep 'analyzer\.\.\.' $INPUT_RUNTYPE/log/b$INPUT_RUNTYPE$RUN_TYPENUM* | cut -d' ' -f3 | awk -F':' '{h+=\$1*3600;m+=\$2*60;s+=\$3} {n=$NEVENTS} END{print h+m+s \"/\" n \" = \" (h+m+s)/n \" s/event\"} '"
#echo $GREP_COMMAND >> $OUTPUT_FILE
echo "$(eval $GREP_COMMAND)" | tee -a $OUTPUT_FILE
echo "\`\`\`" >> $OUTPUT_FILE
echo "---------" | tee -a $OUTPUT_FILE
GREP_COMMAND="grep -h '\.\.\.\.\. :' $INPUT_RUNTYPE/log/b$INPUT_RUNTYPE${RUN_TYPENUM}00-0.*"
echo "$(eval $GREP_COMMAND)" | tee -a $OUTPUT_FILE
echo "\`\`\`" >> $OUTPUT_FILE
