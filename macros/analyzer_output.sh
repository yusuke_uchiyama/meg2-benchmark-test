#!/bin/bash

#  ./macros/analyzer_output.sh 20180501-root6.12.06-geant4.10.04.p00-gcc640/noxec_PR_fit_genfit 111


DIR=$1
RUN_TYPENUM=$2
RUN_TYPENUM_03D=`printf %03d $RUN_TYPENUM`
INPUT_RUNTYPE=`basename $DIR`
ORIGDIR=`pwd`


OUTPUT_RUNTYPE=$INPUT_RUNTYPE
OUTPUT_FILE=NOTE-${OUTPUT_RUNTYPE}.md

echo $RUN_TYPENUM $RUN_TYPENUM_03D $INPUT_RUNTYPE $OUTPUT_FILE  $*

cd $DIR/..

echo "" >> $OUTPUT_FILE
# echo "\`\`\`" >> $OUTPUT_FILE
# echo $0 $*  >> $OUTPUT_FILE
# echo "\`\`\`" >> $OUTPUT_FILE
# echo "- - - -" >> $OUTPUT_FILE
echo "## ${INPUT_RUNTYPE}* ${RUN_TYPENUM_03D}xx ##" >> $OUTPUT_FILE
echo "" >> $OUTPUT_FILE

# Get number of events
GREP_COMMAND="grep 'processed' $INPUT_RUNTYPE/log/a${INPUT_RUNTYPE}${RUN_TYPENUM}* | cut -d':' -f2 | cut -d' ' -f1 | awk '{sum+=\$1}END{print sum}'"
#echo $GREP_COMMAND >> $OUTPUT_FILE
NEVENTS="$(eval $GREP_COMMAND)"
echo $NEVENTS " events" | tee -a $OUTPUT_FILE
echo "" | tee -a $OUTPUT_FILE

echo "### data size ###" >> $OUTPUT_FILE
echo "\`\`\`" >> $OUTPUT_FILE
# Show root file size
#LL_COMMAND="ls -lh $INPUT_RUNTYPE | grep -E \"$RUN_TYPENUM[0-9]{2}.root\""
LL_COMMAND="ls -lh $INPUT_RUNTYPE | grep -E \"${RUN_TYPENUM}00.root\""
echo $LL_COMMAND >> $OUTPUT_FILE
echo "$(eval $LL_COMMAND)" | tee -a $OUTPUT_FILE
echo "" | tee -a $OUTPUT_FILE

DU_COMMAND="du -ch $INPUT_RUNTYPE/rec${RUN_TYPENUM_03D}* | grep -E \"total\""
echo "rec${RUN_TYPENUM_03D}*.root" >> $OUTPUT_FILE
echo "$(eval $DU_COMMAND)" | tee -a $OUTPUT_FILE
echo "" | tee -a $OUTPUT_FILE

TREE_SIZE_COMMAND="\$MEG2SYS/analyzer/meganalyzer -I \"$ORIGDIR/macros/CalculateSizeInTree_macro.C(\\\"$INPUT_RUNTYPE/rec${RUN_TYPENUM_03D}00.root\\\", \\\"$OUTPUT_FILE\\\")\" -b -q"
#echo $TREE_SIZE_COMMAND >> $OUTPUT_FILE
echo "$(eval $TREE_SIZE_COMMAND)"
echo "\`\`\`" >> $OUTPUT_FILE
echo "" | tee -a $OUTPUT_FILE

echo "### execution time ###" >> $OUTPUT_FILE
GREP_COMMAND="grep 'analyzer\.\.\.' $INPUT_RUNTYPE/log/a$INPUT_RUNTYPE$RUN_TYPENUM* | cut -d' ' -f3 | awk -F':' '{h+=\$1*3600;m+=\$2*60;s+=\$3} {n=$NEVENTS} END{print h+m+s \"/\" n \" = \" (h+m+s)/n \" s/event\"} '"
#echo $GREP_COMMAND >> $OUTPUT_FILE
echo "$(eval $GREP_COMMAND)" | tee -a $OUTPUT_FILE
echo "\`\`\`" >> $OUTPUT_FILE
echo "---------" | tee -a $OUTPUT_FILE
GREP_COMMAND="grep -h '\.\. :' $INPUT_RUNTYPE/log/a$INPUT_RUNTYPE${RUN_TYPENUM}00-0.*"
echo "$(eval $GREP_COMMAND)" | tee -a $OUTPUT_FILE
echo "\`\`\`" >> $OUTPUT_FILE
