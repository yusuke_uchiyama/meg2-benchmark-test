#!/usr/bin/env python3
#
#
# Author : UCHIYAMA, Yusuke
# Time-stamp: <2019-11-26 17:56:58 gac-meg2>
#
# ingest and archive benchmark-test directory to SciCat and PetaByte Arcive at CSCS
#
#  Usage: ./ingest.py -h
###############################################################################

import sys, os
import subprocess
import logging
import time, datetime
import re
from argparse import ArgumentParser
import json

directory       = '20211200-root6.22.06-geant4.10.06.p03-gcc930'
tag             = ''

benchmarkdir    = '/meg/data1/shared/mc/benchmark_test/'
logfile         = 'ingest.log'
user          = 'uchiyama'
#passwd        = ''
token         = ''
tokenfile     = '/meg/home/meg/admin/token.txt'
with open(tokenfile, 'r') as f:
    token = f.read().rstrip()
#token         = '3s961tW8gM9c3JvyClowhuk0zrzkm0RIWaUgotyslz2Vjd31kv9vM6AyUCpGbAp6'

opt_v = False
opt_n = False

###############################################################################
def main():
    '''Script to ingest a directory to archive system'''

    ###########################################################################
    #
    # open logfile
    #
    level = logging.INFO
    if opt_v:
        level = logging.DEBUG
    logging.basicConfig(filename=logfile,
                        level=level,
                        format=' %(asctime)s - %(levelname)s - %(message)s')

    # To disable logging, uncomment the following
    #logging.disable(logging.CRITICAL)

    # Do the ingest

    jsonfile = make_json()

    if token:
        cmd = f'datasetIngestor -allowexistingsource -noninteractive -autoarchive  -token={token} '
    else:
        cmd = f'datasetIngestor -allowexistingsource -autoarchive '

    if not opt_n:
        cmd += ' --ingest '
    cmd += jsonfile
    logging.info("Ingest cmd: "+cmd)
    print(cmd)
            
    code = subprocess.run(cmd.split())#,
                          #stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    #if code.returncode == 0:




###############################################################################
def get_option():
    global opt_v
    global opt_n
    #global passwd
    
    argparser = ArgumentParser()
    argparser.add_argument('-v', '--verbose',
                           action='store_true',
                           help='show verbose message')
    argparser.add_argument('-n', '--dryrun',
                           action='store_true',
                           help='Dry run (do not actually ingest)')
    # argparser.add_argument('-p', '--passwd', type=str,
    #                         help='Password')
    args = argparser.parse_args()

    if args.verbose:
        opt_v = True
    if args.dryrun:
        opt_n = True
    # if args.passwd:
    #     passwd = args.passwd



###############################################################################
def make_json():
    # make a JSON file for the ingest


    jsonfile = os.path.join(benchmarkdir,directory+'.json')

    
    # metadata as dict
    data = {
        "sourceFolder": f'{benchmarkdir}{directory}',
        "type": "base",
        "ownerGroup": "a-35267",
        "datasetName": f"{directory}",
        "description": f"MEG II MC benchmark-test {directory}",
        "keywords": ("meg2", "mc", "benchmark-test"),
        "investigator": "uchiyama@icepp.s.u-tokyo.ac.jp",
        "usedSoftware": "https://bitbucket.org/muegamma/offline/"
    }
    if len(tag):
        data["usedSoftware"] += f"commits/tag/{tag}"

    # Output JSON file
    with open(jsonfile, 'w') as f:
        json.dump(data, f, indent=3)
    return jsonfile

    

        
  

###############################################################################
if __name__ == '__main__':
    get_option()
    main()
