2021/12/12

# Analyzer output with noxec_PR_fit_genfit.xml
* No XEC reconstruction
* SPX analysis from WaveformAnalysis (cf30)
* CDC analysis from waveform (WaveformAnalysis2), pattern recognition and fit with GENFIT
* Custom run number 2000116
* Hit z reconstruction used in CYLDCHTrackFinderPR is time-difference
* CYLDCHWaveformAnalysis2 (with not fully optimized parameters).
    * Update TimeWalkCoefficient (to 2.9e-10) and GlobalTimeOffset (to -8.25)
* 


## Run Numbers ##
11500-11504 Spect_signalEOnly_realistic (standard)  

11600-11604 Spect_signalEWithMichel7e7_realistic (standard)   


## Submit jobs ##
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20211200-root6.22.06-geant4.10.06.p03-gcc930/", 11500, 5)'
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit_cdchbadchannel2.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20211200-root6.22.06-geant4.10.06.p03-gcc930/", 11500, 5)'
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit_cdchbadchannel3.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20211200-root6.22.06-geant4.10.06.p03-gcc930/", 11500, 5)'
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit_cdchbadchannel5.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20211200-root6.22.06-geant4.10.06.p03-gcc930/", 11500, 5)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20211200-root6.22.06-geant4.10.06.p03-gcc930/", 11600, 5)'
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit_cdchbadchannel2.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20211200-root6.22.06-geant4.10.06.p03-gcc930/", 11600, 5)'
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit_cdchbadchannel3.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20211200-root6.22.06-geant4.10.06.p03-gcc930/", 11600, 5)'
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit_cdchbadchannel5.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20211200-root6.22.06-geant4.10.06.p03-gcc930/", 11600, 5)'
```



## Calculate positron efficiency ##
### Realistic (standard) ###	 

```
RUNNUM=11500;NRUN=5;DATASET=20211200-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5881 (88.3431 %)
|   |                         
|   | propagation cut         
|   V                         
|  5753 (86.4203 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  5285 (79.3901 %)
|   |                         
|   | matching                
|   V                         
|  5191 (77.9781 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  5191 (77.9781 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  5079 (76.2956 %)
|      + 0.524698
|      - 0.532712
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.36058 +- 0.0492788
|  phi     (mrad)  6.28833 +- 0.0495621
|  momentum (MeV)  0.0915685 +- 0.000792507
|  vertexZ   (mm)  1.66535 +- 0.0141807
|  vertexY   (mm)  0.781658 +- 0.00665011
|  time     (sec)  4.55394e-11 +- 3.82194e-13
@-----------------------------@
```

### Realistic (CYLDCHBadChannelDB.id=2) ###	 

```
RUNNUM=11500;NRUN=5;DATASET=20211200-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit_cdchbadchannel2|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5765 (86.6006 %)
|   |                         
|   | propagation cut         
|   V                         
|  5504 (82.6799 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  4834 (72.6153 %)
|   |                         
|   | matching                
|   V                         
|  4742 (71.2333 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  4742 (71.2333 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  4636 (69.641 %)
|      + 0.568039
|      - 0.574018
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.53944 +- 0.0542986
|  phi     (mrad)  6.49214 +- 0.0534189
|  momentum (MeV)  0.0954811 +- 0.000887847
|  vertexZ   (mm)  1.73323 +- 0.0162056
|  vertexY   (mm)  0.800388 +- 0.00699468
|  time     (sec)  4.67099e-11 +- 4.25145e-13
@-----------------------------@
```

### Realistic (CYLDCHBadChannelDB.id=3) ###	 

```
RUNNUM=11500;NRUN=5;DATASET=20211200-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit_cdchbadchannel3|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5874 (88.2379 %)
|   |                         
|   | propagation cut         
|   V                         
|  5740 (86.225 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  5229 (78.5489 %)
|   |                         
|   | matching                
|   V                         
|  5128 (77.0317 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  5128 (77.0317 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  5009 (75.2441 %)
|      + 0.53261
|      - 0.540301
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.45178 +- 0.0505578
|  phi     (mrad)  6.29302 +- 0.0503263
|  momentum (MeV)  0.0927308 +- 0.000789964
|  vertexZ   (mm)  1.68552 +- 0.0144115
|  vertexY   (mm)  0.783043 +- 0.00669385
|  time     (sec)  4.62735e-11 +- 3.96653e-13
@-----------------------------@
```

### Realistic (CYLDCHBadChannelDB.id=5) ###	 

```
RUNNUM=11500;NRUN=5;DATASET=20211200-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit_cdchbadchannel5|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5811 (87.2916 %)
|   |                         
|   | propagation cut         
|   V                         
|  5604 (84.1821 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  5013 (75.3042 %)
|   |                         
|   | matching                
|   V                         
|  4923 (73.9522 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  4923 (73.9522 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  4817 (72.3599 %)
|      + 0.552196
|      - 0.559005
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.50815 +- 0.0522378
|  phi     (mrad)  6.43483 +- 0.0528787
|  momentum (MeV)  0.0945799 +- 0.000831947
|  vertexZ   (mm)  1.72629 +- 0.0155669
|  vertexY   (mm)  0.800489 +- 0.00698945
|  time     (sec)  4.65498e-11 +- 4.17227e-13
@-----------------------------@
```


### Realistic (Michel mixed, standard) ###

```
RUNNUM=11600;NRUN=5;DATASET=20211200-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5376 (80.7571 %)
|   |                         
|   | propagation cut         
|   V                         
|  4864 (73.0659 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  4001 (60.1021 %)
|   |                         
|   | matching                
|   V                         
|  3918 (58.8553 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  3915 (58.8103 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  3814 (57.2931 %)
|      + 0.612619
|      - 0.614837
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.86383 +- 0.0609624
|  phi     (mrad)  6.54887 +- 0.0676231
|  momentum (MeV)  0.102127 +- 0.00104825
|  vertexZ   (mm)  1.89319 +- 0.017877
|  vertexY   (mm)  0.832636 +- 0.00823397
|  time     (sec)  4.8805e-11 +- 4.80324e-13
@-----------------------------@
```

### Realistic (Michel mixed, CYLDCHBadChannelsDB.id = 2) ###

```
RUNNUM=11600;NRUN=5;DATASET=20211200-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit_cdchbadchannel2|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5072 (76.1905 %)
|   |                         
|   | propagation cut         
|   V                         
|  4248 (63.8125 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  3282 (49.3015 %)
|   |                         
|   | matching                
|   V                         
|  3226 (48.4603 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  3223 (48.4152 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  3136 (47.1083 %)
|      + 0.619695
|      - 0.618815
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  7.03658 +- 0.06874
|  phi     (mrad)  6.8452 +- 0.0784834
|  momentum (MeV)  0.105265 +- 0.0012397
|  vertexZ   (mm)  1.97122 +- 0.0216054
|  vertexY   (mm)  0.849385 +- 0.00935927
|  time     (sec)  4.89789e-11 +- 5.49813e-13
@-----------------------------@
```

### Realistic (Michel mixed, CYLDCHBadChannelsDB.id = 3) ###

```
RUNNUM=11600;NRUN=5;DATASET=20211200-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit_cdchbadchannel3|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5294 (79.5253 %)
|   |                         
|   | propagation cut         
|   V                         
|  4762 (71.5337 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  3888 (58.4047 %)
|   |                         
|   | matching                
|   V                         
|  3811 (57.248 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  3807 (57.1879 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  3705 (55.6557 %)
|      + 0.615488
|      - 0.617208
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.9358 +- 0.0626632
|  phi     (mrad)  6.57921 +- 0.0678488
|  momentum (MeV)  0.102872 +- 0.00107658
|  vertexZ   (mm)  1.87378 +- 0.0182252
|  vertexY   (mm)  0.838397 +- 0.00867746
|  time     (sec)  4.89058e-11 +- 4.85063e-13
@-----------------------------@
```

### Realistic (Michel mixed, CYLDCHBadChannelsDB.id = 5) ###

```
RUNNUM=11600;NRUN=5;DATASET=20211200-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit_cdchbadchannel5|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5195 (78.0382 %)
|   |                         
|   | propagation cut         
|   V                         
|  4511 (67.7633 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  3540 (53.1771 %)
|   |                         
|   | matching                
|   V                         
|  3479 (52.2608 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  3475 (52.2007 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  3387 (50.8788 %)
|      + 0.620052
|      - 0.62032
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.99495 +- 0.0673939
|  phi     (mrad)  6.81438 +- 0.074342
|  momentum (MeV)  0.105736 +- 0.00118275
|  vertexZ   (mm)  1.95503 +- 0.0208083
|  vertexY   (mm)  0.863332 +- 0.00926914
|  time     (sec)  4.92628e-11 +- 5.24532e-13
@-----------------------------@
```


### Analyze execution and output ###
```
cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20211200-root6.22.06-geant4.10.06.p03-gcc930/noxec_PR_fit_genfit 115

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20211200-root6.22.06-geant4.10.06.p03-gcc930/noxec_PR_fit_genfit 116
```

```
./macros/analyzer_output.sh 20211200-root6.22.06-geant4.10.06.p03-gcc930/noxec_PR_fit_genfit 115
```
- - - -

10000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Dec 12 15:41 Eff_results11500.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Dec 12 09:25 histos11500.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Dec 12 09:28 histos11501.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Dec 12 09:26 histos11502.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Dec 12 09:28 histos11503.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Dec 12 09:24 histos11504.root
-rw-r--r--. 1 uchiyama unx-lke 110M Dec 12 09:25 rec11500.root
-rw-r--r--. 1 uchiyama unx-lke 114M Dec 12 09:28 rec11501.root
-rw-r--r--. 1 uchiyama unx-lke 109M Dec 12 09:26 rec11502.root
-rw-r--r--. 1 uchiyama unx-lke 111M Dec 12 09:28 rec11503.root
-rw-r--r--. 1 uchiyama unx-lke 109M Dec 12 09:24 rec11504.root

rec	56.983 kB/event
dch	50.444 kB/event
spx	5.641 kB/event
reco	0.382 kB/event
rdc	0.273 kB/event
trg	0.026 kB/event

```
######### execution time #########
9791.78/10000 = 0.979178 s/event
```
---------
analyzer........................... : 00:31:30.255  00:31:21.740  00:00:00.000
 DAQ bartender..................... : 00:01:12.127  00:01:10.709  00:01:10.389
 Task MEGMain...................... : 00:00:01.709  00:00:01.409  00:00:00.000
 Task ReadData..................... : 00:00:11.751  00:00:11.780  00:00:11.700
 Task SPX.......................... : 00:00:00.021  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.689  00:00:05.470  00:00:05.390
  SubTask SPXHitRec................ : 00:00:00.356  00:00:00.279  00:00:00.259
  SubTask SPXClustering............ : 00:00:11.608  00:00:11.540  00:00:11.479
  SubTask SPXIndependentTracking... : 00:00:32.494  00:00:32.450  00:00:32.440
 Task RDC.......................... : 00:00:00.021  00:00:00.039  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.100  00:00:00.140  00:00:00.130
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.029  00:00:00.019
 Task CYLDCH....................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:02:01.176  00:02:01.119  00:02:01.059
  SubTask CYLDCHHitRec............. : 00:00:00.669  00:00:00.719  00:00:00.689
  SubTask CYLDCHTrackFinderPR...... : 00:03:07.915  00:03:07.159  00:03:07.089
  SubTask DCHKalmanFilterGEN....... : 00:01:54.853  00:01:54.820  00:01:54.750
  SubTask DCHTrackReFit............ : 00:13:13.451  00:13:10.539  00:13:10.519
 Task DCHSPX....................... : 00:00:00.021  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:06.575  00:03:06.110  00:03:06.100
  SubTask DCHSPXReFit.............. : 00:04:26.133  00:04:25.629  00:04:25.609
  SubTask SPXTracking.............. : 00:01:08.393  00:01:08.170  00:01:08.110
  SubTask DCHSPXTrackSelection..... : 00:00:00.152  00:00:00.099  00:00:00.069
 WriteEvent........................ : 00:00:09.232  00:00:08.590
analyzer........................... : 00:34:34.146  00:34:25.949  00:00:00.000
 DAQ bartender..................... : 00:01:10.566  00:01:08.890  00:01:08.550
 Task MEGMain...................... : 00:00:01.735  00:00:01.580  00:00:00.000
 Task ReadData..................... : 00:00:09.582  00:00:09.649  00:00:09.599
 Task SPX.......................... : 00:00:00.021  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.838  00:00:05.840  00:00:05.740
  SubTask SPXHitRec................ : 00:00:00.362  00:00:00.310  00:00:00.300
  SubTask SPXClustering............ : 00:00:10.844  00:00:10.779  00:00:10.659
  SubTask SPXIndependentTracking... : 00:00:41.792  00:00:41.650  00:00:41.600
 Task RDC.......................... : 00:00:00.022  00:00:00.040  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.102  00:00:00.160  00:00:00.119
  SubTask RDCHitRec................ : 00:00:00.047  00:00:00.089  00:00:00.079
 Task CYLDCH....................... : 00:00:00.014  00:00:00.029  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:02:07.010  00:02:06.430  00:02:06.360
  SubTask CYLDCHHitRec............. : 00:00:00.675  00:00:00.689  00:00:00.689
  SubTask CYLDCHTrackFinderPR...... : 00:03:28.649  00:03:27.980  00:03:27.920
  SubTask DCHKalmanFilterGEN....... : 00:02:02.678  00:02:02.299  00:02:02.249
  SubTask DCHTrackReFit............ : 00:14:58.067  00:14:55.809  00:14:55.759
 Task DCHSPX....................... : 00:00:00.022  00:00:00.039  00:00:00.010
  SubTask DCHSPXMatching........... : 00:03:20.130  00:03:19.609  00:03:19.569
  SubTask DCHSPXReFit.............. : 00:04:43.944  00:04:43.010  00:04:42.990
  SubTask SPXTracking.............. : 00:01:16.791  00:01:16.649  00:01:16.579
  SubTask DCHSPXTrackSelection..... : 00:00:00.162  00:00:00.119  00:00:00.099
 WriteEvent........................ : 00:00:09.160  00:00:09.039
analyzer........................... : 00:32:15.324  00:32:07.490  00:00:00.000
 DAQ bartender..................... : 00:01:15.818  00:01:13.920  00:01:13.590
 Task MEGMain...................... : 00:00:01.869  00:00:01.589  00:00:00.009
 Task ReadData..................... : 00:00:13.606  00:00:13.469  00:00:13.429
 Task SPX.......................... : 00:00:00.021  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.903  00:00:06.049  00:00:05.949
  SubTask SPXHitRec................ : 00:00:00.382  00:00:00.370  00:00:00.360
  SubTask SPXClustering............ : 00:00:13.664  00:00:13.670  00:00:13.590
  SubTask SPXIndependentTracking... : 00:00:38.526  00:00:38.429  00:00:38.400
 Task RDC.......................... : 00:00:00.021  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.106  00:00:00.129  00:00:00.119
  SubTask RDCHitRec................ : 00:00:00.047  00:00:00.039  00:00:00.039
 Task CYLDCH....................... : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:02:07.552  00:02:06.930  00:02:06.870
  SubTask CYLDCHHitRec............. : 00:00:00.708  00:00:00.659  00:00:00.619
  SubTask CYLDCHTrackFinderPR...... : 00:03:12.765  00:03:12.410  00:03:12.340
  SubTask DCHKalmanFilterGEN....... : 00:01:58.434  00:01:58.010  00:01:57.940
  SubTask DCHTrackReFit............ : 00:14:03.637  00:14:01.469  00:14:01.389
 Task DCHSPX....................... : 00:00:00.021  00:00:00.070  00:00:00.009
  SubTask DCHSPXMatching........... : 00:02:52.449  00:02:52.070  00:02:52.030
  SubTask DCHSPXReFit.............. : 00:04:04.960  00:04:04.279  00:04:04.239
  SubTask SPXTracking.............. : 00:01:09.290  00:01:08.929  00:01:08.899
  SubTask DCHSPXTrackSelection..... : 00:00:00.159  00:00:00.139  00:00:00.139
 WriteEvent........................ : 00:00:09.155  00:00:09.290
analyzer........................... : 00:34:00.643  00:33:52.349  00:00:00.000
 DAQ bartender..................... : 00:01:12.936  00:01:11.469  00:01:11.029
 Task MEGMain...................... : 00:00:01.744  00:00:01.610  00:00:00.000
 Task ReadData..................... : 00:00:11.620  00:00:11.449  00:00:11.389
 Task SPX.......................... : 00:00:00.025  00:00:00.059  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:05.741  00:00:05.749  00:00:05.659
  SubTask SPXHitRec................ : 00:00:00.377  00:00:00.329  00:00:00.309
  SubTask SPXClustering............ : 00:00:12.338  00:00:12.239  00:00:12.149
  SubTask SPXIndependentTracking... : 00:00:38.504  00:00:38.420  00:00:38.370
 Task RDC.......................... : 00:00:00.025  00:00:00.049  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.110  00:00:00.109  00:00:00.099
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.039  00:00:00.019
 Task CYLDCH....................... : 00:00:00.014  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:02:01.898  00:02:01.660  00:02:01.560
  SubTask CYLDCHHitRec............. : 00:00:00.767  00:00:00.799  00:00:00.759
  SubTask CYLDCHTrackFinderPR...... : 00:03:43.936  00:03:43.069  00:03:42.979
  SubTask DCHKalmanFilterGEN....... : 00:02:03.757  00:02:03.479  00:02:03.409
  SubTask DCHTrackReFit............ : 00:14:47.757  00:14:44.919  00:14:44.869
 Task DCHSPX....................... : 00:00:00.025  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:02:59.597  00:02:59.170  00:02:59.130
  SubTask DCHSPXReFit.............. : 00:04:29.973  00:04:29.370  00:04:29.350
  SubTask SPXTracking.............. : 00:01:14.322  00:01:13.879  00:01:13.859
  SubTask DCHSPXTrackSelection..... : 00:00:00.169  00:00:00.200  00:00:00.170
 WriteEvent........................ : 00:00:08.981  00:00:09.159
analyzer........................... : 00:30:51.417  00:30:47.430  00:00:00.000
 DAQ bartender..................... : 00:01:05.199  00:01:03.650  00:01:03.390
 Task MEGMain...................... : 00:00:01.777  00:00:01.529  00:00:00.000
 Task ReadData..................... : 00:00:07.211  00:00:07.190  00:00:07.140
 Task SPX.......................... : 00:00:00.014  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.610  00:00:05.650  00:00:05.550
  SubTask SPXHitRec................ : 00:00:00.302  00:00:00.230  00:00:00.230
  SubTask SPXClustering............ : 00:00:11.468  00:00:11.380  00:00:11.340
  SubTask SPXIndependentTracking... : 00:00:34.739  00:00:34.789  00:00:34.739
 Task RDC.......................... : 00:00:00.015  00:00:00.019  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.083  00:00:00.080  00:00:00.070
  SubTask RDCHitRec................ : 00:00:00.037  00:00:00.059  00:00:00.049
 Task CYLDCH....................... : 00:00:00.008  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:48.875  00:01:48.569  00:01:48.529
  SubTask CYLDCHHitRec............. : 00:00:00.585  00:00:00.529  00:00:00.509
  SubTask CYLDCHTrackFinderPR...... : 00:03:01.762  00:03:00.839  00:03:00.749
  SubTask DCHKalmanFilterGEN....... : 00:01:55.601  00:01:55.789  00:01:55.769
  SubTask DCHTrackReFit............ : 00:13:27.290  00:13:27.009  00:13:26.999
 Task DCHSPX....................... : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:02:53.378  00:02:53.479  00:02:53.459
  SubTask DCHSPXReFit.............. : 00:04:16.176  00:04:16.090  00:04:16.090
  SubTask SPXTracking.............. : 00:01:08.224  00:01:08.450  00:01:08.390
  SubTask DCHSPXTrackSelection..... : 00:00:00.143  00:00:00.129  00:00:00.129
 WriteEvent........................ : 00:00:08.147  00:00:07.989
```

```
./macros/analyzer_output.sh 20211200-root6.22.06-geant4.10.06.p03-gcc930/noxec_PR_fit_genfit 116
```
- - - -

10000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Dec 13 01:56 Eff_results11600.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Dec 12 17:45 histos11600.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Dec 12 17:51 histos11601.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Dec 12 17:46 histos11602.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Dec 12 17:44 histos11603.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Dec 12 17:42 histos11604.root
-rw-r--r--. 1 uchiyama unx-lke 299M Dec 12 17:45 rec11600.root
-rw-r--r--. 1 uchiyama unx-lke 301M Dec 12 17:51 rec11601.root
-rw-r--r--. 1 uchiyama unx-lke 300M Dec 12 17:46 rec11602.root
-rw-r--r--. 1 uchiyama unx-lke 301M Dec 12 17:44 rec11603.root
-rw-r--r--. 1 uchiyama unx-lke 301M Dec 12 17:42 rec11604.root

rec	156.16 kB/event
dch	143.797 kB/event
spx	7.907 kB/event
rdc	3.063 kB/event
reco	0.807 kB/event
trg	0.056 kB/event

```
######### execution time #########
34057/10000 = 3.4057 s/event
```
---------
analyzer........................... : 01:53:10.257  01:52:13.500  00:00:00.000
 DAQ bartender..................... : 00:04:28.173  00:03:52.280  00:03:51.500
 Task MEGMain...................... : 00:00:02.979  00:00:01.789  00:00:00.120
 Task ReadData..................... : 00:00:25.739  00:00:25.739  00:00:25.669
 Task SPX.......................... : 00:00:00.019  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.557  00:00:09.649  00:00:09.589
  SubTask SPXHitRec................ : 00:00:00.384  00:00:00.390  00:00:00.370
  SubTask SPXClustering............ : 00:00:18.649  00:00:18.500  00:00:18.430
  SubTask SPXIndependentTracking... : 00:00:48.622  00:00:48.409  00:00:48.359
 Task RDC.......................... : 00:00:00.019  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:09.520  00:00:09.339  00:00:09.319
  SubTask RDCHitRec................ : 00:00:00.238  00:00:00.250  00:00:00.200
 Task CYLDCH....................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:12:25.260  00:12:23.350  00:12:23.280
  SubTask CYLDCHHitRec............. : 00:00:17.521  00:00:17.450  00:00:17.410
  SubTask CYLDCHTrackFinderPR...... : 00:55:56.023  00:55:47.229  00:55:47.189
  SubTask DCHKalmanFilterGEN....... : 00:11:28.773  00:11:26.930  00:11:26.850
  SubTask DCHTrackReFit............ : 00:14:31.929  00:14:29.199  00:14:29.169
 Task DCHSPX....................... : 00:00:00.018  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:05:41.697  00:05:40.840  00:05:40.790
  SubTask DCHSPXReFit.............. : 00:04:39.918  00:04:39.150  00:04:39.130
  SubTask SPXTracking.............. : 00:01:12.465  00:01:12.440  00:01:12.420
  SubTask DCHSPXTrackSelection..... : 00:00:00.212  00:00:00.209  00:00:00.199
 WriteEvent........................ : 00:00:19.822  00:00:18.859
analyzer........................... : 01:58:43.917  01:57:48.510  00:00:00.000
 DAQ bartender..................... : 00:04:36.066  00:04:00.469  00:03:59.339
 Task MEGMain...................... : 00:00:02.161  00:00:01.550  00:00:00.039
 Task ReadData..................... : 00:00:25.613  00:00:25.640  00:00:25.570
 Task SPX.......................... : 00:00:00.019  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.773  00:00:09.639  00:00:09.559
  SubTask SPXHitRec................ : 00:00:00.387  00:00:00.370  00:00:00.370
  SubTask SPXClustering............ : 00:00:19.065  00:00:19.160  00:00:19.080
  SubTask SPXIndependentTracking... : 00:00:55.329  00:00:55.069  00:00:55.019
 Task RDC.......................... : 00:00:00.019  00:00:00.030  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:09.784  00:00:09.939  00:00:09.909
  SubTask RDCHitRec................ : 00:00:00.240  00:00:00.269  00:00:00.249
 Task CYLDCH....................... : 00:00:00.013  00:00:00.020  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:12:29.625  00:12:27.340  00:12:27.290
  SubTask CYLDCHHitRec............. : 00:00:17.962  00:00:18.100  00:00:18.070
  SubTask CYLDCHTrackFinderPR...... : 01:00:05.929  00:59:56.629  00:59:56.589
  SubTask DCHKalmanFilterGEN....... : 00:11:41.124  00:11:39.170  00:11:39.090
  SubTask DCHTrackReFit............ : 00:14:27.889  00:14:25.980  00:14:25.930
 Task DCHSPX....................... : 00:00:00.018  00:00:00.019  00:00:00.019
  SubTask DCHSPXMatching........... : 00:06:07.979  00:06:07.039  00:06:06.979
  SubTask DCHSPXReFit.............. : 00:05:03.438  00:05:02.459  00:05:02.449
  SubTask SPXTracking.............. : 00:01:18.850  00:01:18.920  00:01:18.870
  SubTask DCHSPXTrackSelection..... : 00:00:00.216  00:00:00.219  00:00:00.200
 WriteEvent........................ : 00:00:19.995  00:00:18.720
analyzer........................... : 01:53:49.304  01:53:08.640  00:00:00.000
 DAQ bartender..................... : 00:04:20.810  00:03:44.679  00:03:43.829
 Task MEGMain...................... : 00:00:01.486  00:00:01.419  00:00:00.080
 Task ReadData..................... : 00:00:22.617  00:00:22.620  00:00:22.580
 Task SPX.......................... : 00:00:00.013  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.796  00:00:09.659  00:00:09.589
  SubTask SPXHitRec................ : 00:00:00.324  00:00:00.379  00:00:00.349
  SubTask SPXClustering............ : 00:00:24.384  00:00:24.759  00:00:24.680
  SubTask SPXIndependentTracking... : 00:00:51.198  00:00:50.949  00:00:50.929
 Task RDC.......................... : 00:00:00.013  00:00:00.040  00:00:00.020
  SubTask RDCWaveformAnalysis...... : 00:00:08.296  00:00:08.370  00:00:08.330
  SubTask RDCHitRec................ : 00:00:00.219  00:00:00.210  00:00:00.180
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:22.089  00:11:22.109  00:11:22.099
  SubTask CYLDCHHitRec............. : 00:00:16.401  00:00:16.290  00:00:16.290
  SubTask CYLDCHTrackFinderPR...... : 00:58:07.991  00:58:07.599  00:58:07.559
  SubTask DCHKalmanFilterGEN....... : 00:12:01.097  00:12:00.809  00:12:00.749
  SubTask DCHTrackReFit............ : 00:14:09.878  00:14:09.489  00:14:09.479
 Task DCHSPX....................... : 00:00:00.013  00:00:00.040  00:00:00.010
  SubTask DCHSPXMatching........... : 00:05:25.072  00:05:24.850  00:05:24.780
  SubTask DCHSPXReFit.............. : 00:04:24.093  00:04:24.169  00:04:24.119
  SubTask SPXTracking.............. : 00:01:12.933  00:01:12.940  00:01:12.930
  SubTask DCHSPXTrackSelection..... : 00:00:00.199  00:00:00.250  00:00:00.230
 WriteEvent........................ : 00:00:18.367  00:00:17.350
analyzer........................... : 01:51:49.842  01:51:11.610  00:00:00.000
 DAQ bartender..................... : 00:04:18.865  00:03:45.610  00:03:44.750
 Task MEGMain...................... : 00:00:02.563  00:00:01.590  00:00:00.090
 Task ReadData..................... : 00:00:21.977  00:00:21.989  00:00:21.969
 Task SPX.......................... : 00:00:00.014  00:00:00.020  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:09.577  00:00:09.540  00:00:09.470
  SubTask SPXHitRec................ : 00:00:00.318  00:00:00.320  00:00:00.300
  SubTask SPXClustering............ : 00:00:22.043  00:00:21.950  00:00:21.920
  SubTask SPXIndependentTracking... : 00:00:50.638  00:00:50.760  00:00:50.710
 Task RDC.......................... : 00:00:00.013  00:00:00.019  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:08.287  00:00:08.009  00:00:07.999
  SubTask RDCHitRec................ : 00:00:00.219  00:00:00.230  00:00:00.189
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:38.261  00:11:38.039  00:11:37.979
  SubTask CYLDCHHitRec............. : 00:00:16.447  00:00:16.450  00:00:16.429
  SubTask CYLDCHTrackFinderPR...... : 00:55:46.764  00:55:46.170  00:55:46.150
  SubTask DCHKalmanFilterGEN....... : 00:11:28.421  00:11:28.479  00:11:28.459
  SubTask DCHTrackReFit............ : 00:14:04.263  00:14:03.000  00:14:02.980
 Task DCHSPX....................... : 00:00:00.012  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:05:46.556  00:05:46.789  00:05:46.749
  SubTask DCHSPXReFit.............. : 00:04:38.804  00:04:38.629  00:04:38.559
  SubTask SPXTracking.............. : 00:01:26.720  00:01:26.460  00:01:26.440
  SubTask DCHSPXTrackSelection..... : 00:00:00.199  00:00:00.170  00:00:00.150
 WriteEvent........................ : 00:00:18.522  00:00:17.959
analyzer........................... : 01:50:03.717  01:49:31.989  00:00:00.000
 DAQ bartender..................... : 00:04:13.402  00:03:44.080  00:03:43.000
 Task MEGMain...................... : 00:00:01.925  00:00:01.549  00:00:00.089
 Task ReadData..................... : 00:00:22.198  00:00:22.110  00:00:22.010
 Task SPX.......................... : 00:00:00.013  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.696  00:00:09.719  00:00:09.659
  SubTask SPXHitRec................ : 00:00:00.324  00:00:00.300  00:00:00.300
  SubTask SPXClustering............ : 00:00:22.142  00:00:22.400  00:00:22.340
  SubTask SPXIndependentTracking... : 00:00:49.541  00:00:49.169  00:00:49.169
 Task RDC.......................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.377  00:00:08.640  00:00:08.640
  SubTask RDCHitRec................ : 00:00:00.233  00:00:00.299  00:00:00.289
 Task CYLDCH....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:26.336  00:11:25.929  00:11:25.889
  SubTask CYLDCHHitRec............. : 00:00:16.348  00:00:16.330  00:00:16.310
  SubTask CYLDCHTrackFinderPR...... : 00:54:44.475  00:54:44.189  00:54:44.179
  SubTask DCHKalmanFilterGEN....... : 00:11:33.802  00:11:34.210  00:11:34.160
  SubTask DCHTrackReFit............ : 00:13:33.697  00:13:32.940  00:13:32.910
 Task DCHSPX....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:05:56.358  00:05:56.660  00:05:56.650
  SubTask DCHSPXReFit.............. : 00:04:43.154  00:04:42.810  00:04:42.800
  SubTask SPXTracking.............. : 00:01:12.995  00:01:12.989  00:01:12.929
  SubTask DCHSPXTrackSelection..... : 00:00:00.195  00:00:00.259  00:00:00.259
 WriteEvent........................ : 00:00:18.044  00:00:17.440
```
