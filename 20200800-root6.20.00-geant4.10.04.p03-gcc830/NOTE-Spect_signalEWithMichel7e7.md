2020/08/03

# Bartender output with Spect_signalE + Full_michel mixed.

* No mark (optimistic)
   * A signal event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Michel events are randomly mixed at rate of 7e7 Hz in time window (-350,350) ns.
   * Run number convention: 111**  
     10 gem4 runs (sev files) are summed up in a bartender run.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
* realistic
   * Run number convention: 116**

0ead0d99222233997499079e8ba75012cc7a03cb

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_signalEWithMichel7e7.xml","/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830", 11100, 2, "")'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_signalEWithMichel7e7_realistic.xml","/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830", 11600, 2, "")'
```

```
./macros/bartender_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7 111
./macros/bartender_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
cat NOTE-Spect_signalEWithMichel7e7_realistic.md >> NOTE-Spect_signalEWithMichel7e7;rm NOTE-Spect_signalEWithMichel7e7_realistic.md
```


### Data and Statistics ###


```
./macros/bartender_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7 111
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7 | grep -E "111[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 2.8G Aug  3 12:41 raw11100.root
-rw-r--r--. 1 uchiyama unx-lke 2.8G Aug  3 12:41 raw11101.root
-rw-r--r--. 1 uchiyama unx-lke 5.7G Aug  3 12:41 sim11100.root
-rw-r--r--. 1 uchiyama unx-lke 5.7G Aug  3 12:41 sim11101.root

sim	3009.19 kB/event
track	1724.12 kB/event
dch	1234.74 kB/event
kine	27.521 kB/event
spx	9.018 kB/event

```
######### execution time #########
7293.7/4000 = 1.82342 s/event
```
---------
analyzer........................... : 01:00:54.917  01:00:05.240  00:00:00.000
 DAQ gem4.......................... : 00:00:00.704  00:00:00.220  00:00:00.000
 Task MEGMain...................... : 00:00:00.120  00:00:00.140  00:00:00.110
 Task ReadGEM4..................... : 00:04:21.294  00:04:07.799  00:04:07.639
 Task COMMixEvents................. : 00:01:11.122  00:01:10.769  00:01:10.759
 Task CYLDCHMixEvents.............. : 00:01:02.599  00:01:02.489  00:01:02.429
 Task SPXMixEvents................. : 00:00:00.431  00:00:00.340  00:00:00.320
 Task RDCMixEvents................. : 00:00:00.158  00:00:00.180  00:00:00.170
 Task SPXDigitize.................. : 00:01:02.101  00:01:01.820  00:00:59.980
 Task CYLDCHDigitize............... : 00:45:55.716  00:45:49.129  00:45:49.059
 Task RDCDigitize.................. : 00:00:08.165  00:00:07.899  00:00:07.089
 WriteEvent........................ : 00:06:51.339  00:06:32.670
analyzer........................... : 01:00:38.783  00:59:53.159  00:00:00.000
 DAQ gem4.......................... : 00:00:00.691  00:00:00.209  00:00:00.000
 Task MEGMain...................... : 00:00:00.118  00:00:00.069  00:00:00.059
 Task ReadGEM4..................... : 00:04:10.567  00:03:58.609  00:03:58.389
 Task COMMixEvents................. : 00:01:09.868  00:01:09.860  00:01:09.840
 Task CYLDCHMixEvents.............. : 00:01:09.378  00:01:09.169  00:01:09.159
 Task SPXMixEvents................. : 00:00:00.425  00:00:00.440  00:00:00.420
 Task RDCMixEvents................. : 00:00:00.155  00:00:00.160  00:00:00.150
 Task SPXDigitize.................. : 00:01:00.981  00:01:00.699  00:00:58.859
 Task CYLDCHDigitize............... : 00:45:49.199  00:45:42.700  00:45:42.650
 Task RDCDigitize.................. : 00:00:07.950  00:00:07.510  00:00:06.710
 WriteEvent........................ : 00:06:49.139  00:06:32.019
```

```
./macros/bartender_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7_realistic | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 2.9G Aug  3 12:23 raw11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.9G Aug  3 12:24 raw11601.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Aug  3 12:23 sim11600.root
-rw-r--r--. 1 uchiyama unx-lke 5.7G Aug  3 12:24 sim11601.root

sim	2993.45 kB/event
track	1714.82 kB/event
dch	1228.49 kB/event
kine	27.374 kB/event
spx	9.003 kB/event

```
######### execution time #########
5167.73/4000 = 1.29193 s/event
```
---------
analyzer........................... : 00:42:39.974  00:42:03.760  00:00:00.000
 DAQ gem4.......................... : 00:00:00.279  00:00:00.190  00:00:00.000
 Task MEGMain...................... : 00:00:00.129  00:00:00.110  00:00:00.110
 Task ReadGEM4..................... : 00:04:27.048  00:04:13.989  00:04:13.869
 Task COMMixEvents................. : 00:01:12.525  00:01:12.049  00:01:12.029
 Task CYLDCHMixEvents.............. : 00:01:03.338  00:01:03.460  00:01:03.430
 Task SPXMixEvents................. : 00:00:00.441  00:00:00.449  00:00:00.439
 Task RDCMixEvents................. : 00:00:00.160  00:00:00.139  00:00:00.139
 Task SPXDigitize.................. : 00:01:02.861  00:01:02.790  00:01:00.960
 Task CYLDCHDigitize............... : 00:27:35.745  00:27:31.739  00:27:31.719
 Task RDCDigitize.................. : 00:00:08.276  00:00:08.169  00:00:07.339
 WriteEvent........................ : 00:06:48.969  00:06:38.350
analyzer........................... : 00:43:27.753  00:42:47.440  00:00:00.000
 DAQ gem4.......................... : 00:00:00.193  00:00:00.150  00:00:00.009
 Task MEGMain...................... : 00:00:00.131  00:00:00.089  00:00:00.079
 Task ReadGEM4..................... : 00:04:33.054  00:04:19.010  00:04:18.870
 Task COMMixEvents................. : 00:01:13.595  00:01:13.919  00:01:13.879
 Task CYLDCHMixEvents.............. : 00:01:15.649  00:01:15.180  00:01:15.170
 Task SPXMixEvents................. : 00:00:00.443  00:00:00.419  00:00:00.409
 Task RDCMixEvents................. : 00:00:00.161  00:00:00.139  00:00:00.139
 Task SPXDigitize.................. : 00:01:01.218  00:01:01.199  00:00:59.339
 Task CYLDCHDigitize............... : 00:27:55.936  00:27:51.920  00:27:51.860
 Task RDCDigitize.................. : 00:00:08.056  00:00:08.159  00:00:07.349
 WriteEvent........................ : 00:06:58.293  00:06:44.540
```
