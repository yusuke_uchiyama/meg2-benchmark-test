2020/08/03

# Spectrometer (-d XEC).
- Photon tracking in SPX.
- 9 layer CDCH 
- Custom run number 2000116

# Signal positron generated on target
- in the 'extended' range
- No event selection in gem4
- Number of events (generation) in a run: 200

b66c44d7dc4e278dba5f10715ca10a329697424a

## LOG ##
```
 root -q 'macros/benchmark/JobSubmit.C("macros/benchmark/Spect_signalE.mac", "-d xec", "/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830/gem4Out/", 200, 11000, 20, "-p short")'
```

#11000-11019
200 events * 20 run = 4000 events generation,

```
% du -cm gem4Out/sev11*
28      gem4Out/sev11000.root
29      gem4Out/sev11001.root
25      gem4Out/sev11002.root
28      gem4Out/sev11003.root
29      gem4Out/sev11004.root
29      gem4Out/sev11005.root
27      gem4Out/sev11006.root
26      gem4Out/sev11007.root
27      gem4Out/sev11008.root
27      gem4Out/sev11009.root
25      gem4Out/sev11010.root
26      gem4Out/sev11011.root
26      gem4Out/sev11012.root
27      gem4Out/sev11013.root
28      gem4Out/sev11014.root
30      gem4Out/sev11015.root
26      gem4Out/sev11016.root
32      gem4Out/sev11017.root
29      gem4Out/sev11018.root
26      gem4Out/sev11019.root
539     total


```
```
% grep 'User=' gem4Out/log/g1100* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=4000} END{print sum "/" n " = " sum/n "s/event"}'
2903.48/4000 = 0.72587s/event
```
```
megbartender [1] sev->Print("toponly")

```