
```
./macros/bartender_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7_realistic | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 2.9G Aug  3 12:23 raw11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.9G Aug  3 12:24 raw11601.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Aug  3 12:23 sim11600.root
-rw-r--r--. 1 uchiyama unx-lke 5.7G Aug  3 12:24 sim11601.root

sim	2993.45 kB/event
track	1714.82 kB/event
dch	1228.49 kB/event
kine	27.374 kB/event
spx	9.003 kB/event

```
######### execution time #########
5167.73/4000 = 1.29193 s/event
```
---------
analyzer........................... : 00:42:39.974  00:42:03.760  00:00:00.000
 DAQ gem4.......................... : 00:00:00.279  00:00:00.190  00:00:00.000
 Task MEGMain...................... : 00:00:00.129  00:00:00.110  00:00:00.110
 Task ReadGEM4..................... : 00:04:27.048  00:04:13.989  00:04:13.869
 Task COMMixEvents................. : 00:01:12.525  00:01:12.049  00:01:12.029
 Task CYLDCHMixEvents.............. : 00:01:03.338  00:01:03.460  00:01:03.430
 Task SPXMixEvents................. : 00:00:00.441  00:00:00.449  00:00:00.439
 Task RDCMixEvents................. : 00:00:00.160  00:00:00.139  00:00:00.139
 Task SPXDigitize.................. : 00:01:02.861  00:01:02.790  00:01:00.960
 Task CYLDCHDigitize............... : 00:27:35.745  00:27:31.739  00:27:31.719
 Task RDCDigitize.................. : 00:00:08.276  00:00:08.169  00:00:07.339
 WriteEvent........................ : 00:06:48.969  00:06:38.350
analyzer........................... : 00:43:27.753  00:42:47.440  00:00:00.000
 DAQ gem4.......................... : 00:00:00.193  00:00:00.150  00:00:00.009
 Task MEGMain...................... : 00:00:00.131  00:00:00.089  00:00:00.079
 Task ReadGEM4..................... : 00:04:33.054  00:04:19.010  00:04:18.870
 Task COMMixEvents................. : 00:01:13.595  00:01:13.919  00:01:13.879
 Task CYLDCHMixEvents.............. : 00:01:15.649  00:01:15.180  00:01:15.170
 Task SPXMixEvents................. : 00:00:00.443  00:00:00.419  00:00:00.409
 Task RDCMixEvents................. : 00:00:00.161  00:00:00.139  00:00:00.139
 Task SPXDigitize.................. : 00:01:01.218  00:01:01.199  00:00:59.339
 Task CYLDCHDigitize............... : 00:27:55.936  00:27:51.920  00:27:51.860
 Task RDCDigitize.................. : 00:00:08.056  00:00:08.159  00:00:07.349
 WriteEvent........................ : 00:06:58.293  00:06:44.540
```
