2020/08/03

# Full detetctor (default incl. DS-RDC) 
- Photon tracking in SPX.
- 9 layer CDCH
- Custom run number 2000116

Michel posiron generated on target in 4pi

No event selection in gem4

b66c44d7dc4e278dba5f10715ca10a329697424a


## LOG ##
```
 root -q 'macros/benchmark/JobSubmit.C("macros/benchmark/Full_michel.mac", "", "/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830/gem4Out", 10000, 12000, 20, "-p short")'
```

#12000-12019 

```
% du -cm gem4Out/sev12*
542     gem4Out/sev12000.root
556     gem4Out/sev12001.root
553     gem4Out/sev12002.root
549     gem4Out/sev12003.root
541     gem4Out/sev12004.root
539     gem4Out/sev12005.root
547     gem4Out/sev12006.root
542     gem4Out/sev12007.root
549     gem4Out/sev12008.root
563     gem4Out/sev12009.root
562     gem4Out/sev12010.root
541     gem4Out/sev12011.root
560     gem4Out/sev12012.root
546     gem4Out/sev12013.root
553     gem4Out/sev12014.root
540     gem4Out/sev12015.root
548     gem4Out/sev12016.root
551     gem4Out/sev12017.root
549     gem4Out/sev12018.root
545     gem4Out/sev12019.root
10968   total
```
```
% grep 'User=' gem4Out/log/g1200* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
20243.5/200000 = 0.101218s/event
```
```
megbartender [1] sev->Print("toponly")

```