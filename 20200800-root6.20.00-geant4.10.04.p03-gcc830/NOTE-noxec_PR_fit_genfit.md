2020/08/04

# Analyzer output with noxec_PR_fit_genfit.xml
* No XEC reconstruction
* SPX analysis from WaveformAnalysis (cf30)
* CDC analysis from waveform (WaveformAnalysis), pattern recognition and fit with GENFIT
* Custom run number 2000116
* Hit z reconstruction used in CYLDCHTrackFinderPR is time-difference
* CYLDCHWaveformAnalysis2 (with not fully optimized parameters).

mc_20200804.0

## Run Numbers ##
11000-11001 Spect_signalEOnly  
11100-11101 Spect_signalEWithMichel7e7  
11500-11501 Spect_signalEOnly_realistic  
11600-11601 Spect_signalEWithMichel7e7_realistic  


## Submit jobs ##
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix","/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830/", 11000, 2)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_signalEWithMichel7e7","/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830/", 11100, 2)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830/", 11500, 2)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830/", 11600, 2)'
```



## Calculate positron efficiency ##
### Optimistic ###
```
RUNNUM=11000;NRUN=2;DATASET=20200800-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary
|   All events:
|  2000
|   |
|   | LXe acceptance cut
|   V
|  2614 (100 %)
|   |
|   | SPX acceptance cut
|   V
|  2389 (91.3925 %)
|   |
|   | DCH tracking
|   |  including contamination cut
|   |  (FakeThre:0.6)
|   V
|  2324 (88.9059 %)
|   |
|   | propagation cut
|   V
|  2315 (88.5616 %)
|   |
|   | tail cut with
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V
|  2222 (85.0038 %)
|   |
|   | matching
|   V
|  2196 (84.0092 %)
|   |
|   | SPX cluster contamination cut
|   |  (FakeThre:0.6)
|   V
|  2196 (84.0092 %)
|   |
|   | time cut
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V
|  2145 (82.0581 %)
|      + 0.756954
|      - 0.782102
@-----------------------------@
|           Resolutions
|  theta   (mrad)  5.86359
|  phi     (mrad)  5.257
|  momentum (MeV)  0.0706526
|  vertexZ   (mm)  1.2637
|  vertexY   (mm)  0.66469
|  time     (sec)  4.38728e-11
@-----------------------------@
```


```
RUNNUM=11100;NRUN=2;DATASET=20200800-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2614 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2391 (91.469 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2276 (87.0696 %)
|   |                         
|   | propagation cut         
|   V                         
|  2155 (82.4407 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1854 (70.9258 %)
|   |                         
|   | matching                
|   V                         
|  1818 (69.5486 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1816 (69.4721 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1764 (67.4828 %)
|      + 0.928357
|      - 0.942009
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.16748
|  phi     (mrad)  5.67431
|  momentum (MeV)  0.0789037
|  vertexZ   (mm)  1.42965
|  vertexY   (mm)  0.69824
|  time     (sec)  4.62029e-11
@-----------------------------@
```


### Realistic ###

```
RUNNUM=11500;NRUN=2;DATASET=20200800-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2614 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2392 (91.5073 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2325 (88.9441 %)
|   |                         
|   | propagation cut         
|   V                         
|  2311 (88.4086 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  2169 (82.9763 %)
|   |                         
|   | matching                
|   V                         
|  2136 (81.7138 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  2136 (81.7138 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  2083 (79.6863 %)
|      + 0.79432
|      - 0.81758
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.29783
|  phi     (mrad)  5.701
|  momentum (MeV)  0.0783433
|  vertexZ   (mm)  1.52087
|  vertexY   (mm)  0.710633
|  time     (sec)  4.36306e-11
@-----------------------------@
```

```
RUNNUM=11600;NRUN=2;DATASET=20200800-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2614 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2389 (91.3925 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2218 (84.8508 %)
|   |                         
|   | propagation cut         
|   V                         
|  2084 (79.7246 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1775 (67.9036 %)
|   |                         
|   | matching                
|   V                         
|  1741 (66.6029 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1740 (66.5647 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1688 (64.5754 %)
|      + 0.948741
|      - 0.960119
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.60347
|  phi     (mrad)  6.54946
|  momentum (MeV)  0.0919851
|  vertexZ   (mm)  1.76174
|  vertexY   (mm)  0.778892
|  time     (sec)  4.5742e-11
@-----------------------------@
```


### Analyze execution and output ###
```
cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 110

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 111

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```

```
./macros/analyzer_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 110
```
- - - -

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "110[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Aug  4 04:50 Eff_results11000.root
-rw-r--r--. 1 uchiyama unx-lke 2.3M Aug  4 04:22 histos11000.root
-rw-r--r--. 1 uchiyama unx-lke 2.3M Aug  4 04:22 histos11001.root
-rw-r--r--. 1 uchiyama unx-lke  64M Aug  4 04:22 rec11000.root
-rw-r--r--. 1 uchiyama unx-lke  65M Aug  4 04:22 rec11001.root

rec	33.369 kB/event
dch	29.269 kB/event
spx	3.51 kB/event
reco	0.28 kB/event
rdc	0.218 kB/event

```
######### execution time #########
2350.44/4000 = 0.58761 s/event
```
---------
analyzer........................... : 00:19:24.322  00:19:15.590  00:00:00.000
 DAQ bartender..................... : 00:00:56.283  00:00:53.560  00:00:53.250
 Task MEGMain...................... : 00:00:01.159  00:00:01.029  00:00:00.000
 Task ReadData..................... : 00:00:07.620  00:00:07.559  00:00:07.499
 Task SPX.......................... : 00:00:00.059  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.521  00:00:04.460  00:00:04.350
  SubTask SPXHitRec................ : 00:00:00.272  00:00:00.309  00:00:00.299
  SubTask SPXClustering............ : 00:00:06.548  00:00:06.690  00:00:06.650
  SubTask SPXIndependentTracking... : 00:00:26.941  00:00:26.709  00:00:26.699
 Task RDC.......................... : 00:00:00.014  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.074  00:00:00.060  00:00:00.040
  SubTask RDCHitRec................ : 00:00:00.036  00:00:00.029  00:00:00.029
 Task CYLDCH....................... : 00:00:00.010  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:14.951  00:01:14.629  00:01:14.609
  SubTask CYLDCHHitRec............. : 00:00:00.476  00:00:00.550  00:00:00.540
  SubTask CYLDCHTrackFinderPR...... : 00:02:34.330  00:02:33.759  00:02:33.719
  SubTask DCHKalmanFilterGEN....... : 00:10:54.420  00:10:52.069  00:10:52.009
  SubTask DCHTrackReFit............ : 00:00:00.583  00:00:00.610  00:00:00.600
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:02.315  00:01:02.150  00:01:02.140
  SubTask DCHSPXReFit.............. : 00:01:24.714  00:01:24.260  00:01:24.220
  SubTask SPXTracking.............. : 00:00:17.045  00:00:17.369  00:00:17.339
  SubTask DCHSPXTrackSelection..... : 00:00:00.065  00:00:00.029  00:00:00.019
 WriteEvent........................ : 00:00:06.625  00:00:05.680
analyzer........................... : 00:19:46.119  00:19:37.050  00:00:00.000
 DAQ bartender..................... : 00:00:57.993  00:00:55.100  00:00:54.840
 Task MEGMain...................... : 00:00:01.219  00:00:01.019  00:00:00.019
 Task ReadData..................... : 00:00:08.709  00:00:08.669  00:00:08.619
 Task SPX.......................... : 00:00:00.013  00:00:00.020  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:04.352  00:00:04.129  00:00:04.089
  SubTask SPXHitRec................ : 00:00:00.287  00:00:00.300  00:00:00.300
  SubTask SPXClustering............ : 00:00:06.973  00:00:07.210  00:00:07.170
  SubTask SPXIndependentTracking... : 00:00:25.958  00:00:25.779  00:00:25.769
 Task RDC.......................... : 00:00:00.014  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.073  00:00:00.039  00:00:00.029
  SubTask RDCHitRec................ : 00:00:00.035  00:00:00.029  00:00:00.009
 Task CYLDCH....................... : 00:00:00.010  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:15.986  00:01:16.059  00:01:16.059
  SubTask CYLDCHHitRec............. : 00:00:00.483  00:00:00.400  00:00:00.390
  SubTask CYLDCHTrackFinderPR...... : 00:02:31.257  00:02:29.830  00:02:29.790
  SubTask DCHKalmanFilterGEN....... : 00:10:59.362  00:10:56.649  00:10:56.639
  SubTask DCHTrackReFit............ : 00:00:01.123  00:00:01.059  00:00:01.049
 Task DCHSPX....................... : 00:00:00.012  00:00:00.040  00:00:00.030
  SubTask DCHSPXMatching........... : 00:01:12.185  00:01:12.140  00:01:12.140
  SubTask DCHSPXReFit.............. : 00:01:31.780  00:01:31.609  00:01:31.579
  SubTask SPXTracking.............. : 00:00:16.748  00:00:16.539  00:00:16.529
  SubTask DCHSPXTrackSelection..... : 00:00:00.065  00:00:00.069  00:00:00.049
 WriteEvent........................ : 00:00:06.878  00:00:06.400
```

```
./macros/analyzer_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115
```
- - - -

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Aug  4 04:56 Eff_results11500.root
-rw-r--r--. 1 uchiyama unx-lke 2.3M Aug  4 04:23 histos11500.root
-rw-r--r--. 1 uchiyama unx-lke 2.3M Aug  4 04:23 histos11501.root
-rw-r--r--. 1 uchiyama unx-lke  61M Aug  4 04:23 rec11500.root
-rw-r--r--. 1 uchiyama unx-lke  62M Aug  4 04:23 rec11501.root

rec	31.584 kB/event
dch	27.475 kB/event
spx	3.547 kB/event
reco	0.271 kB/event
rdc	0.206 kB/event

```
######### execution time #########
2358.02/4000 = 0.589504 s/event
```
---------
analyzer........................... : 00:19:35.982  00:19:29.900  00:00:00.000
 DAQ bartender..................... : 00:00:58.031  00:00:55.530  00:00:55.220
 Task MEGMain...................... : 00:00:01.063  00:00:00.989  00:00:00.000
 Task ReadData..................... : 00:00:08.878  00:00:08.759  00:00:08.689
 Task SPX.......................... : 00:00:00.014  00:00:00.019  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:04.505  00:00:04.529  00:00:04.489
  SubTask SPXHitRec................ : 00:00:00.289  00:00:00.239  00:00:00.209
  SubTask SPXClustering............ : 00:00:07.100  00:00:07.240  00:00:07.220
  SubTask SPXIndependentTracking... : 00:00:27.507  00:00:27.389  00:00:27.369
 Task RDC.......................... : 00:00:00.013  00:00:00.019  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.075  00:00:00.079  00:00:00.069
  SubTask RDCHitRec................ : 00:00:00.037  00:00:00.029  00:00:00.029
 Task CYLDCH....................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:30.133  00:01:29.840  00:01:29.820
  SubTask CYLDCHHitRec............. : 00:00:00.481  00:00:00.480  00:00:00.460
  SubTask CYLDCHTrackFinderPR...... : 00:02:14.935  00:02:14.749  00:02:14.699
  SubTask DCHKalmanFilterGEN....... : 00:11:11.671  00:11:09.900  00:11:09.870
  SubTask DCHTrackReFit............ : 00:00:03.285  00:00:03.329  00:00:03.329
 Task DCHSPX....................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:58.885  00:00:58.909  00:00:58.909
  SubTask DCHSPXReFit.............. : 00:01:19.852  00:01:19.510  00:01:19.490
  SubTask SPXTracking.............. : 00:00:18.674  00:00:18.589  00:00:18.579
  SubTask DCHSPXTrackSelection..... : 00:00:00.066  00:00:00.029  00:00:00.019
 WriteEvent........................ : 00:00:06.020  00:00:05.789
analyzer........................... : 00:19:42.035  00:19:35.610  00:00:00.000
 DAQ bartender..................... : 00:00:58.128  00:00:55.270  00:00:55.040
 Task MEGMain...................... : 00:00:01.015  00:00:00.950  00:00:00.000
 Task ReadData..................... : 00:00:08.733  00:00:08.550  00:00:08.500
 Task SPX.......................... : 00:00:00.014  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.423  00:00:04.490  00:00:04.440
  SubTask SPXHitRec................ : 00:00:00.283  00:00:00.350  00:00:00.350
  SubTask SPXClustering............ : 00:00:07.010  00:00:06.769  00:00:06.739
  SubTask SPXIndependentTracking... : 00:00:28.337  00:00:28.410  00:00:28.400
 Task RDC.......................... : 00:00:00.013  00:00:00.029  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.073  00:00:00.060  00:00:00.050
  SubTask RDCHitRec................ : 00:00:00.036  00:00:00.019  00:00:00.019
 Task CYLDCH....................... : 00:00:00.010  00:00:00.019  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:26.928  00:01:26.499  00:01:26.499
  SubTask CYLDCHHitRec............. : 00:00:00.487  00:00:00.479  00:00:00.479
  SubTask CYLDCHTrackFinderPR...... : 00:02:10.779  00:02:10.899  00:02:10.899
  SubTask DCHKalmanFilterGEN....... : 00:11:17.891  00:11:16.150  00:11:16.130
  SubTask DCHTrackReFit............ : 00:00:03.185  00:00:03.190  00:00:03.170
 Task DCHSPX....................... : 00:00:00.011  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:03.932  00:01:03.649  00:01:03.639
  SubTask DCHSPXReFit.............. : 00:01:23.058  00:01:22.599  00:01:22.549
  SubTask SPXTracking.............. : 00:00:17.035  00:00:17.120  00:00:17.100
  SubTask DCHSPXTrackSelection..... : 00:00:00.065  00:00:00.049  00:00:00.019
 WriteEvent........................ : 00:00:06.022  00:00:06.060
```

```
./macros/analyzer_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 111
```
- - - -

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "111[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 2.6M Aug  4 05:06 histos11100.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Aug  4 05:07 histos11101.root
-rw-r--r--. 1 uchiyama unx-lke 221M Aug  4 05:06 rec11100.root
-rw-r--r--. 1 uchiyama unx-lke 221M Aug  4 05:07 rec11101.root

rec	115.226 kB/event
dch	104.828 kB/event
spx	6.031 kB/event
rdc	2.988 kB/event
reco	1.108 kB/event

```
######### execution time #########
7656.11/4000 = 1.91403 s/event
```
---------
analyzer........................... : 01:03:17.472  01:02:36.630  00:00:00.000
 DAQ bartender..................... : 00:03:50.080  00:03:19.419  00:03:18.509
 Task MEGMain...................... : 00:00:00.988  00:00:00.950  00:00:00.009
 Task ReadData..................... : 00:00:23.582  00:00:23.670  00:00:23.570
 Task SPX.......................... : 00:00:00.014  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.310  00:00:08.140  00:00:08.080
  SubTask SPXHitRec................ : 00:00:00.304  00:00:00.330  00:00:00.320
  SubTask SPXClustering............ : 00:00:11.651  00:00:11.710  00:00:11.669
  SubTask SPXIndependentTracking... : 00:00:44.862  00:00:44.969  00:00:44.959
 Task RDC.......................... : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:23.611  00:00:23.320  00:00:23.320
  SubTask RDCHitRec................ : 00:00:00.145  00:00:00.130  00:00:00.130
 Task CYLDCH....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:09:47.357  00:09:46.020  00:09:45.990
  SubTask CYLDCHHitRec............. : 00:00:12.992  00:00:12.900  00:00:12.890
  SubTask CYLDCHTrackFinderPR...... : 00:26:06.616  00:26:02.630  00:26:02.590
  SubTask DCHKalmanFilterGEN....... : 00:14:17.214  00:14:15.069  00:14:15.019
  SubTask DCHTrackReFit............ : 00:00:35.941  00:00:35.970  00:00:35.940
 Task DCHSPX....................... : 00:00:00.011  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:17.500  00:03:16.840  00:03:16.820
  SubTask DCHSPXReFit.............. : 00:02:22.117  00:02:21.839  00:02:21.829
  SubTask SPXTracking.............. : 00:00:29.250  00:00:29.000  00:00:28.960
  SubTask DCHSPXTrackSelection..... : 00:00:00.129  00:00:00.100  00:00:00.100
 WriteEvent........................ : 00:00:14.766  00:00:13.890
analyzer........................... : 01:04:18.635  01:03:36.670  00:00:00.000
 DAQ bartender..................... : 00:03:55.542  00:03:24.160  00:03:23.220
 Task MEGMain...................... : 00:00:00.994  00:00:00.929  00:00:00.000
 Task ReadData..................... : 00:00:24.601  00:00:24.519  00:00:24.459
 Task SPX.......................... : 00:00:00.014  00:00:00.010  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:08.259  00:00:08.210  00:00:08.120
  SubTask SPXHitRec................ : 00:00:00.306  00:00:00.259  00:00:00.259
  SubTask SPXClustering............ : 00:00:11.394  00:00:11.310  00:00:11.260
  SubTask SPXIndependentTracking... : 00:00:41.191  00:00:41.320  00:00:41.280
 Task RDC.......................... : 00:00:00.014  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:22.353  00:00:22.409  00:00:22.389
  SubTask RDCHitRec................ : 00:00:00.150  00:00:00.139  00:00:00.129
 Task CYLDCH....................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:09:51.235  00:09:49.709  00:09:49.709
  SubTask CYLDCHHitRec............. : 00:00:13.134  00:00:12.840  00:00:12.810
  SubTask CYLDCHTrackFinderPR...... : 00:26:07.958  00:26:04.170  00:26:04.150
  SubTask DCHKalmanFilterGEN....... : 00:14:50.525  00:14:48.309  00:14:48.289
  SubTask DCHTrackReFit............ : 00:00:41.495  00:00:41.410  00:00:41.400
 Task DCHSPX....................... : 00:00:00.012  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:27.083  00:03:26.530  00:03:26.500
  SubTask DCHSPXReFit.............. : 00:02:28.131  00:02:27.769  00:02:27.769
  SubTask SPXTracking.............. : 00:00:29.353  00:00:29.200  00:00:29.180
  SubTask DCHSPXTrackSelection..... : 00:00:00.130  00:00:00.170  00:00:00.150
 WriteEvent........................ : 00:00:14.357  00:00:13.839
```

```
./macros/analyzer_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```
- - - -

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 2.6M Aug  4 05:28 histos11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Aug  4 05:27 histos11601.root
-rw-r--r--. 1 uchiyama unx-lke 232M Aug  4 05:28 rec11600.root
-rw-r--r--. 1 uchiyama unx-lke 233M Aug  4 05:27 rec11601.root

rec	120.683 kB/event
dch	110.241 kB/event
spx	6.145 kB/event
rdc	2.971 kB/event
reco	1.052 kB/event

```
######### execution time #########
10094.2/4000 = 2.52356 s/event
```
---------
analyzer........................... : 01:24:51.500  01:24:04.229  00:00:00.000
 DAQ bartender..................... : 00:03:55.469  00:03:21.320  00:03:20.320
 Task MEGMain...................... : 00:00:01.024  00:00:00.989  00:00:00.000
 Task ReadData..................... : 00:00:25.571  00:00:25.520  00:00:25.480
 Task SPX.......................... : 00:00:00.015  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.349  00:00:08.410  00:00:08.350
  SubTask SPXHitRec................ : 00:00:00.312  00:00:00.360  00:00:00.360
  SubTask SPXClustering............ : 00:00:12.901  00:00:12.650  00:00:12.560
  SubTask SPXIndependentTracking... : 00:00:44.377  00:00:44.479  00:00:44.479
 Task RDC.......................... : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:23.027  00:00:22.979  00:00:22.969
  SubTask RDCHitRec................ : 00:00:00.143  00:00:00.169  00:00:00.119
 Task CYLDCH....................... : 00:00:00.010  00:00:00.020  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:13.219  00:10:11.499  00:10:11.469
  SubTask CYLDCHHitRec............. : 00:00:13.629  00:00:13.750  00:00:13.720
  SubTask CYLDCHTrackFinderPR...... : 00:47:08.902  00:47:01.629  00:47:01.579
  SubTask DCHKalmanFilterGEN....... : 00:14:11.017  00:14:08.820  00:14:08.810
  SubTask DCHTrackReFit............ : 00:00:22.108  00:00:22.039  00:00:22.029
 Task DCHSPX....................... : 00:00:00.011  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:46.112  00:03:45.789  00:03:45.789
  SubTask DCHSPXReFit.............. : 00:02:08.318  00:02:08.230  00:02:08.210
  SubTask SPXTracking.............. : 00:00:31.145  00:00:30.730  00:00:30.710
  SubTask DCHSPXTrackSelection..... : 00:00:00.139  00:00:00.129  00:00:00.129
 WriteEvent........................ : 00:00:15.230  00:00:14.949
analyzer........................... : 01:23:22.748  01:22:35.470  00:00:00.000
 DAQ bartender..................... : 00:03:55.016  00:03:21.499  00:03:20.499
 Task MEGMain...................... : 00:00:01.035  00:00:00.990  00:00:00.000
 Task ReadData..................... : 00:00:24.874  00:00:24.729  00:00:24.679
 Task SPX.......................... : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.149  00:00:08.279  00:00:08.239
  SubTask SPXHitRec................ : 00:00:00.305  00:00:00.290  00:00:00.290
  SubTask SPXClustering............ : 00:00:11.747  00:00:11.590  00:00:11.540
  SubTask SPXIndependentTracking... : 00:00:41.700  00:00:41.749  00:00:41.739
 Task RDC.......................... : 00:00:00.014  00:00:00.010  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:22.360  00:00:22.330  00:00:22.320
  SubTask RDCHitRec................ : 00:00:00.144  00:00:00.170  00:00:00.150
 Task CYLDCH....................... : 00:00:00.010  00:00:00.019  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:25.959  00:10:24.120  00:10:24.100
  SubTask CYLDCHHitRec............. : 00:00:13.862  00:00:13.899  00:00:13.879
  SubTask CYLDCHTrackFinderPR...... : 00:44:56.232  00:44:49.419  00:44:49.409
  SubTask DCHKalmanFilterGEN....... : 00:14:46.093  00:14:43.899  00:14:43.879
  SubTask DCHTrackReFit............ : 00:00:24.083  00:00:23.940  00:00:23.940
 Task DCHSPX....................... : 00:00:00.012  00:00:00.019  00:00:00.019
  SubTask DCHSPXMatching........... : 00:03:41.658  00:03:41.019  00:03:40.999
  SubTask DCHSPXReFit.............. : 00:02:13.604  00:02:13.220  00:02:13.200
  SubTask SPXTracking.............. : 00:00:29.841  00:00:29.489  00:00:29.459
  SubTask DCHSPXTrackSelection..... : 00:00:00.140  00:00:00.120  00:00:00.110
 WriteEvent........................ : 00:00:15.467  00:00:15.010
```
