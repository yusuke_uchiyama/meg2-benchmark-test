2020/08/03

# Bartender output with Spect_noMix.

* No mark (optimistic)
   * A signal positron event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Run number convention: 110**  
     10 gem4 runs (sev files) are summed up in a bartender run.  
     Originally 200*10 signal events are generated in gem4 but  
     only events satisfied gem4 event-selection are recorded.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
* realistic
   * Run number convention: 115** 

0ead0d99222233997499079e8ba75012cc7a03cb

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix.xml","/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830", 11000, 2, "")'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix_realistic.xml","/meg/data1/shared/mc/benchmark_test/20200800-root6.20.00-geant4.10.04.p03-gcc830", 11500, 2, "-p short")'
```

```
./macros/bartender_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix 110
./macros/bartender_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
```



### Data and Statistics ###


```
./macros/bartender_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix 110
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_noMix | grep -E "110[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 355M Aug  3 11:36 raw11000.root
-rw-r--r--. 1 uchiyama unx-lke 357M Aug  3 11:36 raw11001.root
-rw-r--r--. 1 uchiyama unx-lke 281M Aug  3 11:36 sim11000.root
-rw-r--r--. 1 uchiyama unx-lke 277M Aug  3 11:36 sim11001.root

sim	147.003 kB/event
track	76.158 kB/event
dch	64.748 kB/event
spx	2.65 kB/event
kine	1.521 kB/event

```
######### execution time #########
647.563/4000 = 0.161891 s/event
```
---------
analyzer........................... : 00:05:23.053  00:05:19.589  00:00:00.000
 DAQ gem4.......................... : 00:00:00.878  00:00:00.219  00:00:00.019
 Task MEGMain...................... : 00:00:00.085  00:00:00.079  00:00:00.049
 Task ReadGEM4..................... : 00:00:04.812  00:00:04.770  00:00:04.680
 Task COMMixEvents................. : 00:00:02.441  00:00:02.360  00:00:02.350
 Task CYLDCHMixEvents.............. : 00:00:03.788  00:00:03.359  00:00:03.349
 Task SPXMixEvents................. : 00:00:00.115  00:00:00.199  00:00:00.179
 Task RDCMixEvents................. : 00:00:00.016  00:00:00.009  00:00:00.009
 Task SPXDigitize.................. : 00:00:30.764  00:00:31.080  00:00:29.170
 Task CYLDCHDigitize............... : 00:03:41.952  00:03:41.149  00:03:41.119
 Task RDCDigitize.................. : 00:00:01.237  00:00:01.129  00:00:00.299
 WriteEvent........................ : 00:00:54.519  00:00:53.559
analyzer........................... : 00:05:24.510  00:05:20.409  00:00:00.000
 DAQ gem4.......................... : 00:00:00.887  00:00:00.229  00:00:00.000
 Task MEGMain...................... : 00:00:00.084  00:00:00.089  00:00:00.060
 Task ReadGEM4..................... : 00:00:04.802  00:00:04.719  00:00:04.649
 Task COMMixEvents................. : 00:00:02.432  00:00:02.479  00:00:02.459
 Task CYLDCHMixEvents.............. : 00:00:03.800  00:00:03.650  00:00:03.640
 Task SPXMixEvents................. : 00:00:00.112  00:00:00.100  00:00:00.100
 Task RDCMixEvents................. : 00:00:00.017  00:00:00.010  00:00:00.000
 Task SPXDigitize.................. : 00:00:29.986  00:00:29.820  00:00:27.900
 Task CYLDCHDigitize............... : 00:03:43.955  00:03:43.440  00:03:43.400
 Task RDCDigitize.................. : 00:00:01.234  00:00:01.190  00:00:00.370
 WriteEvent........................ : 00:00:54.558  00:00:53.009
```

```
./macros/bartender_output.sh 20200800-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_noMix_realistic | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 379M Aug  3 11:36 raw11500.root
-rw-r--r--. 1 uchiyama unx-lke 381M Aug  3 11:36 raw11501.root
-rw-r--r--. 1 uchiyama unx-lke 282M Aug  3 11:36 sim11500.root
-rw-r--r--. 1 uchiyama unx-lke 278M Aug  3 11:36 sim11501.root

sim     147.003 kB/event
track   76.158 kB/event
dch     64.748 kB/event
spx     2.65 kB/event
kine    1.521 kB/event

```
######### execution time #########
543.913/4000 = 0.135978 s/event
```
---------
analyzer........................... : 00:04:32.796  00:04:28.470  00:00:00.000
 DAQ gem4.......................... : 00:00:00.884  00:00:00.190  00:00:00.000
 Task MEGMain...................... : 00:00:00.083  00:00:00.089  00:00:00.079
 Task ReadGEM4..................... : 00:00:05.117  00:00:04.410  00:00:04.340
 Task COMMixEvents................. : 00:00:02.430  00:00:02.550  00:00:02.540
 Task CYLDCHMixEvents.............. : 00:00:03.789  00:00:03.740  00:00:03.740
 Task SPXMixEvents................. : 00:00:00.115  00:00:00.100  00:00:00.100
 Task RDCMixEvents................. : 00:00:00.017  00:00:00.020  00:00:00.010
 Task SPXDigitize.................. : 00:00:30.223  00:00:30.150  00:00:28.260
 Task CYLDCHDigitize............... : 00:02:49.655  00:02:49.320  00:02:49.270
 Task RDCDigitize.................. : 00:00:01.229  00:00:01.160  00:00:00.350
 WriteEvent........................ : 00:00:56.759  00:00:55.009
analyzer........................... : 00:04:31.117  00:04:27.490  00:00:00.000
 DAQ gem4.......................... : 00:00:00.886  00:00:00.190  00:00:00.000
 Task MEGMain...................... : 00:00:00.082  00:00:00.059  00:00:00.049
 Task ReadGEM4..................... : 00:00:05.043  00:00:04.599  00:00:04.489
 Task COMMixEvents................. : 00:00:02.431  00:00:02.370  00:00:02.370
 Task CYLDCHMixEvents.............. : 00:00:03.830  00:00:03.850  00:00:03.850
 Task SPXMixEvents................. : 00:00:00.112  00:00:00.119  00:00:00.099
 Task RDCMixEvents................. : 00:00:00.017  00:00:00.019  00:00:00.009
 Task SPXDigitize.................. : 00:00:29.894  00:00:29.839  00:00:27.939
 Task CYLDCHDigitize............... : 00:02:50.008  00:02:49.940  00:02:49.910
 Task RDCDigitize.................. : 00:00:01.237  00:00:01.150  00:00:00.330
 WriteEvent........................ : 00:00:54.875  00:00:53.440
```
      