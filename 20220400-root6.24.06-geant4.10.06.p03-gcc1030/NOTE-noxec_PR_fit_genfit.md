2022/04/26

# Analyzer output with noxec_PR_fit_genfit.xml
* No XEC reconstruction
* SPX analysis from WaveformAnalysis (cf30)
* CDC analysis from waveform (WaveformAnalysis2), pattern recognition and fit with GENFIT
* Custom run number 2000145


## Run Numbers ##


## Submit jobs ##
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix","/meg/data1/shared/mc/benchmark_test/20220400-root6.24.06-geant4.10.06.p03-gcc1030/", 11000, 5, "", "-rc 2000145")'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_withMichel3e7","/meg/data1/shared/mc/benchmark_test/20220400-root6.24.06-geant4.10.06.p03-gcc1030/", 11100, 5, "", "-rc 2000145")'
```



## Calculate positron efficiency ##
### No mix ###	 

```
RUNNUM=11000;NRUN=5;DATASET=20220400-root6.24.06-geant4.10.06.p03-gcc1030;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-6.2718e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```

#### Old config ####
```
@-----------------------------@
|           Summary
|   All events:
|  2000
|   |
|   | LXe acceptance cut
|   V
|  6655 (100 %)
|   |
|   | SPX acceptance cut
|   V
|  5917 (88.9106 %)
|   |
|   | DCH tracking
|   |  including contamination cut
|   |  (FakeThre:0.6)
|   V
|  5622 (84.4778 %)
|   |
|   | propagation cut
|   V
|  5405 (81.2171 %)
|   |
|   | tail cut with
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V
|  4843 (72.7724 %)
|   |
|   | matching
|   V
|  4741 (71.2397 %)
|   |
|   | SPX cluster contamination cut
|   |  (FakeThre:0.6)
|   V
|  4741 (71.2397 %)
|   |
|   | time cut
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V
|  4636 (69.6619 %)
|      + 0.568012
|      - 0.573999
@-----------------------------@
|           Resolutions
|  theta   (mrad)  7.37509 +- 0.0541189
|  phi     (mrad)  7.00112 +- 0.0566338
|  momentum (MeV)  0.102351 +- 0.00086474
|  vertexZ   (mm)  1.92638 +- 0.0158152
|  vertexY   (mm)  0.817507 +- 0.00672305
|  time     (sec)  4.6388e-11 +- 3.9092e-13
@-----------------------------@
```

#### New config ####
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6655 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  5917 (88.9106 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5690 (85.4996 %)
|   |                         
|   | propagation cut         
|   V                         
|  5472 (82.2239 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  4968 (74.6506 %)
|   |                         
|   | matching                
|   V                         
|  4871 (73.1931 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  4871 (73.1931 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  4766 (71.6153 %)
|      + 0.556861
|      - 0.563445
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  7.28568 +- 0.0523541
|  phi     (mrad)  6.90147 +- 0.0535593
|  momentum (MeV)  0.102784 +- 0.000840836
|  vertexZ   (mm)  1.89954 +- 0.0147273
|  vertexY   (mm)  0.820928 +- 0.0062559
|  time     (sec)  4.51105e-11 +- 3.57965e-13
@-----------------------------@
```


### 3e7 ###	 

```
RUNNUM=11100;NRUN=5;DATASET=20220400-root6.24.06-geant4.10.06.p03-gcc1030
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-6.2718e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_withMichel3e7|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
```

```
RUNNUM=11200;NRUN=5;DATASET=20220400-root6.24.06-geant4.10.06.p03-gcc1030
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-6.2718e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_withMubeam3e7|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
```

### 5e7 ###	 

```
RUNNUM=11500;NRUN=5;DATASET=20220400-root6.24.06-geant4.10.06.p03-gcc1030
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-6.2718e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_withMichel5e7|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
```
```
RUNNUM=11600;NRUN=5;DATASET=20220400-root6.24.06-geant4.10.06.p03-gcc1030
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-6.2718e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_withMubeam5e7|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
```


### Analyze execution and output ###
```
cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20220400-root6.24.06-geant4.10.06.p03-gcc1030/noxec_PR_fit_genfit 110

```
