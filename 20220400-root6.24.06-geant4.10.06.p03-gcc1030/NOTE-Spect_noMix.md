2022/04/26

# Bartender output with Spect_noMix.

* A event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
* Custom run number 2000144
* Apply PPDTimeOffset in SPXDigitize (86/106 ps)
* Z-dependent gas gain
* Space-charge effect is omitted in this version.
* CDCH coherent noise from spectrum measured in 2021. Zero-suppression is disabled, digitized with 2 rebin.
* SignalE
   * Run number convention: 110**  
     10 gem4 runs (sev files) are summed up in a bartender run.  
     Originally 200*10 signal events are generated in gem4.

e1c071155cfa80d6d1b3ce17f11ffbd991812199

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix.xml","/meg/data1/shared/mc/benchmark_test/20220400-root6.24.06-geant4.10.06.p03-gcc1030", 11000, 5, "-p short")'
```

```
./macros/bartender_output.sh 20220400-root6.24.06-geant4.10.06.p03-gcc1030/Spect_noMix 110
```

```
./macros/bartender_output.sh 20220400-root6.24.06-geant4.10.06.p03-gcc1030/Spect_noMix 110
```
- - - -

9800  events

######### data size #########
```
ls -lh Spect_noMix | grep -E "110[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 3.2G Apr 26 16:55 raw11000.root
-rw-r--r--. 1 uchiyama unx-lke 3.2G Apr 26 16:56 raw11001.root
-rw-r--r--. 1 uchiyama unx-lke 3.2G Apr 26 16:56 raw11002.root
-rw-r--r--. 1 uchiyama unx-lke 3.2G Apr 26 16:56 raw11003.root
-rw-r--r--. 1 uchiyama unx-lke 2.9G Apr 26 16:53 raw11004.root
-rw-r--r--. 1 uchiyama unx-lke 268M Apr 26 16:55 sim11000.root
-rw-r--r--. 1 uchiyama unx-lke 275M Apr 26 16:56 sim11001.root
-rw-r--r--. 1 uchiyama unx-lke 277M Apr 26 16:56 sim11002.root
-rw-r--r--. 1 uchiyama unx-lke 272M Apr 26 16:56 sim11003.root
-rw-r--r--. 1 uchiyama unx-lke 248M Apr 26 16:53 sim11004.root

sim	139.642 kB/event
track	76.798 kB/event
dch	58.965 kB/event
spx	2.106 kB/event
kine	0.994 kB/event

```
######### execution time #########
8152.11/9800 = 0.831848 s/event
```
---------
analyzer........................... : 00:27:25.743  00:27:11.990  00:00:00.000
 DAQ gem4.......................... : 00:00:00.457  00:00:00.189  00:00:00.009
 Task MEGMain...................... : 00:00:00.143  00:00:00.199  00:00:00.149
 Task ReadGEM4..................... : 00:00:06.861  00:00:06.109  00:00:06.039
 Task COMMixEvents................. : 00:00:03.221  00:00:03.300  00:00:03.280
 Task CYLDCHMixEvents.............. : 00:00:05.740  00:00:05.730  00:00:05.710
 Task SPXMixEvents................. : 00:00:00.160  00:00:00.140  00:00:00.110
 Task RDCMixEvents................. : 00:00:00.024  00:00:00.029  00:00:00.029
 Task SPXDigitize.................. : 00:00:35.614  00:00:35.309  00:00:33.199
 Task CYLDCHDigitize............... : 00:23:30.795  00:23:27.659  00:23:27.599
 Task RDCDigitize.................. : 00:00:01.420  00:00:01.380  00:00:00.470
 WriteEvent........................ : 00:02:53.032  00:02:48.139
analyzer........................... : 00:27:50.685  00:27:35.429  00:00:00.000
 DAQ gem4.......................... : 00:00:00.575  00:00:00.209  00:00:00.000
 Task MEGMain...................... : 00:00:00.146  00:00:00.119  00:00:00.099
 Task ReadGEM4..................... : 00:00:07.070  00:00:06.420  00:00:06.329
 Task COMMixEvents................. : 00:00:03.263  00:00:03.290  00:00:03.270
 Task CYLDCHMixEvents.............. : 00:00:05.947  00:00:05.859  00:00:05.859
 Task SPXMixEvents................. : 00:00:00.160  00:00:00.150  00:00:00.130
 Task RDCMixEvents................. : 00:00:00.024  00:00:00.019  00:00:00.009
 Task SPXDigitize.................. : 00:00:34.798  00:00:34.909  00:00:32.889
 Task CYLDCHDigitize............... : 00:23:55.076  00:23:51.810  00:23:51.760
 Task RDCDigitize.................. : 00:00:01.424  00:00:01.350  00:00:00.440
 WriteEvent........................ : 00:02:55.148  00:02:48.709
analyzer........................... : 00:27:45.656  00:27:30.970  00:00:00.000
 DAQ gem4.......................... : 00:00:00.575  00:00:00.219  00:00:00.000
 Task MEGMain...................... : 00:00:00.146  00:00:00.190  00:00:00.150
 Task ReadGEM4..................... : 00:00:07.105  00:00:05.820  00:00:05.740
 Task COMMixEvents................. : 00:00:03.276  00:00:03.679  00:00:03.629
 Task CYLDCHMixEvents.............. : 00:00:05.925  00:00:05.900  00:00:05.890
 Task SPXMixEvents................. : 00:00:00.159  00:00:00.109  00:00:00.109
 Task RDCMixEvents................. : 00:00:00.025  00:00:00.019  00:00:00.019
 Task SPXDigitize.................. : 00:00:34.808  00:00:34.629  00:00:32.569
 Task CYLDCHDigitize............... : 00:23:49.426  00:23:46.260  00:23:46.190
 Task RDCDigitize.................. : 00:00:01.393  00:00:01.380  00:00:00.470
 WriteEvent........................ : 00:02:54.547  00:02:49.109
analyzer........................... : 00:27:47.186  00:27:32.039  00:00:00.000
 DAQ gem4.......................... : 00:00:00.595  00:00:00.189  00:00:00.000
 Task MEGMain...................... : 00:00:00.144  00:00:00.070  00:00:00.060
 Task ReadGEM4..................... : 00:00:07.049  00:00:06.660  00:00:06.590
 Task COMMixEvents................. : 00:00:03.231  00:00:03.129  00:00:03.109
 Task CYLDCHMixEvents.............. : 00:00:05.858  00:00:06.109  00:00:06.089
 Task SPXMixEvents................. : 00:00:00.158  00:00:00.079  00:00:00.060
 Task RDCMixEvents................. : 00:00:00.024  00:00:00.000  00:00:00.000
 Task SPXDigitize.................. : 00:00:34.760  00:00:34.589  00:00:32.539
 Task CYLDCHDigitize............... : 00:23:51.563  00:23:47.970  00:23:47.920
 Task RDCDigitize.................. : 00:00:01.407  00:00:01.410  00:00:00.490
 WriteEvent........................ : 00:02:55.312  00:02:49.409
analyzer........................... : 00:25:02.837  00:24:49.069  00:00:00.000
 DAQ gem4.......................... : 00:00:00.543  00:00:00.189  00:00:00.000
 Task MEGMain...................... : 00:00:00.130  00:00:00.089  00:00:00.059
 Task ReadGEM4..................... : 00:00:06.279  00:00:05.550  00:00:05.470
 Task COMMixEvents................. : 00:00:02.901  00:00:02.840  00:00:02.830
 Task CYLDCHMixEvents.............. : 00:00:05.313  00:00:05.460  00:00:05.450
 Task SPXMixEvents................. : 00:00:00.143  00:00:00.100  00:00:00.100
 Task RDCMixEvents................. : 00:00:00.023  00:00:00.029  00:00:00.009
 Task SPXDigitize.................. : 00:00:31.787  00:00:31.569  00:00:29.539
 Task CYLDCHDigitize............... : 00:21:29.338  00:21:26.230  00:21:26.130
 Task RDCDigitize.................. : 00:00:01.394  00:00:01.319  00:00:00.399
 WriteEvent........................ : 00:02:37.308  00:02:32.119
```
