2022/04/26

# Spectrometer (-d XEC).
- Photon tracking in SPX.
- 9 layer CDCH
- Flat target with holes and carbon-fiber frame
- Custom run number 2000144

# Signal positron generated on target
- in the 'extended' range
- No event selection in gem4
- Number of events (generation) in a run: 200

e1c071155cfa80d6d1b3ce17f11ffbd991812199

## LOG ##
```
 root -q 'macros/benchmark/JobSubmit.C("macros/benchmark/Spect_signalE.mac", "-d xec", "/meg/data1/shared/mc/benchmark_test/20220400-root6.24.06-geant4.10.06.p03-gcc1030/gem4Out/", 200, 11000, 50, "-p short")'
```

#11000-11049
200 events * 50 run = 10000 events generation,

```
$ du -cm gem4Out/sev11*
```
```
$ grep 'User=' gem4Out/log/g110* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=10000} END{print sum "/" n " = " sum/n "s/event"}'
```
```
megbartender [1] sev->Print("toponly")
```