[TOC]

# General description 

## Directory structure
>**/meg/data1/shared/mc/benchmark_test/**
>>**meg2db/** --- sqlite3 database (common)  
>>**meg2-xx-xx/** --- software directories (for different environments)  
>>**rome3-xx-xx/** --- corresponding ROME directories  
>>**20yymmnn-xx-xx/** --- directory for the data  
>>>**gem4Out/** --- output files from gem4 (sev files).  
>>>**Full(Spect)_bb/** -- output files from bartender (sim and raw files). 'Full' is for the full detector configuration,
'Spect' is for the e+ spectrometer only configuration (w/o LXe). 'bb' represents the bartender mixing mode.  
>>>**(no)xec_PR_fit_genfit/** -- output files from analyzer (rec and histos files). The directory is named from the used configuration file.  
>>>>**log/** -- the SLURM log files  
>>>>**config(macros)/** -- the input configuration files

>>>**NOTE-xx** -- notes for the data production. They contain the detailed configuration, the data size, the execution time, etc.  

## Run number convention
The following is the run number convention used (only) in the benchmark test.

* gem4  
*exxx*: The first 1 or 2 digits represent the generation event type, and the last 3 digits for the serial run number.  
eg. 12009 is 9th run of Michel positron generation.

* bartender/analyzer  
*emxx*: The first 1 or 2 digits represent the gem4 event type for the **main** subevent, the next digit for the mixing type (see below), and the last 2 digits for the serial run number. Note that 10 runs (files) of gem4 output are gathered in a bartender output.  
*m* (mixing type): 0 for no mixing, 1 with Michel, 3 with muon beam.  
eg. 11000 is 0th run of signal e+ (event type 11) only. 11100 is 0th run of signal e+ with Michel e+ mixed.  

## Data production
### Normal production
Usually 2k signal events (either event type 1 with Full or 11 with Spect)
and 100k Michel events (event type 12 with Full) are generated for the standard bench-mark test
when some relevant updates are committed in the repository.  
The generation is triggered by YU or by users on demand.
If you want a new generation with latest version of software, please tell Yusuke.

### Mass production
On demand, mass production can be done up to 200k signal events.
If you need a mass production, please send a request to Yusuke.
The mass production data are desireble to be shared by many users, not for a personal study.


## Performance evaluation 
The performance of e+ analysis is evaluated by using the standard macro,
currently `analyzer/macros/positron/DCHSPXSignalPositronEfficiency.cpp`.
The results are written in NOTE-noxec_PR_fit_genfit.


## How to use the data
* Use the bartender output files as the input of your analyzer
```
cd $MEG2SYS/analyzer
./meganalyzer -i example/benchmark/noxec_PR_fit_genfit.xml -r 11100 -pi /meg/data1/shared/mc/benchmark_test/20180502-root6.12.06-geant4.10.04.p00-gcc640/Spect_signalEWithMichel7e7 -po .
```

---------------


# Short descriptions for datasets


### processes/20211223
Nominal configuration as of Dec. 2021, basically close to 2021 data configuration.

- Signal e+ no mix (#1100x)
- Cosmic mu no mix (#7300x)
- Signal e+ with 3e7 Michel e+ (#1110x)
- Signal e+ with 5e7 Michel e+ (#1150x)
- Michel e+ with 3e7 Michel e+ (#121xx)
- Michel e+ with 5e7 Michel e+ (#125xx)
- Signal e+ with 3e7 mubeam (#1120x)
- Signal e+ with 5e7 mubeam (#1160x)
- Muon with 3e7 mubeam (#302xx)
- Muon with 5e7 mubeam (#306xx)


### 20220400
Many developments for data.  
DRS channel assignment same as data (custom runnumber 2000144/2000145).  
CDCH gas gain adjustement to 2.3e5 (by D. Palo)  
CDCH analysis parameters updated to be similar to those for data (by D. Palo)  

- gem4
    - (Full_michel)
    - Spect_signalE
- bartender
    - Spect_noMix

### processes/20211223
Nominal configuration as of Dec. 2021, basically close to 2021 data configuration.

- Signal e+ no mix (#1100x)
- Cosmic mu no mix (#7300x)
- Signal e+ with 3e7 Michel e+ (#1110x)
- Signal e+ with 5e7 Michel e+ (#1150x)
- Michel e+ with 3e7 Michel e+ (#121xx)
- Michel e+ with 5e7 Michel e+ (#125xx)
- Signal e+ with 3e7 mubeam (#1120x)
- Signale+  with 5e7 mubeam (#1160x)
- Muon with 3e7 mubeam (#302xx)
- Muon with 5e7 mubeam (#306xx)


### 20211200
Many developments for data.  
Test with different sets of CDCH bad channels (only for standard wire).  

- gem4
    - Link to 20211000
- bartender
    - Link to 20211000

### 20211000
Many developments for data.  
Test with CDCH2 (but still with 9 layers) (by C. Voena)  

- gem4 
    - Full_michel
        - Standard: 12000-12049
	- 50 um: 12100-12149
	- 50 um, new coating: 12200-12249
    - Spect_signalE
        - Standard: 11000-11049
	- 50 um: 11100-11149
	- 50 um, new coating: 11200-11249
- bartender
    - Spect_noMix_realistic
        - Standard: #11500-11504
	- 50 um: 11510-11514
	- 50 um, new coating: 11520-11524
    - Spect_signalEWithMichel7e7_realistic
        - Standard: #11600-11604
	- 50 um: 11610-11614
	- 50 um, new coating: 11620-11624


### 20210101
CDCH signal shape improvement with faster rise time (by F. Renga)  
Test thicker cathode wires (by C. Voena)  

- gem4
    - link to 20210100
- bartender
    - Spect_noMix_realistic
        - (0%: #11500-11501, 0.5%: 11502-11503, 1%: 11504-11505, 2%: 11506-11507)
	- (40/50 um: 11500-11501,11522-11524, 50 um: 11510-11511,11532-11534, 60 um: 11512-11513,11542-11544)
    - Spect_signalEWithMichel7e7_realistic
        - (0%: #11600-11601, 0.5%: 11602-11603, 1%: 11604-11605, 2%: 11606-11607)
	- (40/50 um: 11600-11601,11622-11624, 50 um: 11610-11611,11632-11634, 60 um: 11612-11613,11642-11644)

### 20210100
Electron attachment by O2 (by F. Renga)  

- gem4 
    - Full_michel  (O2 fraction 0%: 12000-12019, 0.5%: 12020-12039, 1%: 12040-12059, 2%; 12060-12079)
    - Spect_signalE
- bartender
    - Spect_noMix_realistic  (0%: #11500-11501, 0.5%: 11502-11503, 1%: 11504-11505, 2%: 11506-11507)
    - Spect_signalEWithMichel7e7_realistic (0%: #11600-11601, 0.5%: 11602-11603, 1%: 11604-11605, 2%: 11606-11607)


### 20201201
Use measured noise spectrum (standard gain FE) for CDCH noise simulation in realistic condition (by F. Renga)  

- gem4 
    - link to 20201200
- bartender
    - Spect_noMix_realistic      #11500-11501
    - Spect_signalEWithMichel7e7_realistic #11600-11601


### 20201200
CDCH ionization cluster size PolyaTheta updated with GARFIELD (by F. Renga and G. Tassielli)  
CDCH diffusion is implemented electron-by-electron (by F. Renga)  
CDCH signal response (pulse shape) from laser measurement (by F. Renga and R. Antonietti)  
CDCH space-charge effect is temporary omitted.  

- gem4 
    - Full_michel
    - Spect_signalE
- bartender
    - Spect_noMix_realistic      #11500-11501
    - Spect_signalEWithMichel7e7_realistic #11600-11601


### 20200800
Fix a bug in CYLDCHGASD::SpecificIonizationPositron (by F. Renga)  
Information of positron entrance points to CDCH is added (by F. Renga)  
Z-dependent gain of CDCH is implemented (by F. Renga)  
DCHTrackRefit added (by. D. Palo)  
DCHSPXTrackSelection added (by. YU)  
CYLDCHWaveformAnalysis2 is used instead of CYLDCHWaveformAnalysis.  

- gem4 
    - Full_michel
    - Spect_signalE
- bartender
    - Spect_noMix                #11000-11001
    - Spect_signalEWithMichel7e7 #11100-11101
    - Spect_noMix_realistic      #11500-11501
    - Spect_signalEWithMichel7e7_realistic #11600-11601


### 20200600
Further code cleaup: units, time difference, etc.  
Run bartender with CDCH-realistic configuration, too.  

- gem4 
    - Full_michel
    - Spect_signalE
- bartender
    - Spect_noMix                #11000-11001
    - Spect_signalEWithMichel7e7 #11100-11101
    - Spect_noMix_realistic      #11500-11501
    - Spect_signalEWithMichel7e7_realistic #11600-11601



### 20200500
Code cleanup: namespace, descriptions, coding style, warning suppression, etc.   
Unify time units for CDCH (by YU)  
Always trg 30.
Add cluseter size info in CYLDCHMCHit. (by YU)  
Activate PPD time offset in SPX waveform simulation. (by K. Yanai)  
Fix bug in nturn calculation in bartender. (by F. Ignatov)  
DCHTrackRefit task is added (not activated). (by D. Palo)  

- gem4 
    - Spect_signalE
- bartender
    - Spect_noMix                #11000



### 20200300
DCH gas mixture changed to He:iso-butane=90:10 (it was 85:15) (by F. Renga)  
-> Decrease in the number of ionization clusters -> resulting in reduction of data size.  
DCH waveform analysis updated (by C. Voena)  
With new software versions 2020a  
Custom run number 2000116  

- gem4 
    - Full_michel
    - Spect_signalE
- bartender
    - Spect_noMix                #11000
    - Spect_signalEWithMichel7e7 #11100


### 20191203  
Test deformed target after fixing target T0 calculation (by YU)  
with updated DCH simulation and custom run number 2000117  

- gem4
    - link to 20191201

### 20191202 
Update DCH simulation (by F. Renga & C. Voena)  

- gem4  
    - link to 20191200  


### 20191201
Test deformed target (by D. Palo)  
Custom run number 2000117  
The other setting is the same as 20191200  

### 20191200
New implementation of target in gem4 & analyzer. Use flat target (by D. Palo)  
Update DCH track finding (by F. Ignatov)  
Channel-by-channel constant fraction on SPX (all 30% for MC) (by K. Yanai)  
Custom run number 2000116  


### 20191000
Custom run number 2000116  

- gem4  
    - Full_michel  
    - Spect_signalE  


### 20190200 (@JINR)
Test on the JINR cluster  
Test with ROOT6.16, GenFit in GitHub, Geant4.10.04.p02  
'sensitive' command added in gem4 for each module (by R. Onda)  
Database access in CYLDCHModule was modified (speed up) (by YU)  
Test on the new computing nodes  

- gem4 
    - Full_michel
    - Spect_signalE
- bartender
    - Spect_noMix                #11000
    - Spect_signalEWithMichel7e7 #11100


### 20181202
Activate DCHSPXRefit task (by M. Usami)  
Activate SPXIndependentTracking (by M. Usami)  
Test on the new computing nodes  

- gem4 
    - Full_michel
    - Spect_signalE
- bartender
    - Spect_noMix                #11000-11001
    - Spect_signalEWithMichel7e7 #11100-11101


### 20181201  
Test compression  
Update DCHSPXMatching (TimeCut 15 ns) & DCHSPXTracking (activate DAFForTime, UseEstimatedV, RecalculateTOF)  

- gem4  
    - link to 20180804  
- bartender  
    - Apply compression 3 for raw and sim   

### 20181200  
Use time-diff for z in PatRec (by F. Ignatov)  
TOF recalculation (by M. Usami)  
Refit task implemented (by default turned off) (by M. Usami)  
TC Radial hit position estimation (by YU)   
6e1c0c73e887e244fce684b4af9e93c1bdf10ef1

- gem4  
- bartender  
    - link to 20180804  


### 20180804
Same version as 20180803.  
Regeneration of MC data.  
Custom run number **#2000106** (CDCH 9 layers). From now on, this is the default configuration unless specified.  
Reconstruction of hit z in CYLDCHTrackFinderPR: charge-division -> time-difference. (noxec_PR_fit_genfit_chargediv & noxec_PR_fit_genfit_timediff)  

- gem4 
    - Full_michel
    - Spect_signalE
- bartender
    - Spect_noMix                #11000
    - Spect_signalEWithMichel7e7 #11100


### 20180803 
Same  version as 20180802.  
Custom run number **#2000106** (CDCH 9 layers)  

- gem4
- bartender
    - link to 20180801


### 20180802 
Improve removal of duplicated tracks (by F. Ignatov)  
Fix leakage of DCH hits to inactive wires. (by F. Ignatov)  
Change name of SPX hit local coordinate (by YU)  
Custom run number **#2000105** (CDCH 10 layers)  

- gem4
- bartender
    - link to 20180800

### 20180801
Same as 20180800 but use run number **#2000106 (CDCH 9 layers)**.  

### 20180800
Improvement in SPXClustering, DCHSPXMatching, and SPXTracking (by YU)  
Implementation of RDC bartender and the RDC database updated, turn on RDC tasks in analyzer (by R. Onda)  
Update of XEC pileup analysis (by S. Ogawa)  
Update of XEC position rec (by S. Kobayashi) 

- gem4 
    - Full_michel
    - Spect_signalE
- bartender
    - Spect_noMix                #11000
    - Spect_signalEWithMichel7e7 #11100


### 20180602
Some bug fix in CDCH simulation and reconstruction (by F. Ignatov)  
Some improvement in CDCH wf analysis (by C. Voena)  
Improve T0 seeding based on CDCH (by F. Ignatov)  
Additional track seeding is implemented but by default off (by F. Ignatov)  
Trigger jitter is implemented in bartender (by YU) -> try trigger 31 & 30.  
The bartender output dir name -> _trg31 & _trg30  

- gem4
    - Full_signal
    - Full_michel
    - Spect_signalE
- bartender
    - Full_noMix (signalOnly)    #01000
    - Full_signalWithMichel7e7   #01100
    - Spect_noMix                #11000 
    - Spect_signalEWithMichel7e7 #11100

### 20180601 
Modify DCHSPXMatching: multiple StateSPXs (by YU)  

### 20180600 
Reorganization of RDC simulation codes (by R. Onda)  
Fix bug of missing all CDCH wires (since Apr. 25 c503ed3) (by F. Ignatov)  

### 20180502 
Modify DCHSPXMatching: add distance cut. (by M. Nishimura)

### 20180501   
FGAS volume was changed from inside of COBRA volume to inside of CDCH inner foil (by YU)  
Activate DCHSPXMatching and SPXTracking tasks. (by M. Nishimura) 

### 20180500 
Database and RunHeader structure change for CYLDCH codes (by F. Renga)  
DCH double turn analysis improvement (by P. Schwendiman)  
Target modules treatment was modified in gem4 (should be no difference) (by YU)  
a34fae9  
  
### 20180300 
GCC640, ROOT6.12.06, Geant4.10.04.p00.  
20k signal e+, 1M Michel generated with  
fa9346f99cec6e2eb4c47f5d9c8e9a440bd03460  
Analyze it with noxec_PR_fit_genfit (both with spx MCHitRec and HitRec).  
  
### 20180100 
Test GCC540 with Geant4.10.00.p04.bugfix (lots of warning in gem4 compilation)  
Test Geant4.10.03.p03 with GCC540  
Test Geant4.10.04 (no patch) with GCC540 (EXPAT is not used from 10.04)  
Test ROOT6.12.04 with GCC540  
  
Fix bug in GEMSteppingAction (by K. Ieki) adapted for Full_mubeam, not for Full_signal.  
TAR material can be selectable (MEG1 or scintillatar) (by M. Nishimura)  
CYLDCH wire resisitivity and amp imput impedance are updated (by C. Voena)  
CYLDCH run headers are separated for gem4 (CYLDCHMCRunHeader) (by C. Voena and F. Renga)  
Many analyzer update for data RUN2017.  
  
### 20171100 
Test Geant4.10.03.p03  
  
update of SPX waveformsimulation (TTS 70 ps FWHM -> 70 ps sigma)  
  
### 20171000 
Start to use the new offline cluster.  
Compare computation speed with and without hyper-threading.  
  
Improved CDCH waveform analysis and hit reconstruction (by C. Voena).  
(Set WireResistivity to 2075 by hand.)  
Small improvement in DCHSPX matching (M. Nishimura).  
  
### 20170800 
Number of channels in a DRS chip changed (8 -> 9) for data (clock channel).  
RDC (only DS) is now enabled by default in gem4.  
Aluminum layer is added in CDC outer frame (by M. Chiappini).  
  
### 20170500 
CDC database changed (TESTCYLDCHAnod) (by M. Chiappini).  
Small update of SPX waveformsimulation (by M. Usami).  
XEC database changed (XECCabling2) (by S. Ogawa).  
US RDC disabled in gem4 (by K. Ieki)  
  
### 20170400 
Update spx waveform simulation (by M. Usami).  
  
  
### 20170300 
CDC simulation (ionization at low energy) updated in gem4 (by F. Renga).  
gem4 doesn't write sim file  
Random seed for event mixture is separated from detector simulation. Random seed is set with the runnumber.  
Analyzer SPX waveform and write spxppd but not used for SPXHit.  
  
Git version bc37456  
  
  
### 20170202 
CDC database access changed (using MySQL) (by M. Chiappini).  
CDC analyzer folder cleanup.  
RecData cleanup.  
  
Re-analyze bartender outputs in 20170201  
Git version 945b2ef  
  
### 20170201 
* ver2develop  
Remove requiring SPX hits in gem4 to fill event in Spect_signalE and Full_signal.  
Use SPX time resolution from DB for SPXMCHitRec task and remove tail component.  
  
20k signal e+, 1M Michel generated with  
Git version e4ba58d2f722eb6b52610f5a2e46088a2ce222fa  
  
### 20170200 
* ver2develop  
CDC simulation (ionization cluster formation) updated in gem4 (by F. Renga).  
CDC simulation (active wire and geometory) updated (by M. Chiappini).  
Memory leak (large memory usage) in MCTrackEvent fixed in analyzer.  
SPX waveform simulation is modified (with negative polarity)  
  
  
### 20170101 
* ver2develop  
XEC online waveform clustering was activated in bartender.  
  
### 20170100 
As of Jan. 2017.  
Mixing window is updated to +-350 ns.  
Angular range of signal event generation is changed to the extended region.  
  
* ver2develop  
CDC simulation output strucutre was renewed.  
MCTrackEvent structure was changed.  
  
  
### 2016Nov 
Version before starting ver2develop.  
Mixing window +-250 ns  
