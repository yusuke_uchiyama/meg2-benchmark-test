2020/06/02

# Bartender output with Spect_noMix.

* No mark (optimistic)
   * A signal positron event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Run number convention: 110**  
     10 gem4 runs (sev files) are summed up in a bartender run.  
     Originally 200*10 signal events are generated in gem4 but  
     only events satisfied gem4 event-selection are recorded.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
* realistic
   * Run number convention: 115** 

3eab0097251521438d05c5787f9fc8bb3ca08efc

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix.xml","/meg/data1/shared/mc/benchmark_test/20200600-root6.20.00-geant4.10.04.p03-gcc830", 11000, 2, "")'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix_realistic.xml","/meg/data1/shared/mc/benchmark_test/20200600-root6.20.00-geant4.10.04.p03-gcc830", 11500, 2, "-p short")'
```

```
./macros/bartender_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix 110
```



### Data and Statistics ###

./macros/bartender_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix 110


4000  events

######### data size #########
ls -lh Spect_noMix | grep -E "110[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 353M Jun  2 08:32 raw11000.root
-rw-r--r--. 1 uchiyama unx-lke 357M Jun  2 08:31 raw11001.root
-rw-r--r--. 1 uchiyama unx-lke 229M Jun  2 08:32 sim11000.root
-rw-r--r--. 1 uchiyama unx-lke 225M Jun  2 08:31 sim11001.root

sim	119.306 kB/event
track	75.509 kB/event
dch	40.339 kB/event
spx	1.938 kB/event
kine	0.913 kB/event

######### execution time #########
570.735/4000 = 0.142684 s/event
---------
analyzer........................... : 00:05:19.192  00:05:15.539  00:00:00.000
 DAQ gem4.......................... : 00:00:00.834  00:00:00.199  00:00:00.000
 Task MEGMain...................... : 00:00:00.128  00:00:00.149  00:00:00.099
 Task ReadGEM4..................... : 00:00:05.490  00:00:05.220  00:00:05.190
 Task COMMixEvents................. : 00:00:02.936  00:00:02.789  00:00:02.779
 Task CYLDCHMixEvents.............. : 00:00:04.747  00:00:04.990  00:00:04.990
 Task SPXMixEvents................. : 00:00:00.149  00:00:00.099  00:00:00.099
 Task RDCMixEvents................. : 00:00:00.023  00:00:00.010  00:00:00.010
 Task SPXDigitize.................. : 00:00:37.329  00:00:37.480  00:00:35.120
 Task CYLDCHDigitize............... : 00:03:17.531  00:03:16.639  00:03:16.599
 Task RDCDigitize.................. : 00:00:01.554  00:00:01.529  00:00:00.509
 WriteEvent........................ : 00:01:04.630  00:01:04.329
analyzer........................... : 00:04:11.543  00:04:09.030  00:00:00.000
 DAQ gem4.......................... : 00:00:00.834  00:00:00.149  00:00:00.009
 Task MEGMain...................... : 00:00:00.097  00:00:00.110  00:00:00.100
 Task ReadGEM4..................... : 00:00:04.528  00:00:04.379  00:00:04.319
 Task COMMixEvents................. : 00:00:02.338  00:00:02.319  00:00:02.299
 Task CYLDCHMixEvents.............. : 00:00:03.746  00:00:03.820  00:00:03.820
 Task SPXMixEvents................. : 00:00:00.108  00:00:00.090  00:00:00.090
 Task RDCMixEvents................. : 00:00:00.014  00:00:00.009  00:00:00.000
 Task SPXDigitize.................. : 00:00:32.863  00:00:32.489  00:00:30.189
 Task CYLDCHDigitize............... : 00:02:32.296  00:02:32.559  00:02:32.559
 Task RDCDigitize.................. : 00:00:01.344  00:00:01.380  00:00:00.480
 WriteEvent........................ : 00:00:51.274  00:00:50.380
