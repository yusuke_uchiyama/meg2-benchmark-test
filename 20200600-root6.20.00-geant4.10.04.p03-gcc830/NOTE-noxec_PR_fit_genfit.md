2020/06/02

# Analyzer output with noxec_PR_fit_genfit.xml
* No XEC reconstruction
* SPX analysis from WaveformAnalysis (cf30)
* CDC analysis from waveform (WaveformAnalysis), pattern recognition and fit with GENFIT
* Custom run number 2000116
* Hit z reconstruction used in CYLDCHTrackFinderPR is time-difference

0f68f3e4eed47e6be9a05acf72fdc3209b4ee800

### Run Numbers ###
11000-11001 Spect_signalEOnly  
11100-11101 Spect_signalEWithMichel7e7


### Submit jobs ####
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix","/meg/data1/shared/mc/benchmark_test/20200600-root6.20.00-geant4.10.04.p03-gcc830/", 11000, 2)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_signalEWithMichel7e7","/meg/data1/shared/mc/benchmark_test/20200600-root6.20.00-geant4.10.04.p03-gcc830/", 11100, 2)'
```



### Calculate positron efficiency ###
```
RUNNUM=11000;NRUN=2;DATASET=20200600-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary
|   All events:
|  4000
|   |
|   | LXe acceptance cut
|   V
|  2611 (100 %)
|   |
|   | SPX acceptance cut
|   V
|  2395 (91.7273 %)
|   |
|   | DCH tracking
|   |  including contamination cut
|   |  (FakeThre:0.6)
|   V
|  2395 (91.7273 %)
|   |
|   | propagation cut
|   V
|  2379 (91.1145 %)
|   |
|   | tail cut with
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V
|  2259 (86.5186 %)
|   |
|   | matching
|   V
|  2208 (84.5653 %)
|   |
|   | SPX cluster contamination cut
|   |  (FakeThre:0.6)
|   V
|  2208 (84.5653 %)
|   |
|   | time cut
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V
|  2155 (82.5354 %)
|      + 0.749302
|      - 0.774861
@-----------------------------@
|           Resolutions
|  theta   (mrad)  5.73609
|  phi     (mrad)  5.40079
|  momentum (MeV)  0.0758794
|  vertexZ   (mm)  1.28646
|  vertexY   (mm)  0.658702
|  time     (sec)  4.30525e-11
@-----------------------------@
```

```
RUNNUM=11100;NRUN=2;DATASET=20200600-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```





### Analyze execution and output ###
```
cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 110

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 111
```

```
./macros/analyzer_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 110
```
##########################

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "110[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Jun  2 10:16 Eff_results11000.root
-rw-r--r--. 1 uchiyama unx-lke 2.3M Jun  2 09:43 histos11000.root
-rw-r--r--. 1 uchiyama unx-lke 2.3M Jun  2 09:44 histos11001.root
-rw-r--r--. 1 uchiyama unx-lke  60M Jun  2 09:43 rec11000.root
-rw-r--r--. 1 uchiyama unx-lke  61M Jun  2 09:44 rec11001.root

rec	31.371 kB/event
dch	27.115 kB/event
spx	3.533 kB/event
reco	0.316 kB/event
rdc	0.212 kB/event
xec	0.002 kB/event
```

######### execution time #########
1437.26/4000 = 0.359316 s/event
```
---------
analyzer........................... : 00:11:48.087  00:11:33.990  00:00:00.000
 DAQ bartender..................... : 00:01:02.549  00:00:59.940  00:00:59.700
 Task MEGMain...................... : 00:00:02.584  00:00:01.599  00:00:00.000
 Task ReadData..................... : 00:00:07.977  00:00:07.609  00:00:07.529
 Task SPX.......................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.470  00:00:05.629  00:00:05.569
  SubTask SPXHitRec................ : 00:00:00.295  00:00:00.319  00:00:00.319
  SubTask SPXClustering............ : 00:00:11.403  00:00:11.159  00:00:11.089
 Task RDC.......................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.086  00:00:00.090  00:00:00.060
  SubTask RDCHitRec................ : 00:00:00.047  00:00:00.060  00:00:00.050
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.348  00:00:00.370  00:00:00.360
  SubTask CYLDCHTrackFinderPR...... : 00:03:38.650  00:03:38.730  00:03:38.700
  SubTask DCHKalmanFilterGEN....... : 00:01:45.315  00:01:45.329  00:01:45.309
 Task DCHSPX....................... : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:28.167  00:01:28.000  00:01:28.000
  SubTask DCHSPXReFit.............. : 00:01:55.958  00:01:55.859  00:01:55.819
  SubTask SPXTracking.............. : 00:00:20.135  00:00:20.030  00:00:19.990
 WriteEvent........................ : 00:00:06.127  00:00:06.199
analyzer........................... : 00:12:09.175  00:11:55.389  00:00:00.000
 DAQ bartender..................... : 00:01:03.837  00:01:01.369  00:01:01.129
 Task MEGMain...................... : 00:00:02.588  00:00:01.559  00:00:00.000
 Task ReadData..................... : 00:00:08.063  00:00:07.960  00:00:07.910
 Task SPX.......................... : 00:00:00.013  00:00:00.019  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:05.498  00:00:05.269  00:00:05.179
  SubTask SPXHitRec................ : 00:00:00.305  00:00:00.280  00:00:00.270
  SubTask SPXClustering............ : 00:00:12.091  00:00:12.049  00:00:11.979
 Task RDC.......................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.088  00:00:00.079  00:00:00.079
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.049  00:00:00.039
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.352  00:00:00.279  00:00:00.269
  SubTask CYLDCHTrackFinderPR...... : 00:03:53.572  00:03:53.780  00:03:53.750
  SubTask DCHKalmanFilterGEN....... : 00:01:49.056  00:01:48.940  00:01:48.910
 Task DCHSPX....................... : 00:00:00.014  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:31.008  00:01:30.929  00:01:30.909
  SubTask DCHSPXReFit.............. : 00:01:51.655  00:01:51.660  00:01:51.620
  SubTask SPXTracking.............. : 00:00:20.542  00:00:20.579  00:00:20.559
 WriteEvent........................ : 00:00:06.209  00:00:06.260
```

```
./macros/analyzer_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 111
```
##########################

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "111[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Jun  3 04:19 Eff_results11100.root
-rw-r--r--. 1 uchiyama unx-lke 2.8M Jun  2 18:18 histos11100.root
-rw-r--r--. 1 uchiyama unx-lke 2.8M Jun  2 18:10 histos11101.root
-rw-r--r--. 1 uchiyama unx-lke 608M Jun  2 18:18 rec11100.root
-rw-r--r--. 1 uchiyama unx-lke 610M Jun  2 18:10 rec11101.root

rec	317.961 kB/event
dch	301.184 kB/event
spx	8.006 kB/event
reco	3.855 kB/event
rdc	3.548 kB/event
xec	0.028 kB/event
```

######### execution time #########
25618.4/4000 = 6.40459 s/event
```
---------
analyzer........................... : 03:37:25.902  03:36:29.559  00:00:00.000
 DAQ bartender..................... : 00:04:13.132  00:03:30.769  00:03:29.829
 Task MEGMain...................... : 00:00:01.338  00:00:01.279  00:00:00.000
 Task ReadData..................... : 00:00:22.377  00:00:22.429  00:00:22.359
 Task SPX.......................... : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.994  00:00:09.940  00:00:09.870
  SubTask SPXHitRec................ : 00:00:00.305  00:00:00.439  00:00:00.419
  SubTask SPXClustering............ : 00:00:19.447  00:00:19.049  00:00:18.999
 Task RDC.......................... : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:23.614  00:00:23.749  00:00:23.749
  SubTask RDCHitRec................ : 00:00:00.156  00:00:00.149  00:00:00.139
 Task CYLDCH....................... : 00:00:00.009  00:00:00.009  00:00:00.009
  SubTask CYLDCHHitRec............. : 00:00:09.728  00:00:09.800  00:00:09.780
  SubTask CYLDCHTrackFinderPR...... : 02:34:22.077  02:34:20.349  02:34:20.269
  SubTask DCHKalmanFilterGEN....... : 00:42:32.423  00:42:27.909  00:42:27.889
 Task DCHSPX....................... : 00:00:00.015  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:05:54.207  00:05:54.459  00:05:54.439
  SubTask DCHSPXReFit.............. : 00:03:24.817  00:03:23.440  00:03:23.430
  SubTask SPXTracking.............. : 00:00:46.571  00:00:46.310  00:00:46.270
 WriteEvent........................ : 00:00:40.425  00:00:36.280
analyzer........................... : 03:29:32.456  03:28:37.440  00:00:00.000
 DAQ bartender..................... : 00:04:09.913  00:03:29.940  00:03:29.050
 Task MEGMain...................... : 00:00:03.274  00:00:01.650  00:00:00.010
 Task ReadData..................... : 00:00:22.314  00:00:21.859  00:00:21.789
 Task SPX.......................... : 00:00:00.013  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.882  00:00:10.250  00:00:10.180
  SubTask SPXHitRec................ : 00:00:00.302  00:00:00.339  00:00:00.339
  SubTask SPXClustering............ : 00:00:19.205  00:00:19.080  00:00:18.999
 Task RDC.......................... : 00:00:00.012  00:00:00.029  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:23.546  00:00:23.209  00:00:23.180
  SubTask RDCHitRec................ : 00:00:00.154  00:00:00.130  00:00:00.110
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:09.264  00:00:09.070  00:00:09.070
  SubTask CYLDCHTrackFinderPR...... : 02:27:44.965  02:27:42.859  02:27:42.819
  SubTask DCHKalmanFilterGEN....... : 00:41:52.196  00:41:48.539  00:41:48.519
 Task DCHSPX....................... : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:05:40.083  00:05:39.519  00:05:39.509
  SubTask DCHSPXReFit.............. : 00:03:16.615  00:03:16.020  00:03:16.000
  SubTask SPXTracking.............. : 00:00:46.644  00:00:46.559  00:00:46.539
 WriteEvent........................ : 00:00:39.848  00:00:35.980
```