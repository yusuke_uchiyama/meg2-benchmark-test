2020/06/02

# Bartender output with Spect_signalE + Full_michel mixed.

* No mark (optimistic)
   * A signal event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Michel events are randomly mixed at rate of 7e7 Hz in time window (-350,350) ns.
   * Run number convention: 111**  
     10 gem4 runs (sev files) are summed up in a bartender run.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
* realistic
   * Run number convention: 116**

3eab0097251521438d05c5787f9fc8bb3ca08efc

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_signalEWithMichel7e7.xml","/meg/data1/shared/mc/benchmark_test/20200600-root6.20.00-geant4.10.04.p03-gcc830", 11100, 2, "")'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_signalEWithMichel7e7_realistic.xml","/meg/data1/shared/mc/benchmark_test/20200600-root6.20.00-geant4.10.04.p03-gcc830", 11600, 2, "")'
```

```
./macros/bartender_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7 111
./macros/bartender_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7 116
```


### Data and Statistics ###

./macros/bartender_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7 111
##########################

4000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7 | grep -E "111[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 2.8G Jun  2 11:19 raw11100.root
-rw-r--r--. 1 uchiyama unx-lke 2.8G Jun  2 11:19 raw11101.root
-rw-r--r--. 1 uchiyama unx-lke 4.7G Jun  2 11:19 sim11100.root
-rw-r--r--. 1 uchiyama unx-lke 4.7G Jun  2 11:19 sim11101.root

sim	2506.28 kB/event
track	1716.76 kB/event
dch	739.347 kB/event
kine	27.509 kB/event
spx	8.978 kB/event
```

######### execution time #########
5420.91/4000 = 1.35523 s/event
```
---------
analyzer........................... : 00:44:54.270  00:44:20.000  00:00:00.000
 DAQ gem4.......................... : 00:00:02.015  00:00:00.229  00:00:00.000
 Task MEGMain...................... : 00:00:00.157  00:00:00.129  00:00:00.109
 Task ReadGEM4..................... : 00:06:53.611  00:06:40.150  00:06:40.010
 Task COMMixEvents................. : 00:01:14.163  00:01:14.189  00:01:14.139
 Task CYLDCHMixEvents.............. : 00:01:17.732  00:01:17.570  00:01:17.540
 Task SPXMixEvents................. : 00:00:00.521  00:00:00.489  00:00:00.480
 Task RDCMixEvents................. : 00:00:00.190  00:00:00.169  00:00:00.169
 Task SPXDigitize.................. : 00:01:20.880  00:01:21.069  00:01:18.479
 Task CYLDCHDigitize............... : 00:26:20.705  00:26:20.379  00:26:20.359
 Task RDCDigitize.................. : 00:00:10.520  00:00:10.269  00:00:09.139
 WriteEvent........................ : 00:07:16.234  00:07:04.800
analyzer........................... : 00:45:26.645  00:44:51.570  00:00:00.000
 DAQ gem4.......................... : 00:00:01.943  00:00:00.239  00:00:00.000
 Task MEGMain...................... : 00:00:00.155  00:00:00.139  00:00:00.119
 Task ReadGEM4..................... : 00:06:44.460  00:06:30.339  00:06:30.229
 Task COMMixEvents................. : 00:01:13.378  00:01:13.510  00:01:13.500
 Task CYLDCHMixEvents.............. : 00:01:21.594  00:01:21.409  00:01:21.389
 Task SPXMixEvents................. : 00:00:00.515  00:00:00.530  00:00:00.520
 Task RDCMixEvents................. : 00:00:00.190  00:00:00.139  00:00:00.139
 Task SPXDigitize.................. : 00:01:19.679  00:01:19.750  00:01:17.180
 Task CYLDCHDigitize............... : 00:27:02.291  00:27:02.099  00:27:02.069
 Task RDCDigitize.................. : 00:00:10.570  00:00:10.600  00:00:09.490
 WriteEvent........................ : 00:07:13.718  00:07:02.230
```

./macros/bartender_output.sh 20200600-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
##########################

4000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7_realistic | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 2.9G Jun  5 08:39 raw11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.9G Jun  5 08:39 raw11601.root
-rw-r--r--. 1 uchiyama unx-lke 4.7G Jun  5 08:39 sim11600.root
-rw-r--r--. 1 uchiyama unx-lke 4.7G Jun  5 08:39 sim11601.root

sim     2491.55 kB/event
track   1706.69 kB/event
dch     734.879 kB/event
kine    27.363 kB/event
spx     8.967 kB/event
```

######### execution time #########
3973.36/4000 = 0.993339 s/event
```
---------
analyzer........................... : 00:33:10.319  00:32:40.139  00:00:00.000
 DAQ gem4.......................... : 00:00:01.377  00:00:00.269  00:00:00.019
 Task MEGMain...................... : 00:00:00.147  00:00:00.129  00:00:00.069
 Task ReadGEM4..................... : 00:05:19.145  00:05:08.380  00:05:08.230
 Task COMMixEvents................. : 00:01:21.636  00:01:21.199  00:01:21.179
 Task CYLDCHMixEvents.............. : 00:01:20.776  00:01:20.869  00:01:20.849
 Task SPXMixEvents................. : 00:00:00.637  00:00:00.659  00:00:00.649
 Task RDCMixEvents................. : 00:00:00.220  00:00:00.290  00:00:00.260
 Task SPXDigitize.................. : 00:01:23.655  00:01:23.139  00:01:20.569
 Task CYLDCHDigitize............... : 00:15:14.720  00:15:11.940  00:15:11.870
 Task RDCDigitize.................. : 00:00:10.913  00:00:10.850  00:00:09.700
 WriteEvent........................ : 00:07:59.008  00:07:49.269
analyzer........................... : 00:33:03.036  00:32:32.529  00:00:00.000
 DAQ gem4.......................... : 00:00:01.201  00:00:00.229  00:00:00.000
 Task MEGMain...................... : 00:00:00.134  00:00:00.120  00:00:00.089
 Task ReadGEM4..................... : 00:05:13.018  00:05:02.170  00:05:01.960
 Task COMMixEvents................. : 00:01:20.760  00:01:20.240  00:01:20.220
 Task CYLDCHMixEvents.............. : 00:01:22.051  00:01:21.789  00:01:21.759
 Task SPXMixEvents................. : 00:00:00.632  00:00:00.569  00:00:00.569
 Task RDCMixEvents................. : 00:00:00.219  00:00:00.140  00:00:00.140
 Task SPXDigitize.................. : 00:01:21.690  00:01:21.360  00:01:18.700
 Task CYLDCHDigitize............... : 00:15:15.677  00:15:13.149  00:15:13.109
 Task RDCDigitize.................. : 00:00:10.899  00:00:10.870  00:00:09.730
 WriteEvent........................ : 00:07:58.466  00:07:49.519
```		      