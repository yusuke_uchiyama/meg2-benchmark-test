2021/01/18, 02/05

# Analyzer output with noxec_PR_fit_genfit.xml
* No XEC reconstruction
* SPX analysis from WaveformAnalysis (cf30)
* CDC analysis from waveform (WaveformAnalysis2), pattern recognition and fit with GENFIT
* Custom run number 2000116
* Hit z reconstruction used in CYLDCHTrackFinderPR is time-difference
* CYLDCHWaveformAnalysis2 (with not fully optimized parameters).
    * Update TimeWalkCoefficient (to 2.9e-10) and GlobalTimeOffset (to -8.25)

mc_20210118.0

## Run Numbers ##
11500-11501 Spect_signalEOnly_realistic (O2 0%)  
11502-11503 Spect_signalEOnly_realistic (O2 0.5%)   
11504-11505 Spect_signalEOnly_realistic (O2 1%)   
11506-11507 Spect_signalEOnly_realistic (O2 2%)    
(11510-11511 Spect_signalEOnly_realistic (50 um cathode wires))  
(11512-11513 Spect_signalEOnly_realistic (60 um cathode wires))     
11520-11524 Spect_signalEOnly_realistic (nominal cathode wires)    
11530-11534 Spect_signalEOnly_realistic (50 um cahode wires)   
11540-11544 Spect_signalEOnly_realistic (60 um cahode wires)  

11600-11601 Spect_signalEWithMichel7e7_realistic (O2 0%)   
11602-11603 Spect_signalEWithMichel7e7_realistic (O2 0.5%)  
11604-11605 Spect_signalEWithMichel7e7_realistic (O2 1%)  
11606-11607 Spect_signalEWithMichel7e7_realistic (O2 2%)   
(11610-11611 Spect_signalEWithMichel7e7_realistic (50 um cathode wires))   
(11612-11613 Spect_signalEWithMichel7e7_realistic (60 um cathode wires))      
11620-11624 Spect_signalEWithMichel7e7_realistic (nominal cathode wires)    
11630-11634 Spect_signalEWithMichel7e7_realistic (50 um cahode wires)   
11640-11644 Spect_signalEWithMichel7e7_realistic (60 um cahode wires)  


## Submit jobs ##
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20210101-root6.20.00-geant4.10.04.p03-gcc830/", 11500, 8)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20210101-root6.20.00-geant4.10.04.p03-gcc830/", 11600, 8)'
```



## Calculate positron efficiency ##
### Realistic (O2 0%) ###

```
RUNNUM=11500;NRUN=2;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2628 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2404 (91.4764 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2336 (88.8889 %)
|   |                         
|   | propagation cut         
|   V                         
|  2314 (88.0518 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  2179 (82.9148 %)
|   |                         
|   | matching                
|   V                         
|  2149 (81.7732 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  2149 (81.7732 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  2103 (80.0228 %)
|      + 0.787166
|      - 0.810566
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  5.96087
|  phi     (mrad)  5.5235
|  momentum (MeV)  0.080078
|  vertexZ   (mm)  1.45579
|  vertexY   (mm)  0.68796
|  time     (sec)  4.51493e-11
@-----------------------------@
```

### Realistic (O2 0.5%) ###

```
RUNNUM=11502;NRUN=2;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2620 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2377 (90.7252 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2323 (88.6641 %)
|   |                         
|   | propagation cut         
|   V                         
|  2293 (87.5191 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  2141 (81.7176 %)
|   |                         
|   | matching                
|   V                         
|  2091 (79.8092 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  2091 (79.8092 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  2031 (77.5191 %)
|      + 0.823788
|      - 0.845282
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.32857
|  phi     (mrad)  5.92893
|  momentum (MeV)  0.0819127
|  vertexZ   (mm)  1.5696
|  vertexY   (mm)  0.746099
|  time     (sec)  4.71261e-11
@-----------------------------@
```

### Realistic (O2 1%) ###

```
RUNNUM=11504;NRUN=2;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2592 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2374 (91.5895 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2321 (89.5448 %)
|   |                         
|   | propagation cut         
|   V                         
|  2283 (88.0787 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  2113 (81.5201 %)
|   |                         
|   | matching                
|   V                         
|  2072 (79.9383 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  2072 (79.9383 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  2014 (77.7006 %)
|      + 0.825835
|      - 0.847709
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.18726
|  phi     (mrad)  5.90252
|  momentum (MeV)  0.0807165
|  vertexZ   (mm)  1.54027
|  vertexY   (mm)  0.705834
|  time     (sec)  4.50111e-11
@-----------------------------@
```

### Realistic (O2 2%) ###

```
RUNNUM=11506;NRUN=2;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2685 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2430 (90.5028 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2367 (88.1564 %)
|   |                         
|   | propagation cut         
|   V                         
|  2313 (86.1453 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  2126 (79.1806 %)
|   |                         
|   | matching                
|   V                         
|  2081 (77.5047 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  2081 (77.5047 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  2028 (75.5307 %)
|      + 0.838437
|      - 0.857877
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.39088
|  phi     (mrad)  5.94327
|  momentum (MeV)  0.085483
|  vertexZ   (mm)  1.62
|  vertexY   (mm)  0.732795
|  time     (sec)  4.42379e-11
@-----------------------------@
```



### Realistic (40/50 um, 0% O2) ###

```
RUNNUM=11520;NRUN=5;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6427 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  5863 (91.2245 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5714 (88.9062 %)
|   |                         
|   | propagation cut         
|   V                         
|  5655 (87.9882 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  5290 (82.309 %)
|   |                         
|   | matching                
|   V                         
|  5208 (81.0331 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  5208 (81.0331 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  5081 (79.0571 %)
|      + 0.510719
|      - 0.519899
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.10933 +- 0.0807778
|  phi     (mrad)  5.66919 +- 0.079165
|  momentum (MeV)  0.079919 +- 0.00111854
|  vertexZ   (mm)  1.46765 +- 0.0201034
|  vertexY   (mm)  0.698988 +- 0.00988239
|  time     (sec)  4.52679e-11 +- 6.16872e-13
@-----------------------------@
```

### Realistic (50 um, 0% O2) ###

```
RUNNUM=11530;NRUN=5;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6593 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  5990 (90.8539 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5853 (88.776 %)
|   |                         
|   | propagation cut         
|   V                         
|  5800 (87.9721 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  5393 (81.7989 %)
|   |                         
|   | matching                
|   V                         
|  5307 (80.4945 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  5307 (80.4945 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  5169 (78.4013 %)
|      + 0.509981
|      - 0.518725
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.3593 +- 0.0841142
|  phi     (mrad)  5.94412 +- 0.0799114
|  momentum (MeV)  0.0873526 +- 0.00121895
|  vertexZ   (mm)  1.54787 +- 0.0198585
|  vertexY   (mm)  0.752861 +- 0.0102918
|  time     (sec)  4.52168e-11 +- 6.24016e-13
@-----------------------------@
```

### Realistic (60 um, 0% O2) ###

```
RUNNUM=11540;NRUN=5;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6610 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  5814 (87.9576 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5680 (85.9304 %)
|   |                         
|   | propagation cut         
|   V                         
|  5631 (85.1891 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  5220 (78.9713 %)
|   |                         
|   | matching                
|   V                         
|  5101 (77.171 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  5101 (77.171 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  4974 (75.2496 %)
|      + 0.534472
|      - 0.54222
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.62248 +- 0.0856995
|  phi     (mrad)  6.30642 +- 0.0882356
|  momentum (MeV)  0.0896192 +- 0.00124264
|  vertexZ   (mm)  1.60291 +- 0.020885
|  vertexY   (mm)  0.786142 +- 0.0116164
|  time     (sec)  4.54382e-11 +- 6.62111e-13
@-----------------------------@
```




### Realistic (Michel mixed, O2 0%) ###

```
RUNNUM=11600;NRUN=2;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2628 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2402 (91.4003 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2221 (84.5129 %)
|   |                         
|   | propagation cut         
|   V                         
|  2095 (79.7184 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1778 (67.656 %)
|   |                         
|   | matching                
|   V                         
|  1749 (66.5525 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1746 (66.4384 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1698 (64.6119 %)
|      + 0.945945
|      - 0.957289
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.6648
|  phi     (mrad)  6.65756
|  momentum (MeV)  0.0955012
|  vertexZ   (mm)  1.72492
|  vertexY   (mm)  0.79882
|  time     (sec)  4.48742e-11
@-----------------------------@
```


### Realistic (Michel mixed, O2 0.5%) ###

```
RUNNUM=11602;NRUN=2;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2620 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2378 (90.7634 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2201 (84.0076 %)
|   |                         
|   | propagation cut         
|   V                         
|  2056 (78.4733 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1726 (65.8779 %)
|   |                         
|   | matching                
|   V                         
|  1678 (64.0458 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1676 (63.9695 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1637 (62.4809 %)
|      + 0.959954
|      - 0.969671
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.72912
|  phi     (mrad)  6.2382
|  momentum (MeV)  0.0952774
|  vertexZ   (mm)  1.77936
|  vertexY   (mm)  0.797652
|  time     (sec)  4.63793e-11
@-----------------------------@
```


### Realistic (Michel mixed, O2 1%) ###

```
RUNNUM=11604;NRUN=2;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2592 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2374 (91.5895 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2194 (84.6451 %)
|   |                         
|   | propagation cut         
|   V                         
|  2037 (78.588 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1715 (66.1651 %)
|   |                         
|   | matching                
|   V                         
|  1684 (64.9691 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1684 (64.9691 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1626 (62.7315 %)
|      + 0.963819
|      - 0.97384
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.54602
|  phi     (mrad)  6.39602
|  momentum (MeV)  0.0943665
|  vertexZ   (mm)  1.66857
|  vertexY   (mm)  0.773468
|  time     (sec)  4.71714e-11
@-----------------------------@
```


### Realistic (Michel mixed, O2 2%) ###

```
RUNNUM=11606;NRUN=2;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2685 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2431 (90.54 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2201 (81.9739 %)
|   |                         
|   | propagation cut         
|   V                         
|  2014 (75.0093 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1659 (61.7877 %)
|   |                         
|   | matching                
|   V                         
|  1618 (60.2607 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1614 (60.1117 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1563 (58.2123 %)
|      + 0.967154
|      - 0.973389
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.56139
|  phi     (mrad)  6.33749
|  momentum (MeV)  0.096999
|  vertexZ   (mm)  1.7201
|  vertexY   (mm)  0.835469
|  time     (sec)  4.6616e-11
@-----------------------------@
```

### Realistic (Michel mixed, 40/50 um, 0% O2) ###
```
RUNNUM=11620;NRUN=5;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6427 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  5862 (91.209 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5459 (84.9385 %)
|   |                         
|   | propagation cut         
|   V                         
|  5105 (79.4305 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  4315 (67.1386 %)
|   |                         
|   | matching                
|   V                         
|  4235 (65.8939 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  4230 (65.8161 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  4111 (63.9645 %)
|      + 0.604401
|      - 0.608803
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.70784 +- 0.0996518
|  phi     (mrad)  6.55567 +- 0.0960245
|  momentum (MeV)  0.0941205 +- 0.00144717
|  vertexZ   (mm)  1.73356 +- 0.0278235
|  vertexY   (mm)  0.789228 +- 0.0122841
|  time     (sec)  4.50583e-11 +- 6.97907e-13
@-----------------------------@
```

### Realistic (Michel mixed, 50 um) ###

```
RUNNUM=11630;NRUN=5;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6593 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  5990 (90.8539 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5562 (84.3622 %)
|   |                         
|   | propagation cut         
|   V                         
|  5170 (78.4165 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  4325 (65.5999 %)
|   |                         
|   | matching                
|   V                         
|  4240 (64.3106 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  4236 (64.25 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  4109 (62.3237 %)
|      + 0.602433
|      - 0.606219
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.91071 +- 0.0977778
|  phi     (mrad)  6.56001 +- 0.0975613
|  momentum (MeV)  0.0967875 +- 0.00153659
|  vertexZ   (mm)  1.75789 +- 0.0269956
|  vertexY   (mm)  0.828333 +- 0.0124966
|  time     (sec)  4.58389e-11 +- 6.86345e-13
@-----------------------------@
```

### Realistic (Michel mixed, 60 um) ###

```
RUNNUM=11640;NRUN=5;DATASET=20210101-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6610 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  5810 (87.8971 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5424 (82.0575 %)
|   |                         
|   | propagation cut         
|   V                         
|  5044 (76.3086 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  4178 (63.2073 %)
|   |                         
|   | matching                
|   V                         
|  4075 (61.649 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  4072 (61.6036 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  3963 (59.9546 %)
|      + 0.608674
|      - 0.611724
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  7.18398 +- 0.106069
|  phi     (mrad)  6.89552 +- 0.110752
|  momentum (MeV)  0.102334 +- 0.00165457
|  vertexZ   (mm)  1.83163 +- 0.0287311
|  vertexY   (mm)  0.855682 +- 0.0142309
|  time     (sec)  4.61624e-11 +- 7.23979e-13
@-----------------------------@
```

### Analyze execution and output ###
```
cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```

```
./macros/analyzer_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115
```
- - - -

16000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 11 22:38 Eff_results11500.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 11 22:41 Eff_results11502.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 11 22:43 Eff_results11504.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 11 22:45 Eff_results11506.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:37 histos11500.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:36 histos11501.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:36 histos11502.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:36 histos11503.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:35 histos11504.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:35 histos11505.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:34 histos11506.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:35 histos11507.root
-rw-r--r--. 1 uchiyama unx-lke  59M Jan 11 22:37 rec11500.root
-rw-r--r--. 1 uchiyama unx-lke  58M Jan 11 22:36 rec11501.root
-rw-r--r--. 1 uchiyama unx-lke  56M Jan 11 22:36 rec11502.root
-rw-r--r--. 1 uchiyama unx-lke  56M Jan 11 22:36 rec11503.root
-rw-r--r--. 1 uchiyama unx-lke  56M Jan 11 22:35 rec11504.root
-rw-r--r--. 1 uchiyama unx-lke  55M Jan 11 22:35 rec11505.root
-rw-r--r--. 1 uchiyama unx-lke  52M Jan 11 22:34 rec11506.root
-rw-r--r--. 1 uchiyama unx-lke  52M Jan 11 22:35 rec11507.root

rec	30.348 kB/event
dch	26.301 kB/event
spx	3.512 kB/event
reco	0.234 kB/event
rdc	0.212 kB/event

```
######### execution time #########
4863.05/16000 = 0.303941 s/event
```
---------
analyzer........................... : 00:11:07.429  00:11:01.360  00:00:00.000
 DAQ bartender..................... : 00:01:02.412  00:01:00.880  00:01:00.560
 Task MEGMain...................... : 00:00:03.118  00:00:01.379  00:00:00.000
 Task ReadData..................... : 00:00:09.885  00:00:10.150  00:00:10.080
 Task SPX.......................... : 00:00:00.017  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.233  00:00:05.029  00:00:04.959
  SubTask SPXHitRec................ : 00:00:00.309  00:00:00.280  00:00:00.250
  SubTask SPXClustering............ : 00:00:08.192  00:00:08.119  00:00:08.059
  SubTask SPXIndependentTracking... : 00:00:32.743  00:00:32.639  00:00:32.639
 Task RDC.......................... : 00:00:00.016  00:00:00.030  00:00:00.020
  SubTask RDCWaveformAnalysis...... : 00:00:00.088  00:00:00.049  00:00:00.039
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.039  00:00:00.019
 Task CYLDCH....................... : 00:00:00.012  00:00:00.019  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:42.529  00:01:42.230  00:01:42.150
  SubTask CYLDCHHitRec............. : 00:00:00.584  00:00:00.540  00:00:00.500
  SubTask CYLDCHTrackFinderPR...... : 00:03:10.755  00:03:10.249  00:03:10.189
  SubTask DCHKalmanFilterGEN....... : 00:01:15.999  00:01:15.359  00:01:15.319
  SubTask DCHTrackReFit............ : 00:00:00.150  00:00:00.159  00:00:00.130
 Task DCHSPX....................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:02.405  00:01:02.230  00:01:02.190
  SubTask DCHSPXReFit.............. : 00:01:18.033  00:01:18.040  00:01:18.000
  SubTask SPXTracking.............. : 00:00:22.126  00:00:21.989  00:00:21.969
  SubTask DCHSPXTrackSelection..... : 00:00:00.077  00:00:00.049  00:00:00.039
 WriteEvent........................ : 00:00:06.827  00:00:06.829
analyzer........................... : 00:10:50.235  00:10:43.460  00:00:00.000
 DAQ bartender..................... : 00:01:01.910  00:01:00.299  00:01:00.049
 Task MEGMain...................... : 00:00:03.288  00:00:01.419  00:00:00.000
 Task ReadData..................... : 00:00:09.961  00:00:09.900  00:00:09.850
 Task SPX.......................... : 00:00:00.017  00:00:00.029  00:00:00.019
  SubTask SPXWaveformAnalysis...... : 00:00:05.122  00:00:05.150  00:00:05.080
  SubTask SPXHitRec................ : 00:00:00.311  00:00:00.290  00:00:00.280
  SubTask SPXClustering............ : 00:00:08.017  00:00:07.939  00:00:07.879
  SubTask SPXIndependentTracking... : 00:00:30.694  00:00:30.659  00:00:30.630
 Task RDC.......................... : 00:00:00.016  00:00:00.029  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.085  00:00:00.089  00:00:00.089
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.049  00:00:00.039
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:42.970  00:01:42.709  00:01:42.579
  SubTask CYLDCHHitRec............. : 00:00:00.590  00:00:00.639  00:00:00.629
  SubTask CYLDCHTrackFinderPR...... : 00:02:55.420  00:02:54.019  00:02:53.989
  SubTask DCHKalmanFilterGEN....... : 00:01:15.152  00:01:15.050  00:01:15.020
  SubTask DCHTrackReFit............ : 00:00:00.146  00:00:00.149  00:00:00.149
 Task DCHSPX....................... : 00:00:00.013  00:00:00.029  00:00:00.019
  SubTask DCHSPXMatching........... : 00:01:03.410  00:01:03.290  00:01:03.260
  SubTask DCHSPXReFit.............. : 00:01:19.433  00:01:19.019  00:01:19.019
  SubTask SPXTracking.............. : 00:00:21.283  00:00:21.260  00:00:21.250
  SubTask DCHSPXTrackSelection..... : 00:00:00.077  00:00:00.070  00:00:00.049
 WriteEvent........................ : 00:00:06.790  00:00:06.850
analyzer........................... : 00:10:21.659  00:10:15.589  00:00:00.000
 DAQ bartender..................... : 00:01:01.467  00:01:00.300  00:01:00.070
 Task MEGMain...................... : 00:00:03.240  00:00:01.369  00:00:00.000
 Task ReadData..................... : 00:00:09.845  00:00:09.950  00:00:09.880
 Task SPX.......................... : 00:00:00.017  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.075  00:00:04.979  00:00:04.890
  SubTask SPXHitRec................ : 00:00:00.310  00:00:00.260  00:00:00.250
  SubTask SPXClustering............ : 00:00:08.110  00:00:07.969  00:00:07.909
  SubTask SPXIndependentTracking... : 00:00:33.707  00:00:33.860  00:00:33.840
 Task RDC.......................... : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.085  00:00:00.059  00:00:00.049
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.069  00:00:00.049
 Task CYLDCH....................... : 00:00:00.012  00:00:00.020  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:40.831  00:01:40.270  00:01:40.210
  SubTask CYLDCHHitRec............. : 00:00:00.569  00:00:00.489  00:00:00.459
  SubTask CYLDCHTrackFinderPR...... : 00:02:43.975  00:02:43.599  00:02:43.589
  SubTask DCHKalmanFilterGEN....... : 00:01:14.601  00:01:14.620  00:01:14.600
  SubTask DCHTrackReFit............ : 00:00:00.148  00:00:00.150  00:00:00.110
 Task DCHSPX....................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:56.863  00:00:56.669  00:00:56.659
  SubTask DCHSPXReFit.............. : 00:01:11.115  00:01:10.959  00:01:10.959
  SubTask SPXTracking.............. : 00:00:19.124  00:00:19.139  00:00:19.109
  SubTask DCHSPXTrackSelection..... : 00:00:00.075  00:00:00.059  00:00:00.049
 WriteEvent........................ : 00:00:06.757  00:00:06.320
analyzer........................... : 00:10:23.121  00:10:17.080  00:00:00.000
 DAQ bartender..................... : 00:01:02.325  00:01:01.269  00:01:01.029
 Task MEGMain...................... : 00:00:03.281  00:00:01.399  00:00:00.000
 Task ReadData..................... : 00:00:09.872  00:00:09.290  00:00:09.230
 Task SPX.......................... : 00:00:00.017  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.266  00:00:05.320  00:00:05.250
  SubTask SPXHitRec................ : 00:00:00.312  00:00:00.369  00:00:00.359
  SubTask SPXClustering............ : 00:00:08.523  00:00:08.310  00:00:08.270
  SubTask SPXIndependentTracking... : 00:00:32.155  00:00:31.880  00:00:31.840
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.083  00:00:00.089  00:00:00.079
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.029  00:00:00.019
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:40.705  00:01:40.749  00:01:40.669
  SubTask CYLDCHHitRec............. : 00:00:00.580  00:00:00.539  00:00:00.519
  SubTask CYLDCHTrackFinderPR...... : 00:02:43.970  00:02:43.620  00:02:43.600
  SubTask DCHKalmanFilterGEN....... : 00:01:11.863  00:01:11.660  00:01:11.640
  SubTask DCHTrackReFit............ : 00:00:00.146  00:00:00.209  00:00:00.190
 Task DCHSPX....................... : 00:00:00.013  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:00:57.094  00:00:56.859  00:00:56.859
  SubTask DCHSPXReFit.............. : 00:01:13.397  00:01:13.109  00:01:13.099
  SubTask SPXTracking.............. : 00:00:21.164  00:00:21.159  00:00:21.139
  SubTask DCHSPXTrackSelection..... : 00:00:00.075  00:00:00.069  00:00:00.059
 WriteEvent........................ : 00:00:06.701  00:00:06.549
analyzer........................... : 00:10:01.483  00:09:55.519  00:00:00.000
 DAQ bartender..................... : 00:01:02.523  00:01:01.410  00:01:01.180
 Task MEGMain...................... : 00:00:03.134  00:00:01.399  00:00:00.009
 Task ReadData..................... : 00:00:09.996  00:00:09.729  00:00:09.699
 Task SPX.......................... : 00:00:00.017  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.246  00:00:05.360  00:00:05.270
  SubTask SPXHitRec................ : 00:00:00.314  00:00:00.329  00:00:00.309
  SubTask SPXClustering............ : 00:00:08.317  00:00:08.039  00:00:07.969
  SubTask SPXIndependentTracking... : 00:00:31.796  00:00:31.790  00:00:31.770
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.085  00:00:00.120  00:00:00.120
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.049  00:00:00.049
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:38.931  00:01:38.399  00:01:38.269
  SubTask CYLDCHHitRec............. : 00:00:00.579  00:00:00.519  00:00:00.499
  SubTask CYLDCHTrackFinderPR...... : 00:02:28.888  00:02:28.569  00:02:28.519
  SubTask DCHKalmanFilterGEN....... : 00:01:12.112  00:01:11.820  00:01:11.800
  SubTask DCHTrackReFit............ : 00:00:00.148  00:00:00.169  00:00:00.149
 Task DCHSPX....................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:55.864  00:00:55.789  00:00:55.759
  SubTask DCHSPXReFit.............. : 00:01:11.308  00:01:11.109  00:01:11.089
  SubTask SPXTracking.............. : 00:00:19.439  00:00:19.559  00:00:19.549
  SubTask DCHSPXTrackSelection..... : 00:00:00.075  00:00:00.109  00:00:00.099
 WriteEvent........................ : 00:00:06.669  00:00:06.709
analyzer........................... : 00:09:55.164  00:09:49.279  00:00:00.000
 DAQ bartender..................... : 00:01:01.527  00:01:00.580  00:01:00.340
 Task MEGMain...................... : 00:00:03.131  00:00:01.389  00:00:00.000
 Task ReadData..................... : 00:00:09.826  00:00:09.420  00:00:09.360
 Task SPX.......................... : 00:00:00.017  00:00:00.030  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.164  00:00:05.100  00:00:05.030
  SubTask SPXHitRec................ : 00:00:00.310  00:00:00.370  00:00:00.360
  SubTask SPXClustering............ : 00:00:08.597  00:00:08.500  00:00:08.430
  SubTask SPXIndependentTracking... : 00:00:32.899  00:00:32.619  00:00:32.599
 Task RDC.......................... : 00:00:00.016  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.083  00:00:00.130  00:00:00.120
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.069  00:00:00.039
 Task CYLDCH....................... : 00:00:00.012  00:00:00.050  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:37.953  00:01:37.929  00:01:37.809
  SubTask CYLDCHHitRec............. : 00:00:00.559  00:00:00.580  00:00:00.580
  SubTask CYLDCHTrackFinderPR...... : 00:02:24.115  00:02:23.470  00:02:23.450
  SubTask DCHKalmanFilterGEN....... : 00:01:13.552  00:01:13.179  00:01:13.110
  SubTask DCHTrackReFit............ : 00:00:00.149  00:00:00.139  00:00:00.119
 Task DCHSPX....................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:52.684  00:00:52.650  00:00:52.640
  SubTask DCHSPXReFit.............. : 00:01:11.052  00:01:10.929  00:01:10.919
  SubTask SPXTracking.............. : 00:00:20.673  00:00:20.700  00:00:20.700
  SubTask DCHSPXTrackSelection..... : 00:00:00.075  00:00:00.060  00:00:00.050
 WriteEvent........................ : 00:00:06.738  00:00:06.609
analyzer........................... : 00:09:05.351  00:08:59.519  00:00:00.000
 DAQ bartender..................... : 00:01:02.168  00:01:00.700  00:01:00.480
 Task MEGMain...................... : 00:00:03.177  00:00:01.429  00:00:00.000
 Task ReadData..................... : 00:00:09.817  00:00:10.009  00:00:09.909
 Task SPX.......................... : 00:00:00.017  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.135  00:00:04.960  00:00:04.890
  SubTask SPXHitRec................ : 00:00:00.314  00:00:00.399  00:00:00.399
  SubTask SPXClustering............ : 00:00:08.441  00:00:08.279  00:00:08.219
  SubTask SPXIndependentTracking... : 00:00:33.155  00:00:32.970  00:00:32.950
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.090  00:00:00.109  00:00:00.079
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.049  00:00:00.049
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:33.103  00:01:32.970  00:01:32.870
  SubTask CYLDCHHitRec............. : 00:00:00.549  00:00:00.600  00:00:00.600
  SubTask CYLDCHTrackFinderPR...... : 00:02:09.427  00:02:08.689  00:02:08.649
  SubTask DCHKalmanFilterGEN....... : 00:01:02.361  00:01:02.569  00:01:02.539
  SubTask DCHTrackReFit............ : 00:00:00.148  00:00:00.140  00:00:00.120
 Task DCHSPX....................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:43.271  00:00:43.210  00:00:43.200
  SubTask DCHSPXReFit.............. : 00:01:01.060  00:01:00.849  00:01:00.819
  SubTask SPXTracking.............. : 00:00:20.445  00:00:20.340  00:00:20.330
  SubTask DCHSPXTrackSelection..... : 00:00:00.072  00:00:00.079  00:00:00.049
 WriteEvent........................ : 00:00:06.505  00:00:06.599
analyzer........................... : 00:09:18.608  00:09:12.809  00:00:00.000
 DAQ bartender..................... : 00:01:01.583  00:01:00.690  00:01:00.440
 Task MEGMain...................... : 00:00:03.202  00:00:01.370  00:00:00.000
 Task ReadData..................... : 00:00:09.857  00:00:09.910  00:00:09.850
 Task SPX.......................... : 00:00:00.017  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.274  00:00:05.430  00:00:05.390
  SubTask SPXHitRec................ : 00:00:00.314  00:00:00.270  00:00:00.250
  SubTask SPXClustering............ : 00:00:08.413  00:00:08.059  00:00:07.979
  SubTask SPXIndependentTracking... : 00:00:33.897  00:00:33.580  00:00:33.550
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.085  00:00:00.090  00:00:00.080
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.049  00:00:00.039
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:32.726  00:01:32.569  00:01:32.459
  SubTask CYLDCHHitRec............. : 00:00:00.552  00:00:00.499  00:00:00.479
  SubTask CYLDCHTrackFinderPR...... : 00:01:57.330  00:01:57.030  00:01:57.000
  SubTask DCHKalmanFilterGEN....... : 00:01:07.907  00:01:07.780  00:01:07.760
  SubTask DCHTrackReFit............ : 00:00:00.146  00:00:00.160  00:00:00.140
 Task DCHSPX....................... : 00:00:00.013  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:55.246  00:00:55.030  00:00:55.030
  SubTask DCHSPXReFit.............. : 00:01:10.338  00:01:10.319  00:01:10.309
  SubTask SPXTracking.............. : 00:00:19.084  00:00:18.969  00:00:18.949
  SubTask DCHSPXTrackSelection..... : 00:00:00.074  00:00:00.039  00:00:00.039
 WriteEvent........................ : 00:00:06.514  00:00:06.419
```

```
./macros/analyzer_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```
- - - -

16000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 12 11:51 Eff_results11600.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 12 11:54 Eff_results11602.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 12 11:58 Eff_results11604.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 12 12:03 Eff_results11606.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:34 histos11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:39 histos11601.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:25 histos11602.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:30 histos11603.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:25 histos11604.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:27 histos11605.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:16 histos11606.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:16 histos11607.root
-rw-r--r--. 1 uchiyama unx-lke 222M Jan 12 00:34 rec11600.root
-rw-r--r--. 1 uchiyama unx-lke 223M Jan 12 00:39 rec11601.root
-rw-r--r--. 1 uchiyama unx-lke 209M Jan 12 00:25 rec11602.root
-rw-r--r--. 1 uchiyama unx-lke 213M Jan 12 00:30 rec11603.root
-rw-r--r--. 1 uchiyama unx-lke 206M Jan 12 00:25 rec11604.root
-rw-r--r--. 1 uchiyama unx-lke 208M Jan 12 00:27 rec11605.root
-rw-r--r--. 1 uchiyama unx-lke 189M Jan 12 00:16 rec11606.root
-rw-r--r--. 1 uchiyama unx-lke 191M Jan 12 00:16 rec11607.root

rec	115.999 kB/event
dch	106.258 kB/event
spx	6.024 kB/event
rdc	2.711 kB/event
reco	0.705 kB/event

```
######### execution time #########
40203.5/16000 = 2.51272 s/event
```
---------
analyzer........................... : 01:31:10.364  01:30:24.780  00:00:00.000
 DAQ bartender..................... : 00:03:53.017  00:03:22.710  00:03:21.570
 Task MEGMain...................... : 00:00:01.354  00:00:01.240  00:00:00.000
 Task ReadData..................... : 00:00:25.108  00:00:24.799  00:00:24.759
 Task SPX.......................... : 00:00:00.015  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.704  00:00:08.570  00:00:08.480
  SubTask SPXHitRec................ : 00:00:00.302  00:00:00.190  00:00:00.190
  SubTask SPXClustering............ : 00:00:11.371  00:00:11.579  00:00:11.499
  SubTask SPXIndependentTracking... : 00:00:43.133  00:00:42.960  00:00:42.950
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:22.563  00:00:22.460  00:00:22.450
  SubTask RDCHitRec................ : 00:00:00.242  00:00:00.210  00:00:00.210
 Task CYLDCH....................... : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:13.135  00:10:11.459  00:10:11.379
  SubTask CYLDCHHitRec............. : 00:00:12.466  00:00:12.580  00:00:12.560
  SubTask CYLDCHTrackFinderPR...... : 00:58:08.424  00:57:59.429  00:57:59.419
  SubTask DCHKalmanFilterGEN....... : 00:10:26.742  00:10:25.220  00:10:25.190
  SubTask DCHTrackReFit............ : 00:00:00.139  00:00:00.079  00:00:00.059
 Task DCHSPX....................... : 00:00:00.011  00:00:00.019  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:35.772  00:03:35.279  00:03:35.249
  SubTask DCHSPXReFit.............. : 00:01:50.754  00:01:50.330  00:01:50.310
  SubTask SPXTracking.............. : 00:00:31.749  00:00:31.580  00:00:31.540
  SubTask DCHSPXTrackSelection..... : 00:00:00.133  00:00:00.180  00:00:00.140
 WriteEvent........................ : 00:00:14.777  00:00:14.249
analyzer........................... : 01:35:57.680  01:35:11.180  00:00:00.000
 DAQ bartender..................... : 00:03:58.279  00:03:27.240  00:03:26.250
 Task MEGMain...................... : 00:00:01.024  00:00:00.950  00:00:00.000
 Task ReadData..................... : 00:00:25.295  00:00:25.329  00:00:25.299
 Task SPX.......................... : 00:00:00.015  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.608  00:00:08.290  00:00:08.220
  SubTask SPXHitRec................ : 00:00:00.303  00:00:00.300  00:00:00.260
  SubTask SPXClustering............ : 00:00:11.658  00:00:11.870  00:00:11.830
  SubTask SPXIndependentTracking... : 00:00:42.840  00:00:42.719  00:00:42.699
 Task RDC.......................... : 00:00:00.015  00:00:00.049  00:00:00.020
  SubTask RDCWaveformAnalysis...... : 00:00:24.300  00:00:24.190  00:00:24.180
  SubTask RDCHitRec................ : 00:00:00.251  00:00:00.270  00:00:00.250
 Task CYLDCH....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:34.814  00:10:32.960  00:10:32.850
  SubTask CYLDCHHitRec............. : 00:00:13.097  00:00:13.429  00:00:13.429
  SubTask CYLDCHTrackFinderPR...... : 01:02:00.152  01:01:50.339  01:01:50.309
  SubTask DCHKalmanFilterGEN....... : 00:10:44.063  00:10:42.479  00:10:42.469
  SubTask DCHTrackReFit............ : 00:00:00.138  00:00:00.140  00:00:00.130
 Task DCHSPX....................... : 00:00:00.011  00:00:00.020  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:54.769  00:03:54.090  00:03:54.060
  SubTask DCHSPXReFit.............. : 00:01:43.194  00:01:42.950  00:01:42.920
  SubTask SPXTracking.............. : 00:00:28.760  00:00:28.819  00:00:28.769
  SubTask DCHSPXTrackSelection..... : 00:00:00.141  00:00:00.169  00:00:00.149
 WriteEvent........................ : 00:00:14.983  00:00:14.519
analyzer........................... : 01:22:43.328  01:21:59.760  00:00:00.000
 DAQ bartender..................... : 00:03:56.509  00:03:27.959  00:03:26.969
 Task MEGMain...................... : 00:00:01.043  00:00:00.949  00:00:00.009
 Task ReadData..................... : 00:00:24.370  00:00:24.150  00:00:24.080
 Task SPX.......................... : 00:00:00.016  00:00:00.040  00:00:00.020
  SubTask SPXWaveformAnalysis...... : 00:00:08.654  00:00:08.500  00:00:08.450
  SubTask SPXHitRec................ : 00:00:00.300  00:00:00.230  00:00:00.230
  SubTask SPXClustering............ : 00:00:10.751  00:00:10.799  00:00:10.719
  SubTask SPXIndependentTracking... : 00:00:42.521  00:00:42.350  00:00:42.320
 Task RDC.......................... : 00:00:00.015  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:23.907  00:00:23.609  00:00:23.589
  SubTask RDCHitRec................ : 00:00:00.248  00:00:00.189  00:00:00.189
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:13.030  00:10:11.410  00:10:11.330
  SubTask CYLDCHHitRec............. : 00:00:12.523  00:00:12.699  00:00:12.669
  SubTask CYLDCHTrackFinderPR...... : 00:50:46.825  00:50:38.210  00:50:38.180
  SubTask DCHKalmanFilterGEN....... : 00:09:51.256  00:09:49.789  00:09:49.759
  SubTask DCHTrackReFit............ : 00:00:00.144  00:00:00.069  00:00:00.059
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:18.700  00:03:18.049  00:03:18.019
  SubTask DCHSPXReFit.............. : 00:01:36.099  00:01:35.910  00:01:35.890
  SubTask SPXTracking.............. : 00:00:30.917  00:00:30.809  00:00:30.769
  SubTask DCHSPXTrackSelection..... : 00:00:00.135  00:00:00.170  00:00:00.150
 WriteEvent........................ : 00:00:14.379  00:00:13.759
analyzer........................... : 01:27:35.061  01:26:50.509  00:00:00.000
 DAQ bartender..................... : 00:04:00.096  00:03:29.860  00:03:28.870
 Task MEGMain...................... : 00:00:01.023  00:00:00.930  00:00:00.000
 Task ReadData..................... : 00:00:25.655  00:00:25.240  00:00:25.180
 Task SPX.......................... : 00:00:00.016  00:00:00.019  00:00:00.019
  SubTask SPXWaveformAnalysis...... : 00:00:08.797  00:00:08.900  00:00:08.840
  SubTask SPXHitRec................ : 00:00:00.305  00:00:00.260  00:00:00.249
  SubTask SPXClustering............ : 00:00:11.595  00:00:11.800  00:00:11.760
  SubTask SPXIndependentTracking... : 00:00:44.274  00:00:43.959  00:00:43.929
 Task RDC.......................... : 00:00:00.015  00:00:00.050  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:24.486  00:00:24.450  00:00:24.430
  SubTask RDCHitRec................ : 00:00:00.251  00:00:00.290  00:00:00.280
 Task CYLDCH....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:25.697  00:10:24.069  00:10:23.979
  SubTask CYLDCHHitRec............. : 00:00:12.751  00:00:12.919  00:00:12.899
  SubTask CYLDCHTrackFinderPR...... : 00:54:30.187  00:54:21.520  00:54:21.520
  SubTask DCHKalmanFilterGEN....... : 00:10:08.200  00:10:06.520  00:10:06.480
  SubTask DCHTrackReFit............ : 00:00:00.144  00:00:00.150  00:00:00.140
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:41.268  00:03:40.769  00:03:40.749
  SubTask DCHSPXReFit.............. : 00:01:47.166  00:01:46.830  00:01:46.789
  SubTask SPXTracking.............. : 00:00:27.379  00:00:27.510  00:00:27.500
  SubTask DCHSPXTrackSelection..... : 00:00:00.138  00:00:00.150  00:00:00.150
 WriteEvent........................ : 00:00:14.645  00:00:13.999
analyzer........................... : 01:21:58.525  01:21:12.590  00:00:00.000
 DAQ bartender..................... : 00:04:07.519  00:03:35.009  00:03:33.989
 Task MEGMain...................... : 00:00:01.011  00:00:00.969  00:00:00.000
 Task ReadData..................... : 00:00:27.378  00:00:26.880  00:00:26.830
 Task SPX.......................... : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.768  00:00:08.759  00:00:08.709
  SubTask SPXHitRec................ : 00:00:00.310  00:00:00.470  00:00:00.470
  SubTask SPXClustering............ : 00:00:13.942  00:00:14.009  00:00:13.959
  SubTask SPXIndependentTracking... : 00:00:45.769  00:00:45.729  00:00:45.689
 Task RDC.......................... : 00:00:00.015  00:00:00.039  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:24.189  00:00:23.819  00:00:23.809
  SubTask RDCHitRec................ : 00:00:00.257  00:00:00.239  00:00:00.219
 Task CYLDCH....................... : 00:00:00.012  00:00:00.020  00:00:00.010
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:18.781  00:10:17.180  00:10:17.090
  SubTask CYLDCHHitRec............. : 00:00:13.089  00:00:13.429  00:00:13.409
  SubTask CYLDCHTrackFinderPR...... : 00:49:32.511  00:49:24.590  00:49:24.580
  SubTask DCHKalmanFilterGEN....... : 00:09:46.199  00:09:44.659  00:09:44.639
  SubTask DCHTrackReFit............ : 00:00:00.152  00:00:00.120  00:00:00.110
 Task DCHSPX....................... : 00:00:00.012  00:00:00.030  00:00:00.020
  SubTask DCHSPXMatching........... : 00:03:26.066  00:03:25.350  00:03:25.330
  SubTask DCHSPXReFit.............. : 00:01:37.026  00:01:37.250  00:01:37.240
  SubTask SPXTracking.............. : 00:00:29.474  00:00:28.950  00:00:28.939
  SubTask DCHSPXTrackSelection..... : 00:00:00.135  00:00:00.139  00:00:00.139
 WriteEvent........................ : 00:00:14.776  00:00:14.440
analyzer........................... : 01:24:11.537  01:23:27.559  00:00:00.000
 DAQ bartender..................... : 00:04:02.540  00:03:32.390  00:03:31.380
 Task MEGMain...................... : 00:00:01.023  00:00:01.009  00:00:00.000
 Task ReadData..................... : 00:00:26.883  00:00:26.429  00:00:26.359
 Task SPX.......................... : 00:00:00.016  00:00:00.010  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:08.712  00:00:08.879  00:00:08.829
  SubTask SPXHitRec................ : 00:00:00.312  00:00:00.329  00:00:00.319
  SubTask SPXClustering............ : 00:00:12.938  00:00:12.719  00:00:12.649
  SubTask SPXIndependentTracking... : 00:00:49.847  00:00:49.840  00:00:49.830
 Task RDC.......................... : 00:00:00.016  00:00:00.020  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:24.000  00:00:23.839  00:00:23.809
  SubTask RDCHitRec................ : 00:00:00.277  00:00:00.270  00:00:00.260
 Task CYLDCH....................... : 00:00:00.012  00:00:00.030  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:11.166  00:10:09.310  00:10:09.230
  SubTask CYLDCHHitRec............. : 00:00:12.609  00:00:12.839  00:00:12.839
  SubTask CYLDCHTrackFinderPR...... : 00:51:39.491  00:51:31.679  00:51:31.639
  SubTask DCHKalmanFilterGEN....... : 00:10:02.170  00:10:00.560  00:10:00.530
  SubTask DCHTrackReFit............ : 00:00:00.151  00:00:00.190  00:00:00.170
 Task DCHSPX....................... : 00:00:00.012  00:00:00.019  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:25.655  00:03:24.949  00:03:24.929
  SubTask DCHSPXReFit.............. : 00:01:38.093  00:01:38.070  00:01:38.070
  SubTask SPXTracking.............. : 00:00:29.678  00:00:29.490  00:00:29.480
  SubTask DCHSPXTrackSelection..... : 00:00:00.140  00:00:00.100  00:00:00.080
 WriteEvent........................ : 00:00:14.693  00:00:14.389
analyzer........................... : 01:12:56.122  01:12:16.890  00:00:00.000
 DAQ bartender..................... : 00:04:03.191  00:03:36.189  00:03:35.179
 Task MEGMain...................... : 00:00:01.021  00:00:00.959  00:00:00.000
 Task ReadData..................... : 00:00:28.227  00:00:28.119  00:00:28.049
 Task SPX.......................... : 00:00:00.016  00:00:00.029  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:08.759  00:00:08.479  00:00:08.429
  SubTask SPXHitRec................ : 00:00:00.316  00:00:00.310  00:00:00.300
  SubTask SPXClustering............ : 00:00:14.492  00:00:14.740  00:00:14.700
  SubTask SPXIndependentTracking... : 00:00:46.550  00:00:46.060  00:00:46.000
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:24.519  00:00:24.649  00:00:24.629
  SubTask RDCHitRec................ : 00:00:00.260  00:00:00.179  00:00:00.169
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:09:45.373  00:09:43.870  00:09:43.800
  SubTask CYLDCHHitRec............. : 00:00:11.813  00:00:11.749  00:00:11.749
  SubTask CYLDCHTrackFinderPR...... : 00:43:01.654  00:42:55.010  00:42:54.990
  SubTask DCHKalmanFilterGEN....... : 00:08:25.986  00:08:24.829  00:08:24.779
  SubTask DCHTrackReFit............ : 00:00:00.152  00:00:00.110  00:00:00.100
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:00.636  00:03:00.079  00:03:00.069
  SubTask DCHSPXReFit.............. : 00:01:30.395  00:01:30.140  00:01:30.120
  SubTask SPXTracking.............. : 00:00:26.444  00:00:26.670  00:00:26.640
  SubTask DCHSPXTrackSelection..... : 00:00:00.126  00:00:00.059  00:00:00.050
 WriteEvent........................ : 00:00:14.308  00:00:13.669
analyzer........................... : 01:13:30.909  01:12:50.260  00:00:00.000
 DAQ bartender..................... : 00:04:07.023  00:03:38.699  00:03:37.689
 Task MEGMain...................... : 00:00:01.075  00:00:00.949  00:00:00.019
 Task ReadData..................... : 00:00:28.505  00:00:27.799  00:00:27.740
 Task SPX.......................... : 00:00:00.016  00:00:00.020  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:08.942  00:00:08.850  00:00:08.770
  SubTask SPXHitRec................ : 00:00:00.320  00:00:00.279  00:00:00.279
  SubTask SPXClustering............ : 00:00:14.393  00:00:14.690  00:00:14.650
  SubTask SPXIndependentTracking... : 00:00:46.139  00:00:45.930  00:00:45.910
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:25.005  00:00:25.230  00:00:25.230
  SubTask RDCHitRec................ : 00:00:00.262  00:00:00.149  00:00:00.129
 Task CYLDCH....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:09:48.901  00:09:47.110  00:09:47.010
  SubTask CYLDCHHitRec............. : 00:00:12.048  00:00:11.999  00:00:11.969
  SubTask CYLDCHTrackFinderPR...... : 00:42:35.352  00:42:28.920  00:42:28.880
  SubTask DCHKalmanFilterGEN....... : 00:09:05.573  00:09:04.330  00:09:04.280
  SubTask DCHTrackReFit............ : 00:00:00.157  00:00:00.120  00:00:00.089
 Task DCHSPX....................... : 00:00:00.012  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:09.888  00:03:09.199  00:03:09.159
  SubTask DCHSPXReFit.............. : 00:01:35.322  00:01:35.179  00:01:35.169
  SubTask SPXTracking.............. : 00:00:25.858  00:00:26.130  00:00:26.090
  SubTask DCHSPXTrackSelection..... : 00:00:00.132  00:00:00.109  00:00:00.099
 WriteEvent........................ : 00:00:14.436  00:00:13.750
```

```
./macros/analyzer_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115
```
- - - -

16000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 18 21:38 Eff_results11500.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 18 21:39 Eff_results11502.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 18 21:49 Eff_results11504.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 18 21:41 Eff_results11506.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 18 19:31 histos11500.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 18 19:30 histos11501.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 18 19:30 histos11502.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 18 19:30 histos11503.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 18 19:30 histos11504.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 18 19:29 histos11505.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 18 19:29 histos11506.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 18 19:29 histos11507.root
-rw-r--r--. 1 uchiyama unx-lke  61M Jan 18 19:31 rec11500.root
-rw-r--r--. 1 uchiyama unx-lke  61M Jan 18 19:30 rec11501.root
-rw-r--r--. 1 uchiyama unx-lke  59M Jan 18 19:30 rec11502.root
-rw-r--r--. 1 uchiyama unx-lke  60M Jan 18 19:30 rec11503.root
-rw-r--r--. 1 uchiyama unx-lke  59M Jan 18 19:30 rec11504.root
-rw-r--r--. 1 uchiyama unx-lke  59M Jan 18 19:29 rec11505.root
-rw-r--r--. 1 uchiyama unx-lke  56M Jan 18 19:29 rec11506.root
-rw-r--r--. 1 uchiyama unx-lke  56M Jan 18 19:29 rec11507.root

rec	31.826 kB/event
dch	27.789 kB/event
spx	3.488 kB/event
reco	0.236 kB/event
rdc	0.219 kB/event

```
######### execution time #########
5646.44/16000 = 0.352903 s/event
```
---------
analyzer........................... : 00:12:48.464  00:12:42.009  00:00:00.000
 DAQ bartender..................... : 00:01:04.567  00:01:01.069  00:01:00.729
 Task MEGMain...................... : 00:00:02.380  00:00:01.550  00:00:00.000
 Task ReadData..................... : 00:00:07.853  00:00:07.850  00:00:07.790
 Task SPX.......................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.581  00:00:05.450  00:00:05.360
  SubTask SPXHitRec................ : 00:00:00.303  00:00:00.249  00:00:00.219
  SubTask SPXClustering............ : 00:00:12.153  00:00:12.369  00:00:12.339
  SubTask SPXIndependentTracking... : 00:00:36.071  00:00:35.919  00:00:35.899
 Task RDC.......................... : 00:00:00.012  00:00:00.019  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.083  00:00:00.120  00:00:00.110
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.000  00:00:00.000
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:02:08.918  00:02:08.770  00:02:08.680
  SubTask CYLDCHHitRec............. : 00:00:00.577  00:00:00.560  00:00:00.560
  SubTask CYLDCHTrackFinderPR...... : 00:03:38.136  00:03:37.889  00:03:37.849
  SubTask DCHKalmanFilterGEN....... : 00:01:33.663  00:01:33.649  00:01:33.600
  SubTask DCHTrackReFit............ : 00:00:00.175  00:00:00.179  00:00:00.170
 Task DCHSPX....................... : 00:00:00.009  00:00:00.019  00:00:00.009
  SubTask DCHSPXMatching........... : 00:01:07.841  00:01:07.880  00:01:07.850
  SubTask DCHSPXReFit.............. : 00:01:34.528  00:01:34.339  00:01:34.319
  SubTask SPXTracking.............. : 00:00:23.591  00:00:23.399  00:00:23.359
  SubTask DCHSPXTrackSelection..... : 00:00:00.085  00:00:00.050  00:00:00.030
 WriteEvent........................ : 00:00:07.080  00:00:07.090
analyzer........................... : 00:12:26.004  00:12:18.600  00:00:00.000
 DAQ bartender..................... : 00:01:05.566  00:01:01.260  00:01:01.010
 Task MEGMain...................... : 00:00:02.346  00:00:01.530  00:00:00.000
 Task ReadData..................... : 00:00:07.892  00:00:08.129  00:00:08.049
 Task SPX.......................... : 00:00:00.012  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.653  00:00:05.779  00:00:05.709
  SubTask SPXHitRec................ : 00:00:00.302  00:00:00.299  00:00:00.299
  SubTask SPXClustering............ : 00:00:11.870  00:00:11.510  00:00:11.450
  SubTask SPXIndependentTracking... : 00:00:33.508  00:00:33.750  00:00:33.750
 Task RDC.......................... : 00:00:00.013  00:00:00.030  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.079  00:00:00.089  00:00:00.089
  SubTask RDCHitRec................ : 00:00:00.047  00:00:00.049  00:00:00.039
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:02:08.004  00:02:07.819  00:02:07.699
  SubTask CYLDCHHitRec............. : 00:00:00.585  00:00:00.540  00:00:00.540
  SubTask CYLDCHTrackFinderPR...... : 00:03:17.039  00:03:16.119  00:03:16.109
  SubTask DCHKalmanFilterGEN....... : 00:01:33.236  00:01:33.580  00:01:33.570
  SubTask DCHTrackReFit............ : 00:00:00.178  00:00:00.199  00:00:00.160
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:09.940  00:01:09.629  00:01:09.629
  SubTask DCHSPXReFit.............. : 00:01:35.235  00:01:35.000  00:01:34.970
  SubTask SPXTracking.............. : 00:00:22.626  00:00:22.850  00:00:22.810
  SubTask DCHSPXTrackSelection..... : 00:00:00.085  00:00:00.039  00:00:00.039
 WriteEvent........................ : 00:00:07.077  00:00:06.829
analyzer........................... : 00:12:15.349  00:12:08.970  00:00:00.000
 DAQ bartender..................... : 00:01:03.796  00:00:59.819  00:00:59.569
 Task MEGMain...................... : 00:00:02.565  00:00:01.539  00:00:00.000
 Task ReadData..................... : 00:00:07.758  00:00:08.170  00:00:08.110
 Task SPX.......................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.596  00:00:05.460  00:00:05.380
  SubTask SPXHitRec................ : 00:00:00.301  00:00:00.159  00:00:00.149
  SubTask SPXClustering............ : 00:00:11.801  00:00:11.700  00:00:11.619
  SubTask SPXIndependentTracking... : 00:00:36.628  00:00:36.709  00:00:36.689
 Task RDC.......................... : 00:00:00.013  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.079  00:00:00.089  00:00:00.089
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.080  00:00:00.070
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:57.765  00:01:57.510  00:01:57.430
  SubTask CYLDCHHitRec............. : 00:00:00.562  00:00:00.620  00:00:00.610
  SubTask CYLDCHTrackFinderPR...... : 00:03:27.134  00:03:27.019  00:03:26.959
  SubTask DCHKalmanFilterGEN....... : 00:01:31.667  00:01:31.410  00:01:31.400
  SubTask DCHTrackReFit............ : 00:00:00.175  00:00:00.210  00:00:00.200
 Task DCHSPX....................... : 00:00:00.009  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:05.159  00:01:05.139  00:01:05.129
  SubTask DCHSPXReFit.............. : 00:01:29.530  00:01:29.560  00:01:29.530
  SubTask SPXTracking.............. : 00:00:22.943  00:00:23.049  00:00:23.019
  SubTask DCHSPXTrackSelection..... : 00:00:00.083  00:00:00.040  00:00:00.030
 WriteEvent........................ : 00:00:06.964  00:00:06.899
analyzer........................... : 00:12:04.798  00:11:58.080  00:00:00.000
 DAQ bartender..................... : 00:01:03.930  00:00:59.730  00:00:59.470
 Task MEGMain...................... : 00:00:02.292  00:00:01.500  00:00:00.000
 Task ReadData..................... : 00:00:07.701  00:00:07.919  00:00:07.869
 Task SPX.......................... : 00:00:00.012  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.650  00:00:05.689  00:00:05.579
  SubTask SPXHitRec................ : 00:00:00.301  00:00:00.279  00:00:00.269
  SubTask SPXClustering............ : 00:00:11.924  00:00:11.569  00:00:11.539
  SubTask SPXIndependentTracking... : 00:00:35.417  00:00:35.540  00:00:35.530
 Task RDC.......................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.078  00:00:00.069  00:00:00.060
  SubTask RDCHitRec................ : 00:00:00.047  00:00:00.029  00:00:00.029
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:59.737  00:01:59.739  00:01:59.610
  SubTask CYLDCHHitRec............. : 00:00:00.566  00:00:00.649  00:00:00.619
  SubTask CYLDCHTrackFinderPR...... : 00:03:10.290  00:03:09.809  00:03:09.769
  SubTask DCHKalmanFilterGEN....... : 00:01:29.525  00:01:29.450  00:01:29.400
  SubTask DCHTrackReFit............ : 00:00:00.170  00:00:00.139  00:00:00.139
 Task DCHSPX....................... : 00:00:00.009  00:00:00.019  00:00:00.009
  SubTask DCHSPXMatching........... : 00:01:08.126  00:01:08.170  00:01:08.150
  SubTask DCHSPXReFit.............. : 00:01:32.846  00:01:32.859  00:01:32.849
  SubTask SPXTracking.............. : 00:00:24.530  00:00:24.310  00:00:24.280
  SubTask DCHSPXTrackSelection..... : 00:00:00.082  00:00:00.099  00:00:00.099
 WriteEvent........................ : 00:00:06.970  00:00:07.030
analyzer........................... : 00:11:43.742  00:11:37.320  00:00:00.000
 DAQ bartender..................... : 00:01:03.612  00:00:59.870  00:00:59.630
 Task MEGMain...................... : 00:00:02.682  00:00:01.450  00:00:00.000
 Task ReadData..................... : 00:00:07.619  00:00:07.429  00:00:07.369
 Task SPX.......................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.654  00:00:05.970  00:00:05.870
  SubTask SPXHitRec................ : 00:00:00.301  00:00:00.310  00:00:00.310
  SubTask SPXClustering............ : 00:00:11.681  00:00:11.299  00:00:11.229
  SubTask SPXIndependentTracking... : 00:00:34.423  00:00:34.710  00:00:34.700
 Task RDC.......................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.078  00:00:00.040  00:00:00.040
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.039  00:00:00.039
 Task CYLDCH....................... : 00:00:00.008  00:00:00.019  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:02:04.843  00:02:04.520  00:02:04.410
  SubTask CYLDCHHitRec............. : 00:00:00.562  00:00:00.610  00:00:00.600
  SubTask CYLDCHTrackFinderPR...... : 00:02:58.508  00:02:58.510  00:02:58.490
  SubTask DCHKalmanFilterGEN....... : 00:01:29.426  00:01:29.629  00:01:29.589
  SubTask DCHTrackReFit............ : 00:00:00.168  00:00:00.200  00:00:00.170
 Task DCHSPX....................... : 00:00:00.009  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:01:05.087  00:01:04.749  00:01:04.739
  SubTask DCHSPXReFit.............. : 00:01:25.800  00:01:25.619  00:01:25.599
  SubTask SPXTracking.............. : 00:00:21.685  00:00:21.779  00:00:21.749
  SubTask DCHSPXTrackSelection..... : 00:00:00.082  00:00:00.069  00:00:00.049
 WriteEvent........................ : 00:00:07.087  00:00:06.859
analyzer........................... : 00:11:19.462  00:11:13.600  00:00:00.000
 DAQ bartender..................... : 00:01:04.628  00:01:01.049  00:01:00.799
 Task MEGMain...................... : 00:00:02.574  00:00:01.540  00:00:00.009
 Task ReadData..................... : 00:00:07.701  00:00:07.660  00:00:07.620
 Task SPX.......................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.625  00:00:05.620  00:00:05.510
  SubTask SPXHitRec................ : 00:00:00.300  00:00:00.299  00:00:00.299
  SubTask SPXClustering............ : 00:00:12.567  00:00:12.280  00:00:12.199
  SubTask SPXIndependentTracking... : 00:00:35.842  00:00:36.030  00:00:36.010
 Task RDC.......................... : 00:00:00.012  00:00:00.009  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.077  00:00:00.050  00:00:00.050
  SubTask RDCHitRec................ : 00:00:00.047  00:00:00.009  00:00:00.009
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:58.461  00:01:58.490  00:01:58.400
  SubTask CYLDCHHitRec............. : 00:00:00.546  00:00:00.549  00:00:00.539
  SubTask CYLDCHTrackFinderPR...... : 00:02:47.885  00:02:47.729  00:02:47.709
  SubTask DCHKalmanFilterGEN....... : 00:01:29.508  00:01:29.570  00:01:29.490
  SubTask DCHTrackReFit............ : 00:00:00.176  00:00:00.140  00:00:00.119
 Task DCHSPX....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:58.316  00:00:58.430  00:00:58.420
  SubTask DCHSPXReFit.............. : 00:01:21.950  00:01:21.959  00:01:21.959
  SubTask SPXTracking.............. : 00:00:21.891  00:00:21.770  00:00:21.760
  SubTask DCHSPXTrackSelection..... : 00:00:00.083  00:00:00.100  00:00:00.079
 WriteEvent........................ : 00:00:06.844  00:00:06.790
analyzer........................... : 00:10:53.508  00:10:47.379  00:00:00.000
 DAQ bartender..................... : 00:01:03.358  00:00:59.550  00:00:59.300
 Task MEGMain...................... : 00:00:02.585  00:00:01.530  00:00:00.000
 Task ReadData..................... : 00:00:07.679  00:00:07.689  00:00:07.629
 Task SPX.......................... : 00:00:00.012  00:00:00.019  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:05.564  00:00:05.300  00:00:05.230
  SubTask SPXHitRec................ : 00:00:00.301  00:00:00.270  00:00:00.260
  SubTask SPXClustering............ : 00:00:12.234  00:00:12.540  00:00:12.520
  SubTask SPXIndependentTracking... : 00:00:35.903  00:00:35.649  00:00:35.639
 Task RDC.......................... : 00:00:00.012  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.082  00:00:00.079  00:00:00.070
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.080  00:00:00.070
 Task CYLDCH....................... : 00:00:00.008  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:52.076  00:01:52.219  00:01:52.139
  SubTask CYLDCHHitRec............. : 00:00:00.544  00:00:00.560  00:00:00.560
  SubTask CYLDCHTrackFinderPR...... : 00:02:42.379  00:02:42.060  00:02:42.040
  SubTask DCHKalmanFilterGEN....... : 00:01:21.950  00:01:22.090  00:01:22.060
  SubTask DCHTrackReFit............ : 00:00:00.172  00:00:00.199  00:00:00.159
 Task DCHSPX....................... : 00:00:00.009  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:54.884  00:00:54.780  00:00:54.760
  SubTask DCHSPXReFit.............. : 00:01:19.587  00:01:19.509  00:01:19.499
  SubTask SPXTracking.............. : 00:00:22.669  00:00:22.680  00:00:22.660
  SubTask DCHSPXTrackSelection..... : 00:00:00.082  00:00:00.069  00:00:00.059
 WriteEvent........................ : 00:00:06.783  00:00:06.660
analyzer........................... : 00:10:35.115  00:10:28.610  00:00:00.000
 DAQ bartender..................... : 00:01:03.039  00:00:59.279  00:00:59.039
 Task MEGMain...................... : 00:00:02.408  00:00:01.509  00:00:00.000
 Task ReadData..................... : 00:00:07.554  00:00:07.210  00:00:07.160
 Task SPX.......................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.562  00:00:05.689  00:00:05.599
  SubTask SPXHitRec................ : 00:00:00.298  00:00:00.400  00:00:00.390
  SubTask SPXClustering............ : 00:00:11.661  00:00:11.559  00:00:11.509
  SubTask SPXIndependentTracking... : 00:00:35.840  00:00:35.930  00:00:35.870
 Task RDC.......................... : 00:00:00.012  00:00:00.020  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.079  00:00:00.089  00:00:00.069
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.039  00:00:00.039
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:49.208  00:01:49.200  00:01:49.110
  SubTask CYLDCHHitRec............. : 00:00:00.528  00:00:00.530  00:00:00.520
  SubTask CYLDCHTrackFinderPR...... : 00:02:27.268  00:02:26.930  00:02:26.910
  SubTask DCHKalmanFilterGEN....... : 00:01:19.700  00:01:19.690  00:01:19.640
  SubTask DCHTrackReFit............ : 00:00:00.168  00:00:00.260  00:00:00.229
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:57.605  00:00:57.270  00:00:57.250
  SubTask DCHSPXReFit.............. : 00:01:22.364  00:01:22.370  00:01:22.370
  SubTask SPXTracking.............. : 00:00:20.024  00:00:20.169  00:00:20.089
  SubTask DCHSPXTrackSelection..... : 00:00:00.082  00:00:00.069  00:00:00.049
 WriteEvent........................ : 00:00:06.699  00:00:06.720
```

```
./macros/analyzer_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```
- - - -

16000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 18 21:44 Eff_results11600.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 18 21:47 Eff_results11602.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 18 21:55 Eff_results11604.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 18 21:58 Eff_results11606.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 18 21:23 histos11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 18 21:24 histos11601.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 18 21:09 histos11602.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 18 21:14 histos11603.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 18 21:08 histos11604.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 18 21:13 histos11605.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 18 20:58 histos11606.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 18 20:59 histos11607.root
-rw-r--r--. 1 uchiyama unx-lke 238M Jan 18 21:23 rec11600.root
-rw-r--r--. 1 uchiyama unx-lke 238M Jan 18 21:24 rec11601.root
-rw-r--r--. 1 uchiyama unx-lke 224M Jan 18 21:09 rec11602.root
-rw-r--r--. 1 uchiyama unx-lke 229M Jan 18 21:14 rec11603.root
-rw-r--r--. 1 uchiyama unx-lke 223M Jan 18 21:08 rec11604.root
-rw-r--r--. 1 uchiyama unx-lke 226M Jan 18 21:13 rec11605.root
-rw-r--r--. 1 uchiyama unx-lke 208M Jan 18 20:58 rec11606.root
-rw-r--r--. 1 uchiyama unx-lke 211M Jan 18 20:59 rec11607.root

rec	124.352 kB/event
dch	114.506 kB/event
spx	6.102 kB/event
rdc	2.735 kB/event
reco	0.691 kB/event

```
######### execution time #########
54287.5/16000 = 3.39297 s/event
```
---------
analyzer........................... : 02:05:03.517  02:04:04.189  00:00:00.000
 DAQ bartender..................... : 00:05:17.528  00:04:26.100  00:04:25.380
 Task MEGMain...................... : 00:00:01.272  00:00:01.219  00:00:00.000
 Task ReadData..................... : 00:00:28.088  00:00:28.079  00:00:28.009
 Task SPX.......................... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.902  00:00:10.840  00:00:10.770
  SubTask SPXHitRec................ : 00:00:00.379  00:00:00.300  00:00:00.290
  SubTask SPXClustering............ : 00:00:24.254  00:00:24.289  00:00:24.239
  SubTask SPXIndependentTracking... : 00:00:57.920  00:00:57.780  00:00:57.780
 Task RDC.......................... : 00:00:00.015  00:00:00.019  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:26.650  00:00:26.320  00:00:26.320
  SubTask RDCHitRec................ : 00:00:00.355  00:00:00.270  00:00:00.270
 Task CYLDCH....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:14:27.839  00:14:27.339  00:14:27.249
  SubTask CYLDCHHitRec............. : 00:00:18.282  00:00:18.410  00:00:18.400
  SubTask CYLDCHTrackFinderPR...... : 01:19:15.465  01:19:11.230  01:19:11.190
  SubTask DCHKalmanFilterGEN....... : 00:14:24.615  00:14:23.909  00:14:23.899
  SubTask DCHTrackReFit............ : 00:00:00.227  00:00:00.210  00:00:00.180
 Task DCHSPX....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:04:54.630  00:04:54.429  00:04:54.399
  SubTask DCHSPXReFit.............. : 00:02:42.890  00:02:42.800  00:02:42.750
  SubTask SPXTracking.............. : 00:00:41.596  00:00:41.579  00:00:41.579
  SubTask DCHSPXTrackSelection..... : 00:00:00.225  00:00:00.290  00:00:00.240
 WriteEvent........................ : 00:00:19.298  00:00:18.780
analyzer........................... : 02:06:24.719  02:05:28.649  00:00:00.000
 DAQ bartender..................... : 00:05:14.735  00:04:23.679  00:04:22.689
 Task MEGMain...................... : 00:00:01.265  00:00:01.319  00:00:00.000
 Task ReadData..................... : 00:00:26.738  00:00:26.690  00:00:26.670
 Task SPX.......................... : 00:00:00.014  00:00:00.020  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:10.718  00:00:10.669  00:00:10.609
  SubTask SPXHitRec................ : 00:00:00.364  00:00:00.359  00:00:00.339
  SubTask SPXClustering............ : 00:00:22.209  00:00:22.229  00:00:22.159
  SubTask SPXIndependentTracking... : 00:00:54.606  00:00:54.699  00:00:54.689
 Task RDC.......................... : 00:00:00.015  00:00:00.020  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:28.209  00:00:28.089  00:00:28.089
  SubTask RDCHitRec................ : 00:00:00.386  00:00:00.260  00:00:00.240
 Task CYLDCH....................... : 00:00:00.010  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:14:25.151  00:14:24.760  00:14:24.660
  SubTask CYLDCHHitRec............. : 00:00:19.466  00:00:19.459  00:00:19.429
  SubTask CYLDCHTrackFinderPR...... : 01:20:54.909  01:20:52.720  01:20:52.690
  SubTask DCHKalmanFilterGEN....... : 00:14:29.574  00:14:29.649  00:14:29.639
  SubTask DCHTrackReFit............ : 00:00:00.207  00:00:00.159  00:00:00.139
 Task DCHSPX....................... : 00:00:00.010  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:04:46.745  00:04:46.500  00:04:46.490
  SubTask DCHSPXReFit.............. : 00:02:32.927  00:02:33.130  00:02:33.110
  SubTask SPXTracking.............. : 00:00:45.838  00:00:45.839  00:00:45.809
  SubTask DCHSPXTrackSelection..... : 00:00:00.219  00:00:00.239  00:00:00.229
 WriteEvent........................ : 00:00:19.411  00:00:18.460
analyzer........................... : 01:51:19.907  01:50:25.000  00:00:00.000
 DAQ bartender..................... : 00:05:17.571  00:04:27.019  00:04:26.109
 Task MEGMain...................... : 00:00:01.326  00:00:01.289  00:00:00.000
 Task ReadData..................... : 00:00:27.949  00:00:27.990  00:00:27.940
 Task SPX.......................... : 00:00:00.015  00:00:00.030  00:00:00.020
  SubTask SPXWaveformAnalysis...... : 00:00:10.651  00:00:10.769  00:00:10.709
  SubTask SPXHitRec................ : 00:00:00.370  00:00:00.370  00:00:00.360
  SubTask SPXClustering............ : 00:00:24.933  00:00:24.910  00:00:24.860
  SubTask SPXIndependentTracking... : 00:00:54.315  00:00:53.850  00:00:53.820
 Task RDC.......................... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:26.853  00:00:27.050  00:00:27.020
  SubTask RDCHitRec................ : 00:00:00.346  00:00:00.300  00:00:00.300
 Task CYLDCH....................... : 00:00:00.010  00:00:00.030  00:00:00.030
  SubTask CYLDCHWaveformAnalysis2.. : 00:14:32.608  00:14:32.269  00:14:32.169
  SubTask CYLDCHHitRec............. : 00:00:17.418  00:00:17.450  00:00:17.420
  SubTask CYLDCHTrackFinderPR...... : 01:08:07.066  01:08:05.479  01:08:05.429
  SubTask DCHKalmanFilterGEN....... : 00:13:00.324  00:13:00.240  00:13:00.230
  SubTask DCHTrackReFit............ : 00:00:00.222  00:00:00.230  00:00:00.210
 Task DCHSPX....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:04:08.752  00:04:08.309  00:04:08.279
  SubTask DCHSPXReFit.............. : 00:02:15.581  00:02:15.420  00:02:15.380
  SubTask SPXTracking.............. : 00:00:43.772  00:00:44.050  00:00:44.010
  SubTask DCHSPXTrackSelection..... : 00:00:00.207  00:00:00.209  00:00:00.209
 WriteEvent........................ : 00:00:18.554  00:00:17.829
analyzer........................... : 01:56:21.646  01:55:26.380  00:00:00.000
 DAQ bartender..................... : 00:05:20.916  00:04:29.390  00:04:28.430
 Task MEGMain...................... : 00:00:01.337  00:00:01.270  00:00:00.010
 Task ReadData..................... : 00:00:28.282  00:00:28.480  00:00:28.430
 Task SPX.......................... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.871  00:00:10.700  00:00:10.610
  SubTask SPXHitRec................ : 00:00:00.374  00:00:00.370  00:00:00.330
  SubTask SPXClustering............ : 00:00:25.571  00:00:25.729  00:00:25.669
  SubTask SPXIndependentTracking... : 00:00:58.857  00:00:58.830  00:00:58.810
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:27.102  00:00:26.919  00:00:26.889
  SubTask RDCHitRec................ : 00:00:00.355  00:00:00.310  00:00:00.300
 Task CYLDCH....................... : 00:00:00.010  00:00:00.020  00:00:00.020
  SubTask CYLDCHWaveformAnalysis2.. : 00:13:53.556  00:13:53.459  00:13:53.369
  SubTask CYLDCHHitRec............. : 00:00:17.857  00:00:17.989  00:00:17.969
  SubTask CYLDCHTrackFinderPR...... : 01:11:37.084  01:11:35.179  01:11:35.149
  SubTask DCHKalmanFilterGEN....... : 00:14:11.119  00:14:11.080  00:14:11.070
  SubTask DCHTrackReFit............ : 00:00:00.222  00:00:00.250  00:00:00.240
 Task DCHSPX....................... : 00:00:00.010  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:04:36.410  00:04:36.479  00:04:36.439
  SubTask DCHSPXReFit.............. : 00:02:39.594  00:02:39.769  00:02:39.729
  SubTask SPXTracking.............. : 00:00:41.931  00:00:41.680  00:00:41.650
  SubTask DCHSPXTrackSelection..... : 00:00:00.214  00:00:00.260  00:00:00.250
 WriteEvent........................ : 00:00:19.021  00:00:18.109
analyzer........................... : 01:50:06.889  01:49:12.649  00:00:00.000
 DAQ bartender..................... : 00:05:17.476  00:04:26.949  00:04:26.009
 Task MEGMain...................... : 00:00:01.317  00:00:01.259  00:00:00.000
 Task ReadData..................... : 00:00:27.774  00:00:28.159  00:00:28.079
 Task SPX.......................... : 00:00:00.015  00:00:00.030  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.881  00:00:10.920  00:00:10.860
  SubTask SPXHitRec................ : 00:00:00.369  00:00:00.369  00:00:00.339
  SubTask SPXClustering............ : 00:00:24.703  00:00:24.939  00:00:24.879
  SubTask SPXIndependentTracking... : 00:00:59.055  00:00:58.930  00:00:58.920
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:27.234  00:00:27.120  00:00:27.080
  SubTask RDCHitRec................ : 00:00:00.359  00:00:00.329  00:00:00.319
 Task CYLDCH....................... : 00:00:00.010  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:14:04.673  00:14:03.639  00:14:03.569
  SubTask CYLDCHHitRec............. : 00:00:17.859  00:00:17.780  00:00:17.730
  SubTask CYLDCHTrackFinderPR...... : 01:06:51.663  01:06:50.710  01:06:50.650
  SubTask DCHKalmanFilterGEN....... : 00:13:07.224  00:13:06.890  00:13:06.890
  SubTask DCHTrackReFit............ : 00:00:00.223  00:00:00.270  00:00:00.239
 Task DCHSPX....................... : 00:00:00.010  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:04:20.156  00:04:19.729  00:04:19.679
  SubTask DCHSPXReFit.............. : 00:02:24.761  00:02:24.949  00:02:24.909
  SubTask SPXTracking.............. : 00:00:41.689  00:00:41.500  00:00:41.460
  SubTask DCHSPXTrackSelection..... : 00:00:00.213  00:00:00.230  00:00:00.200
 WriteEvent........................ : 00:00:18.423  00:00:18.310
analyzer........................... : 01:55:09.226  01:54:14.359  00:00:00.000
 DAQ bartender..................... : 00:05:18.445  00:04:27.539  00:04:26.569
 Task MEGMain...................... : 00:00:01.291  00:00:01.260  00:00:00.000
 Task ReadData..................... : 00:00:28.249  00:00:28.699  00:00:28.629
 Task SPX.......................... : 00:00:00.015  00:00:00.020  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:11.167  00:00:11.120  00:00:11.030
  SubTask SPXHitRec................ : 00:00:00.379  00:00:00.399  00:00:00.379
  SubTask SPXClustering............ : 00:00:26.443  00:00:26.620  00:00:26.560
  SubTask SPXIndependentTracking... : 00:01:03.331  00:01:02.909  00:01:02.889
 Task RDC.......................... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:27.048  00:00:26.859  00:00:26.839
  SubTask RDCHitRec................ : 00:00:00.337  00:00:00.420  00:00:00.400
 Task CYLDCH....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:14:20.954  00:14:20.600  00:14:20.490
  SubTask CYLDCHHitRec............. : 00:00:19.096  00:00:19.210  00:00:19.200
  SubTask CYLDCHTrackFinderPR...... : 01:10:54.820  01:10:53.259  01:10:53.219
  SubTask DCHKalmanFilterGEN....... : 00:13:39.663  00:13:39.240  00:13:39.200
  SubTask DCHTrackReFit............ : 00:00:00.227  00:00:00.199  00:00:00.169
 Task DCHSPX....................... : 00:00:00.010  00:00:00.010  00:00:00.010
  SubTask DCHSPXMatching........... : 00:04:24.813  00:04:24.720  00:04:24.710
  SubTask DCHSPXReFit.............. : 00:02:24.767  00:02:24.639  00:02:24.599
  SubTask SPXTracking.............. : 00:00:37.982  00:00:37.950  00:00:37.920
  SubTask DCHSPXTrackSelection..... : 00:00:00.220  00:00:00.139  00:00:00.129
 WriteEvent........................ : 00:00:18.750  00:00:18.350
analyzer........................... : 01:39:48.627  01:38:55.409  00:00:00.000
 DAQ bartender..................... : 00:05:14.925  00:04:25.939  00:04:24.989
 Task MEGMain...................... : 00:00:01.411  00:00:01.290  00:00:00.009
 Task ReadData..................... : 00:00:27.963  00:00:27.819  00:00:27.759
 Task SPX.......................... : 00:00:00.015  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.742  00:00:10.639  00:00:10.529
  SubTask SPXHitRec................ : 00:00:00.381  00:00:00.339  00:00:00.339
  SubTask SPXClustering............ : 00:00:27.685  00:00:27.680  00:00:27.630
  SubTask SPXIndependentTracking... : 00:00:58.257  00:00:58.279  00:00:58.259
 Task RDC.......................... : 00:00:00.015  00:00:00.020  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:27.774  00:00:27.659  00:00:27.649
  SubTask RDCHitRec................ : 00:00:00.348  00:00:00.359  00:00:00.349
 Task CYLDCH....................... : 00:00:00.010  00:00:00.030  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:13:34.665  00:13:33.860  00:13:33.760
  SubTask CYLDCHHitRec............. : 00:00:16.454  00:00:16.609  00:00:16.599
  SubTask CYLDCHTrackFinderPR...... : 00:59:05.909  00:59:05.179  00:59:05.139
  SubTask DCHKalmanFilterGEN....... : 00:11:37.130  00:11:36.760  00:11:36.730
  SubTask DCHTrackReFit............ : 00:00:00.223  00:00:00.200  00:00:00.170
 Task DCHSPX....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:04:05.571  00:04:05.650  00:04:05.610
  SubTask DCHSPXReFit.............. : 00:02:11.706  00:02:11.519  00:02:11.519
  SubTask SPXTracking.............. : 00:00:38.246  00:00:38.080  00:00:38.060
  SubTask DCHSPXTrackSelection..... : 00:00:00.196  00:00:00.159  00:00:00.149
 WriteEvent........................ : 00:00:17.816  00:00:17.270
analyzer........................... : 01:40:32.980  01:39:36.940  00:00:00.000
 DAQ bartender..................... : 00:05:18.486  00:04:27.159  00:04:26.199
 Task MEGMain...................... : 00:00:01.376  00:00:01.310  00:00:00.010
 Task ReadData..................... : 00:00:27.605  00:00:27.490  00:00:27.430
 Task SPX.......................... : 00:00:00.015  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.963  00:00:10.970  00:00:10.900
  SubTask SPXHitRec................ : 00:00:00.377  00:00:00.420  00:00:00.420
  SubTask SPXClustering............ : 00:00:26.376  00:00:26.329  00:00:26.269
  SubTask SPXIndependentTracking... : 00:00:57.721  00:00:57.590  00:00:57.590
 Task RDC.......................... : 00:00:00.015  00:00:00.010  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:27.262  00:00:27.080  00:00:27.080
  SubTask RDCHitRec................ : 00:00:00.333  00:00:00.340  00:00:00.330
 Task CYLDCH....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:13:08.332  00:13:08.009  00:13:07.899
  SubTask CYLDCHHitRec............. : 00:00:16.497  00:00:16.220  00:00:16.190
  SubTask CYLDCHTrackFinderPR...... : 00:59:29.506  00:59:27.939  00:59:27.899
  SubTask DCHKalmanFilterGEN....... : 00:12:11.778  00:12:11.290  00:12:11.250
  SubTask DCHTrackReFit............ : 00:00:00.223  00:00:00.220  00:00:00.200
 Task DCHSPX....................... : 00:00:00.010  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:04:14.270  00:04:14.139  00:04:14.129
  SubTask DCHSPXReFit.............. : 00:02:17.940  00:02:17.979  00:02:17.949
  SubTask SPXTracking.............. : 00:00:35.394  00:00:35.230  00:00:35.210
  SubTask DCHSPXTrackSelection..... : 00:00:00.200  00:00:00.240  00:00:00.200
 WriteEvent........................ : 00:00:17.581  00:00:17.029
```
