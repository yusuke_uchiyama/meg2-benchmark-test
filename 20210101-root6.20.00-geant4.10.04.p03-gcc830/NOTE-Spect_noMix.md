2021/01/18,28, 02/05

# Bartender output with Spect_noMix.

* (realistic)
   * A signal positron event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Run number convention: 115**  
     10 gem4 runs (sev files) are summed up in a bartender run.  
     Originally 200*10 signal events are generated in gem4 but  
     only events satisfied gem4 event-selection are recorded.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.
   * CDCH noise from spectrum measured in 2020 with standard FE.
   * Electron attachment by O2 at different fractions
       - 11500-11501, 11522-11524: 0%
       - 11502-11503: 0.5%
       - 11504-11505: 1%
       - 11506-11507: 2%
   * Different cathode wire thickness (0% O2)
       - 11510-11511, 11532-11534: all 50 um
       - 11512-11513, 11542-11544: all 60 um

3febb066bc8c495958c5252298f967a4ea67598b

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix_realistic.xml","/meg/data1/shared/mc/benchmark_test/20210101-root6.20.00-geant4.10.04.p03-gcc830", 11500, 8, "-p short")'
```

```
./macros/bartender_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
cd 20210101-root6.20.00-geant4.10.04.p03-gcc830
cat NOTE-Spect_noMix_realistic.md >> NOTE-Spect_noMix.md;rm -f NOTE-Spect_noMix_realistic.md
```



### Data and Statistics ###

```
./macros/bartender_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
```
- - - -

16000  events

######### data size #########
```
ls -lh Spect_noMix_realistic | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 398M Jan 18 18:12 raw11500.root
-rw-r--r--. 1 uchiyama unx-lke 398M Jan 18 18:12 raw11501.root
-rw-r--r--. 1 uchiyama unx-lke 389M Jan 18 18:12 raw11502.root
-rw-r--r--. 1 uchiyama unx-lke 391M Jan 18 18:12 raw11503.root
-rw-r--r--. 1 uchiyama unx-lke 391M Jan 18 18:12 raw11504.root
-rw-r--r--. 1 uchiyama unx-lke 386M Jan 18 18:12 raw11505.root
-rw-r--r--. 1 uchiyama unx-lke 377M Jan 18 18:12 raw11506.root
-rw-r--r--. 1 uchiyama unx-lke 377M Jan 18 18:12 raw11507.root
-rw-r--r--. 1 uchiyama unx-lke 278M Jan 18 18:12 sim11500.root
-rw-r--r--. 1 uchiyama unx-lke 276M Jan 18 18:12 sim11501.root
-rw-r--r--. 1 uchiyama unx-lke 272M Jan 18 18:12 sim11502.root
-rw-r--r--. 1 uchiyama unx-lke 277M Jan 18 18:12 sim11503.root
-rw-r--r--. 1 uchiyama unx-lke 281M Jan 18 18:12 sim11504.root
-rw-r--r--. 1 uchiyama unx-lke 271M Jan 18 18:12 sim11505.root
-rw-r--r--. 1 uchiyama unx-lke 269M Jan 18 18:12 sim11506.root
-rw-r--r--. 1 uchiyama unx-lke 271M Jan 18 18:12 sim11507.root

sim	145.241 kB/event
track	75.789 kB/event
dch	63.39 kB/event
spx	2.616 kB/event
kine	1.521 kB/event

```
######### execution time #########
2348.71/16000 = 0.146795 s/event
```
---------
analyzer........................... : 00:04:59.478  00:04:54.889  00:00:00.000
 DAQ gem4.......................... : 00:00:01.018  00:00:00.199  00:00:00.000
 Task MEGMain...................... : 00:00:00.109  00:00:00.129  00:00:00.109
 Task ReadGEM4..................... : 00:00:05.563  00:00:04.859  00:00:04.780
 Task COMMixEvents................. : 00:00:02.648  00:00:02.430  00:00:02.400
 Task CYLDCHMixEvents.............. : 00:00:04.724  00:00:04.690  00:00:04.690
 Task SPXMixEvents................. : 00:00:00.127  00:00:00.089  00:00:00.079
 Task RDCMixEvents................. : 00:00:00.019  00:00:00.000  00:00:00.000
 Task SPXDigitize.................. : 00:00:32.589  00:00:32.479  00:00:30.389
 Task CYLDCHDigitize............... : 00:03:07.812  00:03:07.349  00:03:07.279
 Task RDCDigitize.................. : 00:00:01.410  00:00:01.359  00:00:00.429
 WriteEvent........................ : 00:01:00.325  00:00:59.300
analyzer........................... : 00:05:03.949  00:04:58.640  00:00:00.000
 DAQ gem4.......................... : 00:00:00.987  00:00:00.200  00:00:00.000
 Task MEGMain...................... : 00:00:00.106  00:00:00.089  00:00:00.069
 Task ReadGEM4..................... : 00:00:05.548  00:00:04.939  00:00:04.859
 Task COMMixEvents................. : 00:00:02.640  00:00:02.559  00:00:02.559
 Task CYLDCHMixEvents.............. : 00:00:04.753  00:00:04.789  00:00:04.779
 Task SPXMixEvents................. : 00:00:00.125  00:00:00.070  00:00:00.070
 Task RDCMixEvents................. : 00:00:00.019  00:00:00.019  00:00:00.009
 Task SPXDigitize.................. : 00:00:32.055  00:00:32.130  00:00:30.000
 Task CYLDCHDigitize............... : 00:03:12.681  00:03:11.959  00:03:11.879
 Task RDCDigitize.................. : 00:00:01.394  00:00:01.239  00:00:00.309
 WriteEvent........................ : 00:01:00.514  00:00:58.760
analyzer........................... : 00:04:56.712  00:04:51.739  00:00:00.000
 DAQ gem4.......................... : 00:00:01.052  00:00:00.179  00:00:00.000
 Task MEGMain...................... : 00:00:00.105  00:00:00.130  00:00:00.110
 Task ReadGEM4..................... : 00:00:05.446  00:00:05.010  00:00:04.930
 Task COMMixEvents................. : 00:00:02.614  00:00:02.600  00:00:02.600
 Task CYLDCHMixEvents.............. : 00:00:04.707  00:00:04.859  00:00:04.849
 Task SPXMixEvents................. : 00:00:00.123  00:00:00.179  00:00:00.179
 Task RDCMixEvents................. : 00:00:00.019  00:00:00.009  00:00:00.009
 Task SPXDigitize.................. : 00:00:31.981  00:00:31.710  00:00:29.620
 Task CYLDCHDigitize............... : 00:03:06.399  00:03:06.060  00:03:05.990
 Task RDCDigitize.................. : 00:00:01.390  00:00:01.349  00:00:00.409
 WriteEvent........................ : 00:00:59.849  00:00:57.779
analyzer........................... : 00:04:59.473  00:04:53.810  00:00:00.000
 DAQ gem4.......................... : 00:00:01.077  00:00:00.229  00:00:00.009
 Task MEGMain...................... : 00:00:00.108  00:00:00.150  00:00:00.090
 Task ReadGEM4..................... : 00:00:05.552  00:00:05.140  00:00:05.060
 Task COMMixEvents................. : 00:00:02.643  00:00:02.500  00:00:02.480
 Task CYLDCHMixEvents.............. : 00:00:04.754  00:00:04.580  00:00:04.580
 Task SPXMixEvents................. : 00:00:00.125  00:00:00.089  00:00:00.079
 Task RDCMixEvents................. : 00:00:00.019  00:00:00.019  00:00:00.009
 Task SPXDigitize.................. : 00:00:32.292  00:00:32.259  00:00:30.169
 Task CYLDCHDigitize............... : 00:03:07.463  00:03:06.900  00:03:06.830
 Task RDCDigitize.................. : 00:00:01.390  00:00:01.249  00:00:00.339
 WriteEvent........................ : 00:01:00.882  00:00:58.699
analyzer........................... : 00:04:55.081  00:04:50.369  00:00:00.000
 DAQ gem4.......................... : 00:00:01.046  00:00:00.210  00:00:00.000
 Task MEGMain...................... : 00:00:00.111  00:00:00.119  00:00:00.079
 Task ReadGEM4..................... : 00:00:05.637  00:00:05.179  00:00:05.119
 Task COMMixEvents................. : 00:00:02.710  00:00:02.359  00:00:02.349
 Task CYLDCHMixEvents.............. : 00:00:04.830  00:00:04.919  00:00:04.919
 Task SPXMixEvents................. : 00:00:00.125  00:00:00.160  00:00:00.130
 Task RDCMixEvents................. : 00:00:00.019  00:00:00.019  00:00:00.019
 Task SPXDigitize.................. : 00:00:32.151  00:00:32.060  00:00:30.000
 Task CYLDCHDigitize............... : 00:03:03.827  00:03:03.169  00:03:03.099
 Task RDCDigitize.................. : 00:00:01.391  00:00:01.350  00:00:00.440
 WriteEvent........................ : 00:01:00.221  00:00:58.750
analyzer........................... : 00:04:48.889  00:04:43.930  00:00:00.000
 DAQ gem4.......................... : 00:00:01.011  00:00:00.189  00:00:00.000
 Task MEGMain...................... : 00:00:00.098  00:00:00.079  00:00:00.069
 Task ReadGEM4..................... : 00:00:05.465  00:00:04.810  00:00:04.740
 Task COMMixEvents................. : 00:00:02.654  00:00:02.659  00:00:02.639
 Task CYLDCHMixEvents.............. : 00:00:04.672  00:00:04.590  00:00:04.580
 Task SPXMixEvents................. : 00:00:00.124  00:00:00.100  00:00:00.090
 Task RDCMixEvents................. : 00:00:00.019  00:00:00.009  00:00:00.000
 Task SPXDigitize.................. : 00:00:31.394  00:00:31.150  00:00:29.080
 Task CYLDCHDigitize............... : 00:02:59.439  00:02:59.070  00:02:58.980
 Task RDCDigitize.................. : 00:00:01.390  00:00:01.469  00:00:00.519
 WriteEvent........................ : 00:00:59.521  00:00:57.609
analyzer........................... : 00:04:41.977  00:04:37.209  00:00:00.000
 DAQ gem4.......................... : 00:00:01.025  00:00:00.200  00:00:00.000
 Task MEGMain...................... : 00:00:00.108  00:00:00.110  00:00:00.100
 Task ReadGEM4..................... : 00:00:05.476  00:00:05.239  00:00:05.169
 Task COMMixEvents................. : 00:00:02.654  00:00:02.450  00:00:02.450
 Task CYLDCHMixEvents.............. : 00:00:04.714  00:00:04.619  00:00:04.609
 Task SPXMixEvents................. : 00:00:00.125  00:00:00.150  00:00:00.140
 Task RDCMixEvents................. : 00:00:00.019  00:00:00.009  00:00:00.000
 Task SPXDigitize.................. : 00:00:31.847  00:00:31.779  00:00:29.709
 Task CYLDCHDigitize............... : 00:02:52.239  00:02:51.800  00:02:51.710
 Task RDCDigitize.................. : 00:00:01.393  00:00:01.409  00:00:00.489
 WriteEvent........................ : 00:00:59.350  00:00:57.480
analyzer........................... : 00:04:43.155  00:04:38.780  00:00:00.000
 DAQ gem4.......................... : 00:00:00.974  00:00:00.210  00:00:00.000
 Task MEGMain...................... : 00:00:00.105  00:00:00.109  00:00:00.089
 Task ReadGEM4..................... : 00:00:05.545  00:00:05.019  00:00:04.919
 Task COMMixEvents................. : 00:00:02.643  00:00:02.649  00:00:02.649
 Task CYLDCHMixEvents.............. : 00:00:04.753  00:00:04.660  00:00:04.640
 Task SPXMixEvents................. : 00:00:00.126  00:00:00.179  00:00:00.169
 Task RDCMixEvents................. : 00:00:00.019  00:00:00.019  00:00:00.010
 Task SPXDigitize.................. : 00:00:32.465  00:00:32.299  00:00:30.179
 Task CYLDCHDigitize............... : 00:02:53.120  00:02:52.570  00:02:52.480
 Task RDCDigitize.................. : 00:00:01.398  00:00:01.330  00:00:00.390
 WriteEvent........................ : 00:00:58.827  00:00:57.639
```
