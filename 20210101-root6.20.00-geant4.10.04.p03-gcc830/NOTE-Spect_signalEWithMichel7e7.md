2021/01/18,28

# Bartender output with Spect_signalE + Full_michel mixed.

* (realistic)
   * A signal event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Michel events are randomly mixed at rate of 7e7 Hz in time window (-350,350) ns.
   * Run number convention: 116**  
     10 gem4 runs (sev files) are summed up in a bartender run.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.
   * CDCH noise from spectrum measured in 2020 with standard FE.   
   * Electron attachment by O2 at different fractions
       - 11600-11601, 11622-11624: 0%
       - 11602-11603: 0.5%
       - 11604-11605: 1%
       - 11606-11607: 2%
   * Different cathode wire thickness (0% O2)
       - 11610-11611, 11632-11634: all 50 um
       - 11612-11613, 11642-11644: all 60 um

3febb066bc8c495958c5252298f967a4ea67598b

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_signalEWithMichel7e7_realistic.xml","/meg/data1/shared/mc/benchmark_test/20210101-root6.20.00-geant4.10.04.p03-gcc830", 11600, 8, "")'
```

```
./macros/bartender_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
cd 20210101-root6.20.00-geant4.10.04.p03-gcc830
cat NOTE-Spect_signalEWithMichel7e7_realistic.md >> NOTE-Spect_signalEWithMichel7e7.md;rm -f NOTE-Spect_signalEWithMichel7e7_realistic.md
```


### Data and Statistics ###


```
./macros/bartender_output.sh 20210101-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
```
- - - -

16000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7_realistic | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 3.1G Jan 18 18:57 raw11600.root
-rw-r--r--. 1 uchiyama unx-lke 3.1G Jan 18 18:57 raw11601.root
-rw-r--r--. 1 uchiyama unx-lke 3.1G Jan 18 18:55 raw11602.root
-rw-r--r--. 1 uchiyama unx-lke 3.1G Jan 18 18:55 raw11603.root
-rw-r--r--. 1 uchiyama unx-lke 3.0G Jan 18 18:54 raw11604.root
-rw-r--r--. 1 uchiyama unx-lke 3.1G Jan 18 18:54 raw11605.root
-rw-r--r--. 1 uchiyama unx-lke 3.0G Jan 18 18:51 raw11606.root
-rw-r--r--. 1 uchiyama unx-lke 3.0G Jan 18 18:52 raw11607.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 18 18:57 sim11600.root
-rw-r--r--. 1 uchiyama unx-lke 5.7G Jan 18 18:57 sim11601.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 18 18:55 sim11602.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 18 18:55 sim11603.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 18 18:54 sim11604.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 18 18:54 sim11605.root
-rw-r--r--. 1 uchiyama unx-lke 5.5G Jan 18 18:51 sim11606.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 18 18:52 sim11607.root

sim	2984.36 kB/event
track	1718.79 kB/event
dch	1215.45 kB/event
kine	27.375 kB/event
spx	9.03 kB/event

```
######### execution time #########
21884.6/16000 = 1.36779 s/event
```
---------
analyzer........................... : 00:48:02.916  00:47:12.690  00:00:00.000
 DAQ gem4.......................... : 00:00:01.808  00:00:00.299  00:00:00.009
 Task MEGMain...................... : 00:00:00.134  00:00:00.130  00:00:00.110
 Task ReadGEM4..................... : 00:04:42.612  00:04:26.880  00:04:26.730
 Task COMMixEvents................. : 00:01:19.477  00:01:19.049  00:01:19.029
 Task CYLDCHMixEvents.............. : 00:01:17.161  00:01:17.029  00:01:17.019
 Task SPXMixEvents................. : 00:00:00.502  00:00:00.649  00:00:00.649
 Task RDCMixEvents................. : 00:00:00.175  00:00:00.149  00:00:00.139
 Task SPXDigitize.................. : 00:01:06.881  00:01:06.280  00:01:04.310
 Task CYLDCHDigitize............... : 00:31:31.069  00:31:26.610  00:31:26.540
 Task RDCDigitize.................. : 00:00:08.641  00:00:08.580  00:00:07.750
 WriteEvent........................ : 00:07:32.872  00:07:13.759
analyzer........................... : 00:48:02.530  00:47:07.590  00:00:00.000
 DAQ gem4.......................... : 00:00:01.814  00:00:00.299  00:00:00.000
 Task MEGMain...................... : 00:00:00.141  00:00:00.079  00:00:00.059
 Task ReadGEM4..................... : 00:04:43.725  00:04:26.599  00:04:26.419
 Task COMMixEvents................. : 00:01:20.321  00:01:20.389  00:01:20.349
 Task CYLDCHMixEvents.............. : 00:01:17.607  00:01:17.250  00:01:17.220
 Task SPXMixEvents................. : 00:00:00.505  00:00:00.440  00:00:00.440
 Task RDCMixEvents................. : 00:00:00.177  00:00:00.120  00:00:00.100
 Task SPXDigitize.................. : 00:01:07.102  00:01:06.960  00:01:04.930
 Task CYLDCHDigitize............... : 00:31:20.964  00:31:16.269  00:31:16.199
 Task RDCDigitize.................. : 00:00:08.766  00:00:08.800  00:00:07.930
 WriteEvent........................ : 00:07:39.520  00:07:16.750
analyzer........................... : 00:45:55.754  00:45:06.830  00:00:00.000
 DAQ gem4.......................... : 00:00:01.743  00:00:00.289  00:00:00.000
 Task MEGMain...................... : 00:00:00.131  00:00:00.069  00:00:00.049
 Task ReadGEM4..................... : 00:04:35.909  00:04:19.819  00:04:19.629
 Task COMMixEvents................. : 00:01:19.115  00:01:18.989  00:01:18.959
 Task CYLDCHMixEvents.............. : 00:01:17.116  00:01:16.880  00:01:16.810
 Task SPXMixEvents................. : 00:00:00.492  00:00:00.500  00:00:00.490
 Task RDCMixEvents................. : 00:00:00.174  00:00:00.150  00:00:00.130
 Task SPXDigitize.................. : 00:01:05.763  00:01:05.840  00:01:03.839
 Task CYLDCHDigitize............... : 00:29:35.908  00:29:31.279  00:29:31.219
 Task RDCDigitize.................. : 00:00:08.700  00:00:08.800  00:00:07.960
 WriteEvent........................ : 00:07:29.317  00:07:11.460
analyzer........................... : 00:46:29.004  00:45:40.510  00:00:00.000
 DAQ gem4.......................... : 00:00:01.704  00:00:00.299  00:00:00.000
 Task MEGMain...................... : 00:00:00.132  00:00:00.179  00:00:00.130
 Task ReadGEM4..................... : 00:04:42.053  00:04:25.549  00:04:25.339
 Task COMMixEvents................. : 00:01:20.123  00:01:19.850  00:01:19.820
 Task CYLDCHMixEvents.............. : 00:01:17.599  00:01:17.440  00:01:17.390
 Task SPXMixEvents................. : 00:00:00.502  00:00:00.529  00:00:00.529
 Task RDCMixEvents................. : 00:00:00.177  00:00:00.179  00:00:00.169
 Task SPXDigitize.................. : 00:01:06.820  00:01:06.740  00:01:04.720
 Task CYLDCHDigitize............... : 00:29:59.291  00:29:54.759  00:29:54.699
 Task RDCDigitize.................. : 00:00:08.726  00:00:08.839  00:00:07.999
 WriteEvent........................ : 00:07:30.123  00:07:13.060
analyzer........................... : 00:45:15.787  00:44:25.039  00:00:00.000
 DAQ gem4.......................... : 00:00:01.752  00:00:00.279  00:00:00.000
 Task MEGMain...................... : 00:00:00.135  00:00:00.090  00:00:00.090
 Task ReadGEM4..................... : 00:04:44.022  00:04:27.569  00:04:27.369
 Task COMMixEvents................. : 00:01:20.651  00:01:20.589  00:01:20.569
 Task CYLDCHMixEvents.............. : 00:01:17.964  00:01:17.720  00:01:17.690
 Task SPXMixEvents................. : 00:00:00.503  00:00:00.569  00:00:00.539
 Task RDCMixEvents................. : 00:00:00.176  00:00:00.170  00:00:00.170
 Task SPXDigitize.................. : 00:01:06.775  00:01:06.539  00:01:04.579
 Task CYLDCHDigitize............... : 00:28:40.506  00:28:36.149  00:28:36.099
 Task RDCDigitize.................. : 00:00:08.843  00:00:08.890  00:00:08.020
 WriteEvent........................ : 00:07:32.691  00:07:13.700
analyzer........................... : 00:45:19.749  00:44:33.690  00:00:00.000
 DAQ gem4.......................... : 00:00:01.729  00:00:00.290  00:00:00.000
 Task MEGMain...................... : 00:00:00.135  00:00:00.149  00:00:00.119
 Task ReadGEM4..................... : 00:04:43.566  00:04:26.829  00:04:26.689
 Task COMMixEvents................. : 00:01:19.998  00:01:19.750  00:01:19.710
 Task CYLDCHMixEvents.............. : 00:01:18.338  00:01:18.100  00:01:18.090
 Task SPXMixEvents................. : 00:00:00.502  00:00:00.300  00:00:00.290
 Task RDCMixEvents................. : 00:00:00.174  00:00:00.189  00:00:00.179
 Task SPXDigitize.................. : 00:01:06.635  00:01:06.679  00:01:04.669
 Task CYLDCHDigitize............... : 00:28:52.711  00:28:48.090  00:28:48.050
 Task RDCDigitize.................. : 00:00:08.700  00:00:08.779  00:00:07.919
 WriteEvent........................ : 00:07:25.381  00:07:11.790
analyzer........................... : 00:42:28.799  00:41:44.400  00:00:00.000
 DAQ gem4.......................... : 00:00:01.751  00:00:00.309  00:00:00.000
 Task MEGMain...................... : 00:00:00.134  00:00:00.140  00:00:00.110
 Task ReadGEM4..................... : 00:04:40.440  00:04:24.619  00:04:24.449
 Task COMMixEvents................. : 00:01:19.313  00:01:18.990  00:01:18.960
 Task CYLDCHMixEvents.............. : 00:01:17.225  00:01:17.119  00:01:17.119
 Task SPXMixEvents................. : 00:00:00.492  00:00:00.549  00:00:00.539
 Task RDCMixEvents................. : 00:00:00.174  00:00:00.220  00:00:00.210
 Task SPXDigitize.................. : 00:01:05.755  00:01:05.360  00:01:03.360
 Task CYLDCHDigitize............... : 00:26:12.920  00:26:08.899  00:26:08.839
 Task RDCDigitize.................. : 00:00:08.700  00:00:08.930  00:00:08.090
 WriteEvent........................ : 00:07:20.437  00:07:06.079
analyzer........................... : 00:43:10.047  00:42:26.449  00:00:00.000
 DAQ gem4.......................... : 00:00:01.768  00:00:00.279  00:00:00.009
 Task MEGMain...................... : 00:00:00.135  00:00:00.189  00:00:00.169
 Task ReadGEM4..................... : 00:04:40.273  00:04:23.910  00:04:23.720
 Task COMMixEvents................. : 00:01:19.730  00:01:19.399  00:01:19.369
 Task CYLDCHMixEvents.............. : 00:01:18.488  00:01:18.219  00:01:18.209
 Task SPXMixEvents................. : 00:00:00.503  00:00:00.559  00:00:00.549
 Task RDCMixEvents................. : 00:00:00.176  00:00:00.129  00:00:00.119
 Task SPXDigitize.................. : 00:01:07.050  00:01:07.220  00:01:05.240
 Task CYLDCHDigitize............... : 00:26:49.229  00:26:45.510  00:26:45.480
 Task RDCDigitize.................. : 00:00:08.730  00:00:08.310  00:00:07.470
 WriteEvent........................ : 00:07:21.754  00:07:09.889
```
