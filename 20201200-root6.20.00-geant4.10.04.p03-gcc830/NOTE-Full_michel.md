2020/12/01

# Full detetctor (default incl. DS-RDC) 
- Photon tracking in SPX.
- 9 layer CDCH
- Custom run number 2000116

Michel posiron generated on target in 4pi

No event selection in gem4

d66b10240eacb8acf45e2bd2c20743dc474e19a8

## LOG ##
```
 root -q 'macros/benchmark/JobSubmit.C("macros/benchmark/Full_michel.mac", "", "/meg/data1/shared/mc/benchmark_test/20201200-root6.20.00-geant4.10.04.p03-gcc830/gem4Out", 10000, 12000, 20, "-p short")'
```

#12000-12019 

```
% du -cm gem4Out/sev12*
551     gem4Out/sev12000.root
538     gem4Out/sev12001.root
554     gem4Out/sev12002.root
538     gem4Out/sev12003.root
541     gem4Out/sev12004.root
557     gem4Out/sev12005.root
542     gem4Out/sev12006.root
552     gem4Out/sev12007.root
557     gem4Out/sev12008.root
544     gem4Out/sev12009.root
543     gem4Out/sev12010.root
558     gem4Out/sev12011.root
540     gem4Out/sev12012.root
550     gem4Out/sev12013.root
536     gem4Out/sev12014.root
545     gem4Out/sev12015.root
546     gem4Out/sev12016.root
550     gem4Out/sev12017.root
545     gem4Out/sev12018.root
536     gem4Out/sev12019.root
10915   total
```
```
% grep 'User=' gem4Out/log/g1200* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
32064.1/200000 = 0.16032s/event
```
```
megbartender [1] sev->Print("toponly")

```