2020/12/01

# Bartender output with Spect_noMix.

* (realistic)
   * A signal positron event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Run number convention: 115**  
     10 gem4 runs (sev files) are summed up in a bartender run.  
     Originally 200*10 signal events are generated in gem4 but  
     only events satisfied gem4 event-selection are recorded.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.

2da7e70d1104704916a462c4a60cb298e6e300f4

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix_realistic.xml","/meg/data1/shared/mc/benchmark_test/20201200-root6.20.00-geant4.10.04.p03-gcc830", 11500, 2, "-p short")'
```

```
./macros/bartender_output.sh 20201200-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
cd 20201200-root6.20.00-geant4.10.04.p03-gcc830
cat NOTE-Spect_noMix_realistic.md >> NOTE-Spect_noMix.md;rm -f NOTE-Spect_noMix_realistic.md
```



### Data and Statistics ###


```
./macros/bartender_output.sh 20201200-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_noMix_realistic | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 392M Dec  1 12:42 raw11500.root
-rw-r--r--. 1 uchiyama unx-lke 392M Dec  1 12:42 raw11501.root
-rw-r--r--. 1 uchiyama unx-lke 278M Dec  1 12:42 sim11500.root
-rw-r--r--. 1 uchiyama unx-lke 275M Dec  1 12:42 sim11501.root

sim	145.242 kB/event
track	75.789 kB/event
dch	63.389 kB/event
spx	2.616 kB/event
kine	1.521 kB/event

```
######### execution time #########
709.37/4000 = 0.177343 s/event
```
---------
analyzer........................... : 00:05:53.476  00:05:47.509  00:00:00.000
 DAQ gem4.......................... : 00:00:00.974  00:00:00.229  00:00:00.009
 Task MEGMain...................... : 00:00:00.131  00:00:00.120  00:00:00.110
 Task ReadGEM4..................... : 00:00:06.326  00:00:06.050  00:00:05.980
 Task COMMixEvents................. : 00:00:03.230  00:00:03.110  00:00:03.080
 Task CYLDCHMixEvents.............. : 00:00:05.226  00:00:05.270  00:00:05.230
 Task SPXMixEvents................. : 00:00:00.165  00:00:00.180  00:00:00.180
 Task RDCMixEvents................. : 00:00:00.025  00:00:00.010  00:00:00.010
 Task SPXDigitize.................. : 00:00:38.539  00:00:38.329  00:00:35.899
 Task CYLDCHDigitize............... : 00:03:40.933  00:03:40.050  00:03:40.010
 Task RDCDigitize.................. : 00:00:01.656  00:00:01.629  00:00:00.559
 WriteEvent........................ : 00:01:12.738  00:01:10.189
analyzer........................... : 00:05:55.894  00:05:50.569  00:00:00.000
 DAQ gem4.......................... : 00:00:00.967  00:00:00.200  00:00:00.000
 Task MEGMain...................... : 00:00:00.137  00:00:00.180  00:00:00.170
 Task ReadGEM4..................... : 00:00:06.382  00:00:06.090  00:00:05.990
 Task COMMixEvents................. : 00:00:03.224  00:00:03.340  00:00:03.310
 Task CYLDCHMixEvents.............. : 00:00:05.250  00:00:05.319  00:00:05.309
 Task SPXMixEvents................. : 00:00:00.163  00:00:00.150  00:00:00.130
 Task RDCMixEvents................. : 00:00:00.025  00:00:00.040  00:00:00.030
 Task SPXDigitize.................. : 00:00:38.229  00:00:37.910  00:00:35.460
 Task CYLDCHDigitize............... : 00:03:44.177  00:03:43.119  00:03:43.059
 Task RDCDigitize.................. : 00:00:01.654  00:00:01.680  00:00:00.590
 WriteEvent........................ : 00:01:12.133  00:01:09.980
```
