2020/12/04

# Analyzer output with noxec_PR_fit_genfit.xml
* No XEC reconstruction
* SPX analysis from WaveformAnalysis (cf30)
* CDC analysis from waveform (WaveformAnalysis), pattern recognition and fit with GENFIT
* Custom run number 2000116
* Hit z reconstruction used in CYLDCHTrackFinderPR is time-difference
* CYLDCHWaveformAnalysis2 (with not fully optimized parameters).
    * Update TimeWalkCoefficient (to 3.6e-10) and GlobalTimeOffset (to -5)

mc_20201201.1

## Run Numbers ##
11500-11501 Spect_signalEOnly_realistic  
11600-11601 Spect_signalEWithMichel7e7_realistic  


## Submit jobs ##
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20201200-root6.20.00-geant4.10.04.p03-gcc830/", 11500, 2)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20201200-root6.20.00-geant4.10.04.p03-gcc830/", 11600, 2)'
```



## Calculate positron efficiency ##
### Realistic ###

```
RUNNUM=11500;NRUN=2;DATASET=20201200-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2628 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2403 (91.4384 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2331 (88.6986 %)
|   |                         
|   | propagation cut         
|   V                         
|  2295 (87.3288 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  2184 (83.105 %)
|   |                         
|   | matching                
|   V                         
|  2153 (81.9254 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  2153 (81.9254 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  2107 (80.175 %)
|      + 0.784868
|      - 0.808389
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.03542
|  phi     (mrad)  5.82367
|  momentum (MeV)  0.0784509
|  vertexZ   (mm)  1.42901
|  vertexY   (mm)  0.709681
|  time     (sec)  4.53084e-11
@-----------------------------@
```

```
RUNNUM=11600;NRUN=2;DATASET=20201200-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2628 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2403 (91.4384 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2231 (84.8935 %)
|   |                         
|   | propagation cut         
|   V                         
|  2097 (79.7945 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1781 (67.7702 %)
|   |                         
|   | matching                
|   V                         
|  1748 (66.5145 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1747 (66.4764 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1701 (64.726 %)
|      + 0.945218
|      - 0.956651
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.66522
|  phi     (mrad)  6.35921
|  momentum (MeV)  0.0895551
|  vertexZ   (mm)  1.66093
|  vertexY   (mm)  0.79572
|  time     (sec)  4.51185e-11
@-----------------------------@
```


### Analyze execution and output ###
```
cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20201200-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20201200-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```

```
./macros/analyzer_output.sh 20201200-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115
```
- - - -

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Dec  4 12:14 Eff_results11500.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Dec  4 00:35 histos11500.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Dec  4 00:34 histos11501.root
-rw-r--r--. 1 uchiyama unx-lke  60M Dec  4 00:35 rec11500.root
-rw-r--r--. 1 uchiyama unx-lke  60M Dec  4 00:34 rec11501.root

rec	31.078 kB/event
dch	27.035 kB/event
spx	3.493 kB/event
reco	0.243 kB/event
rdc	0.216 kB/event

```
######### execution time #########
1092.26/4000 = 0.273066 s/event
```
---------
analyzer........................... : 00:09:12.622  00:09:09.039  00:00:00.000
 DAQ bartender..................... : 00:00:54.053  00:00:52.690  00:00:52.440
 Task MEGMain...................... : 00:00:01.077  00:00:00.990  00:00:00.000
 Task ReadData..................... : 00:00:08.220  00:00:08.309  00:00:08.249
 Task SPX.......................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.475  00:00:04.469  00:00:04.419
  SubTask SPXHitRec................ : 00:00:00.272  00:00:00.280  00:00:00.280
  SubTask SPXClustering............ : 00:00:06.735  00:00:06.439  00:00:06.389
  SubTask SPXIndependentTracking... : 00:00:26.225  00:00:26.290  00:00:26.280
 Task RDC.......................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.077  00:00:00.070  00:00:00.060
  SubTask RDCHitRec................ : 00:00:00.038  00:00:00.030  00:00:00.030
 Task CYLDCH....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:21.667  00:01:21.529  00:01:21.529
  SubTask CYLDCHHitRec............. : 00:00:00.454  00:00:00.550  00:00:00.540
  SubTask CYLDCHTrackFinderPR...... : 00:02:30.159  00:02:29.389  00:02:29.359
  SubTask DCHKalmanFilterGEN....... : 00:01:07.063  00:01:07.100  00:01:07.090
  SubTask DCHTrackReFit............ : 00:00:00.128  00:00:00.109  00:00:00.099
 Task DCHSPX....................... : 00:00:00.010  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:54.159  00:00:54.019  00:00:54.019
  SubTask DCHSPXReFit.............. : 00:01:09.854  00:01:09.539  00:01:09.529
  SubTask SPXTracking.............. : 00:00:17.681  00:00:17.620  00:00:17.600
  SubTask DCHSPXTrackSelection..... : 00:00:00.063  00:00:00.070  00:00:00.070
 WriteEvent........................ : 00:00:05.671  00:00:05.709
analyzer........................... : 00:08:59.641  00:08:55.289  00:00:00.000
 DAQ bartender..................... : 00:00:54.038  00:00:51.800  00:00:51.560
 Task MEGMain...................... : 00:00:01.040  00:00:00.959  00:00:00.000
 Task ReadData..................... : 00:00:08.239  00:00:08.259  00:00:08.209
 Task SPX.......................... : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.347  00:00:04.450  00:00:04.410
  SubTask SPXHitRec................ : 00:00:00.265  00:00:00.269  00:00:00.259
  SubTask SPXClustering............ : 00:00:06.577  00:00:06.129  00:00:06.089
  SubTask SPXIndependentTracking... : 00:00:26.320  00:00:26.219  00:00:26.189
 Task RDC.......................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.073  00:00:00.050  00:00:00.050
  SubTask RDCHitRec................ : 00:00:00.037  00:00:00.060  00:00:00.040
 Task CYLDCH....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:21.286  00:01:21.340  00:01:21.310
  SubTask CYLDCHHitRec............. : 00:00:00.458  00:00:00.390  00:00:00.380
  SubTask CYLDCHTrackFinderPR...... : 00:02:20.202  00:02:19.579  00:02:19.549
  SubTask DCHKalmanFilterGEN....... : 00:01:07.630  00:01:07.510  00:01:07.480
  SubTask DCHTrackReFit............ : 00:00:00.126  00:00:00.139  00:00:00.120
 Task DCHSPX....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:51.686  00:00:51.649  00:00:51.629
  SubTask DCHSPXReFit.............. : 00:01:10.820  00:01:10.300  00:01:10.290
  SubTask SPXTracking.............. : 00:00:16.180  00:00:16.349  00:00:16.339
  SubTask DCHSPXTrackSelection..... : 00:00:00.061  00:00:00.079  00:00:00.059
 WriteEvent........................ : 00:00:05.812  00:00:05.680
```

```
./macros/analyzer_output.sh 20201200-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```
- - - -

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Dec  4 12:17 Eff_results11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Dec  4 01:48 histos11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Dec  4 01:49 histos11601.root
-rw-r--r--. 1 uchiyama unx-lke 225M Dec  4 01:48 rec11600.root
-rw-r--r--. 1 uchiyama unx-lke 226M Dec  4 01:49 rec11601.root

rec	117.587 kB/event
dch	107.515 kB/event
spx	6.058 kB/event
rdc	2.968 kB/event
reco	0.746 kB/event

```
######### execution time #########
9989.09/4000 = 2.49727 s/event
```
---------
analyzer........................... : 01:22:26.775  01:21:37.679  00:00:00.000
 DAQ bartender..................... : 00:03:47.973  00:03:13.570  00:03:12.520
 Task MEGMain...................... : 00:00:01.030  00:00:00.989  00:00:00.009
 Task ReadData..................... : 00:00:24.454  00:00:24.669  00:00:24.609
 Task SPX.......................... : 00:00:00.065  00:00:00.020  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:08.148  00:00:08.039  00:00:07.989
  SubTask SPXHitRec................ : 00:00:00.298  00:00:00.370  00:00:00.360
  SubTask SPXClustering............ : 00:00:11.826  00:00:11.740  00:00:11.700
  SubTask SPXIndependentTracking... : 00:00:41.872  00:00:41.819  00:00:41.809
 Task RDC.......................... : 00:00:00.014  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:21.900  00:00:21.800  00:00:21.800
  SubTask RDCHitRec................ : 00:00:00.134  00:00:00.180  00:00:00.170
 Task CYLDCH....................... : 00:00:00.010  00:00:00.009  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:09:16.110  00:09:14.460  00:09:14.420
  SubTask CYLDCHHitRec............. : 00:00:11.647  00:00:11.649  00:00:11.639
  SubTask CYLDCHTrackFinderPR...... : 00:50:51.412  00:50:43.900  00:50:43.870
  SubTask DCHKalmanFilterGEN....... : 00:10:24.460  00:10:23.000  00:10:22.950
  SubTask DCHTrackReFit............ : 00:00:00.139  00:00:00.130  00:00:00.100
 Task DCHSPX....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:22.864  00:03:22.539  00:03:22.539
  SubTask DCHSPXReFit.............. : 00:01:48.640  00:01:47.979  00:01:47.969
  SubTask SPXTracking.............. : 00:00:27.166  00:00:27.069  00:00:27.049
  SubTask DCHSPXTrackSelection..... : 00:00:00.130  00:00:00.160  00:00:00.150
 WriteEvent........................ : 00:00:16.024  00:00:14.310
analyzer........................... : 01:24:02.313  01:23:13.800  00:00:00.000
 DAQ bartender..................... : 00:03:47.894  00:03:13.920  00:03:12.870
 Task MEGMain...................... : 00:00:01.054  00:00:00.989  00:00:00.000
 Task ReadData..................... : 00:00:23.943  00:00:23.659  00:00:23.599
 Task SPX.......................... : 00:00:00.014  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.089  00:00:08.659  00:00:08.619
  SubTask SPXHitRec................ : 00:00:00.296  00:00:00.339  00:00:00.339
  SubTask SPXClustering............ : 00:00:11.124  00:00:10.659  00:00:10.609
  SubTask SPXIndependentTracking... : 00:00:40.865  00:00:40.829  00:00:40.789
 Task RDC.......................... : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:22.781  00:00:22.739  00:00:22.729
  SubTask RDCHitRec................ : 00:00:00.134  00:00:00.100  00:00:00.090
 Task CYLDCH....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:09:31.360  00:09:29.810  00:09:29.770
  SubTask CYLDCHHitRec............. : 00:00:11.799  00:00:11.909  00:00:11.889
  SubTask CYLDCHTrackFinderPR...... : 00:52:30.228  00:52:22.290  00:52:22.240
  SubTask DCHKalmanFilterGEN....... : 00:09:59.891  00:09:58.380  00:09:58.350
  SubTask DCHTrackReFit............ : 00:00:00.134  00:00:00.120  00:00:00.110
 Task DCHSPX....................... : 00:00:00.011  00:00:00.029  00:00:00.020
  SubTask DCHSPXMatching........... : 00:03:32.233  00:03:31.970  00:03:31.920
  SubTask DCHSPXReFit.............. : 00:01:45.634  00:01:45.169  00:01:45.149
  SubTask SPXTracking.............. : 00:00:28.964  00:00:28.869  00:00:28.859
  SubTask DCHSPXTrackSelection..... : 00:00:00.131  00:00:00.139  00:00:00.139
 WriteEvent........................ : 00:00:15.295  00:00:13.960
```
