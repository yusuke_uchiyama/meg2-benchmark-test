2020/12/01

# Bartender output with Spect_signalE + Full_michel mixed.

* (realistic)
   * A signal event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Michel events are randomly mixed at rate of 7e7 Hz in time window (-350,350) ns.
   * Run number convention: 116**  
     10 gem4 runs (sev files) are summed up in a bartender run.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.

2da7e70d1104704916a462c4a60cb298e6e300f4

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_signalEWithMichel7e7_realistic.xml","/meg/data1/shared/mc/benchmark_test/20201200-root6.20.00-geant4.10.04.p03-gcc830", 11600, 2, "")'
```

```
./macros/bartender_output.sh 20201200-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
cd 20201200-root6.20.00-geant4.10.04.p03-gcc830
cat NOTE-Spect_signalEWithMichel7e7_realistic.md >> NOTE-Spect_signalEWithMichel7e7.md;rm -f NOTE-Spect_signalEWithMichel7e7_realistic.md
```


### Data and Statistics ###

```
./macros/bartender_output.sh 20201200-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7_realistic | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 3.0G Dec  1 14:53 raw11600.root
-rw-r--r--. 1 uchiyama unx-lke 3.1G Dec  1 14:52 raw11601.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Dec  1 14:53 sim11600.root
-rw-r--r--. 1 uchiyama unx-lke 5.7G Dec  1 14:52 sim11601.root

sim	2984.37 kB/event
track	1718.79 kB/event
dch	1215.45 kB/event
kine	27.375 kB/event
spx	9.03 kB/event

```
######### execution time #########
6529.41/4000 = 1.63235 s/event
```
---------
analyzer........................... : 00:54:47.104  00:54:07.570  00:00:00.000
 DAQ gem4.......................... : 00:00:01.813  00:00:00.190  00:00:00.000
 Task MEGMain...................... : 00:00:00.201  00:00:00.160  00:00:00.130
 Task ReadGEM4..................... : 00:07:20.313  00:07:04.549  00:07:04.439
 Task COMMixEvents................. : 00:01:31.395  00:01:31.140  00:01:31.120
 Task CYLDCHMixEvents.............. : 00:01:38.135  00:01:38.410  00:01:38.370
 Task SPXMixEvents................. : 00:00:00.586  00:00:00.519  00:00:00.499
 Task RDCMixEvents................. : 00:00:00.205  00:00:00.169  00:00:00.169
 Task SPXDigitize.................. : 00:01:21.439  00:01:21.190  00:01:18.600
 Task CYLDCHDigitize............... : 00:33:59.400  00:33:56.899  00:33:56.859
 Task RDCDigitize.................. : 00:00:10.691  00:00:11.060  00:00:09.930
 WriteEvent........................ : 00:08:20.925  00:08:09.919
analyzer........................... : 00:54:02.307  00:53:25.960  00:00:00.000
 DAQ gem4.......................... : 00:00:01.819  00:00:00.209  00:00:00.000
 Task MEGMain...................... : 00:00:00.196  00:00:00.179  00:00:00.169
 Task ReadGEM4..................... : 00:07:02.307  00:06:46.060  00:06:45.920
 Task COMMixEvents................. : 00:01:29.819  00:01:30.010  00:01:29.980
 Task CYLDCHMixEvents.............. : 00:01:28.443  00:01:28.229  00:01:28.209
 Task SPXMixEvents................. : 00:00:00.561  00:00:00.499  00:00:00.489
 Task RDCMixEvents................. : 00:00:00.200  00:00:00.149  00:00:00.149
 Task SPXDigitize.................. : 00:01:19.399  00:01:19.290  00:01:16.730
 Task CYLDCHDigitize............... : 00:33:50.004  00:33:49.159  00:33:49.129
 Task RDCDigitize.................. : 00:00:10.690  00:00:10.600  00:00:09.510
 WriteEvent........................ : 00:08:16.140  00:08:08.120
```
