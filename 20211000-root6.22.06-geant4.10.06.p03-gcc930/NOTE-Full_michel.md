2021/10/02

# Full detetctor (default incl. DS-RDC) 
- Photon tracking in SPX.
- 9 layer CDCH
    - 12000-12049
    - Cathode wire for CDCH2
        - Modified codes are 20211000-root6.22.06-geant4.10.06.p03-gcc930/CYLDCHModule_AllAg50(NewCoat).cc (patches are in gem4Out/log/)
        - 12100-12149: all 50 um (same as in 20210101)
	- 12200-12249: all 50 um new coating
- Custom run number 2000116

Michel posiron generated on target in 4pi

No event selection in gem4

data_20210929.0

## LOG ##
```
 root -q 'macros/benchmark/JobSubmit.C("macros/benchmark/Full_michel.mac", "", "/meg/data1/shared/mc/benchmark_test/20211000-root6.22.06-geant4.10.06.p03-gcc930/gem4Out", 10000, 12000, 50, "-p short")'
```

#12000-12049 

```
$ du -cm gem4Out/sev12*
```
```
$ grep 'User=' gem4Out/log/g1200* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
34463.8/200000 = 0.172319s/event
$ grep 'User=' gem4Out/log/g1202* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
31157.2/200000 = 0.155786s/event
$ grep 'User=' gem4Out/log/g1204* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
30656.1/200000 = 0.15328s/event
$ grep 'User=' gem4Out/log/g1206* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
31630.2/200000 = 0.158151s/event
```
```
megbartender [1] sev->Print("toponly")

```