2021/10/02

# Bartender output with Spect_noMix.

* (realistic)
   * A signal positron event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Run number convention: 115**  
     10 gem4 runs (sev files) are summed up in a bartender run.  
     Originally 200*10 signal events are generated in gem4 but  
     only events satisfied gem4 event-selection are recorded.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.
   * CDCH noise from spectrum measured in 2020 with standard FE.
   * Different cathode wires
       - 11500-11504: Standard
       - 11510-11514: AgAl50
       - 11520-11524: AgAl50NewCoat

data_20210929.0

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix_realistic.xml","/meg/data1/shared/mc/benchmark_test/20211000-root6.22.06-geant4.10.06.p03-gcc930", 11500, 5, "-p short")'
```

```
./macros/bartender_output.sh 20211000-root6.22.06-geant4.10.06.p03-gcc930/Spect_noMix_realistic 115
cd 20211000-root6.22.06-geant4.10.06.p03-gcc930
cat NOTE-Spect_noMix_realistic.md >> NOTE-Spect_noMix.md;rm -f NOTE-Spect_noMix_realistic.md
```



### Data and Statistics ###

```
./macros/bartender_output.sh 20211000-root6.22.06-geant4.10.06.p03-gcc930/Spect_noMix_realistic 115
```
