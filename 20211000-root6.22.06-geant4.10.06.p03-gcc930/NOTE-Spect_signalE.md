2021/10/02

# Spectrometer (-d XEC).
- Photon tracking in SPX.
- 9 layer CDCH(2)
    - 11000-11049
    - Cathode wire for CDCH2
        - Modified codes are 20211000-root6.22.06-geant4.10.06.p03-gcc930/CYLDCHModule_AllAg50(NewCoat).cc (patches are in gem4Out/log/)
        - 11100-11149: all 50 um (same as in 20210101)
	- 11200-11249: all 50 um new coating
- Custom run number 2000116

# Signal positron generated on target
- in the 'extended' range
- No event selection in gem4
- Number of events (generation) in a run: 200

data_20210929.0

## LOG ##
```
 root -q 'macros/benchmark/JobSubmit.C("macros/benchmark/Spect_signalE.mac", "-d xec", "/meg/data1/shared/mc/benchmark_test/20211000-root6.22.06-geant4.10.06.p03-gcc930/gem4Out/", 200, 11000, 50, "-p short")'
```

#11000-11049
200 events * 50 run = 10000 events generation,

```
$ du -cm gem4Out/sev11*
```
```
$ grep 'User=' gem4Out/log/g110[0-1]* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=4000} END{print sum "/" n " = " sum/n "s/event"}'
3386.3/4000 = 0.846575s/event
$ grep 'User=' gem4Out/log/g110[2-3]* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=4000} END{print sum "/" n " = " sum/n "s/event"}'
3794.08/4000 = 0.94852s/event
$ grep 'User=' gem4Out/log/g110[4-5]* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=4000} END{print sum "/" n " = " sum/n "s/event"}'
3582.44/4000 = 0.89561s/event
$ grep 'User=' gem4Out/log/g110[6-7]* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=4000} END{print sum "/" n " = " sum/n "s/event"}'
3645.94/4000 = 0.911485s/event
```
```
megbartender [1] sev->Print("toponly")

```