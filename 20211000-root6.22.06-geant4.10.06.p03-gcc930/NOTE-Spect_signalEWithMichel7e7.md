2021/10/02

# Bartender output with Spect_signalE + Full_michel mixed.

* (realistic)
   * A signal event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Michel events are randomly mixed at rate of 7e7 Hz in time window (-350,350) ns.
   * Run number convention: 116**  
     10 gem4 runs (sev files) are summed up in a bartender run.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.
   * CDCH noise from spectrum measured in 2020 with standard FE.   
   * Different cathode wires
       - 11500-11504: Standard
       - 11510-11514: AgAl50
       - 11520-11524: AgAl50NewCoat

data_20210929.0

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_signalEWithMichel7e7_realistic.xml","/meg/data1/shared/mc/benchmark_test/20211000-root6.22.06-geant4.10.06.p03-gcc930", 11600, 5, "")'
```

```
./macros/bartender_output.sh 20211000-root6.22.06-geant4.10.06.p03-gcc930/Spect_signalEWithMichel7e7_realistic 116
cd 20211000-root6.22.06-geant4.10.06.p03-gcc930
cat NOTE-Spect_signalEWithMichel7e7_realistic.md >> NOTE-Spect_signalEWithMichel7e7.md;rm -f NOTE-Spect_signalEWithMichel7e7_realistic.md
```


### Data and Statistics ###


```
./macros/bartender_output.sh 20211000-root6.22.06-geant4.10.06.p03-gcc930/Spect_signalEWithMichel7e7_realistic 116
```