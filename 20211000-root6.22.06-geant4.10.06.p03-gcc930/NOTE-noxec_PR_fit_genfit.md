2021/10/02

# Analyzer output with noxec_PR_fit_genfit.xml
* No XEC reconstruction
* SPX analysis from WaveformAnalysis (cf30)
* CDC analysis from waveform (WaveformAnalysis2), pattern recognition and fit with GENFIT
* Custom run number 2000116
* Hit z reconstruction used in CYLDCHTrackFinderPR is time-difference
* CYLDCHWaveformAnalysis2 (with not fully optimized parameters).
    * Update TimeWalkCoefficient (to 2.9e-10) and GlobalTimeOffset (to -8.25)

mc_20211002.0

## Run Numbers ##
11500-11504 Spect_signalEOnly_realistic (standard)  
11510-11514 Spect_signalEOnly_realistic (AgAl50 cathode wires))  
11520-11524 Spect_signalEOnly_realistic (AgAl50NewCoat cathode wires))     

11600-11604 Spect_signalEWithMichel7e7_realistic (standard)   
11610-11614 Spect_signalEWithMichel7e7_realistic (AgAl50)  
11620-11624 Spect_signalEWithMichel7e7_realistic (AgAl50NewCoat)  


## Submit jobs ##
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20211000-root6.22.06-geant4.10.06.p03-gcc930/", 11500, 5)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20211000-root6.22.06-geant4.10.06.p03-gcc930/", 11600, 5)'
```



## Calculate positron efficiency ##
### Realistic (standard) ###

```
RUNNUM=11500;NRUN=5;DATASET=20211000-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5889 (88.4633 %)
|   |                         
|   | propagation cut         
|   V                         
|  5765 (86.6006 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  5215 (78.3386 %)
|   |                         
|   | matching                
|   V                         
|  5115 (76.8364 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  5115 (76.8364 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  4970 (74.6583 %)
|      + 0.536834
|      - 0.544346
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.4573 +- 0.0863521
|  phi     (mrad)  6.19572 +- 0.0848458
|  momentum (MeV)  0.0864302 +- 0.00125128
|  vertexZ   (mm)  1.60631 +- 0.0225911
|  vertexY   (mm)  0.780738 +- 0.0109726
|  time     (sec)  4.44501e-11 +- 5.80206e-13
@-----------------------------@

```

### Realistic (AgAl50) ###

```
RUNNUM=11510;NRUN=5;DATASET=20211000-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6484 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  5940 (91.6101 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5766 (88.9266 %)
|   |                         
|   | propagation cut         
|   V                         
|  5634 (86.8908 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  5052 (77.9149 %)
|   |                         
|   | matching                
|   V                         
|  4952 (76.3726 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  4952 (76.3726 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  4826 (74.4294 %)
|      + 0.545634
|      - 0.553276
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.47773 +- 0.0807728
|  phi     (mrad)  6.37829 +- 0.0875084
|  momentum (MeV)  0.0883393 +- 0.00123544
|  vertexZ   (mm)  1.65402 +- 0.0224976
|  vertexY   (mm)  0.764672 +- 0.0105413
|  time     (sec)  4.579e-11 +- 6.62909e-13
@-----------------------------@
```

### Realistic (AgAl50NewCoat) ###

```
RUNNUM=11520;NRUN=5;DATASET=20211000-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6586 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6048 (91.8312 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5876 (89.2196 %)
|   |                         
|   | propagation cut         
|   V                         
|  5725 (86.9268 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  5180 (78.6517 %)
|   |                         
|   | matching                
|   V                         
|  5072 (77.0118 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  5072 (77.0118 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  4939 (74.9924 %)
|      + 0.537333
|      - 0.54503
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.48017 +- 0.0856004
|  phi     (mrad)  6.35705 +- 0.0900343
|  momentum (MeV)  0.0873939 +- 0.00127229
|  vertexZ   (mm)  1.64407 +- 0.0226419
|  vertexY   (mm)  0.798161 +- 0.0115705
|  time     (sec)  4.52494e-11 +- 6.70726e-13
@-----------------------------@
```



### Realistic (Michel mixed, standard) ###

```
RUNNUM=11600;NRUN=5;DATASET=20211000-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6657 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6064 (91.0921 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5398 (81.0876 %)
|   |                         
|   | propagation cut         
|   V                         
|  4857 (72.9608 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  3930 (59.0356 %)
|   |                         
|   | matching                
|   V                         
|  3848 (57.8038 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  3844 (57.7437 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  3729 (56.0162 %)
|      + 0.614914
|      - 0.616744
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.9011 +- 0.100686
|  phi     (mrad)  6.67579 +- 0.111496
|  momentum (MeV)  0.098502 +- 0.00168106
|  vertexZ   (mm)  1.8586 +- 0.0307748
|  vertexY   (mm)  0.839284 +- 0.0138981
|  time     (sec)  4.62988e-11 +- 7.26343e-13
@-----------------------------@
```

### Realistic (Michel mixed, AgAl50) ###

```
RUNNUM=11610;NRUN=5;DATASET=20211000-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6484 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  5942 (91.641 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5300 (81.7397 %)
|   |                         
|   | propagation cut         
|   V                         
|  4817 (74.2906 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  3853 (59.4232 %)
|   |                         
|   | matching                
|   V                         
|  3757 (57.9426 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  3751 (57.8501 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  3655 (56.3695 %)
|      + 0.622548
|      - 0.624537
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.99241 +- 0.110014
|  phi     (mrad)  6.68751 +- 0.107935
|  momentum (MeV)  0.0981357 +- 0.00161308
|  vertexZ   (mm)  1.85859 +- 0.0313343
|  vertexY   (mm)  0.824547 +- 0.0153651
|  time     (sec)  4.70798e-11 +- 8.1768e-13
@-----------------------------@

```
```

### Realistic (Michel mixed, AgAl50NewCoat) ###

```
RUNNUM=11620;NRUN=5;DATASET=20211000-root6.22.06-geant4.10.06.p03-gcc930;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  6586 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  6047 (91.816 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  5342 (81.1114 %)
|   |                         
|   | propagation cut         
|   V                         
|  4800 (72.8819 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  3883 (58.9584 %)
|   |                         
|   | matching                
|   V                         
|  3785 (57.4704 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  3783 (57.44 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  3673 (55.7698 %)
|      + 0.618653
|      - 0.620427
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.88857 +- 0.100252
|  phi     (mrad)  6.76336 +- 0.112805
|  momentum (MeV)  0.0992844 +- 0.0016307
|  vertexZ   (mm)  1.87853 +- 0.0325085
|  vertexY   (mm)  0.844452 +- 0.0153689
|  time     (sec)  4.73655e-11 +- 7.60145e-13
@-----------------------------@
```

### Analyze execution and output ###
```
cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20211000-root6.22.06-geant4.10.06.p03-gcc930/noxec_PR_fit_genfit 115

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20211000-root6.22.06-geant4.10.06.p03-gcc930/noxec_PR_fit_genfit 116
```

```
./macros/analyzer_output.sh 20211000-root6.22.06-geant4.10.06.p03-gcc930/noxec_PR_fit_genfit 115
```
- - - -

60000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Oct  2 22:33 Eff_results11500.root
-rw-r--r--. 1 uchiyama unx-lke  18K Oct  2 22:38 Eff_results11510.root
-rw-r--r--. 1 uchiyama unx-lke  18K Oct  2 22:40 Eff_results11520.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:45 histos11500.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:46 histos11501.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:45 histos11502.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:46 histos11503.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:46 histos11504.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:44 histos11510.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:44 histos11511.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:45 histos11512.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:46 histos11513.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:46 histos11514.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:44 histos11520.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:44 histos11521.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:44 histos11522.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:45 histos11523.root
-rw-r--r--. 1 uchiyama unx-lke 3.0M Oct  2 21:45 histos11524.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:45 rec11500.root
-rw-r--r--. 1 uchiyama unx-lke  56M Oct  2 21:46 rec11501.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:45 rec11502.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:46 rec11503.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:46 rec11504.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:44 rec11510.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:44 rec11511.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:45 rec11512.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:46 rec11513.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:46 rec11514.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:44 rec11520.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:44 rec11521.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:44 rec11522.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:45 rec11523.root
-rw-r--r--. 1 uchiyama unx-lke  55M Oct  2 21:45 rec11524.root

rec	28.657 kB/event
dch	24.616 kB/event
spx	3.493 kB/event
reco	0.226 kB/event
rdc	0.207 kB/event

```
######### execution time #########
13709.3/60000 = 0.228488 s/event
```
---------
analyzer........................... : 00:02:29.305  00:02:06.199  00:00:00.000
 DAQ bartender..................... : 00:01:18.581  00:01:01.839  00:01:01.489
 Task MEGMain...................... : 00:00:02.896  00:00:01.529  00:00:00.000
 Task ReadData..................... : 00:00:08.190  00:00:08.099  00:00:08.059
 Task SPX.......................... : 00:00:00.014  00:00:00.029  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:05.503  00:00:05.369  00:00:05.289
  SubTask SPXHitRec................ : 00:00:00.302  00:00:00.349  00:00:00.349
  SubTask SPXClustering............ : 00:00:11.607  00:00:11.419  00:00:11.389
  SubTask SPXIndependentTracking... : 00:00:30.439  00:00:30.230  00:00:30.200
 Task RDC.......................... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.086  00:00:00.109  00:00:00.089
  SubTask RDCHitRec................ : 00:00:00.044  00:00:00.039  00:00:00.029
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.013  00:00:00.030  00:00:00.020
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.128  00:00:00.180  00:00:00.140
  SubTask DCHKalmanFilterGEN....... : 00:00:00.030  00:00:00.049  00:00:00.029
  SubTask DCHTrackReFit............ : 00:00:00.156  00:00:00.139  00:00:00.139
 Task DCHSPX....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.152  00:00:00.180  00:00:00.180
  SubTask SPXTracking.............. : 00:00:00.022  00:00:00.030  00:00:00.010
  SubTask DCHSPXTrackSelection..... : 00:00:00.028  00:00:00.000  00:00:00.000
 WriteEvent........................ : 00:00:04.711  00:00:02.670
analyzer........................... : 00:12:21.806  00:09:57.179  00:00:00.000
 DAQ bartender..................... : 00:02:21.876  00:00:55.090  00:00:54.760
 Task MEGMain...................... : 00:00:01.555  00:00:01.299  00:00:00.000
 Task ReadData..................... : 00:00:06.661  00:00:06.269  00:00:06.239
 Task SPX.......................... : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.930  00:00:05.110  00:00:05.040
  SubTask SPXHitRec................ : 00:00:00.252  00:00:00.249  00:00:00.219
  SubTask SPXClustering............ : 00:00:13.421  00:00:13.220  00:00:13.160
  SubTask SPXIndependentTracking... : 00:00:26.834  00:00:26.549  00:00:26.539
 Task RDC.......................... : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.067  00:00:00.040  00:00:00.040
  SubTask RDCHitRec................ : 00:00:00.037  00:00:00.020  00:00:00.010
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:38.397  00:01:38.560  00:01:38.450
  SubTask CYLDCHHitRec............. : 00:00:00.492  00:00:00.540  00:00:00.500
  SubTask CYLDCHTrackFinderPR...... : 00:02:45.654  00:02:45.569  00:02:45.549
  SubTask DCHKalmanFilterGEN....... : 00:01:11.304  00:01:11.170  00:01:11.150
  SubTask DCHTrackReFit............ : 00:00:00.142  00:00:00.089  00:00:00.089
 Task DCHSPX....................... : 00:00:00.008  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:54.633  00:00:54.829  00:00:54.819
  SubTask DCHSPXReFit.............. : 00:01:09.881  00:01:09.699  00:01:09.679
  SubTask SPXTracking.............. : 00:00:19.163  00:00:19.130  00:00:19.070
  SubTask DCHSPXTrackSelection..... : 00:00:00.070  00:00:00.090  00:00:00.090
 WriteEvent........................ : 00:00:57.188  00:00:06.089
analyzer........................... : 00:02:33.058  00:02:18.189  00:00:00.000
 DAQ bartender..................... : 00:01:12.648  00:01:05.020  00:01:04.740
 Task MEGMain...................... : 00:00:03.614  00:00:01.459  00:00:00.000
 Task ReadData..................... : 00:00:10.646  00:00:10.639  00:00:10.579
 Task SPX.......................... : 00:00:00.019  00:00:00.030  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.590  00:00:05.619  00:00:05.519
  SubTask SPXHitRec................ : 00:00:00.340  00:00:00.329  00:00:00.319
  SubTask SPXClustering............ : 00:00:08.040  00:00:07.999  00:00:07.889
  SubTask SPXIndependentTracking... : 00:00:38.764  00:00:38.429  00:00:38.419
 Task RDC.......................... : 00:00:00.019  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.097  00:00:00.099  00:00:00.099
  SubTask RDCHitRec................ : 00:00:00.053  00:00:00.050  00:00:00.030
 Task CYLDCH....................... : 00:00:00.013  00:00:00.019  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.018  00:00:00.000  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.146  00:00:00.169  00:00:00.129
  SubTask DCHKalmanFilterGEN....... : 00:00:00.035  00:00:00.059  00:00:00.029
  SubTask DCHTrackReFit............ : 00:00:00.147  00:00:00.180  00:00:00.150
 Task DCHSPX....................... : 00:00:00.013  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.016  00:00:00.020  00:00:00.010
  SubTask DCHSPXReFit.............. : 00:00:00.163  00:00:00.169  00:00:00.149
  SubTask SPXTracking.............. : 00:00:00.015  00:00:00.010  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.034  00:00:00.029  00:00:00.019
 WriteEvent........................ : 00:00:06.032  00:00:03.210
analyzer........................... : 00:13:41.371  00:11:57.009  00:00:00.000
 DAQ bartender..................... : 00:01:47.513  00:00:59.750  00:00:59.460
 Task MEGMain...................... : 00:00:06.777  00:00:01.609  00:00:00.000
 Task ReadData..................... : 00:00:06.449  00:00:05.930  00:00:05.860
 Task SPX.......................... : 00:00:00.012  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.785  00:00:05.960  00:00:05.890
  SubTask SPXHitRec................ : 00:00:00.280  00:00:00.319  00:00:00.309
  SubTask SPXClustering............ : 00:00:11.763  00:00:11.440  00:00:11.380
  SubTask SPXIndependentTracking... : 00:00:38.217  00:00:38.149  00:00:38.139
 Task RDC.......................... : 00:00:00.012  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.159  00:00:00.049  00:00:00.049
  SubTask RDCHitRec................ : 00:00:00.043  00:00:00.049  00:00:00.049
 Task CYLDCH....................... : 00:00:00.009  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:58.981  00:01:58.590  00:01:58.460
  SubTask CYLDCHHitRec............. : 00:00:00.565  00:00:00.599  00:00:00.589
  SubTask CYLDCHTrackFinderPR...... : 00:03:24.539  00:03:22.250  00:03:22.190
  SubTask DCHKalmanFilterGEN....... : 00:01:26.276  00:01:25.669  00:01:25.629
  SubTask DCHTrackReFit............ : 00:00:00.159  00:00:00.209  00:00:00.209
 Task DCHSPX....................... : 00:00:00.009  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:07.562  00:01:07.270  00:01:07.260
  SubTask DCHSPXReFit.............. : 00:01:24.669  00:01:24.539  00:01:24.509
  SubTask SPXTracking.............. : 00:00:24.052  00:00:23.880  00:00:23.880
  SubTask DCHSPXTrackSelection..... : 00:00:00.080  00:00:00.119  00:00:00.099
 WriteEvent........................ : 00:00:48.588  00:00:06.449
analyzer........................... : 00:02:26.383  00:02:12.539  00:00:00.000
 DAQ bartender..................... : 00:01:13.132  00:01:04.400  00:01:04.110
 Task MEGMain...................... : 00:00:01.301  00:00:01.230  00:00:00.009
 Task ReadData..................... : 00:00:10.780  00:00:10.829  00:00:10.779
 Task SPX.......................... : 00:00:00.018  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.357  00:00:05.189  00:00:05.109
  SubTask SPXHitRec................ : 00:00:00.330  00:00:00.449  00:00:00.399
  SubTask SPXClustering............ : 00:00:07.988  00:00:07.870  00:00:07.830
  SubTask SPXIndependentTracking... : 00:00:33.942  00:00:33.850  00:00:33.840
 Task RDC.......................... : 00:00:00.018  00:00:00.030  00:00:00.020
  SubTask RDCWaveformAnalysis...... : 00:00:00.095  00:00:00.060  00:00:00.040
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.020  00:00:00.010
 Task CYLDCH....................... : 00:00:00.013  00:00:00.019  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.018  00:00:00.020  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.142  00:00:00.240  00:00:00.220
  SubTask DCHKalmanFilterGEN....... : 00:00:00.034  00:00:00.049  00:00:00.039
  SubTask DCHTrackReFit............ : 00:00:00.141  00:00:00.199  00:00:00.159
 Task DCHSPX....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.030  00:00:00.009
  SubTask DCHSPXReFit.............. : 00:00:00.159  00:00:00.150  00:00:00.140
  SubTask SPXTracking.............. : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.031  00:00:00.009  00:00:00.009
 WriteEvent........................ : 00:00:05.669  00:00:03.229
analyzer........................... : 00:13:05.303  00:11:27.439  00:00:00.000
 DAQ bartender..................... : 00:01:46.317  00:01:01.759  00:01:01.499
 Task MEGMain...................... : 00:00:06.960  00:00:01.619  00:00:00.000
 Task ReadData..................... : 00:00:07.530  00:00:07.610  00:00:07.550
 Task SPX.......................... : 00:00:00.012  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.768  00:00:05.730  00:00:05.650
  SubTask SPXHitRec................ : 00:00:00.287  00:00:00.249  00:00:00.229
  SubTask SPXClustering............ : 00:00:14.780  00:00:14.380  00:00:14.320
  SubTask SPXIndependentTracking... : 00:00:34.856  00:00:34.759  00:00:34.709
 Task RDC.......................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.079  00:00:00.069  00:00:00.069
  SubTask RDCHitRec................ : 00:00:00.044  00:00:00.049  00:00:00.039
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:56.742  00:01:56.799  00:01:56.639
  SubTask CYLDCHHitRec............. : 00:00:00.588  00:00:00.589  00:00:00.569
  SubTask CYLDCHTrackFinderPR...... : 00:03:13.006  00:03:10.699  00:03:10.629
  SubTask DCHKalmanFilterGEN....... : 00:01:21.211  00:01:20.980  00:01:20.960
  SubTask DCHTrackReFit............ : 00:00:00.159  00:00:00.239  00:00:00.199
 Task DCHSPX....................... : 00:00:00.009  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:00:58.067  00:00:58.140  00:00:58.120
  SubTask DCHSPXReFit.............. : 00:01:19.923  00:01:19.890  00:01:19.880
  SubTask SPXTracking.............. : 00:00:23.553  00:00:23.599  00:00:23.579
  SubTask DCHSPXTrackSelection..... : 00:00:00.078  00:00:00.100  00:00:00.080
 WriteEvent........................ : 00:00:48.268  00:00:06.470
analyzer........................... : 00:02:26.192  00:02:13.140  00:00:00.000
 DAQ bartender..................... : 00:01:13.403  00:01:05.259  00:01:04.829
 Task MEGMain...................... : 00:00:01.601  00:00:01.300  00:00:00.010
 Task ReadData..................... : 00:00:10.740  00:00:10.480  00:00:10.420
 Task SPX.......................... : 00:00:00.018  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.334  00:00:05.499  00:00:05.399
  SubTask SPXHitRec................ : 00:00:00.331  00:00:00.269  00:00:00.259
  SubTask SPXClustering............ : 00:00:07.717  00:00:07.820  00:00:07.760
  SubTask SPXIndependentTracking... : 00:00:34.253  00:00:34.070  00:00:34.070
 Task RDC.......................... : 00:00:00.018  00:00:00.029  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.100  00:00:00.140  00:00:00.140
  SubTask RDCHitRec................ : 00:00:00.053  00:00:00.029  00:00:00.019
 Task CYLDCH....................... : 00:00:00.013  00:00:00.029  00:00:00.009
  SubTask CYLDCHHitRec............. : 00:00:00.017  00:00:00.009  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.144  00:00:00.109  00:00:00.099
  SubTask DCHKalmanFilterGEN....... : 00:00:00.034  00:00:00.019  00:00:00.000
  SubTask DCHTrackReFit............ : 00:00:00.145  00:00:00.169  00:00:00.159
 Task DCHSPX....................... : 00:00:00.013  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.160  00:00:00.110  00:00:00.110
  SubTask SPXTracking.............. : 00:00:00.015  00:00:00.019  00:00:00.009
  SubTask DCHSPXTrackSelection..... : 00:00:00.031  00:00:00.060  00:00:00.060
 WriteEvent........................ : 00:00:05.729  00:00:02.899
analyzer........................... : 00:13:37.414  00:11:57.060  00:00:00.000
 DAQ bartender..................... : 00:01:40.989  00:01:01.650  00:01:01.280
 Task MEGMain...................... : 00:00:06.862  00:00:01.620  00:00:00.000
 Task ReadData..................... : 00:00:07.555  00:00:07.660  00:00:07.600
 Task SPX.......................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.807  00:00:05.430  00:00:05.340
  SubTask SPXHitRec................ : 00:00:00.292  00:00:00.220  00:00:00.220
  SubTask SPXClustering............ : 00:00:14.628  00:00:14.480  00:00:14.410
  SubTask SPXIndependentTracking... : 00:00:35.422  00:00:35.149  00:00:35.099
 Task RDC.......................... : 00:00:00.013  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.081  00:00:00.120  00:00:00.109
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.029  00:00:00.019
 Task CYLDCH....................... : 00:00:00.009  00:00:00.019  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:57.525  00:01:57.330  00:01:57.170
  SubTask CYLDCHHitRec............. : 00:00:00.589  00:00:00.580  00:00:00.580
  SubTask CYLDCHTrackFinderPR...... : 00:03:36.565  00:03:34.319  00:03:34.299
  SubTask DCHKalmanFilterGEN....... : 00:01:26.330  00:01:26.580  00:01:26.520
  SubTask DCHTrackReFit............ : 00:00:00.169  00:00:00.189  00:00:00.169
 Task DCHSPX....................... : 00:00:00.009  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:56.836  00:00:56.739  00:00:56.719
  SubTask DCHSPXReFit.............. : 00:01:20.843  00:01:20.640  00:01:20.610
  SubTask SPXTracking.............. : 00:00:23.415  00:00:23.180  00:00:23.150
  SubTask DCHSPXTrackSelection..... : 00:00:00.080  00:00:00.069  00:00:00.049
 WriteEvent........................ : 00:00:51.007  00:00:07.240
analyzer........................... : 00:02:21.068  00:02:09.889  00:00:00.000
 DAQ bartender..................... : 00:01:11.211  00:01:03.270  00:01:02.980
 Task MEGMain...................... : 00:00:01.286  00:00:01.229  00:00:00.010
 Task ReadData..................... : 00:00:10.549  00:00:10.720  00:00:10.600
 Task SPX.......................... : 00:00:00.018  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.366  00:00:05.190  00:00:05.100
  SubTask SPXHitRec................ : 00:00:00.329  00:00:00.440  00:00:00.400
  SubTask SPXClustering............ : 00:00:07.928  00:00:07.959  00:00:07.909
  SubTask SPXIndependentTracking... : 00:00:33.682  00:00:33.549  00:00:33.529
 Task RDC.......................... : 00:00:00.018  00:00:00.019  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:00.092  00:00:00.049  00:00:00.049
  SubTask RDCHitRec................ : 00:00:00.047  00:00:00.049  00:00:00.019
 Task CYLDCH....................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.016  00:00:00.010  00:00:00.010
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.140  00:00:00.159  00:00:00.139
  SubTask DCHKalmanFilterGEN....... : 00:00:00.033  00:00:00.039  00:00:00.010
  SubTask DCHTrackReFit............ : 00:00:00.141  00:00:00.119  00:00:00.099
 Task DCHSPX....................... : 00:00:00.012  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.019  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.159  00:00:00.150  00:00:00.130
  SubTask SPXTracking.............. : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.031  00:00:00.019  00:00:00.019
 WriteEvent........................ : 00:00:03.408  00:00:02.759
analyzer........................... : 00:13:23.755  00:11:46.660  00:00:00.000
 DAQ bartender..................... : 00:01:46.866  00:01:02.550  00:01:02.260
 Task MEGMain...................... : 00:00:06.886  00:00:01.639  00:00:00.009
 Task ReadData..................... : 00:00:07.747  00:00:07.709  00:00:07.629
 Task SPX.......................... : 00:00:00.012  00:00:00.009  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:05.718  00:00:05.770  00:00:05.680
  SubTask SPXHitRec................ : 00:00:00.288  00:00:00.180  00:00:00.170
  SubTask SPXClustering............ : 00:00:15.469  00:00:15.379  00:00:15.309
  SubTask SPXIndependentTracking... : 00:00:35.016  00:00:35.060  00:00:35.030
 Task RDC.......................... : 00:00:00.013  00:00:00.020  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:00.080  00:00:00.089  00:00:00.069
  SubTask RDCHitRec................ : 00:00:00.045  00:00:00.039  00:00:00.029
 Task CYLDCH....................... : 00:00:00.009  00:00:00.009  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:55.880  00:01:55.509  00:01:55.379
  SubTask CYLDCHHitRec............. : 00:00:00.586  00:00:00.640  00:00:00.630
  SubTask CYLDCHTrackFinderPR...... : 00:03:05.221  00:03:02.960  00:03:02.910
  SubTask DCHKalmanFilterGEN....... : 00:01:26.647  00:01:26.869  00:01:26.849
  SubTask DCHTrackReFit............ : 00:00:00.166  00:00:00.129  00:00:00.109
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:06.811  00:01:06.619  00:01:06.599
  SubTask DCHSPXReFit.............. : 00:01:31.674  00:01:31.550  00:01:31.510
  SubTask SPXTracking.............. : 00:00:22.782  00:00:22.719  00:00:22.689
  SubTask DCHSPXTrackSelection..... : 00:00:00.082  00:00:00.079  00:00:00.069
 WriteEvent........................ : 00:00:46.065  00:00:06.599
analyzer........................... : 00:02:22.757  00:02:11.610  00:00:00.000
 DAQ bartender..................... : 00:01:11.458  00:01:03.120  00:01:02.840
 Task MEGMain...................... : 00:00:01.342  00:00:01.250  00:00:00.000
 Task ReadData..................... : 00:00:10.640  00:00:10.829  00:00:10.759
 Task SPX.......................... : 00:00:00.018  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.379  00:00:05.359  00:00:05.299
  SubTask SPXHitRec................ : 00:00:00.326  00:00:00.360  00:00:00.360
  SubTask SPXClustering............ : 00:00:07.825  00:00:07.650  00:00:07.580
  SubTask SPXIndependentTracking... : 00:00:34.671  00:00:34.629  00:00:34.579
 Task RDC.......................... : 00:00:00.018  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.095  00:00:00.049  00:00:00.049
  SubTask RDCHitRec................ : 00:00:00.051  00:00:00.049  00:00:00.019
 Task CYLDCH....................... : 00:00:00.013  00:00:00.019  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.017  00:00:00.009  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.140  00:00:00.090  00:00:00.090
  SubTask DCHKalmanFilterGEN....... : 00:00:00.033  00:00:00.039  00:00:00.029
  SubTask DCHTrackReFit............ : 00:00:00.143  00:00:00.160  00:00:00.130
 Task DCHSPX....................... : 00:00:00.012  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.160  00:00:00.090  00:00:00.080
  SubTask SPXTracking.............. : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.030  00:00:00.049  00:00:00.049
 WriteEvent........................ : 00:00:03.751  00:00:03.040
analyzer........................... : 00:11:48.519  00:09:59.970  00:00:00.000
 DAQ bartender..................... : 00:01:50.724  00:00:56.609  00:00:56.319
 Task MEGMain...................... : 00:00:01.221  00:00:01.029  00:00:00.009
 Task ReadData..................... : 00:00:09.125  00:00:09.309  00:00:09.249
 Task SPX.......................... : 00:00:00.015  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.584  00:00:04.510  00:00:04.440
  SubTask SPXHitRec................ : 00:00:00.278  00:00:00.319  00:00:00.309
  SubTask SPXClustering............ : 00:00:10.181  00:00:10.240  00:00:10.200
  SubTask SPXIndependentTracking... : 00:00:29.016  00:00:28.499  00:00:28.469
 Task RDC.......................... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.082  00:00:00.069  00:00:00.059
  SubTask RDCHitRec................ : 00:00:00.042  00:00:00.019  00:00:00.019
 Task CYLDCH....................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:41.057  00:01:40.580  00:01:40.490
  SubTask CYLDCHHitRec............. : 00:00:00.534  00:00:00.739  00:00:00.719
  SubTask CYLDCHTrackFinderPR...... : 00:02:53.677  00:02:49.040  00:02:49.000
  SubTask DCHKalmanFilterGEN....... : 00:01:06.124  00:01:05.569  00:01:05.549
  SubTask DCHTrackReFit............ : 00:00:00.136  00:00:00.119  00:00:00.119
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:54.462  00:00:54.190  00:00:54.180
  SubTask DCHSPXReFit.............. : 00:01:07.589  00:01:07.360  00:01:07.350
  SubTask SPXTracking.............. : 00:00:18.377  00:00:18.389  00:00:18.389
  SubTask DCHSPXTrackSelection..... : 00:00:00.075  00:00:00.060  00:00:00.050
 WriteEvent........................ : 00:00:54.080  00:00:08.780
analyzer........................... : 00:02:40.231  00:02:11.330  00:00:00.000
 DAQ bartender..................... : 00:01:13.664  00:01:02.160  00:01:01.910
 Task MEGMain...................... : 00:00:09.511  00:00:01.569  00:00:00.000
 Task ReadData..................... : 00:00:07.935  00:00:07.880  00:00:07.810
 Task SPX.......................... : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.524  00:00:05.129  00:00:05.069
  SubTask SPXHitRec................ : 00:00:00.300  00:00:00.280  00:00:00.260
  SubTask SPXClustering............ : 00:00:11.968  00:00:12.289  00:00:12.229
  SubTask SPXIndependentTracking... : 00:00:34.954  00:00:34.570  00:00:34.540
 Task RDC.......................... : 00:00:00.014  00:00:00.010  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:00.089  00:00:00.089  00:00:00.079
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.030  00:00:00.020
 Task CYLDCH....................... : 00:00:00.009  00:00:00.009  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.122  00:00:00.140  00:00:00.130
  SubTask DCHKalmanFilterGEN....... : 00:00:00.029  00:00:00.089  00:00:00.039
  SubTask DCHTrackReFit............ : 00:00:00.158  00:00:00.170  00:00:00.139
 Task DCHSPX....................... : 00:00:00.008  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.009  00:00:00.009
  SubTask DCHSPXReFit.............. : 00:00:00.155  00:00:00.139  00:00:00.139
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.010  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.028  00:00:00.019  00:00:00.010
 WriteEvent........................ : 00:00:07.337  00:00:02.549
analyzer........................... : 00:11:47.711  00:09:56.419  00:00:00.000
 DAQ bartender..................... : 00:01:52.914  00:00:57.520  00:00:57.230
 Task MEGMain...................... : 00:00:01.562  00:00:01.060  00:00:00.000
 Task ReadData..................... : 00:00:08.995  00:00:08.899  00:00:08.839
 Task SPX.......................... : 00:00:00.015  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.388  00:00:04.419  00:00:04.359
  SubTask SPXHitRec................ : 00:00:00.267  00:00:00.280  00:00:00.280
  SubTask SPXClustering............ : 00:00:10.399  00:00:10.170  00:00:10.120
  SubTask SPXIndependentTracking... : 00:00:28.158  00:00:27.970  00:00:27.940
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.079  00:00:00.039  00:00:00.019
  SubTask RDCHitRec................ : 00:00:00.042  00:00:00.039  00:00:00.039
 Task CYLDCH....................... : 00:00:00.011  00:00:00.029  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:40.866  00:01:40.730  00:01:40.630
  SubTask CYLDCHHitRec............. : 00:00:00.530  00:00:00.530  00:00:00.520
  SubTask CYLDCHTrackFinderPR...... : 00:02:48.206  00:02:42.479  00:02:42.459
  SubTask DCHKalmanFilterGEN....... : 00:01:06.621  00:01:06.120  00:01:06.060
  SubTask DCHTrackReFit............ : 00:00:00.135  00:00:00.120  00:00:00.090
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:57.763  00:00:57.900  00:00:57.879
  SubTask DCHSPXReFit.............. : 00:01:10.064  00:01:09.709  00:01:09.679
  SubTask SPXTracking.............. : 00:00:15.797  00:00:15.819  00:00:15.779
  SubTask DCHSPXTrackSelection..... : 00:00:00.071  00:00:00.119  00:00:00.109
 WriteEvent........................ : 00:00:50.904  00:00:07.899
analyzer........................... : 00:02:39.100  00:02:09.170  00:00:00.000
 DAQ bartender..................... : 00:01:14.743  00:01:02.340  00:01:02.100
 Task MEGMain...................... : 00:00:09.510  00:00:01.569  00:00:00.000
 Task ReadData..................... : 00:00:08.043  00:00:07.959  00:00:07.919
 Task SPX.......................... : 00:00:00.015  00:00:00.020  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:05.573  00:00:05.489  00:00:05.399
  SubTask SPXHitRec................ : 00:00:00.309  00:00:00.300  00:00:00.290
  SubTask SPXClustering............ : 00:00:10.875  00:00:10.839  00:00:10.789
  SubTask SPXIndependentTracking... : 00:00:33.126  00:00:33.020  00:00:33.000
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.096  00:00:00.139  00:00:00.119
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.049  00:00:00.039
 Task CYLDCH....................... : 00:00:00.009  00:00:00.019  00:00:00.010
  SubTask CYLDCHHitRec............. : 00:00:00.015  00:00:00.010  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.124  00:00:00.139  00:00:00.119
  SubTask DCHKalmanFilterGEN....... : 00:00:00.030  00:00:00.040  00:00:00.040
  SubTask DCHTrackReFit............ : 00:00:00.158  00:00:00.189  00:00:00.179
 Task DCHSPX....................... : 00:00:00.008  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.019  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.152  00:00:00.150  00:00:00.140
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.032  00:00:00.029  00:00:00.029
 WriteEvent........................ : 00:00:07.740  00:00:02.720
analyzer........................... : 00:13:01.241  00:11:34.149  00:00:00.000
 DAQ bartender..................... : 00:01:40.193  00:01:01.339  00:01:01.069
 Task MEGMain...................... : 00:00:01.475  00:00:01.299  00:00:00.000
 Task ReadData..................... : 00:00:07.646  00:00:07.480  00:00:07.400
 Task SPX.......................... : 00:00:00.012  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.613  00:00:05.680  00:00:05.570
  SubTask SPXHitRec................ : 00:00:00.290  00:00:00.330  00:00:00.300
  SubTask SPXClustering............ : 00:00:14.306  00:00:14.099  00:00:14.039
  SubTask SPXIndependentTracking... : 00:00:33.542  00:00:33.400  00:00:33.400
 Task RDC.......................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.083  00:00:00.049  00:00:00.049
  SubTask RDCHitRec................ : 00:00:00.045  00:00:00.040  00:00:00.040
 Task CYLDCH....................... : 00:00:00.009  00:00:00.019  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:57.421  00:01:57.229  00:01:57.119
  SubTask CYLDCHHitRec............. : 00:00:00.598  00:00:00.550  00:00:00.550
  SubTask CYLDCHTrackFinderPR...... : 00:03:10.741  00:03:08.849  00:03:08.829
  SubTask DCHKalmanFilterGEN....... : 00:01:22.638  00:01:22.630  00:01:22.580
  SubTask DCHTrackReFit............ : 00:00:00.165  00:00:00.199  00:00:00.199
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:04.534  00:01:04.380  00:01:04.380
  SubTask DCHSPXReFit.............. : 00:01:21.528  00:01:21.689  00:01:21.689
  SubTask SPXTracking.............. : 00:00:24.644  00:00:24.410  00:00:24.380
  SubTask DCHSPXTrackSelection..... : 00:00:00.083  00:00:00.079  00:00:00.059
 WriteEvent........................ : 00:00:49.574  00:00:06.510
analyzer........................... : 00:02:29.133  00:02:09.550  00:00:00.000
 DAQ bartender..................... : 00:01:15.815  00:01:03.379  00:01:03.119
 Task MEGMain...................... : 00:00:01.747  00:00:01.169  00:00:00.010
 Task ReadData..................... : 00:00:10.555  00:00:10.470  00:00:10.410
 Task SPX.......................... : 00:00:00.018  00:00:00.060  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.272  00:00:05.479  00:00:05.409
  SubTask SPXHitRec................ : 00:00:00.329  00:00:00.350  00:00:00.350
  SubTask SPXClustering............ : 00:00:07.917  00:00:07.920  00:00:07.850
  SubTask SPXIndependentTracking... : 00:00:32.572  00:00:32.219  00:00:32.199
 Task RDC.......................... : 00:00:00.018  00:00:00.050  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.095  00:00:00.069  00:00:00.049
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.049  00:00:00.030
 Task CYLDCH....................... : 00:00:00.013  00:00:00.039  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.017  00:00:00.009  00:00:00.009
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.141  00:00:00.130  00:00:00.099
  SubTask DCHKalmanFilterGEN....... : 00:00:00.034  00:00:00.050  00:00:00.020
  SubTask DCHTrackReFit............ : 00:00:00.142  00:00:00.039  00:00:00.039
 Task DCHSPX....................... : 00:00:00.012  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.040  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.158  00:00:00.150  00:00:00.150
  SubTask SPXTracking.............. : 00:00:00.015  00:00:00.020  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.030  00:00:00.050  00:00:00.050
 WriteEvent........................ : 00:00:06.649  00:00:02.900
analyzer........................... : 00:13:23.616  00:11:43.210  00:00:00.000
 DAQ bartender..................... : 00:01:51.955  00:01:01.199  00:01:00.919
 Task MEGMain...................... : 00:00:01.584  00:00:01.309  00:00:00.000
 Task ReadData..................... : 00:00:07.530  00:00:07.740  00:00:07.660
 Task SPX.......................... : 00:00:00.012  00:00:00.009  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:05.642  00:00:05.590  00:00:05.520
  SubTask SPXHitRec................ : 00:00:00.287  00:00:00.310  00:00:00.310
  SubTask SPXClustering............ : 00:00:14.892  00:00:14.180  00:00:14.130
  SubTask SPXIndependentTracking... : 00:00:33.833  00:00:33.849  00:00:33.819
 Task RDC.......................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.081  00:00:00.090  00:00:00.090
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.029  00:00:00.029
 Task CYLDCH....................... : 00:00:00.009  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:56.850  00:01:56.899  00:01:56.789
  SubTask CYLDCHHitRec............. : 00:00:00.579  00:00:00.560  00:00:00.560
  SubTask CYLDCHTrackFinderPR...... : 00:03:07.898  00:03:05.579  00:03:05.559
  SubTask DCHKalmanFilterGEN....... : 00:01:25.511  00:01:25.600  00:01:25.580
  SubTask DCHTrackReFit............ : 00:00:00.163  00:00:00.140  00:00:00.140
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:08.287  00:01:08.080  00:01:08.070
  SubTask DCHSPXReFit.............. : 00:01:25.721  00:01:25.669  00:01:25.639
  SubTask SPXTracking.............. : 00:00:25.733  00:00:25.820  00:00:25.810
  SubTask DCHSPXTrackSelection..... : 00:00:00.079  00:00:00.080  00:00:00.060
 WriteEvent........................ : 00:00:48.314  00:00:06.499
analyzer........................... : 00:02:26.359  00:02:09.479  00:00:00.000
 DAQ bartender..................... : 00:01:14.681  00:01:02.829  00:01:02.549
 Task MEGMain...................... : 00:00:01.359  00:00:01.049  00:00:00.000
 Task ReadData..................... : 00:00:10.624  00:00:10.820  00:00:10.770
 Task SPX.......................... : 00:00:00.017  00:00:00.030  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:05.166  00:00:04.930  00:00:04.860
  SubTask SPXHitRec................ : 00:00:00.324  00:00:00.330  00:00:00.310
  SubTask SPXClustering............ : 00:00:07.911  00:00:08.070  00:00:08.010
  SubTask SPXIndependentTracking... : 00:00:33.110  00:00:32.569  00:00:32.559
 Task RDC.......................... : 00:00:00.018  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.090  00:00:00.169  00:00:00.129
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.049  00:00:00.049
 Task CYLDCH....................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.017  00:00:00.009  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.138  00:00:00.140  00:00:00.110
  SubTask DCHKalmanFilterGEN....... : 00:00:00.033  00:00:00.020  00:00:00.010
  SubTask DCHTrackReFit............ : 00:00:00.141  00:00:00.159  00:00:00.159
 Task DCHSPX....................... : 00:00:00.012  00:00:00.010  00:00:00.010
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.019  00:00:00.009
  SubTask DCHSPXReFit.............. : 00:00:00.157  00:00:00.150  00:00:00.140
  SubTask SPXTracking.............. : 00:00:00.015  00:00:00.029  00:00:00.009
  SubTask DCHSPXTrackSelection..... : 00:00:00.032  00:00:00.070  00:00:00.039
 WriteEvent........................ : 00:00:04.531  00:00:03.250
analyzer........................... : 00:13:18.638  00:11:47.740  00:00:00.000
 DAQ bartender..................... : 00:01:45.385  00:01:02.449  00:01:02.169
 Task MEGMain...................... : 00:00:01.741  00:00:01.329  00:00:00.000
 Task ReadData..................... : 00:00:07.793  00:00:07.800  00:00:07.740
 Task SPX.......................... : 00:00:00.012  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.677  00:00:05.849  00:00:05.789
  SubTask SPXHitRec................ : 00:00:00.292  00:00:00.209  00:00:00.209
  SubTask SPXClustering............ : 00:00:15.486  00:00:14.910  00:00:14.840
  SubTask SPXIndependentTracking... : 00:00:35.089  00:00:34.879  00:00:34.859
 Task RDC.......................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.078  00:00:00.079  00:00:00.069
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.030  00:00:00.019
 Task CYLDCH....................... : 00:00:00.009  00:00:00.019  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:56.761  00:01:56.799  00:01:56.649
  SubTask CYLDCHHitRec............. : 00:00:00.581  00:00:00.580  00:00:00.540
  SubTask CYLDCHTrackFinderPR...... : 00:03:18.046  00:03:15.920  00:03:15.900
  SubTask DCHKalmanFilterGEN....... : 00:01:26.414  00:01:26.160  00:01:26.140
  SubTask DCHTrackReFit............ : 00:00:00.167  00:00:00.189  00:00:00.169
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:02.979  00:01:03.039  00:01:03.019
  SubTask DCHSPXReFit.............. : 00:01:25.576  00:01:25.550  00:01:25.550
  SubTask SPXTracking.............. : 00:00:21.284  00:00:21.149  00:00:21.109
  SubTask DCHSPXTrackSelection..... : 00:00:00.082  00:00:00.139  00:00:00.099
 WriteEvent........................ : 00:00:44.955  00:00:06.720
analyzer........................... : 00:02:24.631  00:02:02.019  00:00:00.000
 DAQ bartender..................... : 00:01:12.814  00:00:59.300  00:00:59.020
 Task MEGMain...................... : 00:00:05.180  00:00:01.439  00:00:00.000
 Task ReadData..................... : 00:00:09.354  00:00:09.570  00:00:09.490
 Task SPX.......................... : 00:00:00.017  00:00:00.010  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:05.174  00:00:04.979  00:00:04.909
  SubTask SPXHitRec................ : 00:00:00.309  00:00:00.400  00:00:00.370
  SubTask SPXClustering............ : 00:00:07.519  00:00:07.159  00:00:07.109
  SubTask SPXIndependentTracking... : 00:00:31.134  00:00:31.119  00:00:31.109
 Task RDC.......................... : 00:00:00.017  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.088  00:00:00.050  00:00:00.050
  SubTask RDCHitRec................ : 00:00:00.047  00:00:00.070  00:00:00.070
 Task CYLDCH....................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.016  00:00:00.029  00:00:00.019
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.133  00:00:00.109  00:00:00.109
  SubTask DCHKalmanFilterGEN....... : 00:00:00.032  00:00:00.039  00:00:00.019
  SubTask DCHTrackReFit............ : 00:00:00.139  00:00:00.129  00:00:00.119
 Task DCHSPX....................... : 00:00:00.012  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.014  00:00:00.019  00:00:00.009
  SubTask DCHSPXReFit.............. : 00:00:00.151  00:00:00.150  00:00:00.150
  SubTask SPXTracking.............. : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.033  00:00:00.019  00:00:00.009
 WriteEvent........................ : 00:00:04.740  00:00:02.800
analyzer........................... : 00:11:49.242  00:09:58.110  00:00:00.000
 DAQ bartender..................... : 00:01:52.381  00:00:57.019  00:00:56.719
 Task MEGMain...................... : 00:00:01.228  00:00:01.040  00:00:00.009
 Task ReadData..................... : 00:00:09.055  00:00:09.100  00:00:09.030
 Task SPX.......................... : 00:00:00.015  00:00:00.049  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:04.554  00:00:04.080  00:00:04.030
  SubTask SPXHitRec................ : 00:00:00.268  00:00:00.300  00:00:00.290
  SubTask SPXClustering............ : 00:00:10.245  00:00:10.620  00:00:10.560
  SubTask SPXIndependentTracking... : 00:00:29.264  00:00:27.450  00:00:27.420
 Task RDC.......................... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.076  00:00:00.059  00:00:00.049
  SubTask RDCHitRec................ : 00:00:00.041  00:00:00.040  00:00:00.040
 Task CYLDCH....................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:41.569  00:01:41.409  00:01:41.289
  SubTask CYLDCHHitRec............. : 00:00:00.539  00:00:00.510  00:00:00.500
  SubTask CYLDCHTrackFinderPR...... : 00:02:46.908  00:02:43.010  00:02:42.970
  SubTask DCHKalmanFilterGEN....... : 00:01:06.342  00:01:05.849  00:01:05.819
  SubTask DCHTrackReFit............ : 00:00:00.136  00:00:00.159  00:00:00.139
 Task DCHSPX....................... : 00:00:00.012  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:55.853  00:00:55.800  00:00:55.790
  SubTask DCHSPXReFit.............. : 00:01:11.050  00:01:10.880  00:01:10.860
  SubTask SPXTracking.............. : 00:00:18.007  00:00:17.890  00:00:17.880
  SubTask DCHSPXTrackSelection..... : 00:00:00.070  00:00:00.049  00:00:00.049
 WriteEvent........................ : 00:00:53.200  00:00:08.239
analyzer........................... : 00:02:32.026  00:02:09.960  00:00:00.000
 DAQ bartender..................... : 00:01:16.212  00:01:02.680  00:01:02.400
 Task MEGMain...................... : 00:00:02.721  00:00:01.280  00:00:00.000
 Task ReadData..................... : 00:00:09.983  00:00:10.009  00:00:09.959
 Task SPX.......................... : 00:00:00.018  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.289  00:00:05.629  00:00:05.539
  SubTask SPXHitRec................ : 00:00:00.326  00:00:00.280  00:00:00.240
  SubTask SPXClustering............ : 00:00:08.131  00:00:07.700  00:00:07.640
  SubTask SPXIndependentTracking... : 00:00:34.031  00:00:33.769  00:00:33.739
 Task RDC.......................... : 00:00:00.019  00:00:00.009  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.093  00:00:00.079  00:00:00.059
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.050  00:00:00.029
 Task CYLDCH....................... : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.017  00:00:00.029  00:00:00.009
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.142  00:00:00.100  00:00:00.090
  SubTask DCHKalmanFilterGEN....... : 00:00:00.033  00:00:00.009  00:00:00.009
  SubTask DCHTrackReFit............ : 00:00:00.145  00:00:00.149  00:00:00.149
 Task DCHSPX....................... : 00:00:00.013  00:00:00.019  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.160  00:00:00.159  00:00:00.159
  SubTask SPXTracking.............. : 00:00:00.015  00:00:00.010  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.033  00:00:00.039  00:00:00.020
 WriteEvent........................ : 00:00:06.685  00:00:03.319
analyzer........................... : 00:11:38.487  00:09:35.810  00:00:00.000
 DAQ bartender..................... : 00:02:02.440  00:00:56.679  00:00:56.349
 Task MEGMain...................... : 00:00:02.226  00:00:01.189  00:00:00.000
 Task ReadData..................... : 00:00:08.668  00:00:08.179  00:00:08.129
 Task SPX.......................... : 00:00:00.014  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.463  00:00:04.620  00:00:04.540
  SubTask SPXHitRec................ : 00:00:00.267  00:00:00.290  00:00:00.290
  SubTask SPXClustering............ : 00:00:10.601  00:00:10.570  00:00:10.510
  SubTask SPXIndependentTracking... : 00:00:27.799  00:00:27.509  00:00:27.489
 Task RDC.......................... : 00:00:00.014  00:00:00.060  00:00:00.019
  SubTask RDCWaveformAnalysis...... : 00:00:00.077  00:00:00.060  00:00:00.050
  SubTask RDCHitRec................ : 00:00:00.042  00:00:00.069  00:00:00.029
 Task CYLDCH....................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:38.718  00:01:38.370  00:01:38.280
  SubTask CYLDCHHitRec............. : 00:00:00.514  00:00:00.530  00:00:00.530
  SubTask CYLDCHTrackFinderPR...... : 00:02:33.669  00:02:32.530  00:02:32.490
  SubTask DCHKalmanFilterGEN....... : 00:01:03.524  00:01:03.299  00:01:03.269
  SubTask DCHTrackReFit............ : 00:00:00.136  00:00:00.139  00:00:00.139
 Task DCHSPX....................... : 00:00:00.011  00:00:00.030  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:55.258  00:00:54.959  00:00:54.939
  SubTask DCHSPXReFit.............. : 00:01:08.815  00:01:08.730  00:01:08.720
  SubTask SPXTracking.............. : 00:00:15.241  00:00:15.129  00:00:15.129
  SubTask DCHSPXTrackSelection..... : 00:00:00.071  00:00:00.049  00:00:00.049
 WriteEvent........................ : 00:00:52.222  00:00:08.209
analyzer........................... : 00:02:46.207  00:02:11.139  00:00:00.000
 DAQ bartender..................... : 00:01:16.430  00:01:00.959  00:01:00.689
 Task MEGMain...................... : 00:00:11.744  00:00:01.470  00:00:00.000
 Task ReadData..................... : 00:00:07.924  00:00:08.190  00:00:08.100
 Task SPX.......................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.513  00:00:05.489  00:00:05.429
  SubTask SPXHitRec................ : 00:00:00.299  00:00:00.330  00:00:00.320
  SubTask SPXClustering............ : 00:00:11.762  00:00:11.549  00:00:11.509
  SubTask SPXIndependentTracking... : 00:00:35.978  00:00:36.020  00:00:35.990
 Task RDC.......................... : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.154  00:00:00.039  00:00:00.029
  SubTask RDCHitRec................ : 00:00:00.045  00:00:00.030  00:00:00.020
 Task CYLDCH....................... : 00:00:00.009  00:00:00.009  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.126  00:00:00.070  00:00:00.070
  SubTask DCHKalmanFilterGEN....... : 00:00:00.030  00:00:00.019  00:00:00.019
  SubTask DCHTrackReFit............ : 00:00:00.154  00:00:00.200  00:00:00.180
 Task DCHSPX....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.019  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.153  00:00:00.220  00:00:00.199
  SubTask SPXTracking.............. : 00:00:00.028  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.030  00:00:00.040  00:00:00.019
 WriteEvent........................ : 00:00:05.023  00:00:02.450
analyzer........................... : 00:11:38.483  00:09:32.770  00:00:00.000
 DAQ bartender..................... : 00:01:55.313  00:00:54.669  00:00:54.349
 Task MEGMain...................... : 00:00:02.326  00:00:01.219  00:00:00.000
 Task ReadData..................... : 00:00:07.537  00:00:07.759  00:00:07.719
 Task SPX.......................... : 00:00:00.014  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.452  00:00:04.640  00:00:04.570
  SubTask SPXHitRec................ : 00:00:00.255  00:00:00.280  00:00:00.270
  SubTask SPXClustering............ : 00:00:08.513  00:00:08.110  00:00:08.070
  SubTask SPXIndependentTracking... : 00:00:28.931  00:00:27.920  00:00:27.920
 Task RDC.......................... : 00:00:00.015  00:00:00.019  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.328  00:00:00.049  00:00:00.049
  SubTask RDCHitRec................ : 00:00:00.040  00:00:00.040  00:00:00.030
 Task CYLDCH....................... : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:38.771  00:01:38.570  00:01:38.490
  SubTask CYLDCHHitRec............. : 00:00:00.513  00:00:00.439  00:00:00.439
  SubTask CYLDCHTrackFinderPR...... : 00:02:38.479  00:02:36.929  00:02:36.899
  SubTask DCHKalmanFilterGEN....... : 00:01:03.927  00:01:04.239  00:01:04.159
  SubTask DCHTrackReFit............ : 00:00:00.131  00:00:00.080  00:00:00.080
 Task DCHSPX....................... : 00:00:00.012  00:00:00.030  00:00:00.020
  SubTask DCHSPXMatching........... : 00:00:51.680  00:00:51.649  00:00:51.629
  SubTask DCHSPXReFit.............. : 00:01:06.412  00:01:05.770  00:01:05.740
  SubTask SPXTracking.............. : 00:00:17.220  00:00:17.519  00:00:17.499
  SubTask DCHSPXTrackSelection..... : 00:00:00.070  00:00:00.029  00:00:00.029
 WriteEvent........................ : 00:01:03.089  00:00:08.799
analyzer........................... : 00:02:26.706  00:02:09.320  00:00:00.000
 DAQ bartender..................... : 00:01:16.141  00:01:03.650  00:01:03.370
 Task MEGMain...................... : 00:00:01.320  00:00:01.210  00:00:00.010
 Task ReadData..................... : 00:00:10.374  00:00:09.959  00:00:09.859
 Task SPX.......................... : 00:00:00.018  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.327  00:00:05.359  00:00:05.269
  SubTask SPXHitRec................ : 00:00:00.329  00:00:00.409  00:00:00.389
  SubTask SPXClustering............ : 00:00:07.921  00:00:07.810  00:00:07.760
  SubTask SPXIndependentTracking... : 00:00:33.167  00:00:33.050  00:00:33.000
 Task RDC.......................... : 00:00:00.018  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.091  00:00:00.109  00:00:00.089
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.050  00:00:00.040
 Task CYLDCH....................... : 00:00:00.013  00:00:00.020  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.017  00:00:00.029  00:00:00.010
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.143  00:00:00.120  00:00:00.110
  SubTask DCHKalmanFilterGEN....... : 00:00:00.034  00:00:00.040  00:00:00.020
  SubTask DCHTrackReFit............ : 00:00:00.142  00:00:00.090  00:00:00.080
 Task DCHSPX....................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.019  00:00:00.009
  SubTask DCHSPXReFit.............. : 00:00:00.159  00:00:00.139  00:00:00.139
  SubTask SPXTracking.............. : 00:00:00.015  00:00:00.020  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.033  00:00:00.009  00:00:00.009
 WriteEvent........................ : 00:00:04.341  00:00:02.769
analyzer........................... : 00:12:58.400  00:11:24.850  00:00:00.000
 DAQ bartender..................... : 00:01:43.986  00:01:01.270  00:01:01.010
 Task MEGMain...................... : 00:00:01.463  00:00:01.319  00:00:00.000
 Task ReadData..................... : 00:00:07.533  00:00:07.510  00:00:07.450
 Task SPX.......................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.509  00:00:05.529  00:00:05.449
  SubTask SPXHitRec................ : 00:00:00.289  00:00:00.359  00:00:00.349
  SubTask SPXClustering............ : 00:00:14.869  00:00:14.719  00:00:14.659
  SubTask SPXIndependentTracking... : 00:00:34.208  00:00:33.859  00:00:33.819
 Task RDC.......................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.078  00:00:00.150  00:00:00.130
  SubTask RDCHitRec................ : 00:00:00.044  00:00:00.069  00:00:00.049
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:56.702  00:01:56.660  00:01:56.550
  SubTask CYLDCHHitRec............. : 00:00:00.606  00:00:00.619  00:00:00.619
  SubTask CYLDCHTrackFinderPR...... : 00:03:00.967  00:02:59.479  00:02:59.449
  SubTask DCHKalmanFilterGEN....... : 00:01:22.079  00:01:22.110  00:01:22.090
  SubTask DCHTrackReFit............ : 00:00:00.162  00:00:00.170  00:00:00.140
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:06.349  00:01:06.250  00:01:06.230
  SubTask DCHSPXReFit.............. : 00:01:22.684  00:01:22.980  00:01:22.970
  SubTask SPXTracking.............. : 00:00:21.811  00:00:21.669  00:00:21.659
  SubTask DCHSPXTrackSelection..... : 00:00:00.082  00:00:00.069  00:00:00.049
 WriteEvent........................ : 00:00:49.998  00:00:06.090
analyzer........................... : 00:02:50.187  00:02:14.180  00:00:00.000
 DAQ bartender..................... : 00:01:21.578  00:01:02.659  00:01:02.339
 Task MEGMain...................... : 00:00:09.049  00:00:01.550  00:00:00.000
 Task ReadData..................... : 00:00:08.065  00:00:07.859  00:00:07.809
 Task SPX.......................... : 00:00:00.015  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.692  00:00:05.850  00:00:05.780
  SubTask SPXHitRec................ : 00:00:00.310  00:00:00.329  00:00:00.299
  SubTask SPXClustering............ : 00:00:12.201  00:00:12.170  00:00:12.130
  SubTask SPXIndependentTracking... : 00:00:36.720  00:00:36.369  00:00:36.329
 Task RDC.......................... : 00:00:00.016  00:00:00.030  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.091  00:00:00.100  00:00:00.100
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.029  00:00:00.029
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.127  00:00:00.119  00:00:00.099
  SubTask DCHKalmanFilterGEN....... : 00:00:00.030  00:00:00.050  00:00:00.040
  SubTask DCHTrackReFit............ : 00:00:00.164  00:00:00.129  00:00:00.119
 Task DCHSPX....................... : 00:00:00.008  00:00:00.020  00:00:00.010
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.156  00:00:00.089  00:00:00.089
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.020  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.032  00:00:00.039  00:00:00.029
 WriteEvent........................ : 00:00:08.096  00:00:02.610
analyzer........................... : 00:13:01.922  00:11:35.579  00:00:00.000
 DAQ bartender..................... : 00:01:43.297  00:01:00.819  00:01:00.549
 Task MEGMain...................... : 00:00:01.461  00:00:01.340  00:00:00.009
 Task ReadData..................... : 00:00:07.569  00:00:07.449  00:00:07.349
 Task SPX.......................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.605  00:00:05.820  00:00:05.740
  SubTask SPXHitRec................ : 00:00:00.289  00:00:00.299  00:00:00.299
  SubTask SPXClustering............ : 00:00:15.108  00:00:14.989  00:00:14.969
  SubTask SPXIndependentTracking... : 00:00:36.969  00:00:36.569  00:00:36.559
 Task RDC.......................... : 00:00:00.012  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.079  00:00:00.090  00:00:00.080
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.050  00:00:00.050
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:57.282  00:01:57.250  00:01:57.140
  SubTask CYLDCHHitRec............. : 00:00:00.587  00:00:00.609  00:00:00.579
  SubTask CYLDCHTrackFinderPR...... : 00:03:13.470  00:03:12.290  00:03:12.260
  SubTask DCHKalmanFilterGEN....... : 00:01:23.556  00:01:23.309  00:01:23.289
  SubTask DCHTrackReFit............ : 00:00:00.167  00:00:00.189  00:00:00.160
 Task DCHSPX....................... : 00:00:00.009  00:00:00.020  00:00:00.009
  SubTask DCHSPXMatching........... : 00:01:00.489  00:01:00.479  00:01:00.449
  SubTask DCHSPXReFit.............. : 00:01:23.120  00:01:23.040  00:01:23.030
  SubTask SPXTracking.............. : 00:00:20.498  00:00:20.159  00:00:20.159
  SubTask DCHSPXTrackSelection..... : 00:00:00.080  00:00:00.090  00:00:00.069
 WriteEvent........................ : 00:00:44.224  00:00:06.799
```

```
./macros/analyzer_output.sh 20211000-root6.22.06-geant4.10.06.p03-gcc930/noxec_PR_fit_genfit 116
```
- - - -

60000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Oct  3 00:35 Eff_results11600.root
-rw-r--r--. 1 uchiyama unx-lke  18K Oct  3 00:50 Eff_results11610.root
-rw-r--r--. 1 uchiyama unx-lke  18K Oct  3 01:39 Eff_results11620.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:27 histos11600.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:31 histos11601.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:35 histos11602.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:42 histos11603.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:28 histos11604.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:29 histos11610.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:35 histos11611.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:43 histos11612.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:31 histos11613.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:31 histos11614.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:42 histos11620.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:41 histos11621.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:35 histos11622.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:30 histos11623.root
-rw-r--r--. 1 uchiyama unx-lke 3.2M Oct  2 23:41 histos11624.root
-rw-r--r--. 1 uchiyama unx-lke 228M Oct  2 23:27 rec11600.root
-rw-r--r--. 1 uchiyama unx-lke 229M Oct  2 23:31 rec11601.root
-rw-r--r--. 1 uchiyama unx-lke 231M Oct  2 23:35 rec11602.root
-rw-r--r--. 1 uchiyama unx-lke 230M Oct  2 23:42 rec11603.root
-rw-r--r--. 1 uchiyama unx-lke 228M Oct  2 23:28 rec11604.root
-rw-r--r--. 1 uchiyama unx-lke 231M Oct  2 23:29 rec11610.root
-rw-r--r--. 1 uchiyama unx-lke 233M Oct  2 23:35 rec11611.root
-rw-r--r--. 1 uchiyama unx-lke 231M Oct  2 23:43 rec11612.root
-rw-r--r--. 1 uchiyama unx-lke 231M Oct  2 23:31 rec11613.root
-rw-r--r--. 1 uchiyama unx-lke 231M Oct  2 23:31 rec11614.root
-rw-r--r--. 1 uchiyama unx-lke 232M Oct  2 23:42 rec11620.root
-rw-r--r--. 1 uchiyama unx-lke 231M Oct  2 23:41 rec11621.root
-rw-r--r--. 1 uchiyama unx-lke 233M Oct  2 23:35 rec11622.root
-rw-r--r--. 1 uchiyama unx-lke 227M Oct  2 23:30 rec11623.root
-rw-r--r--. 1 uchiyama unx-lke 233M Oct  2 23:41 rec11624.root

rec	119.069 kB/event
dch	109.348 kB/event
spx	5.788 kB/event
rdc	2.956 kB/event
reco	0.637 kB/event

```
######### execution time #########
96772.4/60000 = 1.61287 s/event
```
---------
analyzer........................... : 00:09:47.056  00:06:49.870  00:00:00.000
 DAQ bartender..................... : 00:07:15.755  00:04:26.339  00:04:25.679
 Task MEGMain...................... : 00:00:01.722  00:00:01.410  00:00:00.000
 Task ReadData..................... : 00:00:26.522  00:00:26.460  00:00:26.400
 Task SPX.......................... : 00:00:00.016  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.931  00:00:10.859  00:00:10.739
  SubTask SPXHitRec................ : 00:00:00.367  00:00:00.340  00:00:00.330
  SubTask SPXClustering............ : 00:00:24.263  00:00:24.019  00:00:23.959
  SubTask SPXIndependentTracking... : 00:00:56.833  00:00:56.750  00:00:56.730
 Task RDC.......................... : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:09.892  00:00:09.899  00:00:09.869
  SubTask RDCHitRec................ : 00:00:00.197  00:00:00.230  00:00:00.230
 Task CYLDCH....................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.014  00:00:00.020  00:00:00.010
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.136  00:00:00.169  00:00:00.139
  SubTask DCHKalmanFilterGEN....... : 00:00:00.039  00:00:00.020  00:00:00.010
  SubTask DCHTrackReFit............ : 00:00:00.175  00:00:00.289  00:00:00.259
 Task DCHSPX....................... : 00:00:00.009  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.012  00:00:00.019  00:00:00.010
  SubTask DCHSPXReFit.............. : 00:00:00.191  00:00:00.179  00:00:00.169
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.028  00:00:00.010  00:00:00.010
 WriteEvent........................ : 00:00:07.441  00:00:03.449
analyzer........................... : 01:30:36.133  01:24:38.380  00:00:00.000
 DAQ bartender..................... : 00:08:18.877  00:03:24.810  00:03:24.000
 Task MEGMain...................... : 00:00:01.350  00:00:01.060  00:00:00.000
 Task ReadData..................... : 00:00:23.444  00:00:23.260  00:00:23.190
 Task SPX.......................... : 00:00:00.016  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.280  00:00:08.069  00:00:07.989
  SubTask SPXHitRec................ : 00:00:00.291  00:00:00.260  00:00:00.250
  SubTask SPXClustering............ : 00:00:16.933  00:00:16.869  00:00:16.799
  SubTask SPXIndependentTracking... : 00:00:43.657  00:00:42.610  00:00:42.570
 Task RDC.......................... : 00:00:00.015  00:00:00.030  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:08.424  00:00:08.549  00:00:08.519
  SubTask RDCHitRec................ : 00:00:00.141  00:00:00.070  00:00:00.060
 Task CYLDCH....................... : 00:00:00.012  00:00:00.029  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:34.785  00:11:32.919  00:11:32.839
  SubTask CYLDCHHitRec............. : 00:00:14.354  00:00:14.300  00:00:14.290
  SubTask CYLDCHTrackFinderPR...... : 00:51:57.451  00:51:49.280  00:51:49.240
  SubTask DCHKalmanFilterGEN....... : 00:09:51.985  00:09:50.559  00:09:50.499
  SubTask DCHTrackReFit............ : 00:00:00.147  00:00:00.120  00:00:00.110
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:26.650  00:03:25.929  00:03:25.909
  SubTask DCHSPXReFit.............. : 00:01:44.131  00:01:44.050  00:01:44.030
  SubTask SPXTracking.............. : 00:00:27.996  00:00:27.889  00:00:27.859
  SubTask DCHSPXTrackSelection..... : 00:00:00.142  00:00:00.189  00:00:00.189
 WriteEvent........................ : 00:01:04.646  00:00:17.509
analyzer........................... : 00:10:09.057  00:07:07.560  00:00:00.000
 DAQ bartender..................... : 00:07:21.641  00:04:34.579  00:04:33.539
 Task MEGMain...................... : 00:00:01.465  00:00:01.349  00:00:00.000
 Task ReadData..................... : 00:00:26.151  00:00:26.090  00:00:26.030
 Task SPX.......................... : 00:00:00.017  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:11.211  00:00:11.369  00:00:11.279
  SubTask SPXHitRec................ : 00:00:00.371  00:00:00.280  00:00:00.270
  SubTask SPXClustering............ : 00:00:26.323  00:00:26.229  00:00:26.099
  SubTask SPXIndependentTracking... : 00:01:04.062  00:01:03.919  00:01:03.879
 Task RDC.......................... : 00:00:00.017  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:10.060  00:00:09.880  00:00:09.880
  SubTask RDCHitRec................ : 00:00:00.199  00:00:00.179  00:00:00.150
 Task CYLDCH....................... : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.015  00:00:00.020  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.146  00:00:00.219  00:00:00.199
  SubTask DCHKalmanFilterGEN....... : 00:00:00.040  00:00:00.030  00:00:00.030
  SubTask DCHTrackReFit............ : 00:00:00.176  00:00:00.090  00:00:00.090
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.186  00:00:00.209  00:00:00.209
  SubTask SPXTracking.............. : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.033  00:00:00.010  00:00:00.010
 WriteEvent........................ : 00:00:14.207  00:00:03.820
analyzer........................... : 01:34:19.985  01:28:26.420  00:00:00.000
 DAQ bartender..................... : 00:08:27.861  00:03:31.309  00:03:30.109
 Task MEGMain...................... : 00:00:01.251  00:00:01.070  00:00:00.000
 Task ReadData..................... : 00:00:23.935  00:00:23.590  00:00:23.540
 Task SPX.......................... : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.450  00:00:08.690  00:00:08.610
  SubTask SPXHitRec................ : 00:00:00.293  00:00:00.289  00:00:00.289
  SubTask SPXClustering............ : 00:00:18.687  00:00:18.270  00:00:18.220
  SubTask SPXIndependentTracking... : 00:00:49.454  00:00:49.400  00:00:49.380
 Task RDC.......................... : 00:00:00.016  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.640  00:00:08.859  00:00:08.839
  SubTask RDCHitRec................ : 00:00:00.143  00:00:00.209  00:00:00.199
 Task CYLDCH....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:37.528  00:11:35.369  00:11:35.249
  SubTask CYLDCHHitRec............. : 00:00:15.547  00:00:15.680  00:00:15.640
  SubTask CYLDCHTrackFinderPR...... : 00:55:20.775  00:55:12.379  00:55:12.369
  SubTask DCHKalmanFilterGEN....... : 00:09:46.976  00:09:45.380  00:09:45.340
  SubTask DCHTrackReFit............ : 00:00:00.148  00:00:00.110  00:00:00.100
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:36.253  00:03:35.670  00:03:35.650
  SubTask DCHSPXReFit.............. : 00:01:42.971  00:01:42.699  00:01:42.679
  SubTask SPXTracking.............. : 00:00:29.449  00:00:29.110  00:00:29.069
  SubTask DCHSPXTrackSelection..... : 00:00:00.141  00:00:00.159  00:00:00.139
 WriteEvent........................ : 00:01:00.230  00:00:17.900
analyzer........................... : 00:09:16.632  00:06:09.000  00:00:00.000
 DAQ bartender..................... : 00:06:52.103  00:04:00.190  00:03:58.980
 Task MEGMain...................... : 00:00:01.427  00:00:01.170  00:00:00.010
 Task ReadData..................... : 00:00:28.165  00:00:28.409  00:00:28.309
 Task SPX.......................... : 00:00:00.019  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.474  00:00:09.389  00:00:09.329
  SubTask SPXHitRec................ : 00:00:00.353  00:00:00.279  00:00:00.269
  SubTask SPXClustering............ : 00:00:14.171  00:00:14.149  00:00:14.099
  SubTask SPXIndependentTracking... : 00:00:51.157  00:00:50.660  00:00:50.650
 Task RDC.......................... : 00:00:00.019  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:09.581  00:00:09.429  00:00:09.409
  SubTask RDCHitRec................ : 00:00:00.171  00:00:00.219  00:00:00.199
 Task CYLDCH....................... : 00:00:00.013  00:00:00.019  00:00:00.009
  SubTask CYLDCHHitRec............. : 00:00:00.017  00:00:00.010  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.146  00:00:00.199  00:00:00.170
  SubTask DCHKalmanFilterGEN....... : 00:00:00.040  00:00:00.050  00:00:00.029
  SubTask DCHTrackReFit............ : 00:00:00.146  00:00:00.210  00:00:00.169
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.049  00:00:00.019
  SubTask DCHSPXReFit.............. : 00:00:00.155  00:00:00.169  00:00:00.169
  SubTask SPXTracking.............. : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.033  00:00:00.019  00:00:00.000
 WriteEvent........................ : 00:00:17.062  00:00:04.390
analyzer........................... : 01:38:02.982  01:33:18.190  00:00:00.000
 DAQ bartender..................... : 00:07:23.891  00:03:31.999  00:03:30.929
 Task MEGMain...................... : 00:00:01.441  00:00:01.240  00:00:00.010
 Task ReadData..................... : 00:00:20.085  00:00:20.180  00:00:20.120
 Task SPX.......................... : 00:00:00.013  00:00:00.020  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:09.559  00:00:09.809  00:00:09.739
  SubTask SPXHitRec................ : 00:00:00.270  00:00:00.290  00:00:00.280
  SubTask SPXClustering............ : 00:00:20.967  00:00:20.440  00:00:20.390
  SubTask SPXIndependentTracking... : 00:00:46.580  00:00:46.699  00:00:46.669
 Task RDC.......................... : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.028  00:00:07.760  00:00:07.760
  SubTask RDCHitRec................ : 00:00:00.137  00:00:00.100  00:00:00.100
 Task CYLDCH....................... : 00:00:00.009  00:00:00.010  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:34.849  00:11:35.170  00:11:35.030
  SubTask CYLDCHHitRec............. : 00:00:15.800  00:00:15.400  00:00:15.380
  SubTask CYLDCHTrackFinderPR...... : 00:59:44.106  00:59:37.229  00:59:37.209
  SubTask DCHKalmanFilterGEN....... : 00:10:13.531  00:10:13.580  00:10:13.550
  SubTask DCHTrackReFit............ : 00:00:00.151  00:00:00.079  00:00:00.079
 Task DCHSPX....................... : 00:00:00.008  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:34.761  00:03:34.760  00:03:34.760
  SubTask DCHSPXReFit.............. : 00:01:46.725  00:01:46.599  00:01:46.579
  SubTask SPXTracking.............. : 00:00:33.193  00:00:33.339  00:00:33.299
  SubTask DCHSPXTrackSelection..... : 00:00:00.144  00:00:00.100  00:00:00.080
 WriteEvent........................ : 00:00:59.148  00:00:15.230
analyzer........................... : 00:10:06.219  00:07:00.250  00:00:00.000
 DAQ bartender..................... : 00:07:23.442  00:04:32.509  00:04:31.589
 Task MEGMain...................... : 00:00:01.574  00:00:01.229  00:00:00.000
 Task ReadData..................... : 00:00:26.767  00:00:26.830  00:00:26.750
 Task SPX.......................... : 00:00:00.015  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:11.209  00:00:10.960  00:00:10.880
  SubTask SPXHitRec................ : 00:00:00.370  00:00:00.479  00:00:00.469
  SubTask SPXClustering............ : 00:00:24.274  00:00:24.180  00:00:24.110
  SubTask SPXIndependentTracking... : 00:01:00.501  00:01:00.359  00:01:00.339
 Task RDC.......................... : 00:00:00.015  00:00:00.039  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:09.902  00:00:10.019  00:00:09.999
  SubTask RDCHitRec................ : 00:00:00.196  00:00:00.220  00:00:00.199
 Task CYLDCH....................... : 00:00:00.010  00:00:00.009  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.014  00:00:00.029  00:00:00.029
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.133  00:00:00.119  00:00:00.109
  SubTask DCHKalmanFilterGEN....... : 00:00:00.038  00:00:00.039  00:00:00.039
  SubTask DCHTrackReFit............ : 00:00:00.175  00:00:00.160  00:00:00.149
 Task DCHSPX....................... : 00:00:00.008  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.020  00:00:00.020
  SubTask DCHSPXReFit.............. : 00:00:00.182  00:00:00.150  00:00:00.129
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.010  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.027  00:00:00.039  00:00:00.029
 WriteEvent........................ : 00:00:14.650  00:00:03.510
analyzer........................... : 01:44:56.787  01:40:04.489  00:00:00.000
 DAQ bartender..................... : 00:08:06.266  00:03:56.420  00:03:55.320
 Task MEGMain...................... : 00:00:02.274  00:00:01.519  00:00:00.009
 Task ReadData..................... : 00:00:21.739  00:00:21.809  00:00:21.739
 Task SPX.......................... : 00:00:00.014  00:00:00.019  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:09.805  00:00:09.489  00:00:09.409
  SubTask SPXHitRec................ : 00:00:00.294  00:00:00.259  00:00:00.239
  SubTask SPXClustering............ : 00:00:22.923  00:00:23.050  00:00:22.990
  SubTask SPXIndependentTracking... : 00:00:54.637  00:00:54.239  00:00:54.219
 Task RDC.......................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.880  00:00:08.470  00:00:08.470
  SubTask RDCHitRec................ : 00:00:00.152  00:00:00.129  00:00:00.109
 Task CYLDCH....................... : 00:00:00.009  00:00:00.010  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:12:51.399  00:12:51.589  00:12:51.459
  SubTask CYLDCHHitRec............. : 00:00:17.829  00:00:17.739  00:00:17.709
  SubTask CYLDCHTrackFinderPR...... : 01:02:16.984  01:02:16.639  01:02:16.609
  SubTask DCHKalmanFilterGEN....... : 00:11:35.733  00:11:35.320  00:11:35.300
  SubTask DCHTrackReFit............ : 00:00:00.178  00:00:00.209  00:00:00.169
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:04:00.825  00:04:00.650  00:04:00.620
  SubTask DCHSPXReFit.............. : 00:02:02.742  00:02:02.730  00:02:02.690
  SubTask SPXTracking.............. : 00:00:38.665  00:00:38.689  00:00:38.659
  SubTask DCHSPXTrackSelection..... : 00:00:00.165  00:00:00.190  00:00:00.190
 WriteEvent........................ : 00:00:55.218  00:00:16.650
analyzer........................... : 00:09:14.045  00:06:08.870  00:00:00.000
 DAQ bartender..................... : 00:06:50.335  00:03:59.850  00:03:58.660
 Task MEGMain...................... : 00:00:01.370  00:00:01.239  00:00:00.000
 Task ReadData..................... : 00:00:28.045  00:00:27.809  00:00:27.729
 Task SPX.......................... : 00:00:00.019  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.299  00:00:09.660  00:00:09.570
  SubTask SPXHitRec................ : 00:00:00.355  00:00:00.330  00:00:00.330
  SubTask SPXClustering............ : 00:00:14.515  00:00:14.330  00:00:14.270
  SubTask SPXIndependentTracking... : 00:00:50.952  00:00:50.500  00:00:50.500
 Task RDC.......................... : 00:00:00.019  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:09.614  00:00:09.659  00:00:09.639
  SubTask RDCHitRec................ : 00:00:00.170  00:00:00.119  00:00:00.109
 Task CYLDCH....................... : 00:00:00.014  00:00:00.019  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.017  00:00:00.020  00:00:00.010
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.151  00:00:00.159  00:00:00.149
  SubTask DCHKalmanFilterGEN....... : 00:00:00.039  00:00:00.030  00:00:00.010
  SubTask DCHTrackReFit............ : 00:00:00.147  00:00:00.130  00:00:00.120
 Task DCHSPX....................... : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.010  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.164  00:00:00.090  00:00:00.090
  SubTask SPXTracking.............. : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.035  00:00:00.000  00:00:00.000
 WriteEvent........................ : 00:00:15.427  00:00:04.429
analyzer........................... : 01:30:56.768  01:25:52.110  00:00:00.000
 DAQ bartender..................... : 00:07:42.058  00:03:24.649  00:03:23.529
 Task MEGMain...................... : 00:00:03.273  00:00:01.529  00:00:00.000
 Task ReadData..................... : 00:00:20.390  00:00:20.560  00:00:20.490
 Task SPX.......................... : 00:00:00.013  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.752  00:00:08.589  00:00:08.519
  SubTask SPXHitRec................ : 00:00:00.270  00:00:00.280  00:00:00.260
  SubTask SPXClustering............ : 00:00:22.672  00:00:22.719  00:00:22.679
  SubTask SPXIndependentTracking... : 00:00:45.272  00:00:45.110  00:00:45.100
 Task RDC.......................... : 00:00:00.011  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:07.857  00:00:08.199  00:00:08.189
  SubTask RDCHitRec................ : 00:00:00.136  00:00:00.080  00:00:00.080
 Task CYLDCH....................... : 00:00:00.008  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:16.141  00:11:15.729  00:11:15.629
  SubTask CYLDCHHitRec............. : 00:00:15.524  00:00:15.550  00:00:15.530
  SubTask CYLDCHTrackFinderPR...... : 00:53:17.899  00:53:17.790  00:53:17.750
  SubTask DCHKalmanFilterGEN....... : 00:09:37.375  00:09:37.099  00:09:37.059
  SubTask DCHTrackReFit............ : 00:00:00.159  00:00:00.140  00:00:00.130
 Task DCHSPX....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:34.807  00:03:34.789  00:03:34.769
  SubTask DCHSPXReFit.............. : 00:01:44.383  00:01:44.190  00:01:44.190
  SubTask SPXTracking.............. : 00:00:31.707  00:00:31.840  00:00:31.840
  SubTask DCHSPXTrackSelection..... : 00:00:00.134  00:00:00.120  00:00:00.120
 WriteEvent........................ : 00:00:58.015  00:00:14.570
analyzer........................... : 00:09:37.384  00:06:15.939  00:00:00.000
 DAQ bartender..................... : 00:07:09.097  00:04:05.210  00:04:04.029
 Task MEGMain...................... : 00:00:01.496  00:00:01.159  00:00:00.009
 Task ReadData..................... : 00:00:28.527  00:00:28.139  00:00:28.089
 Task SPX.......................... : 00:00:00.018  00:00:00.029  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:09.346  00:00:09.709  00:00:09.619
  SubTask SPXHitRec................ : 00:00:00.356  00:00:00.350  00:00:00.340
  SubTask SPXClustering............ : 00:00:15.035  00:00:14.860  00:00:14.790
  SubTask SPXIndependentTracking... : 00:00:51.943  00:00:51.370  00:00:51.350
 Task RDC.......................... : 00:00:00.018  00:00:00.030  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:09.535  00:00:09.450  00:00:09.430
  SubTask RDCHitRec................ : 00:00:00.169  00:00:00.209  00:00:00.199
 Task CYLDCH....................... : 00:00:00.014  00:00:00.039  00:00:00.009
  SubTask CYLDCHHitRec............. : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.144  00:00:00.080  00:00:00.040
  SubTask DCHKalmanFilterGEN....... : 00:00:00.039  00:00:00.039  00:00:00.029
  SubTask DCHTrackReFit............ : 00:00:00.143  00:00:00.169  00:00:00.159
 Task DCHSPX....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.014  00:00:00.020  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.166  00:00:00.209  00:00:00.209
  SubTask SPXTracking.............. : 00:00:00.014  00:00:00.030  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.032  00:00:00.019  00:00:00.009
 WriteEvent........................ : 00:00:17.020  00:00:04.530
analyzer........................... : 01:32:14.832  01:27:30.250  00:00:00.000
 DAQ bartender..................... : 00:07:25.010  00:03:27.610  00:03:26.500
 Task MEGMain...................... : 00:00:01.440  00:00:01.249  00:00:00.000
 Task ReadData..................... : 00:00:19.898  00:00:19.850  00:00:19.790
 Task SPX.......................... : 00:00:00.013  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.157  00:00:09.149  00:00:09.069
  SubTask SPXHitRec................ : 00:00:00.267  00:00:00.219  00:00:00.219
  SubTask SPXClustering............ : 00:00:21.529  00:00:21.399  00:00:21.329
  SubTask SPXIndependentTracking... : 00:00:46.310  00:00:45.929  00:00:45.909
 Task RDC.......................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:07.905  00:00:08.380  00:00:08.380
  SubTask RDCHitRec................ : 00:00:00.134  00:00:00.160  00:00:00.160
 Task CYLDCH....................... : 00:00:00.026  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:33.993  00:11:33.860  00:11:33.739
  SubTask CYLDCHHitRec............. : 00:00:15.440  00:00:15.340  00:00:15.320
  SubTask CYLDCHTrackFinderPR...... : 00:54:37.739  00:54:37.549  00:54:37.509
  SubTask DCHKalmanFilterGEN....... : 00:09:42.735  00:09:42.289  00:09:42.239
  SubTask DCHTrackReFit............ : 00:00:00.149  00:00:00.150  00:00:00.140
 Task DCHSPX....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:27.992  00:03:28.030  00:03:28.010
  SubTask DCHSPXReFit.............. : 00:01:48.575  00:01:48.450  00:01:48.440
  SubTask SPXTracking.............. : 00:00:27.534  00:00:27.380  00:00:27.370
  SubTask DCHSPXTrackSelection..... : 00:00:00.138  00:00:00.150  00:00:00.140
 WriteEvent........................ : 00:00:59.378  00:00:14.989
analyzer........................... : 00:09:28.507  00:06:09.580  00:00:00.000
 DAQ bartender..................... : 00:07:04.185  00:04:02.849  00:04:01.639
 Task MEGMain...................... : 00:00:01.687  00:00:01.200  00:00:00.000
 Task ReadData..................... : 00:00:27.905  00:00:27.620  00:00:27.570
 Task SPX.......................... : 00:00:00.018  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.318  00:00:09.229  00:00:09.109
  SubTask SPXHitRec................ : 00:00:00.352  00:00:00.359  00:00:00.359
  SubTask SPXClustering............ : 00:00:14.431  00:00:14.290  00:00:14.190
  SubTask SPXIndependentTracking... : 00:00:49.084  00:00:48.479  00:00:48.449
 Task RDC.......................... : 00:00:00.018  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:09.751  00:00:09.919  00:00:09.909
  SubTask RDCHitRec................ : 00:00:00.168  00:00:00.189  00:00:00.169
 Task CYLDCH....................... : 00:00:00.013  00:00:00.039  00:00:00.010
  SubTask CYLDCHHitRec............. : 00:00:00.016  00:00:00.019  00:00:00.009
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.142  00:00:00.179  00:00:00.139
  SubTask DCHKalmanFilterGEN....... : 00:00:00.039  00:00:00.049  00:00:00.040
  SubTask DCHTrackReFit............ : 00:00:00.147  00:00:00.129  00:00:00.129
 Task DCHSPX....................... : 00:00:00.012  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.015  00:00:00.019  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.171  00:00:00.109  00:00:00.109
  SubTask SPXTracking.............. : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.031  00:00:00.030  00:00:00.020
 WriteEvent........................ : 00:00:17.197  00:00:04.409
analyzer........................... : 01:37:49.547  01:33:09.050  00:00:00.000
 DAQ bartender..................... : 00:07:23.311  00:03:30.400  00:03:29.230
 Task MEGMain...................... : 00:00:03.085  00:00:01.579  00:00:00.000
 Task ReadData..................... : 00:00:20.885  00:00:21.019  00:00:20.939
 Task SPX.......................... : 00:00:00.013  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.130  00:00:08.999  00:00:08.929
  SubTask SPXHitRec................ : 00:00:00.268  00:00:00.270  00:00:00.260
  SubTask SPXClustering............ : 00:00:23.468  00:00:23.089  00:00:23.049
  SubTask SPXIndependentTracking... : 00:00:44.790  00:00:44.520  00:00:44.520
 Task RDC.......................... : 00:00:00.011  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.015  00:00:07.949  00:00:07.919
  SubTask RDCHitRec................ : 00:00:00.138  00:00:00.190  00:00:00.170
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:32.960  00:11:32.769  00:11:32.589
  SubTask CYLDCHHitRec............. : 00:00:16.046  00:00:15.850  00:00:15.820
  SubTask CYLDCHTrackFinderPR...... : 00:59:23.088  00:59:21.240  00:59:21.220
  SubTask DCHKalmanFilterGEN....... : 00:10:19.215  00:10:19.299  00:10:19.249
  SubTask DCHTrackReFit............ : 00:00:00.184  00:00:00.070  00:00:00.070
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:35.867  00:03:36.049  00:03:36.049
  SubTask DCHSPXReFit.............. : 00:01:51.578  00:01:51.090  00:01:51.070
  SubTask SPXTracking.............. : 00:00:30.886  00:00:30.980  00:00:30.930
  SubTask DCHSPXTrackSelection..... : 00:00:00.140  00:00:00.120  00:00:00.120
 WriteEvent........................ : 00:00:56.690  00:00:15.340
analyzer........................... : 00:10:13.493  00:06:53.310  00:00:00.000
 DAQ bartender..................... : 00:07:36.765  00:04:28.859  00:04:27.799
 Task MEGMain...................... : 00:00:02.855  00:00:01.579  00:00:00.000
 Task ReadData..................... : 00:00:26.350  00:00:26.289  00:00:26.179
 Task SPX.......................... : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.724  00:00:10.760  00:00:10.680
  SubTask SPXHitRec................ : 00:00:00.380  00:00:00.350  00:00:00.330
  SubTask SPXClustering............ : 00:00:24.903  00:00:24.579  00:00:24.489
  SubTask SPXIndependentTracking... : 00:00:57.959  00:00:57.390  00:00:57.340
 Task RDC.......................... : 00:00:00.016  00:00:00.029  00:00:00.019
  SubTask RDCWaveformAnalysis...... : 00:00:09.672  00:00:09.619  00:00:09.599
  SubTask RDCHitRec................ : 00:00:00.193  00:00:00.220  00:00:00.200
 Task CYLDCH....................... : 00:00:00.010  00:00:00.019  00:00:00.010
  SubTask CYLDCHHitRec............. : 00:00:00.051  00:00:00.009  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.162  00:00:00.149  00:00:00.130
  SubTask DCHKalmanFilterGEN....... : 00:00:00.040  00:00:00.030  00:00:00.020
  SubTask DCHTrackReFit............ : 00:00:00.172  00:00:00.180  00:00:00.170
 Task DCHSPX....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.020  00:00:00.020
  SubTask DCHSPXReFit.............. : 00:00:00.169  00:00:00.210  00:00:00.179
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.019  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.028  00:00:00.030  00:00:00.019
 WriteEvent........................ : 00:00:08.800  00:00:03.440
analyzer........................... : 01:45:48.940  01:40:46.359  00:00:00.000
 DAQ bartender..................... : 00:08:19.306  00:03:59.189  00:03:58.069
 Task MEGMain...................... : 00:00:01.375  00:00:01.320  00:00:00.010
 Task ReadData..................... : 00:00:23.267  00:00:23.319  00:00:23.259
 Task SPX.......................... : 00:00:00.014  00:00:00.029  00:00:00.019
  SubTask SPXWaveformAnalysis...... : 00:00:09.613  00:00:09.330  00:00:09.260
  SubTask SPXHitRec................ : 00:00:00.300  00:00:00.359  00:00:00.339
  SubTask SPXClustering............ : 00:00:25.031  00:00:25.039  00:00:24.979
  SubTask SPXIndependentTracking... : 00:00:52.063  00:00:51.710  00:00:51.710
 Task RDC.......................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.883  00:00:08.949  00:00:08.919
  SubTask RDCHitRec................ : 00:00:00.155  00:00:00.180  00:00:00.170
 Task CYLDCH....................... : 00:00:00.009  00:00:00.019  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:12:55.563  00:12:55.550  00:12:55.450
  SubTask CYLDCHHitRec............. : 00:00:17.846  00:00:17.879  00:00:17.869
  SubTask CYLDCHTrackFinderPR...... : 01:03:03.741  01:03:03.319  01:03:03.269
  SubTask DCHKalmanFilterGEN....... : 00:11:38.455  00:11:38.219  00:11:38.159
  SubTask DCHTrackReFit............ : 00:00:00.176  00:00:00.249  00:00:00.219
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:54.768  00:03:54.610  00:03:54.580
  SubTask DCHSPXReFit.............. : 00:01:58.208  00:01:58.009  00:01:57.999
  SubTask SPXTracking.............. : 00:00:33.000  00:00:33.000  00:00:32.980
  SubTask DCHSPXTrackSelection..... : 00:00:00.162  00:00:00.190  00:00:00.170
 WriteEvent........................ : 00:00:56.741  00:00:16.789
analyzer........................... : 00:10:11.322  00:06:59.740  00:00:00.000
 DAQ bartender..................... : 00:07:23.071  00:04:31.329  00:04:30.369
 Task MEGMain...................... : 00:00:01.840  00:00:01.320  00:00:00.010
 Task ReadData..................... : 00:00:26.558  00:00:26.689  00:00:26.619
 Task SPX.......................... : 00:00:00.015  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:11.235  00:00:11.039  00:00:10.989
  SubTask SPXHitRec................ : 00:00:00.365  00:00:00.370  00:00:00.360
  SubTask SPXClustering............ : 00:00:26.309  00:00:26.080  00:00:26.010
  SubTask SPXIndependentTracking... : 00:00:59.850  00:00:59.729  00:00:59.709
 Task RDC.......................... : 00:00:00.015  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:09.654  00:00:09.489  00:00:09.479
  SubTask RDCHitRec................ : 00:00:00.194  00:00:00.210  00:00:00.170
 Task CYLDCH....................... : 00:00:00.010  00:00:00.009  00:00:00.009
  SubTask CYLDCHHitRec............. : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.132  00:00:00.110  00:00:00.100
  SubTask DCHKalmanFilterGEN....... : 00:00:00.038  00:00:00.049  00:00:00.049
  SubTask DCHTrackReFit............ : 00:00:00.172  00:00:00.149  00:00:00.139
 Task DCHSPX....................... : 00:00:00.009  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.179  00:00:00.140  00:00:00.140
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.019  00:00:00.009
  SubTask DCHSPXTrackSelection..... : 00:00:00.029  00:00:00.050  00:00:00.040
 WriteEvent........................ : 00:00:15.762  00:00:03.700
analyzer........................... : 01:33:43.679  01:28:48.949  00:00:00.000
 DAQ bartender..................... : 00:07:38.354  00:03:26.400  00:03:25.280
 Task MEGMain...................... : 00:00:03.353  00:00:01.520  00:00:00.020
 Task ReadData..................... : 00:00:21.268  00:00:21.379  00:00:21.309
 Task SPX.......................... : 00:00:00.014  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.033  00:00:09.270  00:00:09.200
  SubTask SPXHitRec................ : 00:00:00.275  00:00:00.260  00:00:00.250
  SubTask SPXClustering............ : 00:00:26.278  00:00:26.139  00:00:26.069
  SubTask SPXIndependentTracking... : 00:00:45.656  00:00:45.409  00:00:45.389
 Task RDC.......................... : 00:00:00.011  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:07.575  00:00:07.720  00:00:07.710
  SubTask RDCHitRec................ : 00:00:00.136  00:00:00.090  00:00:00.090
 Task CYLDCH....................... : 00:00:00.008  00:00:00.019  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:16.435  00:11:16.200  00:11:16.100
  SubTask CYLDCHHitRec............. : 00:00:15.810  00:00:16.020  00:00:16.010
  SubTask CYLDCHTrackFinderPR...... : 00:56:08.547  00:56:07.759  00:56:07.709
  SubTask DCHKalmanFilterGEN....... : 00:09:47.414  00:09:47.409  00:09:47.399
  SubTask DCHTrackReFit............ : 00:00:00.155  00:00:00.170  00:00:00.160
 Task DCHSPX....................... : 00:00:00.008  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:24.777  00:03:24.540  00:03:24.530
  SubTask DCHSPXReFit.............. : 00:01:45.386  00:01:45.550  00:01:45.540
  SubTask SPXTracking.............. : 00:00:29.754  00:00:29.679  00:00:29.659
  SubTask DCHSPXTrackSelection..... : 00:00:00.137  00:00:00.130  00:00:00.119
 WriteEvent........................ : 00:00:53.675  00:00:14.700
analyzer........................... : 00:10:10.810  00:06:58.100  00:00:00.000
 DAQ bartender..................... : 00:07:27.365  00:04:33.629  00:04:32.619
 Task MEGMain...................... : 00:00:01.611  00:00:01.430  00:00:00.000
 Task ReadData..................... : 00:00:26.523  00:00:26.289  00:00:26.229
 Task SPX.......................... : 00:00:00.016  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.890  00:00:10.880  00:00:10.800
  SubTask SPXHitRec................ : 00:00:00.361  00:00:00.299  00:00:00.269
  SubTask SPXClustering............ : 00:00:23.582  00:00:23.580  00:00:23.540
  SubTask SPXIndependentTracking... : 00:00:59.489  00:00:58.579  00:00:58.549
 Task RDC.......................... : 00:00:00.016  00:00:00.029  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:10.020  00:00:09.680  00:00:09.670
  SubTask RDCHitRec................ : 00:00:00.192  00:00:00.160  00:00:00.160
 Task CYLDCH....................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.014  00:00:00.019  00:00:00.009
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.133  00:00:00.139  00:00:00.129
  SubTask DCHKalmanFilterGEN....... : 00:00:00.038  00:00:00.040  00:00:00.030
  SubTask DCHTrackReFit............ : 00:00:00.177  00:00:00.190  00:00:00.180
 Task DCHSPX....................... : 00:00:00.009  00:00:00.019  00:00:00.009
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.186  00:00:00.200  00:00:00.189
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.028  00:00:00.019  00:00:00.009
 WriteEvent........................ : 00:00:15.291  00:00:03.719
analyzer........................... : 01:34:01.693  01:29:26.729  00:00:00.000
 DAQ bartender..................... : 00:07:21.201  00:03:27.279  00:03:26.129
 Task MEGMain...................... : 00:00:01.386  00:00:01.320  00:00:00.000
 Task ReadData..................... : 00:00:20.209  00:00:20.100  00:00:20.050
 Task SPX.......................... : 00:00:00.012  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.877  00:00:09.019  00:00:08.959
  SubTask SPXHitRec................ : 00:00:00.270  00:00:00.219  00:00:00.219
  SubTask SPXClustering............ : 00:00:22.106  00:00:21.940  00:00:21.880
  SubTask SPXIndependentTracking... : 00:00:46.501  00:00:46.050  00:00:46.050
 Task RDC.......................... : 00:00:00.011  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:07.936  00:00:07.629  00:00:07.609
  SubTask RDCHitRec................ : 00:00:00.135  00:00:00.129  00:00:00.119
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:28.126  00:11:28.009  00:11:27.919
  SubTask CYLDCHHitRec............. : 00:00:15.772  00:00:16.080  00:00:16.050
  SubTask CYLDCHTrackFinderPR...... : 00:56:07.074  00:56:05.379  00:56:05.349
  SubTask DCHKalmanFilterGEN....... : 00:10:10.748  00:10:10.869  00:10:10.849
  SubTask DCHTrackReFit............ : 00:00:00.149  00:00:00.149  00:00:00.139
 Task DCHSPX....................... : 00:00:00.007  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:30.964  00:03:30.990  00:03:30.990
  SubTask DCHSPXReFit.............. : 00:01:50.774  00:01:50.930  00:01:50.910
  SubTask SPXTracking.............. : 00:00:27.951  00:00:27.769  00:00:27.749
  SubTask DCHSPXTrackSelection..... : 00:00:00.135  00:00:00.130  00:00:00.130
 WriteEvent........................ : 00:00:52.100  00:00:14.500
analyzer........................... : 00:10:06.800  00:07:00.389  00:00:00.000
 DAQ bartender..................... : 00:07:22.298  00:04:31.510  00:04:30.580
 Task MEGMain...................... : 00:00:01.773  00:00:01.330  00:00:00.009
 Task ReadData..................... : 00:00:26.816  00:00:26.440  00:00:26.380
 Task SPX.......................... : 00:00:00.015  00:00:00.030  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.896  00:00:11.130  00:00:11.060
  SubTask SPXHitRec................ : 00:00:00.362  00:00:00.330  00:00:00.320
  SubTask SPXClustering............ : 00:00:24.579  00:00:24.699  00:00:24.609
  SubTask SPXIndependentTracking... : 00:01:01.704  00:01:01.479  00:01:01.419
 Task RDC.......................... : 00:00:00.016  00:00:00.029  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:09.712  00:00:09.669  00:00:09.639
  SubTask RDCHitRec................ : 00:00:00.194  00:00:00.210  00:00:00.180
 Task CYLDCH....................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.014  00:00:00.009  00:00:00.009
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.133  00:00:00.149  00:00:00.129
  SubTask DCHKalmanFilterGEN....... : 00:00:00.039  00:00:00.010  00:00:00.000
  SubTask DCHTrackReFit............ : 00:00:00.175  00:00:00.210  00:00:00.210
 Task DCHSPX....................... : 00:00:00.009  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.012  00:00:00.020  00:00:00.010
  SubTask DCHSPXReFit.............. : 00:00:00.187  00:00:00.220  00:00:00.190
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.030  00:00:00.030
  SubTask DCHSPXTrackSelection..... : 00:00:00.028  00:00:00.019  00:00:00.000
 WriteEvent........................ : 00:00:14.749  00:00:03.769
analyzer........................... : 01:45:00.499  01:39:48.319  00:00:00.000
 DAQ bartender..................... : 00:08:23.259  00:03:57.970  00:03:57.200
 Task MEGMain...................... : 00:00:01.342  00:00:01.230  00:00:00.000
 Task ReadData..................... : 00:00:24.336  00:00:23.640  00:00:23.610
 Task SPX.......................... : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.914  00:00:09.769  00:00:09.689
  SubTask SPXHitRec................ : 00:00:00.305  00:00:00.380  00:00:00.350
  SubTask SPXClustering............ : 00:00:27.871  00:00:27.819  00:00:27.739
  SubTask SPXIndependentTracking... : 00:00:53.331  00:00:53.290  00:00:53.280
 Task RDC.......................... : 00:00:00.013  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.788  00:00:08.879  00:00:08.859
  SubTask RDCHitRec................ : 00:00:00.156  00:00:00.200  00:00:00.180
 Task CYLDCH....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:12:54.551  00:12:54.119  00:12:54.019
  SubTask CYLDCHHitRec............. : 00:00:18.445  00:00:18.560  00:00:18.540
  SubTask CYLDCHTrackFinderPR...... : 01:02:08.285  01:02:06.800  01:02:06.790
  SubTask DCHKalmanFilterGEN....... : 00:11:13.372  00:11:13.369  00:11:13.329
  SubTask DCHTrackReFit............ : 00:00:00.192  00:00:00.169  00:00:00.139
 Task DCHSPX....................... : 00:00:00.009  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:04:08.906  00:04:08.810  00:04:08.790
  SubTask DCHSPXReFit.............. : 00:02:03.871  00:02:03.880  00:02:03.840
  SubTask SPXTracking.............. : 00:00:34.259  00:00:34.099  00:00:34.089
  SubTask DCHSPXTrackSelection..... : 00:00:00.165  00:00:00.219  00:00:00.219
 WriteEvent........................ : 00:00:59.092  00:00:16.199
analyzer........................... : 00:10:00.987  00:06:53.239  00:00:00.000
 DAQ bartender..................... : 00:07:23.685  00:04:25.669  00:04:24.769
 Task MEGMain...................... : 00:00:01.838  00:00:01.240  00:00:00.009
 Task ReadData..................... : 00:00:26.770  00:00:26.630  00:00:26.550
 Task SPX.......................... : 00:00:00.016  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.853  00:00:11.090  00:00:11.020
  SubTask SPXHitRec................ : 00:00:00.365  00:00:00.310  00:00:00.300
  SubTask SPXClustering............ : 00:00:26.032  00:00:26.150  00:00:26.050
  SubTask SPXIndependentTracking... : 00:00:58.983  00:00:58.689  00:00:58.659
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:09.940  00:00:09.810  00:00:09.800
  SubTask RDCHitRec................ : 00:00:00.198  00:00:00.189  00:00:00.179
 Task CYLDCH....................... : 00:00:00.011  00:00:00.029  00:00:00.010
  SubTask CYLDCHHitRec............. : 00:00:00.031  00:00:00.009  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.134  00:00:00.119  00:00:00.109
  SubTask DCHKalmanFilterGEN....... : 00:00:00.040  00:00:00.060  00:00:00.030
  SubTask DCHTrackReFit............ : 00:00:00.174  00:00:00.189  00:00:00.179
 Task DCHSPX....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.168  00:00:00.200  00:00:00.200
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.028  00:00:00.049  00:00:00.049
 WriteEvent........................ : 00:00:09.093  00:00:03.649
analyzer........................... : 01:43:59.300  01:39:31.989  00:00:00.000
 DAQ bartender..................... : 00:07:18.626  00:03:50.729  00:03:49.669
 Task MEGMain...................... : 00:00:02.482  00:00:01.389  00:00:00.000
 Task ReadData..................... : 00:00:21.166  00:00:21.439  00:00:21.329
 Task SPX.......................... : 00:00:00.016  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:10.465  00:00:09.979  00:00:09.909
  SubTask SPXHitRec................ : 00:00:00.298  00:00:00.190  00:00:00.190
  SubTask SPXClustering............ : 00:00:24.403  00:00:24.140  00:00:24.060
  SubTask SPXIndependentTracking... : 00:00:49.870  00:00:49.850  00:00:49.830
 Task RDC.......................... : 00:00:00.013  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.736  00:00:08.360  00:00:08.350
  SubTask RDCHitRec................ : 00:00:00.153  00:00:00.109  00:00:00.109
 Task CYLDCH....................... : 00:00:00.009  00:00:00.010  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:12:36.120  00:12:36.140  00:12:36.010
  SubTask CYLDCHHitRec............. : 00:00:17.866  00:00:18.229  00:00:18.189
  SubTask CYLDCHTrackFinderPR...... : 01:03:36.861  01:03:26.009  01:03:25.959
  SubTask DCHKalmanFilterGEN....... : 00:10:51.231  00:10:51.180  00:10:51.140
  SubTask DCHTrackReFit............ : 00:00:00.176  00:00:00.150  00:00:00.150
 Task DCHSPX....................... : 00:00:00.009  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:42.449  00:03:42.510  00:03:42.500
  SubTask DCHSPXReFit.............. : 00:01:54.201  00:01:53.999  00:01:53.989
  SubTask SPXTracking.............. : 00:00:32.566  00:00:32.510  00:00:32.470
  SubTask DCHSPXTrackSelection..... : 00:00:00.163  00:00:00.230  00:00:00.210
 WriteEvent........................ : 00:01:00.488  00:00:16.199
analyzer........................... : 00:10:15.426  00:06:58.240  00:00:00.000
 DAQ bartender..................... : 00:07:32.507  00:04:33.280  00:04:32.280
 Task MEGMain...................... : 00:00:01.703  00:00:01.360  00:00:00.000
 Task ReadData..................... : 00:00:26.657  00:00:26.499  00:00:26.439
 Task SPX.......................... : 00:00:00.016  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:11.248  00:00:11.259  00:00:11.169
  SubTask SPXHitRec................ : 00:00:00.366  00:00:00.360  00:00:00.340
  SubTask SPXClustering............ : 00:00:24.787  00:00:24.490  00:00:24.400
  SubTask SPXIndependentTracking... : 00:00:57.665  00:00:57.340  00:00:57.310
 Task RDC.......................... : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:09.980  00:00:10.409  00:00:10.399
  SubTask RDCHitRec................ : 00:00:00.197  00:00:00.160  00:00:00.150
 Task CYLDCH....................... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.014  00:00:00.050  00:00:00.019
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.133  00:00:00.150  00:00:00.120
  SubTask DCHKalmanFilterGEN....... : 00:00:00.039  00:00:00.039  00:00:00.019
  SubTask DCHTrackReFit............ : 00:00:00.176  00:00:00.079  00:00:00.069
 Task DCHSPX....................... : 00:00:00.009  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.020  00:00:00.020
  SubTask DCHSPXReFit.............. : 00:00:00.181  00:00:00.140  00:00:00.140
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.019  00:00:00.009
  SubTask DCHSPXTrackSelection..... : 00:00:00.028  00:00:00.009  00:00:00.009
 WriteEvent........................ : 00:00:15.352  00:00:03.609
analyzer........................... : 01:37:44.669  01:33:07.640  00:00:00.000
 DAQ bartender..................... : 00:07:13.292  00:03:35.120  00:03:34.050
 Task MEGMain...................... : 00:00:02.731  00:00:01.420  00:00:00.000
 Task ReadData..................... : 00:00:21.007  00:00:21.070  00:00:21.020
 Task SPX.......................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:09.841  00:00:09.750  00:00:09.670
  SubTask SPXHitRec................ : 00:00:00.275  00:00:00.229  00:00:00.219
  SubTask SPXClustering............ : 00:00:23.369  00:00:22.929  00:00:22.869
  SubTask SPXIndependentTracking... : 00:00:46.381  00:00:45.480  00:00:45.480
 Task RDC.......................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.164  00:00:08.160  00:00:08.160
  SubTask RDCHitRec................ : 00:00:00.143  00:00:00.149  00:00:00.149
 Task CYLDCH....................... : 00:00:00.008  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:51.576  00:11:51.709  00:11:51.579
  SubTask CYLDCHHitRec............. : 00:00:16.169  00:00:16.170  00:00:16.140
  SubTask CYLDCHTrackFinderPR...... : 00:59:04.162  00:59:03.769  00:59:03.719
  SubTask DCHKalmanFilterGEN....... : 00:10:23.958  00:10:24.079  00:10:24.069
  SubTask DCHTrackReFit............ : 00:00:00.164  00:00:00.179  00:00:00.159
 Task DCHSPX....................... : 00:00:00.008  00:00:00.030  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:25.790  00:03:25.580  00:03:25.560
  SubTask DCHSPXReFit.............. : 00:01:45.580  00:01:45.830  00:01:45.790
  SubTask SPXTracking.............. : 00:00:31.533  00:00:31.569  00:00:31.529
  SubTask DCHSPXTrackSelection..... : 00:00:00.148  00:00:00.100  00:00:00.089
 WriteEvent........................ : 00:01:09.444  00:00:15.709
analyzer........................... : 00:10:01.457  00:06:52.849  00:00:00.000
 DAQ bartender..................... : 00:07:19.884  00:04:30.170  00:04:29.140
 Task MEGMain...................... : 00:00:01.854  00:00:01.240  00:00:00.009
 Task ReadData..................... : 00:00:26.160  00:00:26.160  00:00:26.110
 Task SPX.......................... : 00:00:00.016  00:00:00.040  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:11.350  00:00:11.269  00:00:11.169
  SubTask SPXHitRec................ : 00:00:00.363  00:00:00.400  00:00:00.380
  SubTask SPXClustering............ : 00:00:24.033  00:00:23.929  00:00:23.809
  SubTask SPXIndependentTracking... : 00:00:56.662  00:00:56.179  00:00:56.169
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:09.898  00:00:09.719  00:00:09.699
  SubTask RDCHitRec................ : 00:00:00.191  00:00:00.169  00:00:00.149
 Task CYLDCH....................... : 00:00:00.011  00:00:00.019  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.014  00:00:00.009  00:00:00.000
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.135  00:00:00.169  00:00:00.139
  SubTask DCHKalmanFilterGEN....... : 00:00:00.038  00:00:00.070  00:00:00.050
  SubTask DCHTrackReFit............ : 00:00:00.172  00:00:00.209  00:00:00.199
 Task DCHSPX....................... : 00:00:00.008  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXReFit.............. : 00:00:00.172  00:00:00.210  00:00:00.200
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.028  00:00:00.039  00:00:00.039
 WriteEvent........................ : 00:00:16.529  00:00:03.459
analyzer........................... : 01:32:03.750  01:26:46.719  00:00:00.000
 DAQ bartender..................... : 00:07:43.116  00:03:30.810  00:03:29.610
 Task MEGMain...................... : 00:00:01.387  00:00:01.159  00:00:00.009
 Task ReadData..................... : 00:00:24.818  00:00:24.959  00:00:24.879
 Task SPX.......................... : 00:00:00.016  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.545  00:00:08.440  00:00:08.360
  SubTask SPXHitRec................ : 00:00:00.296  00:00:00.259  00:00:00.259
  SubTask SPXClustering............ : 00:00:19.472  00:00:19.410  00:00:19.360
  SubTask SPXIndependentTracking... : 00:00:43.103  00:00:43.109  00:00:43.099
 Task RDC.......................... : 00:00:00.016  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.529  00:00:08.140  00:00:08.140
  SubTask RDCHitRec................ : 00:00:00.143  00:00:00.210  00:00:00.200
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:11:35.236  00:11:33.780  00:11:33.680
  SubTask CYLDCHHitRec............. : 00:00:14.599  00:00:14.279  00:00:14.269
  SubTask CYLDCHTrackFinderPR...... : 00:53:58.305  00:53:50.060  00:53:50.010
  SubTask DCHKalmanFilterGEN....... : 00:10:03.219  00:10:01.509  00:10:01.459
  SubTask DCHTrackReFit............ : 00:00:00.149  00:00:00.149  00:00:00.109
 Task DCHSPX....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:14.411  00:03:14.070  00:03:14.050
  SubTask DCHSPXReFit.............. : 00:01:38.281  00:01:37.849  00:01:37.839
  SubTask SPXTracking.............. : 00:00:30.352  00:00:30.409  00:00:30.409
  SubTask DCHSPXTrackSelection..... : 00:00:00.143  00:00:00.139  00:00:00.139
 WriteEvent........................ : 00:01:08.589  00:00:17.919
analyzer........................... : 00:09:19.267  00:06:55.939  00:00:00.000
 DAQ bartender..................... : 00:06:31.385  00:04:28.059  00:04:27.169
 Task MEGMain...................... : 00:00:04.702  00:00:01.499  00:00:00.000
 Task ReadData..................... : 00:00:26.466  00:00:26.560  00:00:26.530
 Task SPX.......................... : 00:00:00.016  00:00:00.010  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:11.308  00:00:11.299  00:00:11.219
  SubTask SPXHitRec................ : 00:00:00.364  00:00:00.239  00:00:00.219
  SubTask SPXClustering............ : 00:00:24.709  00:00:23.899  00:00:23.809
  SubTask SPXIndependentTracking... : 00:01:01.689  00:01:01.470  00:01:01.440
 Task RDC.......................... : 00:00:00.016  00:00:00.029  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:09.672  00:00:09.209  00:00:09.199
  SubTask RDCHitRec................ : 00:00:00.190  00:00:00.190  00:00:00.170
 Task CYLDCH....................... : 00:00:00.011  00:00:00.010  00:00:00.000
  SubTask CYLDCHHitRec............. : 00:00:00.014  00:00:00.010  00:00:00.010
  SubTask CYLDCHTrackFinderPR...... : 00:00:00.193  00:00:00.090  00:00:00.080
  SubTask DCHKalmanFilterGEN....... : 00:00:00.038  00:00:00.030  00:00:00.010
  SubTask DCHTrackReFit............ : 00:00:00.170  00:00:00.120  00:00:00.120
 Task DCHSPX....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:00.011  00:00:00.009  00:00:00.009
  SubTask DCHSPXReFit.............. : 00:00:00.187  00:00:00.190  00:00:00.190
  SubTask SPXTracking.............. : 00:00:00.011  00:00:00.000  00:00:00.000
  SubTask DCHSPXTrackSelection..... : 00:00:00.029  00:00:00.030  00:00:00.030
 WriteEvent........................ : 00:00:12.464  00:00:03.799
analyzer........................... : 01:43:34.405  01:39:14.390  00:00:00.000
 DAQ bartender..................... : 00:07:19.658  00:03:50.980  00:03:49.980
 Task MEGMain...................... : 00:00:01.377  00:00:01.210  00:00:00.010
 Task ReadData..................... : 00:00:21.780  00:00:21.760  00:00:21.710
 Task SPX.......................... : 00:00:00.015  00:00:00.009  00:00:00.009
  SubTask SPXWaveformAnalysis...... : 00:00:09.954  00:00:09.710  00:00:09.620
  SubTask SPXHitRec................ : 00:00:00.303  00:00:00.260  00:00:00.250
  SubTask SPXClustering............ : 00:00:24.343  00:00:24.410  00:00:24.330
  SubTask SPXIndependentTracking... : 00:00:54.176  00:00:53.619  00:00:53.599
 Task RDC.......................... : 00:00:00.014  00:00:00.010  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:08.625  00:00:08.390  00:00:08.370
  SubTask RDCHitRec................ : 00:00:00.151  00:00:00.139  00:00:00.129
 Task CYLDCH....................... : 00:00:00.010  00:00:00.010  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:12:40.766  00:12:41.149  00:12:41.049
  SubTask CYLDCHHitRec............. : 00:00:17.791  00:00:17.639  00:00:17.629
  SubTask CYLDCHTrackFinderPR...... : 01:02:17.377  01:02:17.130  01:02:17.020
  SubTask DCHKalmanFilterGEN....... : 00:11:05.645  00:11:05.790  00:11:05.780
  SubTask DCHTrackReFit............ : 00:00:00.179  00:00:00.160  00:00:00.150
 Task DCHSPX....................... : 00:00:00.009  00:00:00.040  00:00:00.009
  SubTask DCHSPXMatching........... : 00:04:00.694  00:04:00.639  00:04:00.619
  SubTask DCHSPXReFit.............. : 00:01:58.314  00:01:58.350  00:01:58.330
  SubTask SPXTracking.............. : 00:00:38.528  00:00:37.429  00:00:37.409
  SubTask DCHSPXTrackSelection..... : 00:00:00.161  00:00:00.190  00:00:00.159
 WriteEvent........................ : 00:01:04.583  00:00:17.089
```
