2021/01/11

# Analyzer output with noxec_PR_fit_genfit.xml
* No XEC reconstruction
* SPX analysis from WaveformAnalysis (cf30)
* CDC analysis from waveform (WaveformAnalysis2), pattern recognition and fit with GENFIT
* Custom run number 2000116
* Hit z reconstruction used in CYLDCHTrackFinderPR is time-difference
* CYLDCHWaveformAnalysis2 (with not fully optimized parameters).
    * Update TimeWalkCoefficient (to 3.6e-10) and GlobalTimeOffset (to -5)

mc_20210111.0

## Run Numbers ##
11500-11501 Spect_signalEOnly_realistic (O2 0%)  
11502-11503 Spect_signalEOnly_realistic (O2 0.5%)   
11504-11505 Spect_signalEOnly_realistic (O2 1%)   
11506-11507 Spect_signalEOnly_realistic (O2 2%)    
11600-11601 Spect_signalEWithMichel7e7_realistic (O2 0%)   
11602-11603 Spect_signalEWithMichel7e7_realistic (O2 0.5%)  
11604-11605 Spect_signalEWithMichel7e7_realistic (O2 1%)  
11606-11607 Spect_signalEWithMichel7e7_realistic (O2 2%)    


## Submit jobs ##
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20210100-root6.20.00-geant4.10.04.p03-gcc830/", 11500, 8)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20210100-root6.20.00-geant4.10.04.p03-gcc830/", 11600, 8)'
```



## Calculate positron efficiency ##
### Realistic (O2 0%) ###

```
RUNNUM=11500;NRUN=2;DATASET=20210100-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2628 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2404 (91.4764 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2324 (88.4323 %)
|   |                         
|   | propagation cut         
|   V                         
|  2273 (86.4916 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  2070 (78.7671 %)
|   |                         
|   | matching                
|   V                         
|  2038 (77.5495 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  2038 (77.5495 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1995 (75.9132 %)
|      + 0.842949
|      - 0.863115
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.36411
|  phi     (mrad)  5.85087
|  momentum (MeV)  0.088592
|  vertexZ   (mm)  1.60626
|  vertexY   (mm)  0.737382
|  time     (sec)  4.59404e-11
@-----------------------------@
```

### Realistic (O2 0.5%) ###

```
RUNNUM=11502;NRUN=2;DATASET=20210100-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2620 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2377 (90.7252 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2308 (88.0916 %)
|   |                         
|   | propagation cut         
|   V                         
|  2231 (85.1527 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1993 (76.0687 %)
|   |                         
|   | matching                
|   V                         
|  1946 (74.2748 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1946 (74.2748 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1896 (72.3664 %)
|      + 0.883864
|      - 0.901307
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.57595
|  phi     (mrad)  6.33589
|  momentum (MeV)  0.0896106
|  vertexZ   (mm)  1.65732
|  vertexY   (mm)  0.786395
|  time     (sec)  4.73582e-11
@-----------------------------@
```

### Realistic (O2 1%) ###

```
RUNNUM=11504;NRUN=2;DATASET=20210100-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2592 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2374 (91.5895 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2293 (88.4645 %)
|   |                         
|   | propagation cut         
|   V                         
|  2219 (85.6096 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1999 (77.1219 %)
|   |                         
|   | matching                
|   V                         
|  1952 (75.3086 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1952 (75.3086 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1900 (73.3025 %)
|      + 0.878873
|      - 0.897249
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.66054
|  phi     (mrad)  6.27334
|  momentum (MeV)  0.0852211
|  vertexZ   (mm)  1.74492
|  vertexY   (mm)  0.756425
|  time     (sec)  4.50768e-11
@-----------------------------@
```

### Realistic (O2 2%) ###

```
RUNNUM=11506;NRUN=2;DATASET=20210100-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2685 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2430 (90.5028 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2297 (85.5493 %)
|   |                         
|   | propagation cut         
|   V                         
|  2185 (81.378 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1891 (70.4283 %)
|   |                         
|   | matching                
|   V                         
|  1852 (68.9758 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1852 (68.9758 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1802 (67.1136 %)
|      + 0.918611
|      - 0.931618
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.57738
|  phi     (mrad)  6.41387
|  momentum (MeV)  0.0946509
|  vertexZ   (mm)  1.74864
|  vertexY   (mm)  0.826858
|  time     (sec)  4.47628e-11
@-----------------------------@
```







### Realistic (Michel mixed, O2 0%) ###

```
RUNNUM=11600;NRUN=2;DATASET=20210100-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2628 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2402 (91.4003 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2133 (81.1644 %)
|   |                         
|   | propagation cut         
|   V                         
|  1940 (73.8204 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1565 (59.551 %)
|   |                         
|   | matching                
|   V                         
|  1541 (58.6377 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1541 (58.6377 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1503 (57.1918 %)
|      + 0.981249
|      - 0.986828
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.96272
|  phi     (mrad)  6.86427
|  momentum (MeV)  0.0964483
|  vertexZ   (mm)  1.96175
|  vertexY   (mm)  0.843297
|  time     (sec)  4.72982e-11
@-----------------------------@
```


### Realistic (Michel mixed, O2 0.5%) ###

```
RUNNUM=11602;NRUN=2;DATASET=20210100-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2620 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2378 (90.7634 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2093 (79.8855 %)
|   |                         
|   | propagation cut         
|   V                         
|  1880 (71.7557 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1496 (57.0992 %)
|   |                         
|   | matching                
|   V                         
|  1456 (55.5725 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1455 (55.5344 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1409 (53.7786 %)
|      + 0.991459
|      - 0.994399
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.96454
|  phi     (mrad)  6.46584
|  momentum (MeV)  0.103557
|  vertexZ   (mm)  1.88205
|  vertexY   (mm)  0.859505
|  time     (sec)  5.08903e-11
@-----------------------------@
```


### Realistic (Michel mixed, O2 1%) ###

```
RUNNUM=11604;NRUN=2;DATASET=20210100-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2592 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2374 (91.5895 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2079 (80.2083 %)
|   |                         
|   | propagation cut         
|   V                         
|  1851 (71.412 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1463 (56.4429 %)
|   |                         
|   | matching                
|   V                         
|  1419 (54.7454 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1419 (54.7454 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1375 (53.0478 %)
|      + 0.998162
|      - 1.00056
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.61769
|  phi     (mrad)  6.94403
|  momentum (MeV)  0.100745
|  vertexZ   (mm)  1.8872
|  vertexY   (mm)  0.794529
|  time     (sec)  4.67422e-11
@-----------------------------@
```


### Realistic (Michel mixed, O2 2%) ###

```
RUNNUM=11606;NRUN=2;DATASET=20210100-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2685 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2431 (90.54 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2031 (75.6425 %)
|   |                         
|   | propagation cut         
|   V                         
|  1743 (64.9162 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1360 (50.6518 %)
|   |                         
|   | matching                
|   V                         
|  1321 (49.1993 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1317 (49.0503 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1263 (47.0391 %)
|      + 0.982801
|      - 0.980553
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.88034
|  phi     (mrad)  6.65637
|  momentum (MeV)  0.107267
|  vertexZ   (mm)  1.80892
|  vertexY   (mm)  0.859123
|  time     (sec)  5.04096e-11
@-----------------------------@
```


### Analyze execution and output ###
```
cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20210100-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20210100-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```

```
./macros/analyzer_output.sh 20210100-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115
```
- - - -

16000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 11 22:38 Eff_results11500.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 11 22:41 Eff_results11502.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 11 22:43 Eff_results11504.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 11 22:45 Eff_results11506.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:37 histos11500.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:36 histos11501.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:36 histos11502.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:36 histos11503.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:35 histos11504.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:35 histos11505.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:34 histos11506.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Jan 11 22:35 histos11507.root
-rw-r--r--. 1 uchiyama unx-lke  59M Jan 11 22:37 rec11500.root
-rw-r--r--. 1 uchiyama unx-lke  58M Jan 11 22:36 rec11501.root
-rw-r--r--. 1 uchiyama unx-lke  56M Jan 11 22:36 rec11502.root
-rw-r--r--. 1 uchiyama unx-lke  56M Jan 11 22:36 rec11503.root
-rw-r--r--. 1 uchiyama unx-lke  56M Jan 11 22:35 rec11504.root
-rw-r--r--. 1 uchiyama unx-lke  55M Jan 11 22:35 rec11505.root
-rw-r--r--. 1 uchiyama unx-lke  52M Jan 11 22:34 rec11506.root
-rw-r--r--. 1 uchiyama unx-lke  52M Jan 11 22:35 rec11507.root

rec	30.348 kB/event
dch	26.301 kB/event
spx	3.512 kB/event
reco	0.234 kB/event
rdc	0.212 kB/event

```
######### execution time #########
4863.05/16000 = 0.303941 s/event
```
---------
analyzer........................... : 00:11:07.429  00:11:01.360  00:00:00.000
 DAQ bartender..................... : 00:01:02.412  00:01:00.880  00:01:00.560
 Task MEGMain...................... : 00:00:03.118  00:00:01.379  00:00:00.000
 Task ReadData..................... : 00:00:09.885  00:00:10.150  00:00:10.080
 Task SPX.......................... : 00:00:00.017  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.233  00:00:05.029  00:00:04.959
  SubTask SPXHitRec................ : 00:00:00.309  00:00:00.280  00:00:00.250
  SubTask SPXClustering............ : 00:00:08.192  00:00:08.119  00:00:08.059
  SubTask SPXIndependentTracking... : 00:00:32.743  00:00:32.639  00:00:32.639
 Task RDC.......................... : 00:00:00.016  00:00:00.030  00:00:00.020
  SubTask RDCWaveformAnalysis...... : 00:00:00.088  00:00:00.049  00:00:00.039
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.039  00:00:00.019
 Task CYLDCH....................... : 00:00:00.012  00:00:00.019  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:42.529  00:01:42.230  00:01:42.150
  SubTask CYLDCHHitRec............. : 00:00:00.584  00:00:00.540  00:00:00.500
  SubTask CYLDCHTrackFinderPR...... : 00:03:10.755  00:03:10.249  00:03:10.189
  SubTask DCHKalmanFilterGEN....... : 00:01:15.999  00:01:15.359  00:01:15.319
  SubTask DCHTrackReFit............ : 00:00:00.150  00:00:00.159  00:00:00.130
 Task DCHSPX....................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:01:02.405  00:01:02.230  00:01:02.190
  SubTask DCHSPXReFit.............. : 00:01:18.033  00:01:18.040  00:01:18.000
  SubTask SPXTracking.............. : 00:00:22.126  00:00:21.989  00:00:21.969
  SubTask DCHSPXTrackSelection..... : 00:00:00.077  00:00:00.049  00:00:00.039
 WriteEvent........................ : 00:00:06.827  00:00:06.829
analyzer........................... : 00:10:50.235  00:10:43.460  00:00:00.000
 DAQ bartender..................... : 00:01:01.910  00:01:00.299  00:01:00.049
 Task MEGMain...................... : 00:00:03.288  00:00:01.419  00:00:00.000
 Task ReadData..................... : 00:00:09.961  00:00:09.900  00:00:09.850
 Task SPX.......................... : 00:00:00.017  00:00:00.029  00:00:00.019
  SubTask SPXWaveformAnalysis...... : 00:00:05.122  00:00:05.150  00:00:05.080
  SubTask SPXHitRec................ : 00:00:00.311  00:00:00.290  00:00:00.280
  SubTask SPXClustering............ : 00:00:08.017  00:00:07.939  00:00:07.879
  SubTask SPXIndependentTracking... : 00:00:30.694  00:00:30.659  00:00:30.630
 Task RDC.......................... : 00:00:00.016  00:00:00.029  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.085  00:00:00.089  00:00:00.089
  SubTask RDCHitRec................ : 00:00:00.046  00:00:00.049  00:00:00.039
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:42.970  00:01:42.709  00:01:42.579
  SubTask CYLDCHHitRec............. : 00:00:00.590  00:00:00.639  00:00:00.629
  SubTask CYLDCHTrackFinderPR...... : 00:02:55.420  00:02:54.019  00:02:53.989
  SubTask DCHKalmanFilterGEN....... : 00:01:15.152  00:01:15.050  00:01:15.020
  SubTask DCHTrackReFit............ : 00:00:00.146  00:00:00.149  00:00:00.149
 Task DCHSPX....................... : 00:00:00.013  00:00:00.029  00:00:00.019
  SubTask DCHSPXMatching........... : 00:01:03.410  00:01:03.290  00:01:03.260
  SubTask DCHSPXReFit.............. : 00:01:19.433  00:01:19.019  00:01:19.019
  SubTask SPXTracking.............. : 00:00:21.283  00:00:21.260  00:00:21.250
  SubTask DCHSPXTrackSelection..... : 00:00:00.077  00:00:00.070  00:00:00.049
 WriteEvent........................ : 00:00:06.790  00:00:06.850
analyzer........................... : 00:10:21.659  00:10:15.589  00:00:00.000
 DAQ bartender..................... : 00:01:01.467  00:01:00.300  00:01:00.070
 Task MEGMain...................... : 00:00:03.240  00:00:01.369  00:00:00.000
 Task ReadData..................... : 00:00:09.845  00:00:09.950  00:00:09.880
 Task SPX.......................... : 00:00:00.017  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.075  00:00:04.979  00:00:04.890
  SubTask SPXHitRec................ : 00:00:00.310  00:00:00.260  00:00:00.250
  SubTask SPXClustering............ : 00:00:08.110  00:00:07.969  00:00:07.909
  SubTask SPXIndependentTracking... : 00:00:33.707  00:00:33.860  00:00:33.840
 Task RDC.......................... : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.085  00:00:00.059  00:00:00.049
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.069  00:00:00.049
 Task CYLDCH....................... : 00:00:00.012  00:00:00.020  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:40.831  00:01:40.270  00:01:40.210
  SubTask CYLDCHHitRec............. : 00:00:00.569  00:00:00.489  00:00:00.459
  SubTask CYLDCHTrackFinderPR...... : 00:02:43.975  00:02:43.599  00:02:43.589
  SubTask DCHKalmanFilterGEN....... : 00:01:14.601  00:01:14.620  00:01:14.600
  SubTask DCHTrackReFit............ : 00:00:00.148  00:00:00.150  00:00:00.110
 Task DCHSPX....................... : 00:00:00.013  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:56.863  00:00:56.669  00:00:56.659
  SubTask DCHSPXReFit.............. : 00:01:11.115  00:01:10.959  00:01:10.959
  SubTask SPXTracking.............. : 00:00:19.124  00:00:19.139  00:00:19.109
  SubTask DCHSPXTrackSelection..... : 00:00:00.075  00:00:00.059  00:00:00.049
 WriteEvent........................ : 00:00:06.757  00:00:06.320
analyzer........................... : 00:10:23.121  00:10:17.080  00:00:00.000
 DAQ bartender..................... : 00:01:02.325  00:01:01.269  00:01:01.029
 Task MEGMain...................... : 00:00:03.281  00:00:01.399  00:00:00.000
 Task ReadData..................... : 00:00:09.872  00:00:09.290  00:00:09.230
 Task SPX.......................... : 00:00:00.017  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.266  00:00:05.320  00:00:05.250
  SubTask SPXHitRec................ : 00:00:00.312  00:00:00.369  00:00:00.359
  SubTask SPXClustering............ : 00:00:08.523  00:00:08.310  00:00:08.270
  SubTask SPXIndependentTracking... : 00:00:32.155  00:00:31.880  00:00:31.840
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.083  00:00:00.089  00:00:00.079
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.029  00:00:00.019
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:40.705  00:01:40.749  00:01:40.669
  SubTask CYLDCHHitRec............. : 00:00:00.580  00:00:00.539  00:00:00.519
  SubTask CYLDCHTrackFinderPR...... : 00:02:43.970  00:02:43.620  00:02:43.600
  SubTask DCHKalmanFilterGEN....... : 00:01:11.863  00:01:11.660  00:01:11.640
  SubTask DCHTrackReFit............ : 00:00:00.146  00:00:00.209  00:00:00.190
 Task DCHSPX....................... : 00:00:00.013  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:00:57.094  00:00:56.859  00:00:56.859
  SubTask DCHSPXReFit.............. : 00:01:13.397  00:01:13.109  00:01:13.099
  SubTask SPXTracking.............. : 00:00:21.164  00:00:21.159  00:00:21.139
  SubTask DCHSPXTrackSelection..... : 00:00:00.075  00:00:00.069  00:00:00.059
 WriteEvent........................ : 00:00:06.701  00:00:06.549
analyzer........................... : 00:10:01.483  00:09:55.519  00:00:00.000
 DAQ bartender..................... : 00:01:02.523  00:01:01.410  00:01:01.180
 Task MEGMain...................... : 00:00:03.134  00:00:01.399  00:00:00.009
 Task ReadData..................... : 00:00:09.996  00:00:09.729  00:00:09.699
 Task SPX.......................... : 00:00:00.017  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.246  00:00:05.360  00:00:05.270
  SubTask SPXHitRec................ : 00:00:00.314  00:00:00.329  00:00:00.309
  SubTask SPXClustering............ : 00:00:08.317  00:00:08.039  00:00:07.969
  SubTask SPXIndependentTracking... : 00:00:31.796  00:00:31.790  00:00:31.770
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.085  00:00:00.120  00:00:00.120
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.049  00:00:00.049
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:38.931  00:01:38.399  00:01:38.269
  SubTask CYLDCHHitRec............. : 00:00:00.579  00:00:00.519  00:00:00.499
  SubTask CYLDCHTrackFinderPR...... : 00:02:28.888  00:02:28.569  00:02:28.519
  SubTask DCHKalmanFilterGEN....... : 00:01:12.112  00:01:11.820  00:01:11.800
  SubTask DCHTrackReFit............ : 00:00:00.148  00:00:00.169  00:00:00.149
 Task DCHSPX....................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:55.864  00:00:55.789  00:00:55.759
  SubTask DCHSPXReFit.............. : 00:01:11.308  00:01:11.109  00:01:11.089
  SubTask SPXTracking.............. : 00:00:19.439  00:00:19.559  00:00:19.549
  SubTask DCHSPXTrackSelection..... : 00:00:00.075  00:00:00.109  00:00:00.099
 WriteEvent........................ : 00:00:06.669  00:00:06.709
analyzer........................... : 00:09:55.164  00:09:49.279  00:00:00.000
 DAQ bartender..................... : 00:01:01.527  00:01:00.580  00:01:00.340
 Task MEGMain...................... : 00:00:03.131  00:00:01.389  00:00:00.000
 Task ReadData..................... : 00:00:09.826  00:00:09.420  00:00:09.360
 Task SPX.......................... : 00:00:00.017  00:00:00.030  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.164  00:00:05.100  00:00:05.030
  SubTask SPXHitRec................ : 00:00:00.310  00:00:00.370  00:00:00.360
  SubTask SPXClustering............ : 00:00:08.597  00:00:08.500  00:00:08.430
  SubTask SPXIndependentTracking... : 00:00:32.899  00:00:32.619  00:00:32.599
 Task RDC.......................... : 00:00:00.016  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.083  00:00:00.130  00:00:00.120
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.069  00:00:00.039
 Task CYLDCH....................... : 00:00:00.012  00:00:00.050  00:00:00.009
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:37.953  00:01:37.929  00:01:37.809
  SubTask CYLDCHHitRec............. : 00:00:00.559  00:00:00.580  00:00:00.580
  SubTask CYLDCHTrackFinderPR...... : 00:02:24.115  00:02:23.470  00:02:23.450
  SubTask DCHKalmanFilterGEN....... : 00:01:13.552  00:01:13.179  00:01:13.110
  SubTask DCHTrackReFit............ : 00:00:00.149  00:00:00.139  00:00:00.119
 Task DCHSPX....................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:52.684  00:00:52.650  00:00:52.640
  SubTask DCHSPXReFit.............. : 00:01:11.052  00:01:10.929  00:01:10.919
  SubTask SPXTracking.............. : 00:00:20.673  00:00:20.700  00:00:20.700
  SubTask DCHSPXTrackSelection..... : 00:00:00.075  00:00:00.060  00:00:00.050
 WriteEvent........................ : 00:00:06.738  00:00:06.609
analyzer........................... : 00:09:05.351  00:08:59.519  00:00:00.000
 DAQ bartender..................... : 00:01:02.168  00:01:00.700  00:01:00.480
 Task MEGMain...................... : 00:00:03.177  00:00:01.429  00:00:00.000
 Task ReadData..................... : 00:00:09.817  00:00:10.009  00:00:09.909
 Task SPX.......................... : 00:00:00.017  00:00:00.029  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.135  00:00:04.960  00:00:04.890
  SubTask SPXHitRec................ : 00:00:00.314  00:00:00.399  00:00:00.399
  SubTask SPXClustering............ : 00:00:08.441  00:00:08.279  00:00:08.219
  SubTask SPXIndependentTracking... : 00:00:33.155  00:00:32.970  00:00:32.950
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.090  00:00:00.109  00:00:00.079
  SubTask RDCHitRec................ : 00:00:00.048  00:00:00.049  00:00:00.049
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:33.103  00:01:32.970  00:01:32.870
  SubTask CYLDCHHitRec............. : 00:00:00.549  00:00:00.600  00:00:00.600
  SubTask CYLDCHTrackFinderPR...... : 00:02:09.427  00:02:08.689  00:02:08.649
  SubTask DCHKalmanFilterGEN....... : 00:01:02.361  00:01:02.569  00:01:02.539
  SubTask DCHTrackReFit............ : 00:00:00.148  00:00:00.140  00:00:00.120
 Task DCHSPX....................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:43.271  00:00:43.210  00:00:43.200
  SubTask DCHSPXReFit.............. : 00:01:01.060  00:01:00.849  00:01:00.819
  SubTask SPXTracking.............. : 00:00:20.445  00:00:20.340  00:00:20.330
  SubTask DCHSPXTrackSelection..... : 00:00:00.072  00:00:00.079  00:00:00.049
 WriteEvent........................ : 00:00:06.505  00:00:06.599
analyzer........................... : 00:09:18.608  00:09:12.809  00:00:00.000
 DAQ bartender..................... : 00:01:01.583  00:01:00.690  00:01:00.440
 Task MEGMain...................... : 00:00:03.202  00:00:01.370  00:00:00.000
 Task ReadData..................... : 00:00:09.857  00:00:09.910  00:00:09.850
 Task SPX.......................... : 00:00:00.017  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:05.274  00:00:05.430  00:00:05.390
  SubTask SPXHitRec................ : 00:00:00.314  00:00:00.270  00:00:00.250
  SubTask SPXClustering............ : 00:00:08.413  00:00:08.059  00:00:07.979
  SubTask SPXIndependentTracking... : 00:00:33.897  00:00:33.580  00:00:33.550
 Task RDC.......................... : 00:00:00.016  00:00:00.019  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:00.085  00:00:00.090  00:00:00.080
  SubTask RDCHitRec................ : 00:00:00.049  00:00:00.049  00:00:00.039
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:32.726  00:01:32.569  00:01:32.459
  SubTask CYLDCHHitRec............. : 00:00:00.552  00:00:00.499  00:00:00.479
  SubTask CYLDCHTrackFinderPR...... : 00:01:57.330  00:01:57.030  00:01:57.000
  SubTask DCHKalmanFilterGEN....... : 00:01:07.907  00:01:07.780  00:01:07.760
  SubTask DCHTrackReFit............ : 00:00:00.146  00:00:00.160  00:00:00.140
 Task DCHSPX....................... : 00:00:00.013  00:00:00.010  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:55.246  00:00:55.030  00:00:55.030
  SubTask DCHSPXReFit.............. : 00:01:10.338  00:01:10.319  00:01:10.309
  SubTask SPXTracking.............. : 00:00:19.084  00:00:18.969  00:00:18.949
  SubTask DCHSPXTrackSelection..... : 00:00:00.074  00:00:00.039  00:00:00.039
 WriteEvent........................ : 00:00:06.514  00:00:06.419
```

```
./macros/analyzer_output.sh 20210100-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```
- - - -

16000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 12 11:51 Eff_results11600.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 12 11:54 Eff_results11602.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 12 11:58 Eff_results11604.root
-rw-r--r--. 1 uchiyama unx-lke  18K Jan 12 12:03 Eff_results11606.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:34 histos11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:39 histos11601.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:25 histos11602.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:30 histos11603.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:25 histos11604.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:27 histos11605.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:16 histos11606.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Jan 12 00:16 histos11607.root
-rw-r--r--. 1 uchiyama unx-lke 222M Jan 12 00:34 rec11600.root
-rw-r--r--. 1 uchiyama unx-lke 223M Jan 12 00:39 rec11601.root
-rw-r--r--. 1 uchiyama unx-lke 209M Jan 12 00:25 rec11602.root
-rw-r--r--. 1 uchiyama unx-lke 213M Jan 12 00:30 rec11603.root
-rw-r--r--. 1 uchiyama unx-lke 206M Jan 12 00:25 rec11604.root
-rw-r--r--. 1 uchiyama unx-lke 208M Jan 12 00:27 rec11605.root
-rw-r--r--. 1 uchiyama unx-lke 189M Jan 12 00:16 rec11606.root
-rw-r--r--. 1 uchiyama unx-lke 191M Jan 12 00:16 rec11607.root

rec	115.999 kB/event
dch	106.258 kB/event
spx	6.024 kB/event
rdc	2.711 kB/event
reco	0.705 kB/event

```
######### execution time #########
40203.5/16000 = 2.51272 s/event
```
---------
analyzer........................... : 01:31:10.364  01:30:24.780  00:00:00.000
 DAQ bartender..................... : 00:03:53.017  00:03:22.710  00:03:21.570
 Task MEGMain...................... : 00:00:01.354  00:00:01.240  00:00:00.000
 Task ReadData..................... : 00:00:25.108  00:00:24.799  00:00:24.759
 Task SPX.......................... : 00:00:00.015  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.704  00:00:08.570  00:00:08.480
  SubTask SPXHitRec................ : 00:00:00.302  00:00:00.190  00:00:00.190
  SubTask SPXClustering............ : 00:00:11.371  00:00:11.579  00:00:11.499
  SubTask SPXIndependentTracking... : 00:00:43.133  00:00:42.960  00:00:42.950
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:22.563  00:00:22.460  00:00:22.450
  SubTask RDCHitRec................ : 00:00:00.242  00:00:00.210  00:00:00.210
 Task CYLDCH....................... : 00:00:00.011  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:13.135  00:10:11.459  00:10:11.379
  SubTask CYLDCHHitRec............. : 00:00:12.466  00:00:12.580  00:00:12.560
  SubTask CYLDCHTrackFinderPR...... : 00:58:08.424  00:57:59.429  00:57:59.419
  SubTask DCHKalmanFilterGEN....... : 00:10:26.742  00:10:25.220  00:10:25.190
  SubTask DCHTrackReFit............ : 00:00:00.139  00:00:00.079  00:00:00.059
 Task DCHSPX....................... : 00:00:00.011  00:00:00.019  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:35.772  00:03:35.279  00:03:35.249
  SubTask DCHSPXReFit.............. : 00:01:50.754  00:01:50.330  00:01:50.310
  SubTask SPXTracking.............. : 00:00:31.749  00:00:31.580  00:00:31.540
  SubTask DCHSPXTrackSelection..... : 00:00:00.133  00:00:00.180  00:00:00.140
 WriteEvent........................ : 00:00:14.777  00:00:14.249
analyzer........................... : 01:35:57.680  01:35:11.180  00:00:00.000
 DAQ bartender..................... : 00:03:58.279  00:03:27.240  00:03:26.250
 Task MEGMain...................... : 00:00:01.024  00:00:00.950  00:00:00.000
 Task ReadData..................... : 00:00:25.295  00:00:25.329  00:00:25.299
 Task SPX.......................... : 00:00:00.015  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.608  00:00:08.290  00:00:08.220
  SubTask SPXHitRec................ : 00:00:00.303  00:00:00.300  00:00:00.260
  SubTask SPXClustering............ : 00:00:11.658  00:00:11.870  00:00:11.830
  SubTask SPXIndependentTracking... : 00:00:42.840  00:00:42.719  00:00:42.699
 Task RDC.......................... : 00:00:00.015  00:00:00.049  00:00:00.020
  SubTask RDCWaveformAnalysis...... : 00:00:24.300  00:00:24.190  00:00:24.180
  SubTask RDCHitRec................ : 00:00:00.251  00:00:00.270  00:00:00.250
 Task CYLDCH....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:34.814  00:10:32.960  00:10:32.850
  SubTask CYLDCHHitRec............. : 00:00:13.097  00:00:13.429  00:00:13.429
  SubTask CYLDCHTrackFinderPR...... : 01:02:00.152  01:01:50.339  01:01:50.309
  SubTask DCHKalmanFilterGEN....... : 00:10:44.063  00:10:42.479  00:10:42.469
  SubTask DCHTrackReFit............ : 00:00:00.138  00:00:00.140  00:00:00.130
 Task DCHSPX....................... : 00:00:00.011  00:00:00.020  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:54.769  00:03:54.090  00:03:54.060
  SubTask DCHSPXReFit.............. : 00:01:43.194  00:01:42.950  00:01:42.920
  SubTask SPXTracking.............. : 00:00:28.760  00:00:28.819  00:00:28.769
  SubTask DCHSPXTrackSelection..... : 00:00:00.141  00:00:00.169  00:00:00.149
 WriteEvent........................ : 00:00:14.983  00:00:14.519
analyzer........................... : 01:22:43.328  01:21:59.760  00:00:00.000
 DAQ bartender..................... : 00:03:56.509  00:03:27.959  00:03:26.969
 Task MEGMain...................... : 00:00:01.043  00:00:00.949  00:00:00.009
 Task ReadData..................... : 00:00:24.370  00:00:24.150  00:00:24.080
 Task SPX.......................... : 00:00:00.016  00:00:00.040  00:00:00.020
  SubTask SPXWaveformAnalysis...... : 00:00:08.654  00:00:08.500  00:00:08.450
  SubTask SPXHitRec................ : 00:00:00.300  00:00:00.230  00:00:00.230
  SubTask SPXClustering............ : 00:00:10.751  00:00:10.799  00:00:10.719
  SubTask SPXIndependentTracking... : 00:00:42.521  00:00:42.350  00:00:42.320
 Task RDC.......................... : 00:00:00.015  00:00:00.020  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:23.907  00:00:23.609  00:00:23.589
  SubTask RDCHitRec................ : 00:00:00.248  00:00:00.189  00:00:00.189
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:13.030  00:10:11.410  00:10:11.330
  SubTask CYLDCHHitRec............. : 00:00:12.523  00:00:12.699  00:00:12.669
  SubTask CYLDCHTrackFinderPR...... : 00:50:46.825  00:50:38.210  00:50:38.180
  SubTask DCHKalmanFilterGEN....... : 00:09:51.256  00:09:49.789  00:09:49.759
  SubTask DCHTrackReFit............ : 00:00:00.144  00:00:00.069  00:00:00.059
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:18.700  00:03:18.049  00:03:18.019
  SubTask DCHSPXReFit.............. : 00:01:36.099  00:01:35.910  00:01:35.890
  SubTask SPXTracking.............. : 00:00:30.917  00:00:30.809  00:00:30.769
  SubTask DCHSPXTrackSelection..... : 00:00:00.135  00:00:00.170  00:00:00.150
 WriteEvent........................ : 00:00:14.379  00:00:13.759
analyzer........................... : 01:27:35.061  01:26:50.509  00:00:00.000
 DAQ bartender..................... : 00:04:00.096  00:03:29.860  00:03:28.870
 Task MEGMain...................... : 00:00:01.023  00:00:00.930  00:00:00.000
 Task ReadData..................... : 00:00:25.655  00:00:25.240  00:00:25.180
 Task SPX.......................... : 00:00:00.016  00:00:00.019  00:00:00.019
  SubTask SPXWaveformAnalysis...... : 00:00:08.797  00:00:08.900  00:00:08.840
  SubTask SPXHitRec................ : 00:00:00.305  00:00:00.260  00:00:00.249
  SubTask SPXClustering............ : 00:00:11.595  00:00:11.800  00:00:11.760
  SubTask SPXIndependentTracking... : 00:00:44.274  00:00:43.959  00:00:43.929
 Task RDC.......................... : 00:00:00.015  00:00:00.050  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:24.486  00:00:24.450  00:00:24.430
  SubTask RDCHitRec................ : 00:00:00.251  00:00:00.290  00:00:00.280
 Task CYLDCH....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:25.697  00:10:24.069  00:10:23.979
  SubTask CYLDCHHitRec............. : 00:00:12.751  00:00:12.919  00:00:12.899
  SubTask CYLDCHTrackFinderPR...... : 00:54:30.187  00:54:21.520  00:54:21.520
  SubTask DCHKalmanFilterGEN....... : 00:10:08.200  00:10:06.520  00:10:06.480
  SubTask DCHTrackReFit............ : 00:00:00.144  00:00:00.150  00:00:00.140
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:41.268  00:03:40.769  00:03:40.749
  SubTask DCHSPXReFit.............. : 00:01:47.166  00:01:46.830  00:01:46.789
  SubTask SPXTracking.............. : 00:00:27.379  00:00:27.510  00:00:27.500
  SubTask DCHSPXTrackSelection..... : 00:00:00.138  00:00:00.150  00:00:00.150
 WriteEvent........................ : 00:00:14.645  00:00:13.999
analyzer........................... : 01:21:58.525  01:21:12.590  00:00:00.000
 DAQ bartender..................... : 00:04:07.519  00:03:35.009  00:03:33.989
 Task MEGMain...................... : 00:00:01.011  00:00:00.969  00:00:00.000
 Task ReadData..................... : 00:00:27.378  00:00:26.880  00:00:26.830
 Task SPX.......................... : 00:00:00.016  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.768  00:00:08.759  00:00:08.709
  SubTask SPXHitRec................ : 00:00:00.310  00:00:00.470  00:00:00.470
  SubTask SPXClustering............ : 00:00:13.942  00:00:14.009  00:00:13.959
  SubTask SPXIndependentTracking... : 00:00:45.769  00:00:45.729  00:00:45.689
 Task RDC.......................... : 00:00:00.015  00:00:00.039  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:24.189  00:00:23.819  00:00:23.809
  SubTask RDCHitRec................ : 00:00:00.257  00:00:00.239  00:00:00.219
 Task CYLDCH....................... : 00:00:00.012  00:00:00.020  00:00:00.010
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:18.781  00:10:17.180  00:10:17.090
  SubTask CYLDCHHitRec............. : 00:00:13.089  00:00:13.429  00:00:13.409
  SubTask CYLDCHTrackFinderPR...... : 00:49:32.511  00:49:24.590  00:49:24.580
  SubTask DCHKalmanFilterGEN....... : 00:09:46.199  00:09:44.659  00:09:44.639
  SubTask DCHTrackReFit............ : 00:00:00.152  00:00:00.120  00:00:00.110
 Task DCHSPX....................... : 00:00:00.012  00:00:00.030  00:00:00.020
  SubTask DCHSPXMatching........... : 00:03:26.066  00:03:25.350  00:03:25.330
  SubTask DCHSPXReFit.............. : 00:01:37.026  00:01:37.250  00:01:37.240
  SubTask SPXTracking.............. : 00:00:29.474  00:00:28.950  00:00:28.939
  SubTask DCHSPXTrackSelection..... : 00:00:00.135  00:00:00.139  00:00:00.139
 WriteEvent........................ : 00:00:14.776  00:00:14.440
analyzer........................... : 01:24:11.537  01:23:27.559  00:00:00.000
 DAQ bartender..................... : 00:04:02.540  00:03:32.390  00:03:31.380
 Task MEGMain...................... : 00:00:01.023  00:00:01.009  00:00:00.000
 Task ReadData..................... : 00:00:26.883  00:00:26.429  00:00:26.359
 Task SPX.......................... : 00:00:00.016  00:00:00.010  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:08.712  00:00:08.879  00:00:08.829
  SubTask SPXHitRec................ : 00:00:00.312  00:00:00.329  00:00:00.319
  SubTask SPXClustering............ : 00:00:12.938  00:00:12.719  00:00:12.649
  SubTask SPXIndependentTracking... : 00:00:49.847  00:00:49.840  00:00:49.830
 Task RDC.......................... : 00:00:00.016  00:00:00.020  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:24.000  00:00:23.839  00:00:23.809
  SubTask RDCHitRec................ : 00:00:00.277  00:00:00.270  00:00:00.260
 Task CYLDCH....................... : 00:00:00.012  00:00:00.030  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:11.166  00:10:09.310  00:10:09.230
  SubTask CYLDCHHitRec............. : 00:00:12.609  00:00:12.839  00:00:12.839
  SubTask CYLDCHTrackFinderPR...... : 00:51:39.491  00:51:31.679  00:51:31.639
  SubTask DCHKalmanFilterGEN....... : 00:10:02.170  00:10:00.560  00:10:00.530
  SubTask DCHTrackReFit............ : 00:00:00.151  00:00:00.190  00:00:00.170
 Task DCHSPX....................... : 00:00:00.012  00:00:00.019  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:25.655  00:03:24.949  00:03:24.929
  SubTask DCHSPXReFit.............. : 00:01:38.093  00:01:38.070  00:01:38.070
  SubTask SPXTracking.............. : 00:00:29.678  00:00:29.490  00:00:29.480
  SubTask DCHSPXTrackSelection..... : 00:00:00.140  00:00:00.100  00:00:00.080
 WriteEvent........................ : 00:00:14.693  00:00:14.389
analyzer........................... : 01:12:56.122  01:12:16.890  00:00:00.000
 DAQ bartender..................... : 00:04:03.191  00:03:36.189  00:03:35.179
 Task MEGMain...................... : 00:00:01.021  00:00:00.959  00:00:00.000
 Task ReadData..................... : 00:00:28.227  00:00:28.119  00:00:28.049
 Task SPX.......................... : 00:00:00.016  00:00:00.029  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:08.759  00:00:08.479  00:00:08.429
  SubTask SPXHitRec................ : 00:00:00.316  00:00:00.310  00:00:00.300
  SubTask SPXClustering............ : 00:00:14.492  00:00:14.740  00:00:14.700
  SubTask SPXIndependentTracking... : 00:00:46.550  00:00:46.060  00:00:46.000
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:24.519  00:00:24.649  00:00:24.629
  SubTask RDCHitRec................ : 00:00:00.260  00:00:00.179  00:00:00.169
 Task CYLDCH....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:09:45.373  00:09:43.870  00:09:43.800
  SubTask CYLDCHHitRec............. : 00:00:11.813  00:00:11.749  00:00:11.749
  SubTask CYLDCHTrackFinderPR...... : 00:43:01.654  00:42:55.010  00:42:54.990
  SubTask DCHKalmanFilterGEN....... : 00:08:25.986  00:08:24.829  00:08:24.779
  SubTask DCHTrackReFit............ : 00:00:00.152  00:00:00.110  00:00:00.100
 Task DCHSPX....................... : 00:00:00.012  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:00.636  00:03:00.079  00:03:00.069
  SubTask DCHSPXReFit.............. : 00:01:30.395  00:01:30.140  00:01:30.120
  SubTask SPXTracking.............. : 00:00:26.444  00:00:26.670  00:00:26.640
  SubTask DCHSPXTrackSelection..... : 00:00:00.126  00:00:00.059  00:00:00.050
 WriteEvent........................ : 00:00:14.308  00:00:13.669
analyzer........................... : 01:13:30.909  01:12:50.260  00:00:00.000
 DAQ bartender..................... : 00:04:07.023  00:03:38.699  00:03:37.689
 Task MEGMain...................... : 00:00:01.075  00:00:00.949  00:00:00.019
 Task ReadData..................... : 00:00:28.505  00:00:27.799  00:00:27.740
 Task SPX.......................... : 00:00:00.016  00:00:00.020  00:00:00.010
  SubTask SPXWaveformAnalysis...... : 00:00:08.942  00:00:08.850  00:00:08.770
  SubTask SPXHitRec................ : 00:00:00.320  00:00:00.279  00:00:00.279
  SubTask SPXClustering............ : 00:00:14.393  00:00:14.690  00:00:14.650
  SubTask SPXIndependentTracking... : 00:00:46.139  00:00:45.930  00:00:45.910
 Task RDC.......................... : 00:00:00.015  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:25.005  00:00:25.230  00:00:25.230
  SubTask RDCHitRec................ : 00:00:00.262  00:00:00.149  00:00:00.129
 Task CYLDCH....................... : 00:00:00.012  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:09:48.901  00:09:47.110  00:09:47.010
  SubTask CYLDCHHitRec............. : 00:00:12.048  00:00:11.999  00:00:11.969
  SubTask CYLDCHTrackFinderPR...... : 00:42:35.352  00:42:28.920  00:42:28.880
  SubTask DCHKalmanFilterGEN....... : 00:09:05.573  00:09:04.330  00:09:04.280
  SubTask DCHTrackReFit............ : 00:00:00.157  00:00:00.120  00:00:00.089
 Task DCHSPX....................... : 00:00:00.012  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:09.888  00:03:09.199  00:03:09.159
  SubTask DCHSPXReFit.............. : 00:01:35.322  00:01:35.179  00:01:35.169
  SubTask SPXTracking.............. : 00:00:25.858  00:00:26.130  00:00:26.090
  SubTask DCHSPXTrackSelection..... : 00:00:00.132  00:00:00.109  00:00:00.099
 WriteEvent........................ : 00:00:14.436  00:00:13.750
```
