2021/01/11

# Bartender output with Spect_noMix.

* (realistic)
   * A signal positron event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Run number convention: 115**  
     10 gem4 runs (sev files) are summed up in a bartender run.  
     Originally 200*10 signal events are generated in gem4 but  
     only events satisfied gem4 event-selection are recorded.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.
   * CDCH noise from spectrum measured in 2020 with standard FE.
   * Electron attachment by O2 at different fractions
       - 11500-11501: 0%
       - 11502-11503: 0.5%
       - 11504-11505: 1%
       - 11506-11507: 2%

d0ef70a2ba25302be7cd66881ff83631de526fa4

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix_realistic.xml","/meg/data1/shared/mc/benchmark_test/20210100-root6.20.00-geant4.10.04.p03-gcc830", 11500, 8, "-p short")'
```

```
./macros/bartender_output.sh 20210100-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
cd 20210100-root6.20.00-geant4.10.04.p03-gcc830
cat NOTE-Spect_noMix_realistic.md >> NOTE-Spect_noMix.md;rm -f NOTE-Spect_noMix_realistic.md
```



### Data and Statistics ###


```
./macros/bartender_output.sh 20210100-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
```
- - - -

16000  events

######### data size #########
```
ls -lh Spect_noMix_realistic | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 396M Jan 11 22:14 raw11500.root
-rw-r--r--. 1 uchiyama unx-lke 396M Jan 11 22:14 raw11501.root
-rw-r--r--. 1 uchiyama unx-lke 388M Jan 11 22:14 raw11502.root
-rw-r--r--. 1 uchiyama unx-lke 390M Jan 11 22:14 raw11503.root
-rw-r--r--. 1 uchiyama unx-lke 390M Jan 11 22:14 raw11504.root
-rw-r--r--. 1 uchiyama unx-lke 385M Jan 11 22:14 raw11505.root
-rw-r--r--. 1 uchiyama unx-lke 376M Jan 11 22:14 raw11506.root
-rw-r--r--. 1 uchiyama unx-lke 376M Jan 11 22:14 raw11507.root
-rw-r--r--. 1 uchiyama unx-lke 278M Jan 11 22:14 sim11500.root
-rw-r--r--. 1 uchiyama unx-lke 276M Jan 11 22:14 sim11501.root
-rw-r--r--. 1 uchiyama unx-lke 272M Jan 11 22:14 sim11502.root
-rw-r--r--. 1 uchiyama unx-lke 278M Jan 11 22:14 sim11503.root
-rw-r--r--. 1 uchiyama unx-lke 281M Jan 11 22:14 sim11504.root
-rw-r--r--. 1 uchiyama unx-lke 272M Jan 11 22:14 sim11505.root
-rw-r--r--. 1 uchiyama unx-lke 269M Jan 11 22:14 sim11506.root
-rw-r--r--. 1 uchiyama unx-lke 271M Jan 11 22:14 sim11507.root

sim	145.242 kB/event
track	75.789 kB/event
dch	63.39 kB/event
spx	2.616 kB/event
kine	1.521 kB/event

```
######### execution time #########
2668.75/16000 = 0.166797 s/event
```
---------
analyzer........................... : 00:05:39.626  00:05:34.000  00:00:00.000
 DAQ gem4.......................... : 00:00:01.272  00:00:00.239  00:00:00.009
 Task MEGMain...................... : 00:00:00.125  00:00:00.130  00:00:00.080
 Task ReadGEM4..................... : 00:00:06.058  00:00:05.499  00:00:05.429
 Task COMMixEvents................. : 00:00:03.010  00:00:02.949  00:00:02.949
 Task CYLDCHMixEvents.............. : 00:00:05.393  00:00:05.669  00:00:05.659
 Task SPXMixEvents................. : 00:00:00.146  00:00:00.099  00:00:00.089
 Task RDCMixEvents................. : 00:00:00.022  00:00:00.029  00:00:00.000
 Task SPXDigitize.................. : 00:00:36.418  00:00:35.949  00:00:33.999
 Task CYLDCHDigitize............... : 00:03:33.417  00:03:32.740  00:03:32.660
 Task RDCDigitize.................. : 00:00:01.408  00:00:01.400  00:00:00.560
 WriteEvent........................ : 00:01:08.885  00:01:06.980
analyzer........................... : 00:05:45.853  00:05:39.829  00:00:00.000
 DAQ gem4.......................... : 00:00:01.136  00:00:00.210  00:00:00.000
 Task MEGMain...................... : 00:00:00.121  00:00:00.090  00:00:00.090
 Task ReadGEM4..................... : 00:00:06.118  00:00:05.279  00:00:05.219
 Task COMMixEvents................. : 00:00:03.005  00:00:03.010  00:00:03.010
 Task CYLDCHMixEvents.............. : 00:00:05.425  00:00:05.709  00:00:05.689
 Task SPXMixEvents................. : 00:00:00.143  00:00:00.209  00:00:00.179
 Task RDCMixEvents................. : 00:00:00.021  00:00:00.039  00:00:00.000
 Task SPXDigitize.................. : 00:00:36.420  00:00:36.040  00:00:34.100
 Task CYLDCHDigitize............... : 00:03:39.433  00:03:38.780  00:03:38.740
 Task RDCDigitize.................. : 00:00:01.404  00:00:01.309  00:00:00.469
 WriteEvent........................ : 00:01:08.944  00:01:06.919
analyzer........................... : 00:05:36.538  00:05:30.410  00:00:00.000
 DAQ gem4.......................... : 00:00:01.202  00:00:00.229  00:00:00.000
 Task MEGMain...................... : 00:00:00.121  00:00:00.070  00:00:00.060
 Task ReadGEM4..................... : 00:00:05.980  00:00:05.589  00:00:05.519
 Task COMMixEvents................. : 00:00:02.957  00:00:03.069  00:00:03.029
 Task CYLDCHMixEvents.............. : 00:00:05.347  00:00:05.139  00:00:05.129
 Task SPXMixEvents................. : 00:00:00.140  00:00:00.080  00:00:00.070
 Task RDCMixEvents................. : 00:00:00.021  00:00:00.040  00:00:00.030
 Task SPXDigitize.................. : 00:00:35.533  00:00:35.500  00:00:33.520
 Task CYLDCHDigitize............... : 00:03:31.589  00:03:30.719  00:03:30.639
 Task RDCDigitize.................. : 00:00:01.397  00:00:01.369  00:00:00.539
 WriteEvent........................ : 00:01:08.575  00:01:06.199
analyzer........................... : 00:05:39.706  00:05:33.500  00:00:00.000
 DAQ gem4.......................... : 00:00:01.176  00:00:00.229  00:00:00.000
 Task MEGMain...................... : 00:00:00.121  00:00:00.099  00:00:00.089
 Task ReadGEM4..................... : 00:00:06.049  00:00:05.390  00:00:05.310
 Task COMMixEvents................. : 00:00:02.993  00:00:03.040  00:00:03.020
 Task CYLDCHMixEvents.............. : 00:00:05.414  00:00:05.420  00:00:05.390
 Task SPXMixEvents................. : 00:00:00.143  00:00:00.169  00:00:00.149
 Task RDCMixEvents................. : 00:00:00.021  00:00:00.049  00:00:00.039
 Task SPXDigitize.................. : 00:00:36.180  00:00:35.930  00:00:33.950
 Task CYLDCHDigitize............... : 00:03:33.349  00:03:32.709  00:03:32.659
 Task RDCDigitize.................. : 00:00:01.405  00:00:01.359  00:00:00.519
 WriteEvent........................ : 00:01:08.973  00:01:06.619
analyzer........................... : 00:05:35.277  00:05:29.170  00:00:00.000
 DAQ gem4.......................... : 00:00:01.111  00:00:00.219  00:00:00.000
 Task MEGMain...................... : 00:00:00.123  00:00:00.100  00:00:00.090
 Task ReadGEM4..................... : 00:00:06.206  00:00:06.120  00:00:06.060
 Task COMMixEvents................. : 00:00:03.064  00:00:02.920  00:00:02.910
 Task CYLDCHMixEvents.............. : 00:00:05.499  00:00:05.330  00:00:05.320
 Task SPXMixEvents................. : 00:00:00.143  00:00:00.169  00:00:00.149
 Task RDCMixEvents................. : 00:00:00.021  00:00:00.049  00:00:00.019
 Task SPXDigitize.................. : 00:00:36.211  00:00:35.940  00:00:34.010
 Task CYLDCHDigitize............... : 00:03:28.627  00:03:28.069  00:03:28.009
 Task RDCDigitize.................. : 00:00:01.403  00:00:01.239  00:00:00.389
 WriteEvent........................ : 00:01:09.089  00:01:06.580
analyzer........................... : 00:05:29.287  00:05:23.579  00:00:00.000
 DAQ gem4.......................... : 00:00:01.192  00:00:00.219  00:00:00.000
 Task MEGMain...................... : 00:00:00.125  00:00:00.189  00:00:00.149
 Task ReadGEM4..................... : 00:00:05.979  00:00:05.770  00:00:05.710
 Task COMMixEvents................. : 00:00:03.009  00:00:03.159  00:00:03.119
 Task CYLDCHMixEvents.............. : 00:00:05.326  00:00:04.969  00:00:04.959
 Task SPXMixEvents................. : 00:00:00.141  00:00:00.169  00:00:00.149
 Task RDCMixEvents................. : 00:00:00.021  00:00:00.009  00:00:00.009
 Task SPXDigitize.................. : 00:00:35.711  00:00:35.330  00:00:33.350
 Task CYLDCHDigitize............... : 00:03:24.381  00:03:24.080  00:03:24.020
 Task RDCDigitize.................. : 00:00:01.401  00:00:01.260  00:00:00.420
 WriteEvent........................ : 00:01:08.377  00:01:05.979
analyzer........................... : 00:05:20.703  00:05:14.709  00:00:00.000
 DAQ gem4.......................... : 00:00:01.185  00:00:00.229  00:00:00.000
 Task MEGMain...................... : 00:00:00.122  00:00:00.089  00:00:00.089
 Task ReadGEM4..................... : 00:00:05.995  00:00:05.620  00:00:05.570
 Task COMMixEvents................. : 00:00:02.984  00:00:03.100  00:00:03.070
 Task CYLDCHMixEvents.............. : 00:00:05.362  00:00:04.989  00:00:04.969
 Task SPXMixEvents................. : 00:00:00.141  00:00:00.120  00:00:00.110
 Task RDCMixEvents................. : 00:00:00.022  00:00:00.020  00:00:00.020
 Task SPXDigitize.................. : 00:00:35.930  00:00:35.989  00:00:34.059
 Task CYLDCHDigitize............... : 00:03:15.711  00:03:14.929  00:03:14.869
 Task RDCDigitize.................. : 00:00:01.422  00:00:01.260  00:00:00.420
 WriteEvent........................ : 00:01:08.251  00:01:05.960
analyzer........................... : 00:05:21.756  00:05:16.020  00:00:00.000
 DAQ gem4.......................... : 00:00:01.233  00:00:00.219  00:00:00.000
 Task MEGMain...................... : 00:00:00.121  00:00:00.150  00:00:00.130
 Task ReadGEM4..................... : 00:00:06.033  00:00:05.589  00:00:05.519
 Task COMMixEvents................. : 00:00:02.992  00:00:03.079  00:00:03.059
 Task CYLDCHMixEvents.............. : 00:00:05.399  00:00:05.550  00:00:05.550
 Task SPXMixEvents................. : 00:00:00.144  00:00:00.130  00:00:00.109
 Task RDCMixEvents................. : 00:00:00.021  00:00:00.030  00:00:00.019
 Task SPXDigitize.................. : 00:00:36.374  00:00:35.999  00:00:34.039
 Task CYLDCHDigitize............... : 00:03:16.837  00:03:16.149  00:03:16.069
 Task RDCDigitize.................. : 00:00:01.408  00:00:01.460  00:00:00.630
 WriteEvent........................ : 00:01:07.680  00:01:05.470
```
