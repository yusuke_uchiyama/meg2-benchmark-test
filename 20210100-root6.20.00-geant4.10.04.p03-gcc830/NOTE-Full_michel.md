2021/01/11,28, 02/05

# Full detetctor (default incl. DS-RDC) 
- Photon tracking in SPX.
- 9 layer CDCH
    - O2 fraction for electron attachment study
    - (O2 is not simulated as the gas material, but for the calculation
    for the electron attachment.)
        - 12000-12019, 12220-12249: 0%
	- 12020-12039: 0.5%
	- 12040-12059: 1%
	- 12060-12079: 2%
    - Cathode wire thickness study(0% O2)
        - Modified codes are 20210101-root6.20.00-geant4.10.04.p03-gcc830CYLDCHModule.cc.50/60 (patches are in gem4Out/log/)
        - 12100-12119, 12320-12349: all 50 um
	- 12120-12139, 12420-12449: all 60 um
- Custom run number 2000116

Michel posiron generated on target in 4pi

No event selection in gem4

d0ef70a2ba25302be7cd66881ff83631de526fa4

## LOG ##
```
 root -q 'macros/benchmark/JobSubmit.C("macros/benchmark/Full_michel.mac", "", "/meg/data1/shared/mc/benchmark_test/20210100-root6.20.00-geant4.10.04.p03-gcc830/gem4Out", 10000, 12000, 20, "-p short")'
```

#12000-12019 

```
$ du -cm gem4Out/sev12*
```
```
$ grep 'User=' gem4Out/log/g1200* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
34463.8/200000 = 0.172319s/event
$ grep 'User=' gem4Out/log/g1202* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
31157.2/200000 = 0.155786s/event
$ grep 'User=' gem4Out/log/g1204* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
30656.1/200000 = 0.15328s/event
$ grep 'User=' gem4Out/log/g1206* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=200000} END{print sum "/" n " = " sum/n "s/event"}'
31630.2/200000 = 0.158151s/event
```
```
megbartender [1] sev->Print("toponly")

```