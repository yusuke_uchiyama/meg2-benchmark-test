2021/01/11

# Bartender output with Spect_signalE + Full_michel mixed.

* (realistic)
   * A signal event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Michel events are randomly mixed at rate of 7e7 Hz in time window (-350,350) ns.
   * Run number convention: 116**  
     10 gem4 runs (sev files) are summed up in a bartender run.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.
   * CDCH noise from spectrum measured in 2020 with standard FE.   
   * Electron attachment by O2 at different fractions
       - 11500-11501: 0%
       - 11502-11503: 0.5%
       - 11504-11505: 1%
       - 11506-11507: 2%

d0ef70a2ba25302be7cd66881ff83631de526fa4

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_signalEWithMichel7e7_realistic.xml","/meg/data1/shared/mc/benchmark_test/20210100-root6.20.00-geant4.10.04.p03-gcc830", 11600, 8, "")'
```

```
./macros/bartender_output.sh 20210100-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
cd 20210100-root6.20.00-geant4.10.04.p03-gcc830
cat NOTE-Spect_signalEWithMichel7e7_realistic.md >> NOTE-Spect_signalEWithMichel7e7.md;rm -f NOTE-Spect_signalEWithMichel7e7_realistic.md
```


### Data and Statistics ###


```
./macros/bartender_output.sh 20210100-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
```
- - - -

16000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7_realistic | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 3.1G Jan 11 23:00 raw11600.root
-rw-r--r--. 1 uchiyama unx-lke 3.1G Jan 11 23:01 raw11601.root
-rw-r--r--. 1 uchiyama unx-lke 3.0G Jan 11 22:58 raw11602.root
-rw-r--r--. 1 uchiyama unx-lke 3.1G Jan 11 22:58 raw11603.root
-rw-r--r--. 1 uchiyama unx-lke 3.0G Jan 11 22:57 raw11604.root
-rw-r--r--. 1 uchiyama unx-lke 3.0G Jan 11 22:57 raw11605.root
-rw-r--r--. 1 uchiyama unx-lke 3.0G Jan 11 22:54 raw11606.root
-rw-r--r--. 1 uchiyama unx-lke 3.0G Jan 11 22:55 raw11607.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 11 23:00 sim11600.root
-rw-r--r--. 1 uchiyama unx-lke 5.7G Jan 11 23:01 sim11601.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 11 22:58 sim11602.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 11 22:58 sim11603.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 11 22:57 sim11604.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 11 22:57 sim11605.root
-rw-r--r--. 1 uchiyama unx-lke 5.5G Jan 11 22:54 sim11606.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Jan 11 22:55 sim11607.root

sim	2984.36 kB/event
track	1718.79 kB/event
dch	1215.45 kB/event
kine	27.375 kB/event
spx	9.03 kB/event

```
######### execution time #########
23583.6/16000 = 1.47398 s/event
```
---------
analyzer........................... : 00:51:11.637  00:50:15.619  00:00:00.000
 DAQ gem4.......................... : 00:00:00.918  00:00:00.250  00:00:00.020
 Task MEGMain...................... : 00:00:00.143  00:00:00.159  00:00:00.109
 Task ReadGEM4..................... : 00:05:05.894  00:04:48.999  00:04:48.749
 Task COMMixEvents................. : 00:01:27.680  00:01:27.180  00:01:27.140
 Task CYLDCHMixEvents.............. : 00:01:24.926  00:01:24.569  00:01:24.559
 Task SPXMixEvents................. : 00:00:00.567  00:00:00.580  00:00:00.580
 Task RDCMixEvents................. : 00:00:00.193  00:00:00.179  00:00:00.169
 Task SPXDigitize.................. : 00:01:13.402  00:01:13.519  00:01:11.259
 Task CYLDCHDigitize............... : 00:33:07.703  00:33:01.850  00:33:01.800
 Task RDCDigitize.................. : 00:00:09.500  00:00:09.720  00:00:08.750
 WriteEvent........................ : 00:08:18.423  00:07:54.270
analyzer........................... : 00:52:06.713  00:51:15.300  00:00:00.000
 DAQ gem4.......................... : 00:00:01.090  00:00:00.250  00:00:00.000
 Task MEGMain...................... : 00:00:00.146  00:00:00.149  00:00:00.149
 Task ReadGEM4..................... : 00:05:10.583  00:04:54.230  00:04:54.050
 Task COMMixEvents................. : 00:01:29.360  00:01:29.319  00:01:29.289
 Task CYLDCHMixEvents.............. : 00:01:25.580  00:01:25.029  00:01:24.999
 Task SPXMixEvents................. : 00:00:00.570  00:00:00.520  00:00:00.510
 Task RDCMixEvents................. : 00:00:00.196  00:00:00.240  00:00:00.210
 Task SPXDigitize.................. : 00:01:13.532  00:01:13.109  00:01:10.829
 Task CYLDCHDigitize............... : 00:33:55.774  00:33:50.240  00:33:50.190
 Task RDCDigitize.................. : 00:00:09.736  00:00:09.509  00:00:08.499
 WriteEvent........................ : 00:08:16.551  00:07:57.930
analyzer........................... : 00:49:37.658  00:48:47.289  00:00:00.000
 DAQ gem4.......................... : 00:00:01.191  00:00:00.240  00:00:00.000
 Task MEGMain...................... : 00:00:00.147  00:00:00.179  00:00:00.169
 Task ReadGEM4..................... : 00:05:11.856  00:04:56.780  00:04:56.600
 Task COMMixEvents................. : 00:01:29.390  00:01:29.120  00:01:29.100
 Task CYLDCHMixEvents.............. : 00:01:25.648  00:01:25.209  00:01:25.159
 Task SPXMixEvents................. : 00:00:00.565  00:00:00.529  00:00:00.499
 Task RDCMixEvents................. : 00:00:00.197  00:00:00.199  00:00:00.169
 Task SPXDigitize.................. : 00:01:12.348  00:01:12.600  00:01:10.320
 Task CYLDCHDigitize............... : 00:31:29.549  00:31:23.919  00:31:23.859
 Task RDCDigitize.................. : 00:00:09.536  00:00:09.589  00:00:08.609
 WriteEvent........................ : 00:08:14.570  00:07:54.739
analyzer........................... : 00:50:01.080  00:49:07.279  00:00:00.000
 DAQ gem4.......................... : 00:00:01.294  00:00:00.239  00:00:00.009
 Task MEGMain...................... : 00:00:00.146  00:00:00.190  00:00:00.170
 Task ReadGEM4..................... : 00:05:07.495  00:04:52.519  00:04:52.339
 Task COMMixEvents................. : 00:01:29.427  00:01:29.059  00:01:29.009
 Task CYLDCHMixEvents.............. : 00:01:25.751  00:01:25.589  00:01:25.559
 Task SPXMixEvents................. : 00:00:00.576  00:00:00.590  00:00:00.540
 Task RDCMixEvents................. : 00:00:00.196  00:00:00.200  00:00:00.180
 Task SPXDigitize.................. : 00:01:14.275  00:01:14.009  00:01:11.739
 Task CYLDCHDigitize............... : 00:31:50.589  00:31:45.250  00:31:45.220
 Task RDCDigitize.................. : 00:00:09.648  00:00:09.700  00:00:08.710
 WriteEvent........................ : 00:08:19.080  00:07:55.669
analyzer........................... : 00:48:45.690  00:47:55.120  00:00:00.000
 DAQ gem4.......................... : 00:00:01.144  00:00:00.229  00:00:00.000
 Task MEGMain...................... : 00:00:00.145  00:00:00.150  00:00:00.090
 Task ReadGEM4..................... : 00:05:06.984  00:04:52.349  00:04:52.209
 Task COMMixEvents................. : 00:01:29.477  00:01:29.460  00:01:29.420
 Task CYLDCHMixEvents.............. : 00:01:25.815  00:01:25.220  00:01:25.190
 Task SPXMixEvents................. : 00:00:00.572  00:00:00.519  00:00:00.499
 Task RDCMixEvents................. : 00:00:00.196  00:00:00.110  00:00:00.110
 Task SPXDigitize.................. : 00:01:13.475  00:01:13.299  00:01:11.009
 Task CYLDCHDigitize............... : 00:30:39.193  00:30:34.390  00:30:34.320
 Task RDCDigitize.................. : 00:00:09.741  00:00:09.939  00:00:08.969
 WriteEvent........................ : 00:08:15.842  00:07:55.109
analyzer........................... : 00:48:34.917  00:47:41.140  00:00:00.000
 DAQ gem4.......................... : 00:00:01.149  00:00:00.229  00:00:00.009
 Task MEGMain...................... : 00:00:00.149  00:00:00.090  00:00:00.090
 Task ReadGEM4..................... : 00:05:10.475  00:04:55.160  00:04:54.980
 Task COMMixEvents................. : 00:01:29.654  00:01:29.330  00:01:29.290
 Task CYLDCHMixEvents.............. : 00:01:27.065  00:01:27.089  00:01:27.069
 Task SPXMixEvents................. : 00:00:00.576  00:00:00.520  00:00:00.520
 Task RDCMixEvents................. : 00:00:00.196  00:00:00.150  00:00:00.140
 Task SPXDigitize.................. : 00:01:13.914  00:01:13.709  00:01:11.449
 Task CYLDCHDigitize............... : 00:30:17.912  00:30:13.280  00:30:13.230
 Task RDCDigitize.................. : 00:00:09.672  00:00:09.369  00:00:08.379
 WriteEvent........................ : 00:08:21.283  00:07:57.449
analyzer........................... : 00:45:54.195  00:45:09.509  00:00:00.000
 DAQ gem4.......................... : 00:00:00.969  00:00:00.219  00:00:00.000
 Task MEGMain...................... : 00:00:00.146  00:00:00.090  00:00:00.070
 Task ReadGEM4..................... : 00:05:08.993  00:04:54.989  00:04:54.809
 Task COMMixEvents................. : 00:01:29.354  00:01:29.259  00:01:29.219
 Task CYLDCHMixEvents.............. : 00:01:25.547  00:01:25.280  00:01:25.280
 Task SPXMixEvents................. : 00:00:00.570  00:00:00.530  00:00:00.530
 Task RDCMixEvents................. : 00:00:00.195  00:00:00.169  00:00:00.149
 Task SPXDigitize.................. : 00:01:12.638  00:01:12.419  00:01:10.129
 Task CYLDCHDigitize............... : 00:27:56.805  00:27:52.349  00:27:52.269
 Task RDCDigitize.................. : 00:00:09.647  00:00:09.069  00:00:08.099
 WriteEvent........................ : 00:08:05.993  00:07:50.760
analyzer........................... : 00:46:51.744  00:45:56.650  00:00:00.000
 DAQ gem4.......................... : 00:00:01.249  00:00:00.239  00:00:00.000
 Task MEGMain...................... : 00:00:00.145  00:00:00.159  00:00:00.149
 Task ReadGEM4..................... : 00:05:07.886  00:04:53.669  00:04:53.529
 Task COMMixEvents................. : 00:01:29.708  00:01:29.629  00:01:29.589
 Task CYLDCHMixEvents.............. : 00:01:27.123  00:01:26.579  00:01:26.529
 Task SPXMixEvents................. : 00:00:00.581  00:00:00.589  00:00:00.570
 Task RDCMixEvents................. : 00:00:00.198  00:00:00.280  00:00:00.270
 Task SPXDigitize.................. : 00:01:14.331  00:01:14.189  00:01:11.909
 Task CYLDCHDigitize............... : 00:28:36.871  00:28:32.210  00:28:32.180
 Task RDCDigitize.................. : 00:00:09.611  00:00:09.340  00:00:08.350
 WriteEvent........................ : 00:08:21.285  00:07:55.859
```
