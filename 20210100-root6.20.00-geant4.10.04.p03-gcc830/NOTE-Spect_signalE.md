2021/01/11,28, 02/05

# Spectrometer (-d XEC).
- Photon tracking in SPX.
- 9 layer CDCH
    - O2 fraction for electron attachment study
    - (O2 is not simulated as the gas material, but for the calculation
    for the electron attachment.)
        - 11000-11019, 11220-11249: 0%
	- 11020-11039: 0.5%
	- 11040-11059: 1%
	- 11060-11079: 2%
    - Cathode wire thickness study (0% O2)
        - Modified codes are 20210101-root6.20.00-geant4.10.04.p03-gcc830CYLDCHModule.cc.50/60 (patches are in gem4Out/log/)
        - 11100-11119, 11320-11349: all 50 um
	- 11120-11139, 11420-11449: all 60 um
- Custom run number 2000116

# Signal positron generated on target
- in the 'extended' range
- No event selection in gem4
- Number of events (generation) in a run: 200

d0ef70a2ba25302be7cd66881ff83631de526fa4

## LOG ##
```
 root -q 'macros/benchmark/JobSubmit.C("macros/benchmark/Spect_signalE.mac", "-d xec", "/meg/data1/shared/mc/benchmark_test/20210100-root6.20.00-geant4.10.04.p03-gcc830/gem4Out/", 200, 11000, 20, "-p short")'
```

#11000-11019
200 events * 20 run = 4000 events generation,

```
$ du -cm gem4Out/sev11*
```
```
$ grep 'User=' gem4Out/log/g110[0-1]* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=4000} END{print sum "/" n " = " sum/n "s/event"}'
3386.3/4000 = 0.846575s/event
$ grep 'User=' gem4Out/log/g110[2-3]* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=4000} END{print sum "/" n " = " sum/n "s/event"}'
3794.08/4000 = 0.94852s/event
$ grep 'User=' gem4Out/log/g110[4-5]* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=4000} END{print sum "/" n " = " sum/n "s/event"}'
3582.44/4000 = 0.89561s/event
$ grep 'User=' gem4Out/log/g110[6-7]* | cut -d'=' -f3 | cut -ds -f1 | awk '{sum+=$1} {n=4000} END{print sum "/" n " = " sum/n "s/event"}'
3645.94/4000 = 0.911485s/event
```
```
megbartender [1] sev->Print("toponly")

```