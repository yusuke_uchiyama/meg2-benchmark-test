2020/12/16

# Analyzer output with noxec_PR_fit_genfit.xml
* No XEC reconstruction
* SPX analysis from WaveformAnalysis (cf30)
* CDC analysis from waveform (WaveformAnalysis2), pattern recognition and fit with GENFIT
* Custom run number 2000116
* Hit z reconstruction used in CYLDCHTrackFinderPR is time-difference
* CYLDCHWaveformAnalysis2 (with not fully optimized parameters).
    * Update TimeWalkCoefficient (to 3.6e-10) and GlobalTimeOffset (to -5)

mc_20201216.0

## Run Numbers ##
11500-11501 Spect_signalEOnly_realistic  
11600-11601 Spect_signalEWithMichel7e7_realistic  


## Submit jobs ##
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_noMix_realistic","/meg/data1/shared/mc/benchmark_test/20201201-root6.20.00-geant4.10.04.p03-gcc830/", 11500, 2)'

root -q 'example/benchmark/JobSubmit.C("example/benchmark/noxec_PR_fit_genfit.xml","Spect_signalEWithMichel7e7_realistic","/meg/data1/shared/mc/benchmark_test/20201201-root6.20.00-geant4.10.04.p03-gcc830/", 11600, 2)'
```



## Calculate positron efficiency ##
### Realistic ###

```
RUNNUM=11500;NRUN=2;DATASET=20201201-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_noMix_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h >	macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2628 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2404 (91.4764 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2324 (88.4323 %)
|   |                         
|   | propagation cut         
|   V                         
|  2278 (86.6819 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  2088 (79.4521 %)
|   |                         
|   | matching                
|   V                         
|  2064 (78.5388 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  2064 (78.5388 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  2017 (76.7504 %)
|      + 0.832508
|      - 0.853331
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  6.23262
|  phi     (mrad)  5.97657
|  momentum (MeV)  0.0829061
|  vertexZ   (mm)  1.59683
|  vertexY   (mm)  0.753651
|  time     (sec)  4.6483e-11
@-----------------------------@
```

```
RUNNUM=11600;NRUN=2;DATASET=20201201-root6.20.00-geant4.10.04.p03-gcc830;
INPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
OUTPUTDIR="/meg/data1/shared/mc/benchmark_test/$DATASET"
cd $MEG2SYS/analyzer;
sed -e "s|= START_RUN|= $RUNNUM|g" -e "s|-3.975e-7|-3.9747e-7|g" -e "s|NRUN|$NRUN|g" -e "s|INPUT_DIR|$INPUTDIR|g" -e "s|OUTPUT_DIR|$OUTPUTDIR|g" -e "s|noxec_PR_fit_genfit|noxec_PR_fit_genfit|g" -e "s|Spect_noMix|Spect_signalEWithMichel7e7_realistic|g" -e "s|Eff_results|Eff_results$RUNNUM|g" macros/positron/setting_template.h > macros/positron/setting_$DATASET_$RUNNUM.h
cd macros/positron;ln -fs setting_$DATASET_$RUNNUM.h setting.h; cd ../../;
./meganalyzer -I 'macros/positron/DCHSPXSignalPositronEfficiency.cpp+'
```
```
@-----------------------------@
|           Summary           
|   All events:               
|  2000
|   |                         
|   | LXe acceptance cut      
|   V                         
|  2628 (100 %)
|   |                         
|   | SPX acceptance cut      
|   V                         
|  2402 (91.4003 %)
|   |                         
|   | DCH tracking            
|   |  including contamination cut  
|   |  (FakeThre:0.6)
|   V                         
|  2125 (80.86 %)
|   |                         
|   | propagation cut         
|   V                         
|  1946 (74.0487 %)
|   |                         
|   | tail cut with           
|   | theta   (mrad)  5 sigma, sigma = 6.1
|   | phi     (mrad)  5 sigma, sigma = 5.6
|   | momentum (MeV)  5 sigma, sigma = 0.09
|   | vertexZ   (mm)  5 sigma, sigma = 1.5
|   | vertexY   (mm)  5 sigma, sigma = 0.76
|   V                         
|  1570 (59.7412 %)
|   |                         
|   | matching                
|   V                         
|  1546 (58.828 %)
|   |                         
|   | SPX cluster contamination cut 
|   |  (FakeThre:0.6)
|   V                         
|  1544 (58.7519 %)
|   |                         
|   | time cut                
|   | time     (sec)  5 sigma, sigma = 4e-11
|   V                         
|  1503 (57.1918 %)
|      + 0.981249
|      - 0.986828
@-----------------------------@
|           Resolutions           
|  theta   (mrad)  7.13218
|  phi     (mrad)  6.6434
|  momentum (MeV)  0.100687
|  vertexZ   (mm)  1.89819
|  vertexY   (mm)  0.833751
|  time     (sec)  4.58218e-11
@-----------------------------@
```


### Analyze execution and output ###
```
cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20201201-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115

cd /meg/data1/shared/mc/benchmark_test;
./macros/analyzer_output.sh 20201201-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```

```
./macros/analyzer_output.sh 20201201-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 115
```
- - - -

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke  18K Dec 16 18:26 Eff_results11500.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Dec 16 17:40 histos11500.root
-rw-r--r--. 1 uchiyama unx-lke 2.4M Dec 16 17:39 histos11501.root
-rw-r--r--. 1 uchiyama unx-lke  57M Dec 16 17:40 rec11500.root
-rw-r--r--. 1 uchiyama unx-lke  57M Dec 16 17:39 rec11501.root

rec	29.799 kB/event
dch	25.774 kB/event
spx	3.491 kB/event
reco	0.233 kB/event
rdc	0.212 kB/event

```
######### execution time #########
1127.94/4000 = 0.281985 s/event
```
---------
analyzer........................... : 00:09:27.111  00:09:23.639  00:00:00.000
 DAQ bartender..................... : 00:00:50.659  00:00:48.619  00:00:48.229
 Task MEGMain...................... : 00:00:01.382  00:00:01.189  00:00:00.000
 Task ReadData..................... : 00:00:06.178  00:00:06.310  00:00:06.240
 Task SPX.......................... : 00:00:00.009  00:00:00.010  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.541  00:00:04.690  00:00:04.610
  SubTask SPXHitRec................ : 00:00:00.245  00:00:00.270  00:00:00.249
  SubTask SPXClustering............ : 00:00:10.540  00:00:10.380  00:00:10.360
  SubTask SPXIndependentTracking... : 00:00:28.163  00:00:28.069  00:00:28.059
 Task RDC.......................... : 00:00:00.009  00:00:00.010  00:00:00.010
  SubTask RDCWaveformAnalysis...... : 00:00:00.059  00:00:00.050  00:00:00.050
  SubTask RDCHitRec................ : 00:00:00.037  00:00:00.010  00:00:00.010
 Task CYLDCH....................... : 00:00:00.007  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:26.822  00:01:26.550  00:01:26.540
  SubTask CYLDCHHitRec............. : 00:00:00.398  00:00:00.370  00:00:00.360
  SubTask CYLDCHTrackFinderPR...... : 00:02:44.155  00:02:44.019  00:02:44.009
  SubTask DCHKalmanFilterGEN....... : 00:01:09.888  00:01:10.040  00:01:10.030
  SubTask DCHTrackReFit............ : 00:00:00.131  00:00:00.159  00:00:00.149
 Task DCHSPX....................... : 00:00:00.007  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:48.786  00:00:48.630  00:00:48.630
  SubTask DCHSPXReFit.............. : 00:01:07.890  00:01:07.880  00:01:07.870
  SubTask SPXTracking.............. : 00:00:17.924  00:00:17.889  00:00:17.869
  SubTask DCHSPXTrackSelection..... : 00:00:00.059  00:00:00.019  00:00:00.019
 WriteEvent........................ : 00:00:05.693  00:00:05.460
analyzer........................... : 00:09:20.827  00:09:16.160  00:00:00.000
 DAQ bartender..................... : 00:00:50.357  00:00:47.900  00:00:47.620
 Task MEGMain...................... : 00:00:01.323  00:00:01.179  00:00:00.000
 Task ReadData..................... : 00:00:06.197  00:00:06.640  00:00:06.610
 Task SPX.......................... : 00:00:00.009  00:00:00.019  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:04.761  00:00:04.380  00:00:04.330
  SubTask SPXHitRec................ : 00:00:00.247  00:00:00.229  00:00:00.229
  SubTask SPXClustering............ : 00:00:10.370  00:00:10.489  00:00:10.449
  SubTask SPXIndependentTracking... : 00:00:26.380  00:00:26.380  00:00:26.370
 Task RDC.......................... : 00:00:00.010  00:00:00.009  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:00.059  00:00:00.050  00:00:00.030
  SubTask RDCHitRec................ : 00:00:00.034  00:00:00.020  00:00:00.020
 Task CYLDCH....................... : 00:00:00.007  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:01:26.643  00:01:26.469  00:01:26.469
  SubTask CYLDCHHitRec............. : 00:00:00.405  00:00:00.499  00:00:00.499
  SubTask CYLDCHTrackFinderPR...... : 00:02:39.285  00:02:38.189  00:02:38.179
  SubTask DCHKalmanFilterGEN....... : 00:01:07.438  00:01:07.540  00:01:07.530
  SubTask DCHTrackReFit............ : 00:00:00.131  00:00:00.140  00:00:00.100
 Task DCHSPX....................... : 00:00:00.007  00:00:00.000  00:00:00.000
  SubTask DCHSPXMatching........... : 00:00:51.712  00:00:51.529  00:00:51.519
  SubTask DCHSPXReFit.............. : 00:01:07.036  00:01:07.260  00:01:07.240
  SubTask SPXTracking.............. : 00:00:18.333  00:00:18.459  00:00:18.439
  SubTask DCHSPXTrackSelection..... : 00:00:00.060  00:00:00.090  00:00:00.070
 WriteEvent........................ : 00:00:05.731  00:00:05.440
```

```
./macros/analyzer_output.sh 20201201-root6.20.00-geant4.10.04.p03-gcc830/noxec_PR_fit_genfit 116
```
- - - -

4000  events

######### data size #########
```
ls -lh noxec_PR_fit_genfit | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 2.6M Dec 16 19:09 histos11600.root
-rw-r--r--. 1 uchiyama unx-lke 2.6M Dec 16 19:10 histos11601.root
-rw-r--r--. 1 uchiyama unx-lke 217M Dec 16 19:09 rec11600.root
-rw-r--r--. 1 uchiyama unx-lke 216M Dec 16 19:10 rec11601.root

rec	113.294 kB/event
dch	103.66 kB/event
spx	5.972 kB/event
rdc	2.687 kB/event
reco	0.69 kB/event

```
######### execution time #########
11158.6/4000 = 2.78965 s/event
```
---------
analyzer........................... : 01:32:37.090  01:32:04.599  00:00:00.000
 DAQ bartender..................... : 00:03:39.312  00:03:09.460  00:03:08.550
 Task MEGMain...................... : 00:00:01.208  00:00:01.140  00:00:00.010
 Task ReadData..................... : 00:00:20.466  00:00:20.519  00:00:20.459
 Task SPX.......................... : 00:00:00.012  00:00:00.020  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.564  00:00:08.429  00:00:08.379
  SubTask SPXHitRec................ : 00:00:00.262  00:00:00.319  00:00:00.309
  SubTask SPXClustering............ : 00:00:19.136  00:00:19.199  00:00:19.139
  SubTask SPXIndependentTracking... : 00:00:43.435  00:00:43.470  00:00:43.450
 Task RDC.......................... : 00:00:00.010  00:00:00.009  00:00:00.009
  SubTask RDCWaveformAnalysis...... : 00:00:20.508  00:00:20.250  00:00:20.240
  SubTask RDCHitRec................ : 00:00:00.229  00:00:00.220  00:00:00.220
 Task CYLDCH....................... : 00:00:00.008  00:00:00.000  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:16.415  00:10:16.040  00:10:16.020
  SubTask CYLDCHHitRec............. : 00:00:12.626  00:00:12.959  00:00:12.939
  SubTask CYLDCHTrackFinderPR...... : 01:00:27.261  01:00:26.690  01:00:26.690
  SubTask DCHKalmanFilterGEN....... : 00:10:19.487  00:10:19.549  00:10:19.509
  SubTask DCHTrackReFit............ : 00:00:00.149  00:00:00.189  00:00:00.189
 Task DCHSPX....................... : 00:00:00.007  00:00:00.009  00:00:00.000
  SubTask DCHSPXMatching........... : 00:03:12.250  00:03:12.310  00:03:12.290
  SubTask DCHSPXReFit.............. : 00:01:45.333  00:01:45.100  00:01:45.090
  SubTask SPXTracking.............. : 00:00:27.512  00:00:27.570  00:00:27.570
  SubTask DCHSPXTrackSelection..... : 00:00:00.133  00:00:00.119  00:00:00.119
 WriteEvent........................ : 00:00:14.131  00:00:13.170
analyzer........................... : 01:33:21.507  01:32:49.340  00:00:00.000
 DAQ bartender..................... : 00:03:40.764  00:03:10.470  00:03:09.550
 Task MEGMain...................... : 00:00:01.283  00:00:01.180  00:00:00.000
 Task ReadData..................... : 00:00:20.872  00:00:21.120  00:00:21.060
 Task SPX.......................... : 00:00:00.013  00:00:00.000  00:00:00.000
  SubTask SPXWaveformAnalysis...... : 00:00:08.577  00:00:08.389  00:00:08.309
  SubTask SPXHitRec................ : 00:00:00.261  00:00:00.259  00:00:00.259
  SubTask SPXClustering............ : 00:00:19.431  00:00:19.540  00:00:19.480
  SubTask SPXIndependentTracking... : 00:00:41.993  00:00:41.709  00:00:41.689
 Task RDC.......................... : 00:00:00.010  00:00:00.000  00:00:00.000
  SubTask RDCWaveformAnalysis...... : 00:00:21.274  00:00:21.729  00:00:21.719
  SubTask RDCHitRec................ : 00:00:00.239  00:00:00.160  00:00:00.160
 Task CYLDCH....................... : 00:00:00.007  00:00:00.009  00:00:00.000
  SubTask CYLDCHWaveformAnalysis2.. : 00:10:24.286  00:10:24.240  00:10:24.240
  SubTask CYLDCHHitRec............. : 00:00:12.679  00:00:12.640  00:00:12.630
  SubTask CYLDCHTrackFinderPR...... : 01:00:18.468  01:00:17.940  01:00:17.930
  SubTask DCHKalmanFilterGEN....... : 00:10:43.994  00:10:43.949  00:10:43.929
  SubTask DCHTrackReFit............ : 00:00:00.142  00:00:00.159  00:00:00.139
 Task DCHSPX....................... : 00:00:00.007  00:00:00.009  00:00:00.009
  SubTask DCHSPXMatching........... : 00:03:29.239  00:03:29.120  00:03:29.120
  SubTask DCHSPXReFit.............. : 00:01:41.728  00:01:41.780  00:01:41.770
  SubTask SPXTracking.............. : 00:00:33.254  00:00:33.139  00:00:33.119
  SubTask DCHSPXTrackSelection..... : 00:00:00.132  00:00:00.089  00:00:00.089
 WriteEvent........................ : 00:00:14.045  00:00:13.370
```
