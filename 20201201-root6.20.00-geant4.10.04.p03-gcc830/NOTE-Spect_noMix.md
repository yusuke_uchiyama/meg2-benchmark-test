2020/12/16

# Bartender output with Spect_noMix.

* (realistic)
   * A signal positron event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Run number convention: 115**  
     10 gem4 runs (sev files) are summed up in a bartender run.  
     Originally 200*10 signal events are generated in gem4 but  
     only events satisfied gem4 event-selection are recorded.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.
   * CDCH noise from spectrum measured in 2020 with standard FE.

110322d761261306ff2efc1a026fd256217502de

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_noMix_realistic.xml","/meg/data1/shared/mc/benchmark_test/20201201-root6.20.00-geant4.10.04.p03-gcc830", 11500, 2, "-p short")'
```

```
./macros/bartender_output.sh 20201201-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
cd 20201201-root6.20.00-geant4.10.04.p03-gcc830
cat NOTE-Spect_noMix_realistic.md >> NOTE-Spect_noMix.md;rm -f NOTE-Spect_noMix_realistic.md
```



### Data and Statistics ###


```
./macros/bartender_output.sh 20201201-root6.20.00-geant4.10.04.p03-gcc830/Spect_noMix_realistic 115
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_noMix_realistic | grep -E "115[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 396M Dec 16 16:44 raw11500.root
-rw-r--r--. 1 uchiyama unx-lke 396M Dec 16 16:44 raw11501.root
-rw-r--r--. 1 uchiyama unx-lke 278M Dec 16 16:44 sim11500.root
-rw-r--r--. 1 uchiyama unx-lke 276M Dec 16 16:44 sim11501.root

sim	145.242 kB/event
track	75.789 kB/event
dch	63.39 kB/event
spx	2.616 kB/event
kine	1.521 kB/event

```
######### execution time #########
568.443/4000 = 0.142111 s/event
```
---------
analyzer........................... : 00:04:43.370  00:04:39.300  00:00:00.000
 DAQ gem4.......................... : 00:00:00.289  00:00:00.149  00:00:00.009
 Task MEGMain...................... : 00:00:00.089  00:00:00.069  00:00:00.069
 Task ReadGEM4..................... : 00:00:04.890  00:00:04.800  00:00:04.730
 Task COMMixEvents................. : 00:00:02.470  00:00:02.339  00:00:02.319
 Task CYLDCHMixEvents.............. : 00:00:04.483  00:00:04.479  00:00:04.479
 Task SPXMixEvents................. : 00:00:00.119  00:00:00.120  00:00:00.120
 Task RDCMixEvents................. : 00:00:00.018  00:00:00.019  00:00:00.009
 Task SPXDigitize.................. : 00:00:30.250  00:00:30.469  00:00:28.569
 Task CYLDCHDigitize............... : 00:02:58.468  00:02:57.890  00:02:57.850
 Task RDCDigitize.................. : 00:00:01.245  00:00:01.190  00:00:00.380
 WriteEvent........................ : 00:00:58.375  00:00:56.149
analyzer........................... : 00:04:45.073  00:04:41.000  00:00:00.000
 DAQ gem4.......................... : 00:00:00.294  00:00:00.159  00:00:00.009
 Task MEGMain...................... : 00:00:00.092  00:00:00.029  00:00:00.029
 Task ReadGEM4..................... : 00:00:04.910  00:00:04.760  00:00:04.700
 Task COMMixEvents................. : 00:00:02.457  00:00:02.589  00:00:02.589
 Task CYLDCHMixEvents.............. : 00:00:04.502  00:00:04.339  00:00:04.329
 Task SPXMixEvents................. : 00:00:00.117  00:00:00.110  00:00:00.100
 Task RDCMixEvents................. : 00:00:00.017  00:00:00.009  00:00:00.000
 Task SPXDigitize.................. : 00:00:30.169  00:00:30.169  00:00:28.259
 Task CYLDCHDigitize............... : 00:03:00.730  00:03:00.250  00:03:00.190
 Task RDCDigitize.................. : 00:00:01.244  00:00:01.219  00:00:00.379
 WriteEvent........................ : 00:00:57.819  00:00:55.579
```
