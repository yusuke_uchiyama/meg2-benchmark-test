2020/12/16

# Bartender output with Spect_signalE + Full_michel mixed.

* (realistic)
   * A signal event is put at time 0, trigger is fired at random within full width of 15ns (trg 30).
   * Michel events are randomly mixed at rate of 7e7 Hz in time window (-350,350) ns.
   * Run number convention: 116**  
     10 gem4 runs (sev files) are summed up in a bartender run.
   * Custom run number 2000116
   * Apply PPDTimeOffset in SPXDigitize (86/106 ps)
   * Z-dependent gas gain
   * Space-charge effect is omitted in this version.
   * CDCH response (pulse shape) is from UV laser measurement.
   * CDCH noise from spectrum measured in 2020 with standard FE.   

110322d761261306ff2efc1a026fd256217502de

### Command ###
```
root -q 'example/benchmark/JobSubmit.C("example/benchmark/Spect_signalEWithMichel7e7_realistic.xml","/meg/data1/shared/mc/benchmark_test/20201201-root6.20.00-geant4.10.04.p03-gcc830", 11600, 2, "")'
```

```
./macros/bartender_output.sh 20201201-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
cd 20201201-root6.20.00-geant4.10.04.p03-gcc830
cat NOTE-Spect_signalEWithMichel7e7_realistic.md >> NOTE-Spect_signalEWithMichel7e7.md;rm -f NOTE-Spect_signalEWithMichel7e7_realistic.md
```


### Data and Statistics ###

```
./macros/bartender_output.sh 20201201-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7_realistic | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 3.1G Dec 16 17:27 raw11600.root
-rw-r--r--. 1 uchiyama unx-lke 3.1G Dec 16 17:28 raw11601.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Dec 16 17:27 sim11600.root
-rw-r--r--. 1 uchiyama unx-lke 5.7G Dec 16 17:28 sim11601.root

sim	2984.36 kB/event
track	1718.79 kB/event
dch	1215.45 kB/event
kine	27.375 kB/event
spx	9.03 kB/event

```
######### execution time #########
5854.55/4000 = 1.46364 s/event
```
---------
analyzer........................... : 00:48:32.674  00:47:40.569  00:00:00.000
 DAQ gem4.......................... : 00:00:01.838  00:00:00.249  00:00:00.000
 Task MEGMain...................... : 00:00:00.114  00:00:00.110  00:00:00.080
 Task ReadGEM4..................... : 00:04:39.393  00:04:22.929  00:04:22.759
 Task COMMixEvents................. : 00:01:19.651  00:01:19.090  00:01:19.070
 Task CYLDCHMixEvents.............. : 00:01:19.950  00:01:20.040  00:01:20.020
 Task SPXMixEvents................. : 00:00:00.509  00:00:00.429  00:00:00.409
 Task RDCMixEvents................. : 00:00:00.175  00:00:00.239  00:00:00.209
 Task SPXDigitize.................. : 00:01:08.403  00:01:07.830  00:01:05.900
 Task CYLDCHDigitize............... : 00:31:45.744  00:31:40.160  00:31:40.080
 Task RDCDigitize.................. : 00:00:08.751  00:00:08.739  00:00:07.899
 WriteEvent........................ : 00:07:46.168  00:07:27.959
analyzer........................... : 00:49:01.876  00:48:08.410  00:00:00.000
 DAQ gem4.......................... : 00:00:01.801  00:00:00.290  00:00:00.000
 Task MEGMain...................... : 00:00:00.115  00:00:00.100  00:00:00.080
 Task ReadGEM4..................... : 00:04:45.227  00:04:27.820  00:04:27.660
 Task COMMixEvents................. : 00:01:21.115  00:01:20.840  00:01:20.830
 Task CYLDCHMixEvents.............. : 00:01:21.188  00:01:20.679  00:01:20.659
 Task SPXMixEvents................. : 00:00:00.514  00:00:00.500  00:00:00.479
 Task RDCMixEvents................. : 00:00:00.177  00:00:00.209  00:00:00.209
 Task SPXDigitize.................. : 00:01:08.303  00:01:07.979  00:01:06.059
 Task CYLDCHDigitize............... : 00:32:02.942  00:31:57.340  00:31:57.220
 Task RDCDigitize.................. : 00:00:08.969  00:00:08.749  00:00:07.939
 WriteEvent........................ : 00:07:48.746  00:07:30.769
```

```
./macros/bartender_output.sh 20201201-root6.20.00-geant4.10.04.p03-gcc830/Spect_signalEWithMichel7e7_realistic 116
```
- - - -

4000  events

######### data size #########
```
ls -lh Spect_signalEWithMichel7e7_realistic | grep -E "116[0-9]{2}.root"
-rw-r--r--. 1 uchiyama unx-lke 3.1G Dec 16 17:27 raw11600.root
-rw-r--r--. 1 uchiyama unx-lke 3.1G Dec 16 17:28 raw11601.root
-rw-r--r--. 1 uchiyama unx-lke 5.6G Dec 16 17:27 sim11600.root
-rw-r--r--. 1 uchiyama unx-lke 5.7G Dec 16 17:28 sim11601.root

sim	2984.36 kB/event
track	1718.79 kB/event
dch	1215.45 kB/event
kine	27.375 kB/event
spx	9.03 kB/event

```
######### execution time #########
5854.55/4000 = 1.46364 s/event
```
---------
analyzer........................... : 00:48:32.674  00:47:40.569  00:00:00.000
 DAQ gem4.......................... : 00:00:01.838  00:00:00.249  00:00:00.000
 Task MEGMain...................... : 00:00:00.114  00:00:00.110  00:00:00.080
 Task ReadGEM4..................... : 00:04:39.393  00:04:22.929  00:04:22.759
 Task COMMixEvents................. : 00:01:19.651  00:01:19.090  00:01:19.070
 Task CYLDCHMixEvents.............. : 00:01:19.950  00:01:20.040  00:01:20.020
 Task SPXMixEvents................. : 00:00:00.509  00:00:00.429  00:00:00.409
 Task RDCMixEvents................. : 00:00:00.175  00:00:00.239  00:00:00.209
 Task SPXDigitize.................. : 00:01:08.403  00:01:07.830  00:01:05.900
 Task CYLDCHDigitize............... : 00:31:45.744  00:31:40.160  00:31:40.080
 Task RDCDigitize.................. : 00:00:08.751  00:00:08.739  00:00:07.899
 WriteEvent........................ : 00:07:46.168  00:07:27.959
analyzer........................... : 00:49:01.876  00:48:08.410  00:00:00.000
 DAQ gem4.......................... : 00:00:01.801  00:00:00.290  00:00:00.000
 Task MEGMain...................... : 00:00:00.115  00:00:00.100  00:00:00.080
 Task ReadGEM4..................... : 00:04:45.227  00:04:27.820  00:04:27.660
 Task COMMixEvents................. : 00:01:21.115  00:01:20.840  00:01:20.830
 Task CYLDCHMixEvents.............. : 00:01:21.188  00:01:20.679  00:01:20.659
 Task SPXMixEvents................. : 00:00:00.514  00:00:00.500  00:00:00.479
 Task RDCMixEvents................. : 00:00:00.177  00:00:00.209  00:00:00.209
 Task SPXDigitize.................. : 00:01:08.303  00:01:07.979  00:01:06.059
 Task CYLDCHDigitize............... : 00:32:02.942  00:31:57.340  00:31:57.220
 Task RDCDigitize.................. : 00:00:08.969  00:00:08.749  00:00:07.939
 WriteEvent........................ : 00:07:48.746  00:07:30.769
```
